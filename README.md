
# 项目目标
- 单体应用的模块化应用框架。
- 可直接投入生产环境使用。
- 包含类库和架构两部分（以后会分开）。
- 实现领域驱动设计。

## 模块
模块是项目的基础，项目中所有的功能点（业务或基础功能）都进行模块化。
### 已经创建的模块
#### 基础模块
##### Authorization模块
- 内置基于权限授权的处理器。
- 超级管理员授权处理器。
- 实现Jwt认证流程以及生成token服务。
- todo
	- 基于资源的授权

##### Web主机模块
默认添加 
- ProblemDetails
- Controller
- ApiDocument(基于NSwag)

##### 业务模块
业务模块包含的功能

- CQRS
- CQRSDecorator
- Repository
- EFCore
- SqlSugar（开发中）
- DomainEventBus
- IntegrationEventBus
###### 架构（设计规范）
- 每个业务模块有自己的数据，模块间数据隔离。
- 模块间共同需要通过集成事件，只能依赖其他模块的集成事件。
- 对于简单的模块可以不分层，模块分层（如果需要）请参考一下
- API
API不包含业务逻辑，只通过Command或者Query与业务模块沟通。
- Infrastructure
只包含模块需要的配置
- Application
包含业务逻辑
Query中包含Sql语句直接访问数据库
Command中应通过仓储访问数据库
对外只暴露Command与Query，其他类型均应该为internal或者private
- Repository
仓储实现具体的数据库操作逻辑，只依赖Domain。
- Domain
定义业务对象

## 功能
模块可包含功能，功能必须添加于模块之中。

## API
IModule
