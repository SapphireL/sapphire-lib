﻿Param(
    [string] $apikey,
    [string] $packageName
)

# 设置控制台输出为 UTF-8 编码
[Console]::OutputEncoding = [System.Text.Encoding]::UTF8

# 获取所有 nupkg 文件
$nupkgs = Get-ChildItem artifacts/package/release

# 遍历所有的文件
foreach ($item in $nupkgs) {
    
    # 检查包名是否匹配
    if ($item.Name -eq $packageName) {

        $nupkg = $item.FullName

        Write-Output "-----------------"
        Write-Output "准备发布的包: $nupkg"

        # 推送到 NuGet 源
        dotnet nuget push $nupkg --skip-duplicate --api-key $apikey --source https://api.nuget.org/v3/index.json

        Write-Output "-----------------"
    }
}
