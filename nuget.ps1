Param(
    [string] $apikey
)

$nupkgs = Get-ChildItem artifacts/package/release

for ($i = 0; $i -le $nupkgs.Length - 1; $i++){
    $item = $nupkgs[$i];

    $nupkg = $item.FullName;

    Write-Output "-----------------";
    $nupkg;

    dotnet nuget push $nupkg --skip-duplicate --api-key $apikey --source https://api.nuget.org/v3/index.json;

    Write-Output "-----------------";
}
