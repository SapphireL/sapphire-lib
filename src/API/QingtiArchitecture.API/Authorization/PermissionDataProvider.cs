﻿using Mapster;

using QingtiArchitecture.Modules.Administration.AccessControl.Application.CommandHandlers.GetUserPermission;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Command.Abstractions;

namespace QingtiArchitecture.API.Authorization
{
    public class PermissionDataProvider(ICommandSender sender) : IPermissionDataProvider
    {
        public async Task<List<PermissionData>> GetPermissions(string userId)
        {
            var result = await sender.SendAsync(new UserPermissionQuery
            {
                UserId = new Guid(userId)
            });

            return result.Adapt<List<PermissionData>>();
        }
    }
}
