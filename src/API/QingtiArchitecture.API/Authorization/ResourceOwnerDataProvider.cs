﻿using SapphireLib.Application.Queries;
using SapphireLib.Authorization.Abstractions;
using SapphireLib.Command.Abstractions;

namespace QingtiArchitecture.API.Authorization
{
    public class ResourceOwnerDataProvider<TQuery>(ICommandSender sender) : IResourceOwnerDataProvider<TQuery>
        where TQuery : QueryBase<bool>
    {
        public async Task<bool> GetResourceData(TQuery query)
        {
            return (await sender.SendAsync(query)).Value;
        }
    }
}
