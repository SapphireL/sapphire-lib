﻿using Mapster;

using SapphireLib.Command.Abstractions;

namespace QingtiArchitecture.API.Controllers.AccessControlControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController(ICommandSender sender) : ControllerBase
    {
        [HttpPost]
        [ProducesResponseType(typeof(AuthenticateResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> AuthenticateUser(AuthenticateRequest request)
        {
            var result = await sender.SendAsync(request.Adapt<AuthenticateCommand>());
            if (result.Value.IsAuthenticated)
            {
                return Ok(result.Value);
            }

            return Unauthorized();
        }
    }
}
