﻿namespace QingtiArchitecture.API.Controllers.AccessControlControllers
{
    public class AuthenticateRequest
    {
        public string UserName { get; set; } = default!;

        public string Password { get; set; } = default!;
    }
}
