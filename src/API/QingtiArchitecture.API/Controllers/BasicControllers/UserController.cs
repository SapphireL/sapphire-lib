﻿using SapphireArchitecture.Application.CommandHandlers.AccessControls.TestOwner;

using SapphireLib.Command.Abstractions;

namespace QingtiArchitecture.API.Controllers.BasicControllers
{
    [Route("api/baisc/[controller]")]
    [ApiController]
    public class UserController(ICommandSender sender) : ControllerBase
    {
        [PermissionAuthorize("GETUSERS")]
        [ResourceOwner<TestResourseQuery>]

        // [ResourceOwner<ResourseQuery>]
        [HttpGet]
        public async Task<IActionResult> GetUsers(string? key, int current, int pageSize)
        {
            return Ok(await sender.SendAsync(new GetUsersQuery
            {
                Keyword = key,
                Current = current,
                PageSize = pageSize
            }));
        }
    }
}
