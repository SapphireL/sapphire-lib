﻿global using Microsoft.AspNetCore.Mvc;

global using QingtiArchitecture.Modules.Administration.AccessControl.Application.Authentication;
global using QingtiArchitecture.Modules.Administration.AccessControl.Infrastructure;
global using QingtiArchitecture.Modules.Administration.Basic.Application.UserHandlers;

global using SapphireLib.Authorization.Attributes;
global using SapphireLib.Authorization.Extensions;

global using SapphireLib.Module.Extensions;
global using SapphireLib.WebApp.Controller;
global using SapphireLib.WebApp.Extensions;
