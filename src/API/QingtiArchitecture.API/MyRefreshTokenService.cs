﻿using FastEndpoints;
using FastEndpoints.Security;

using Microsoft.Extensions.Caching.Distributed;

using QingtiArchitecture.API.Endpoints.AccessControlModule;
using QingtiArchitecture.Modules.Administration.AccessControl.Application.CommandHandlers.ValidateUser;

using SapphireLib.CQRS;

namespace QingtiArchitecture.API
{
    public class MyRefreshTokenService : RefreshTokenService<AuthenticateRequest, AuthenticateResult>, ICQRSEndpoint
    {
        private readonly IDistributedCache _distributedCache;

        public MyRefreshTokenService(IConfiguration config, IDistributedCache distributedCache, CommandExecutor commandExecutor)
        {
            _distributedCache = distributedCache;
            CommandExecutor = commandExecutor;
            Setup(x =>
            {
                x.TokenSigningKey = config["JWTSecret"];
                x.AccessTokenValidity = TimeSpan.FromMinutes(10);
                x.RefreshTokenValidity = TimeSpan.FromHours(1);
                x.Endpoint("/user/auth/refresh-token", ep =>
                {
                    ep.Summary(s => s.Description = "this is the refresh token endpoint");
                });
            });
        }

        public CommandExecutor CommandExecutor { get; }

        public override async Task PersistTokenAsync(AuthenticateResult response)
        {
            await _distributedCache.SetStringAsync(response.UserId, response.RefreshToken, new DistributedCacheEntryOptions().SetAbsoluteExpiration(response.RefreshExpiry));
        }

        public override async Task RefreshRequestValidationAsync(AuthenticateRequest req)
        {
            var token = await _distributedCache.GetStringAsync(req.UserId);
            if (string.IsNullOrEmpty(token))
            {
                AddError("The refresh token is not valid!");
            }
        }

        public override async Task SetRenewalPrivilegesAsync(AuthenticateRequest request, UserPrivileges privileges)
        {
            var result = await CommandExecutor.ExecuteQueryAsync(new GetUserPermissionsQuery(new Guid(request.UserId)));
            privileges.Permissions.AddRange(result.Permissions ?? []);
            privileges.Roles.AddRange(result.Roles ?? []);
            privileges.Claims.Add(("UserName", request.UserName));
        }
    }
}
