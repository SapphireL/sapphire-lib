using QingtiArchitecture.API.Authorization;

using SapphireArchitecture.Application.CommandHandlers.AccessControls.TestOwner;

using SapphireLib.Cache;
using SapphireLib.WebApp.Definitions;

var builder = WebApplication.CreateBuilder(args);

builder.AddSappAuthorization(action =>
{
    action.AddJwtFeature()
    .AddSuperAdminAutoAllowFeature()
    .AddDataPermissionFeature<PermissionDataProvider>()
    .AddDataOwnerResourceFeature<ResourceOwnerDataProvider<TestResourseQuery>, TestResourseQuery>();
});

builder.AddSappWebApp(action =>
{
    action.AddApiDocumentFeature(configure =>
    {
        configure.DocumentName = "Test";
    });
    action.AddController();
});

builder.AddSapphireModule<AccessControlModule>();
builder.AddSapphireModule<CacheModule>();

// builder.AddSapphireModule<BasicModule>();
var app = builder.Build();

// app.UseSapphireModules();
app.UseSapphireModule<WebAppModule>();
app.UseSapphireModule<CacheModule>();
app.Run();