﻿using Autofac;
using Autofac.Extensions.DependencyInjection;

using FastEndpoints;
using FastEndpoints.Swagger;

using Hellang.Middleware.ProblemDetails;

using QingtiArchitecture.BuildingBlocks.Application;
using QingtiArchitecture.BuildingBlocks.Application.Contracts;
using QingtiArchitecture.BuildingBlocks.Domain;
using QingtiArchitecture.BuildingBlocks.Infrastructure;
using QingtiArchitecture.BuildingBlocks.Middleware.Authorization;
using QingtiArchitecture.BuildingBlocks.Middleware.Logger;
using QingtiArchitecture.BuildingBlocks.Middleware.RequestContext;
using QingtiArchitecture.BuildingBlocks.Middleware.Validation;
using QingtiArchitecture.Modules.Administration.AccessControl.Application.IntegrationEvents;
using QingtiArchitecture.Modules.Administration.AccessControl.Infrastructure;
using QingtiArchitecture.Modules.Administration.Basic.Infrastructure;

using ILogger = Serilog.ILogger;

namespace QingtiArchitecture.API
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private static ILogger _logger = null!;
        private static ILogger _loggerForApi = null!;
        private string? connectionString;

        public Startup(IWebHostEnvironment env)
        {
            _logger = SerilogConfig.GetLogger();

            _loggerForApi = _logger.ForContext("Module", "API");

            _loggerForApi.Information("Logger configured");
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")

                // 设置机密方式 dotnet user-secrets set "Section:Key" "Value"
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables("Qingti_")
                .Build();

            connectionString = _configuration["ConnectionString"];
            _loggerForApi.Information("Connection string:" + _configuration["ConnectionString"]);

            // AuthorizationChecker.CheckAllEndpoints<Startup>();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddFastEndpoints().SwaggerDocument();

            // services.AddQingtiSwagger();

            // services.AddQingtiIdentityServer<AccessControlModule>();
            services.AddRequestContextAccessor();

            services.AddProblemDetails(x =>
            {
                x.Map<InvalidCommandException>(ex => new InvalidCommandProblemDetails(ex));
                x.Map<BusinessRuleValidationException>(ex => new BusinessRuleValidationExceptionProblemDetails(ex));
            });

            services.AddQingtiAuthorization<AccessControlModule>();

            services.AddTransient<ICommandExecutor<BasicModule>, CommandExecutor<BasicModule>>();
        }

        public void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            if (connectionString != null)
            {
                containerBuilder.Register(module =>
                {
                    return new BasicModule(connectionString, _logger, _configuration);
                }).SingleInstance().As<BaseModule<BasicModule>>();

                containerBuilder.RegisterType<UserAddedIntergrationEventHandler>().InstancePerDependency();
            }
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            // var db = serviceProvider.GetRequiredService<AccessControlDbContext>();
            // db.Database.EnsureCreated();
            var container = serviceProvider.GetAutofacRoot();

            app.UseCors(builder =>
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            InitializeModules(container);

            app.UseCorrelation();

            // app.UseQingtiSwagger();

            // app.UseQingtiIdentityServer();
            if (env.IsDevelopment())
            {
                app.UseProblemDetails();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            // app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(router =>
            {
                router.MapFastEndpoints();
            }).UseSwaggerGen();
        }

        private void InitializeModules(ILifetimeScope container)
        {
        }
    }
}
