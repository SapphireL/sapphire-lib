﻿using Microsoft.Extensions.Options;

using SapphireLib.ADO.SqlServer;
using SapphireLib.ADO.SqlServer.Definitions;

namespace SapphireLibApp.ApplicationLogs.Application;

public class ApplicationLogsQueryClient(IOptionsSnapshot<SqlServerOptions<ApplicationLogsQueryClient>> options) : SqlServerConnectionClient(options)
{
}
