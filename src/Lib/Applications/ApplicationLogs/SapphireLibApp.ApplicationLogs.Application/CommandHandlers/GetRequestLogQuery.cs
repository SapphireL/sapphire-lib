﻿using SapphireLib.Application.Queries;

namespace SapphireLibApp.ApplicationLogs.Application.CommandHandlers;

public class GetRequestLogQuery : PagedQuery<PagedQueryResult<GetRequestLogQueryResult>>
{
    public string? Path { get; set; } = null;

    public string? ClientIp { get; set; } = null;

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public int? StatusCode { get; set; }

    public string? Username { get; set; }

    public string? Method { get; set; }

    public int? ConsumingMillisecondsStart { get; set; }

    public int? ConsumingMillisecondsEnd { get; set; }

    public override string? SortBy { get; set; } = "CreationTime";
}
