﻿using Dapper;

using SapphireLib.Application.Commands;
using SapphireLib.Application.Queries;
using SapphireLib.Domain;

namespace SapphireLibApp.ApplicationLogs.Application.CommandHandlers;

public class GetRequestLogQueryHandler(ApplicationLogsQueryClient client) : CommandHandler<GetRequestLogQuery, PagedQueryResult<GetRequestLogQueryResult>>
{
    public override async Task<Result<PagedQueryResult<GetRequestLogQueryResult>>> HandleAsync(GetRequestLogQuery command, CancellationToken cancellationToken = default)
    {
        using var conn = client.GetOpenConnection();
        var builder = new SqlBuilder();

        var selectTemplate = builder.AddTemplate(
           $@"SELECT 
[RequestLogs].Id, 
[RequestLogs].Path, 
[RequestLogs].Method, 
[RequestLogs].DeviceBrand, 
[RequestLogs].Device, 
[RequestLogs].Browser, 
[RequestLogs].BrowserVersion, 
[RequestLogs].Header as RequestHeader, 
[RequestLogs].Body as RequestBody, 
[RequestLogs].ClientIp,
[RequestLogs].Address, 
[RequestLogs].OS, 
[RequestLogs].UserAgent, 
[RequestLogs].UserId, 
[RequestLogs].Username, 
[ResponseLogs].StatusCode, 
[ResponseLogs].Header as ResponseHeader, 
[ResponseLogs].Body as ResponseBody, 
[RequestLogs].CreationTime,
DATEDIFF(millisecond, [RequestLogs].CreationTime, [ResponseLogs].CreationTime) AS ConsumingMilliseconds
           FROM {client.Schema}.[RequestLogs] as [RequestLogs]
              LEFT JOIN 
            {client.Schema}.[ResponseLogs] as [ResponseLogs]
            on 
            [RequestLogs].Id=[ResponseLogs].Id
            /**where**/
            /**orderby**/
            OFFSET @Offset ROWS FETCH NEXT @Next ROWS ONLY
            ",
           new
           {
               Offset = command.Offset(),
               Next = command.Next()
           });

        var countTemplate = builder.AddTemplate(@$"select count(1) from {client.Schema}.[RequestLogs] as [RequestLogs] LEFT JOIN 
            {client.Schema}.[ResponseLogs] as [ResponseLogs]
            on 
            [RequestLogs].Id=[ResponseLogs].Id /**where**/");

        if (!string.IsNullOrWhiteSpace(command.Username))
        {
            var username = $"%{command.Username.Trim()}%";
            builder.Where("Username like @Username", new { Username = username });
        }

        if (!string.IsNullOrWhiteSpace(command.Path))
        {
            builder.Where("Path like @Path", new { Path = $"%{command.Path.Trim()}%" });
        }

        if (!string.IsNullOrWhiteSpace(command.ClientIp))
        {
            builder.Where("ClientIp like @ClientIp", new { ClientIp = $"%{command.ClientIp.Trim()}%" });
        }

        if (command.StatusCode != null)
        {
            builder.Where("StatusCode = @StatusCode", new { command.StatusCode });
        }

        if (!string.IsNullOrWhiteSpace(command.Method))
        {
            builder.Where("Method = @Method", new { command.Method });
        }

        if (command.ConsumingMillisecondsStart != null)
        {
            builder.Where("DATEDIFF(millisecond, [RequestLogs].CreationTime, [ResponseLogs].CreationTime) >= @ConsumingMillisecondsStart", new { command.ConsumingMillisecondsStart });
        }

        if (command.ConsumingMillisecondsEnd != null)
        {
            builder.Where("DATEDIFF(millisecond, [RequestLogs].CreationTime, [ResponseLogs].CreationTime) <= @ConsumingMillisecondsEnd", new { command.ConsumingMillisecondsEnd });
        }

        if (command.StartTime != null)
        {
            builder.Where("CreationTime >= @StartTime", new { command.StartTime });
        }

        if (command.EndTime != null)
        {
            builder.Where("CreationTime <= @EndTime", new { command.EndTime });
        }

        if (!string.IsNullOrWhiteSpace(command.SortBy))
        {
            var desc = command.IsDescending ? "desc" : "asc";
            builder.OrderBy($"{command.SortBy} {desc}");
        }

        var data = await conn.QueryAsync<GetRequestLogQueryResult>(selectTemplate.RawSql, selectTemplate.Parameters);
        var total = conn.ExecuteScalar<int>(countTemplate.RawSql, countTemplate.Parameters);

        var result = new PagedQueryResult<GetRequestLogQueryResult>
        {
            Total = total,
            Current = command.Current,
            PageSize = command.PageSize,
            Data = data.ToList()
        };

        return result;
    }
}
