﻿namespace SapphireLibApp.ApplicationLogs.Application.CommandHandlers;

public record GetRequestLogQueryResult
{
    public Guid Id { get; set; }

    /// <summary>
    /// 请求接口名称.
    /// </summary>
    public string? Path { get; set; }

    /// <summary>
    /// 请求方法名称.
    /// </summary>
    public string? Method { get; set; }

    public string? DeviceBrand { get; set; }

    public string? Device { get; set; }

    public string? Browser { get; set; }

    public string? BrowserVersion { get; set; }

    public string? RequestHeader { get; set; }

    public string? RequestBody { get; set; }

    /// <summary>
    /// 响应头.
    /// </summary>
    public string? ResponseHeader { get; set; }

    /// <summary>
    /// 响应内容.
    /// </summary>
    public string? ResponseBody { get; set; }

    /// <summary>
    /// 请求IP地址.
    /// </summary>
    public string? ClientIp { get; set; }

    /// <summary>
    /// 请求地址.
    /// </summary>
    public string? Address { get; set; }

    /// <summary>
    /// 请求操作系统.
    /// </summary>
    public string? OS { get; set; }

    /// <summary>
    /// 请求用户代理.
    /// </summary>
    public string? UserAgent { get; set; }

    /// <summary>
    /// 请求用户ID.
    /// </summary>
    public string? UserId { get; set; }

    public string? Username { get; set; }

    public int StatusCode { get; set; }

    public DateTime CreationTime { get; set; }

    public int? ConsumingMilliseconds { get; set; }
}
