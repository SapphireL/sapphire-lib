﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using SapphireLib.EventBus.EventBus;
using SapphireLib.UnitOfWork.Abstractions;
using SapphireLib.WebApp.Events;

using SapphireLibApp.ApplicationLogs.Domain;
using SapphireLibApp.ApplicationLogs.Domain.Definitions;

namespace SapphireLibApp.ApplicationLogs.Application.EventHandlers;

public class NewRequestExecutedEventHandler(IApplicationLogRepository applicationLogRepository, [FromKeyedServices("SappAppLogs")] IUnitOfWork unitOfWork, IOptions<SappAppLogsModuleOptions> options) : BaseEventHandler<NewRequestExecutedEvent>
{
    public override async Task HandleAsync(NewRequestExecutedEvent @event, CancellationToken cancellationToken = default)
    {
        var path = @event.Path;

        if (string.IsNullOrEmpty(path) ||
            string.Equals(path, options.Value.RequestLogPath, StringComparison.OrdinalIgnoreCase))
        {
            return;
        }

        var ignorePaths = options.Value.IgnorePath?.Split(';', StringSplitOptions.RemoveEmptyEntries);

        if (ignorePaths?.Any(ignorePath => string.Equals(ignorePath, path, StringComparison.OrdinalIgnoreCase)) == true)
        {
            return;
        }

        var responseLog = new ApplicationResponseLog(@event.Id, @event.Header, @event.Body, @event.StatusCode, @event.OccurredOn);
        await applicationLogRepository.InsertAsync(responseLog);
        await unitOfWork.SaveChangesAsync(cancellationToken);
    }
}
