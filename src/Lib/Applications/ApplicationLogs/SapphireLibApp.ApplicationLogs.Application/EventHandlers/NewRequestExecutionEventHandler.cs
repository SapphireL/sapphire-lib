﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using SapphireLib.EventBus.EventBus;
using SapphireLib.UnitOfWork.Abstractions;
using SapphireLib.WebApp.Events;

using SapphireLibApp.ApplicationLogs.Domain;
using SapphireLibApp.ApplicationLogs.Domain.Definitions;

namespace SapphireLibApp.ApplicationLogs.Application.EventHandlers;

public class NewRequestExecutionEventHandler(IApplicationLogRepository applicationLogRepository, [FromKeyedServices("SappAppLogs")] IUnitOfWork unitOfWork, IOptions<SappAppLogsModuleOptions> options) : BaseEventHandler<NewRequestExecutionEvent>
{
    public override async Task HandleAsync(NewRequestExecutionEvent @event, CancellationToken cancellationToken = default)
    {
        var path = @event.Path;

        if (string.IsNullOrEmpty(path) ||
            string.Equals(path, options.Value.RequestLogPath, StringComparison.OrdinalIgnoreCase))
        {
            return;
        }

        var ignorePaths = options.Value.IgnorePath?.Split(';', StringSplitOptions.RemoveEmptyEntries);

        if (ignorePaths?.Any(ignorePath => string.Equals(ignorePath, path, StringComparison.OrdinalIgnoreCase)) == true)
        {
            return;
        }

        var requestLog = new ApplicationRequestLog(@event.Id, @event.Path, @event.Method, @event.DeviceBrand, @event.Device, @event.Browser, @event.BrowserVersion, @event.Header, @event.Body, @event.ClientIp, @event.Address, @event.OS, @event.UserAgent, @event.Username, @event.UserId, @event.OccurredOn);
        await applicationLogRepository.InsertAsync(requestLog);
        await unitOfWork.SaveChangesAsync(cancellationToken);
    }
}
