﻿using SapphireLib.Domain;
using SapphireLib.Domain.Entities;

namespace SapphireLibApp.ApplicationLogs.Domain;

public class ApplicationRequestLog : EntityId<Guid>, ICreationTime
{
    public ApplicationRequestLog(Guid traceId, string? path, string? method, string? deviceBrand, string? device, string? browser, string? browserVersion, string? header, string? body, string? clientIp, string? address, string? os, string? userAgent, string? username, string? userId, DateTime creationTime)
        : base(traceId)
    {
        Path = path;
        Method = method;
        DeviceBrand = deviceBrand;
        Device = device;
        Browser = browser;
        BrowserVersion = browserVersion;
        Header = header;
        Body = body;
        ClientIp = clientIp;
        Address = address;
        OS = os;
        UserAgent = userAgent;
        UserId = userId;
        Username = username;
        CreationTime = creationTime;
    }

    protected ApplicationRequestLog()
    {
    }

    /// <summary>
    /// 请求接口名称.
    /// </summary>
    public string? Path { get; private set; }

    /// <summary>
    /// 请求方法名称.
    /// </summary>
    public string? Method { get; private set; }

    public string? DeviceBrand { get; private set; }

    public string? Device { get; private set; }

    public string? Browser { get; private set; }

    public string? BrowserVersion { get; private set; }

    public string? Header { get; private set; }

    public string? Body { get; private set; }

    /// <summary>
    /// 请求IP地址.
    /// </summary>
    public string? ClientIp { get; private set; }

    /// <summary>
    /// 请求地址.
    /// </summary>
    public string? Address { get; private set; }

    /// <summary>
    /// 请求操作系统.
    /// </summary>
    public string? OS { get; private set; }

    /// <summary>
    /// 请求用户代理.
    /// </summary>
    public string? UserAgent { get; private set; }

    /// <summary>
    /// 请求用户ID.
    /// </summary>
    public string? UserId { get; private set; }

    public string? Username { get; private set; }

    public DateTime CreationTime { get; private set; }
}
