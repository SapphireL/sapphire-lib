﻿using SapphireLib.Domain;
using SapphireLib.Domain.Entities;

namespace SapphireLibApp.ApplicationLogs.Domain;

public class ApplicationResponseLog
    : EntityId<Guid>, ICreationTime
{
    public ApplicationResponseLog(Guid traceId, string? header, string? body, int statusCode, DateTime creationTime)
        : base(traceId)
    {
        Header = header;
        Body = body;
        StatusCode = statusCode;
        CreationTime = creationTime;
    }

    protected ApplicationResponseLog()
    {
    }

    /// <summary>
    /// 响应头.
    /// </summary>
    public string? Header { get; private set; }

    /// <summary>
    /// 响应内容.
    /// </summary>
    public string? Body { get; private set; }

    public int StatusCode { get; private set; }

    public DateTime CreationTime { get; private set; }
}
