﻿namespace SapphireLibApp.ApplicationLogs.Domain.Definitions;

public class SappAppLogsModuleOptions
{
    public string? RequestLogPath { get; set; } = "/api/SystemLogs/SystemLogs";

    public string? IgnorePath { get; set; }
}
