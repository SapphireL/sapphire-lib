﻿namespace SapphireLibApp.ApplicationLogs.Domain;

public interface IApplicationLogRepository
{
    public Task InsertAsync(ApplicationRequestLog log);

    public Task InsertAsync(ApplicationResponseLog log);
}
