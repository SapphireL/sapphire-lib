﻿using SapphireLib.ADO.Extensions;
using SapphireLib.ADO.SqlServer.Definitions;
using SapphireLib.ADO.SqlServer.Extensions;
using SapphireLib.Application.Queries;
using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Extensions;
using SapphireLib.Domain;
using SapphireLib.EventBus.Extensions;
using SapphireLib.Module.BusinessModule.Extensions;
using SapphireLib.Module.Core;
using SapphireLib.Repository.EFCore.Extensions;
using SapphireLib.Repository.Extensions;
using SapphireLib.UnitOfWork.EFCore.Extensions;
using SapphireLib.UnitOfWork.Extensions;
using SapphireLib.WebApp.Events;

using SapphireLibApp.ApplicationLogs.Application;
using SapphireLibApp.ApplicationLogs.Application.CommandHandlers;
using SapphireLibApp.ApplicationLogs.Application.EventHandlers;
using SapphireLibApp.ApplicationLogs.Domain;
using SapphireLibApp.ApplicationLogs.Repository.Database;
using SapphireLibApp.ApplicationLogs.Repository.Repositories;

namespace SapphireLibApp.ApplicationLogs.Infrastructure.Definitions;

internal class SappAppLogsModule : BaseModule
{
    protected override void ConfigureServices(BuilderContext context)
    {
        context.WebApplicationBuilder.AddSappLibRepository(action => action.AddEFCore<ApplicationLogsDbContext>(configure: configure =>
        {
            configure.ConnectionString = "Server=.;Database=SapphireArchitecture;User Id=sa;Password=123456;TrustServerCertificate=true;";
            configure.DbType = "SqlServer";
            configure.Schema = "ApplicationLogs";
        }));
    }

    protected override void OnConfiguring(FeaturesBuilder builder)
    {
        builder.AddUnitOfWorkFeature(
        config =>
        {
            config.UseEFCore<ApplicationLogsDbContext>();
        },
        options =>
        {
            options.DbContextServiceKey = "SappAppLogs";
        });

        builder.AddADOClient(configure =>
        {
            configure.UseSqlServer((SqlServerOptions<ApplicationLogsQueryClient> sqlServerOptions) =>
            {
                sqlServerOptions.DatabaseName = "SapphireArchitecture";
                sqlServerOptions.ConnectionString = "Server=.;Database=SapphireArchitecture;User Id=sa;Password=123456;TrustServerCertificate=true;";
                sqlServerOptions.Schema = "ApplicationLogs";
            });
        });

        builder.AddRepository<IApplicationLogRepository, ApplicationLogRepositoriy>();

        builder.AddEventHandler<NewRequestExecutedEventHandler, NewRequestExecutedEvent>();
        builder.AddEventHandler<NewRequestExecutionEventHandler, NewRequestExecutionEvent>();
        builder.AddCommandHandler<ICommandHandler<GetRequestLogQuery, Result<PagedQueryResult<GetRequestLogQueryResult>>>, GetRequestLogQueryHandler>();
    }
}
