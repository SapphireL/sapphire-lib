﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Extensions;

using SapphireLibApp.ApplicationLogs.Domain.Definitions;
using SapphireLibApp.ApplicationLogs.Infrastructure.Definitions;

namespace SapphireLibApp.ApplicationLogs.Infrastructure.Extensions;

public static class WebApplicationBuilderExtensions
{
    public static WebApplicationBuilder AddSappAppRequestLogs(this WebApplicationBuilder builder, string section = "AppRequestLogs")
    {
        builder.AddSappLib<SappAppLogsModule, SappAppLogsModuleOptions>(section);
        return builder;
    }
}
