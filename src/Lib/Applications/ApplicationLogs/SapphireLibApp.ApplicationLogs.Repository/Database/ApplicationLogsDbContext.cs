﻿using Microsoft.EntityFrameworkCore;

using SapphireLib.Repository.EFCore;
using SapphireLib.Repository.EFCore.Definitions;

using SapphireLibApp.ApplicationLogs.Domain;

namespace SapphireLibApp.ApplicationLogs.Repository.Database;

public class ApplicationLogsDbContext : BaseDbContext<ApplicationLogsDbContext>
{
    public ApplicationLogsDbContext(EFCoreOptions<ApplicationLogsDbContext> configOptions)
        : base(configOptions)
    {
    }

    protected ApplicationLogsDbContext()
        : base()
    {
    }

    public DbSet<ApplicationRequestLog> ApplicationRequestLogs { get; set; }

    public DbSet<ApplicationResponseLog> ApplicationResponseLogs { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyEntityTypeConfiguration(BaseDbConfigOptions.Schema);
    }
}
