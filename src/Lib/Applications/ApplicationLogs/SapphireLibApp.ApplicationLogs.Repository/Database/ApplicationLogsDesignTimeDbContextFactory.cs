﻿using SapphireLib.Repository.EFCore;
using SapphireLib.Repository.EFCore.Definitions;

namespace SapphireLibApp.ApplicationLogs.Repository.Database;

public class ApplicationLogsDesignTimeDbContextFactory : BaseDesignTimeDbContextFactory<ApplicationLogsDbContext, EFCoreOptions<ApplicationLogsDbContext>>
{
    public override ApplicationLogsDbContext CreateDbContext(string[] args)
    {
        var context = base.CreateDbContext(args);

        return context;
    }
}
