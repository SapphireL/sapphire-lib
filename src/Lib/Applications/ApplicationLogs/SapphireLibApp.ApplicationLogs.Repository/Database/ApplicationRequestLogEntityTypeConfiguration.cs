﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireLib.Repository.EFCore;

using SapphireLibApp.ApplicationLogs.Domain;

namespace SapphireLibApp.ApplicationLogs.Repository.Database;

internal class ApplicationRequestLogEntityTypeConfiguration(string? schema)
: BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<ApplicationRequestLog>
{
    public void Configure(EntityTypeBuilder<ApplicationRequestLog> builder)
    {
        builder.ToTable($"RequestLogs", Schema);
        builder.HasOne<ApplicationResponseLog>().WithOne().HasForeignKey<ApplicationResponseLog>(e => e.Id).IsRequired(false);
        builder.HasKey(u => u.Id);
    }
}
