﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireLib.Repository.EFCore;

using SapphireLibApp.ApplicationLogs.Domain;

namespace SapphireLibApp.ApplicationLogs.Repository.Database;

internal class ApplicationResponseLogEntityTypeConfiguration(string? schema)
: BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<ApplicationResponseLog>
{
    public void Configure(EntityTypeBuilder<ApplicationResponseLog> builder)
    {
        builder.ToTable($"ResponseLogs", Schema);
        builder.HasKey(u => u.Id);
    }
}
