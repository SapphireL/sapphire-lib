﻿using Microsoft.EntityFrameworkCore;

namespace SapphireLibApp.ApplicationLogs.Repository.Database;

internal static class ModelBuilderExtension
{
    internal static void ApplyEntityTypeConfiguration(this ModelBuilder modelBuilder, string? schema)
    {
        modelBuilder.ApplyConfiguration(new ApplicationRequestLogEntityTypeConfiguration(schema));
        modelBuilder.ApplyConfiguration(new ApplicationResponseLogEntityTypeConfiguration(schema));
    }
}
