﻿using SapphireLib.Repository;

using SapphireLibApp.ApplicationLogs.Domain;
using SapphireLibApp.ApplicationLogs.Repository.Database;

namespace SapphireLibApp.ApplicationLogs.Repository.Repositories;

public class ApplicationLogRepositoriy(ApplicationLogsDbContext context) : IApplicationLogRepository, IRepository
{
    public async Task InsertAsync(ApplicationRequestLog log)
    {
        await context.ApplicationRequestLogs.AddAsync(log);
    }

    public async Task InsertAsync(ApplicationResponseLog log)
    {
        await context.ApplicationResponseLogs.AddAsync(log);
    }
}
