﻿using Microsoft.Extensions.Options;

using SapphireLib.ADO.SqlServer;
using SapphireLib.ADO.SqlServer.Definitions;

namespace SapphireLibApp.AuditableEntities.Application;

public class AuditableEntitiesQueryClient(IOptionsSnapshot<SqlServerOptions<AuditableEntitiesQueryClient>> options) : SqlServerConnectionClient(options)
{
}
