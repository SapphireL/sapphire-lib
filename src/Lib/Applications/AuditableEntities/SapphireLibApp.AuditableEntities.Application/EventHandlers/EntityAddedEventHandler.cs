﻿using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;

using SapphireLib.Domain.Entities;
using SapphireLib.Domain.Events;
using SapphireLib.EventBus.EventBus;
using SapphireLib.UnitOfWork.Abstractions;

using SapphireLibApp.AuditableEntities.Domain;

namespace SapphireLibApp.AuditableEntities.Application.EventHandlers;

internal class EntityAddedEventHandler<TEntity>(IAuditableEntityRepository repository, [FromKeyedServices("SappAppAuditableEntity")] IUnitOfWork unitOfWork) : BaseEventHandler<EntityAddedEvent<TEntity>>
    where TEntity : Entity
{
    public override async Task HandleAsync(EntityAddedEvent<TEntity> @event, CancellationToken cancellationToken = default)
    {
        var entity = new AuditableEntity<TEntity>(@event.GetType().Name.ToString(), AuditableEntityType.Added, JsonConvert.SerializeObject(@event));
        await repository.InsertAsync(entity);
        await unitOfWork.SaveChangesAsync(cancellationToken);
    }
}
