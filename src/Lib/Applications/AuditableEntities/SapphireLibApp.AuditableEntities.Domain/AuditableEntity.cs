﻿using SapphireLib.Domain;
using SapphireLib.Domain.Entities;

namespace SapphireLibApp.AuditableEntities.Domain;

public class AuditableEntity<TEntity> : EntityId<Guid>, ICreationTime
    where TEntity : Entity
{
    public AuditableEntity(string entityType, AuditableEntityType auditableEntityType, string entityValue, string? previousEntityValue = null, AuditableEntityOperator? auditableEntityOperator = null)
    {
        PreviousEntityValue = previousEntityValue;
        Operator = auditableEntityOperator;

        var tempEntity = new AuditableEntity<TEntity>(entityType, auditableEntityType, entityValue);
        EntityType = tempEntity.EntityType;
        AuditableEntityType = tempEntity.AuditableEntityType;
        EntityValue = tempEntity.EntityValue;
    }

    private AuditableEntity(string entityType, AuditableEntityType auditableEntityType, string entityValue)
    {
        EntityType = entityType;
        AuditableEntityType = auditableEntityType;
        EntityValue = entityValue;
        CreationTime = DateTime.Now;
    }

    public DateTime CreationTime { get; private set; }

    public string EntityType { get; private set; }

    public AuditableEntityType AuditableEntityType { get; private set; }

    public string EntityValue { get; private set; }

    public string? PreviousEntityValue { get; private set; }

    public AuditableEntityOperator? Operator { get; private set; }
}
