﻿namespace SapphireLibApp.AuditableEntities.Domain;

public record AuditableEntityOperator(string UserId, string Username, string? EmployeeCode, string? EmployeeName);