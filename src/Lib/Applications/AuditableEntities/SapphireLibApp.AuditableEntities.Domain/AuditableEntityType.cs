﻿namespace SapphireLibApp.AuditableEntities.Domain;

public enum AuditableEntityType
{
    /// <summary>
    /// 添加.
    /// </summary>
    Added = 0,

    /// <summary>
    /// 删除.
    /// </summary>
    Deleted = 1,

    /// <summary>
    /// 修改.
    /// </summary>
    Updated = 2
}
