﻿using SapphireLib.Domain.Entities;

namespace SapphireLibApp.AuditableEntities.Domain;

public interface IAuditableEntityRepository
{
    public Task InsertAsync<TEntity>(AuditableEntity<TEntity> entity)
        where TEntity : Entity;
}
