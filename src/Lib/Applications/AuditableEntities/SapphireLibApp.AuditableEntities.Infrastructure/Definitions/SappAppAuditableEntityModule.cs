﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.ADO.Extensions;
using SapphireLib.ADO.SqlServer.Extensions;
using SapphireLib.Module.BusinessModule.Extensions;
using SapphireLib.Module.Core;
using SapphireLib.Repository.EFCore.Extensions;
using SapphireLib.UnitOfWork.EFCore.Extensions;
using SapphireLib.UnitOfWork.Extensions;

using SapphireLibApp.AuditableEntities.Application;
using SapphireLibApp.AuditableEntities.Domain;
using SapphireLibApp.AuditableEntities.Repository.Database;
using SapphireLibApp.AuditableEntities.Repository.Repositories;

namespace SapphireLibApp.AuditableEntities.Infrastructure.Definitions;

public class SappAppAuditableEntityModule : BaseModule
{
    protected override void OnConfiguring(FeaturesBuilder builder)
    {
        builder.AddUnitOfWorkFeature(
        config =>
        {
            config.UseEFCore<AuditableEntitiesDbContext>();
        },
        options =>
        {
            options.DbContextServiceKey = "SappAppLogs";
        });

        builder.AddADOClient(configure =>
        {
            configure.UseSqlServer<AuditableEntitiesQueryClient>();
        });

        builder.AddRepository<IAuditableEntityRepository, AuditableEntityRepositoriy>();

        var options = builder.Context.GetRequiredOptions<SappAppAuditableEntityModuleOptions>();

        builder.AddEFCore<AuditableEntitiesDbContext>();

        builder.AddEventHandlers();
    }

    protected override void OnRunning(RunningContext context)
    {
        base.OnRunning(context);
        using (var serviceScope = context.WebApplication.Services.CreateScope())
        {
            var dbContext = serviceScope.ServiceProvider.GetRequiredService<AuditableEntitiesDbContext>();

            // 确保数据库已创建
            dbContext.Database.EnsureCreated();
        }
    }
}
