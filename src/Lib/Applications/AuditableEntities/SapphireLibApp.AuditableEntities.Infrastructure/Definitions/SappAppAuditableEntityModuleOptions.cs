﻿namespace SapphireLibApp.AuditableEntities.Infrastructure.Definitions;

public class SappAppAuditableEntityModuleOptions
{
    public string ConnectionString { get; set; } = default!;

    public string? Schema { get; set; }

    public string DbType { get; set; } = default!;
}
