﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Extensions;

using SapphireLibApp.AuditableEntities.Infrastructure.Definitions;

namespace SapphireLibApp.ApplicationLogs.Infrastructure.Extensions;

public static class WebApplicationBuilderExtensions
{
    public static WebApplicationBuilder AddSappAppAuditable(this WebApplicationBuilder builder, string section = "Auditable")
    {
        builder.AddSappApp<SappAppAuditableEntityModule, SappAppAuditableEntityModuleOptions>(section);
        return builder;
    }
}
