﻿using System.Reflection;

using Microsoft.EntityFrameworkCore;

using SapphireLib.Domain.Attributes;
using SapphireLib.Domain.Entities;
using SapphireLib.Repository.EFCore;
using SapphireLib.Repository.EFCore.Definitions;

using SapphireLibApp.AuditableEntities.Domain;

namespace SapphireLibApp.AuditableEntities.Repository.Database;

public class AuditableEntitiesDbContext : BaseDbContext<AuditableEntitiesDbContext>
{
    public AuditableEntitiesDbContext(EFCoreOptions<AuditableEntitiesDbContext> configOptions)
     : base(configOptions)
    {
    }

    protected AuditableEntitiesDbContext()
    : base()
    {
    }

    public DbSet<AuditableEntity<TEntity>> GetAuditLogDbSet<TEntity>()
        where TEntity : Entity
    {
        return Set<AuditableEntity<TEntity>>();
    }

    // public DbSet<AuditableEntity> AuditableEntities { get; set; }
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        // 遍历所有引用了当前程序集的程序集
        var assemblies = AppDomain.CurrentDomain.GetAssemblies()
            .Where(a => a.GetReferencedAssemblies().Any(r => r.Name == typeof(AuditableEventPersistentAttribute).Assembly.GetName().Name));

        // 遍历找到的程序集中的所有类型，筛选出带有指定属性的类型
        var entityTypes = assemblies
            .SelectMany(a => a.GetTypes())
            .Where(type => type.GetCustomAttributes<AuditableEventPersistentAttribute>().Any());

        foreach (var entityType in entityTypes)
        {
            Console.WriteLine($"Registering entity: {entityType.Name}");

            // 动态创建并应用 AuditableEntityEntityTypeConfiguration 配置
            var configType = typeof(AuditableEntityEntityTypeConfiguration<>).MakeGenericType(entityType);
            var configInstance = Activator.CreateInstance(configType, BaseDbConfigOptions.Schema);

            // 使用 ApplyConfiguration 动态应用配置
            builder.ApplyConfiguration((dynamic)configInstance!);
        }
    }
}
