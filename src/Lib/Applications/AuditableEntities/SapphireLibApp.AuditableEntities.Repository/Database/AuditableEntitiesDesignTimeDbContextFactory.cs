﻿using SapphireLib.Repository.EFCore;
using SapphireLib.Repository.EFCore.Definitions;

namespace SapphireLibApp.AuditableEntities.Repository.Database;

public class AuditableEntitiesDesignTimeDbContextFactory : BaseDesignTimeDbContextFactory<AuditableEntitiesDbContext, EFCoreOptions<AuditableEntitiesDbContext>>
{
    public override AuditableEntitiesDbContext CreateDbContext(string[] args)
    {
        var context = base.CreateDbContext(args);

        return context;
    }
}
