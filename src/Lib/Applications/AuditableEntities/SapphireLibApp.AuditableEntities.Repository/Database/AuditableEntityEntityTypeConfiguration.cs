﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireLib.Domain.Entities;
using SapphireLib.Repository.EFCore;

using SapphireLibApp.AuditableEntities.Domain;

namespace SapphireLibApp.AuditableEntities.Repository.Database;

internal class AuditableEntityEntityTypeConfiguration<TEntity>(string? schema)
: BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AuditableEntity<TEntity>>
where TEntity : Entity
{
    public void Configure(EntityTypeBuilder<AuditableEntity<TEntity>> builder)
    {
        builder.ToTable("AuditableEntities_" + typeof(TEntity).Name, Schema);

        builder.Ignore(a => a.DomainEvents);

        // 配置属性映射
        builder.Property(e => e.EntityType).HasColumnName("EntityType");
        builder.Property(e => e.AuditableEntityType).HasColumnName("AuditableEntityType");
        builder.Property(e => e.EntityValue).HasColumnName("EntityValue");
        builder.Property(e => e.PreviousEntityValue).HasColumnName("PreviousEntityValue");
        builder.Property(e => e.CreationTime).HasColumnName("CreationTime");

        // 配置拥有类型 Operator
        builder.OwnsOne(a => a.Operator, @operator =>
        {
            @operator.Property(o => o.UserId).HasColumnName("UserId");
            @operator.Property(o => o.Username).HasColumnName("Username");
            @operator.Property(o => o.EmployeeCode).HasColumnName("EmployeeCode");
            @operator.Property(o => o.EmployeeName).HasColumnName("EmployeeName");
        });

        // 配置主键
        builder.HasKey(u => u.Id);
    }
}
