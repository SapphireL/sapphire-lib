﻿using Microsoft.EntityFrameworkCore;

using SapphireLib.Domain.Entities;

namespace SapphireLibApp.AuditableEntities.Repository.Database;

internal static class ModelBuilderExtension
{
    internal static void ApplyEntityTypeConfiguration<TEntity>(this ModelBuilder modelBuilder, string? schema)
        where TEntity : Entity
    {
        modelBuilder.ApplyConfiguration(new AuditableEntityEntityTypeConfiguration<TEntity>(schema));
    }
}
