﻿using SapphireLib.Domain.Entities;
using SapphireLib.Repository;

using SapphireLibApp.AuditableEntities.Domain;
using SapphireLibApp.AuditableEntities.Repository.Database;

namespace SapphireLibApp.AuditableEntities.Repository.Repositories;

public class AuditableEntityRepositoriy(AuditableEntitiesDbContext context) : IAuditableEntityRepository, IRepository
{
    public async Task InsertAsync<TEntity>(AuditableEntity<TEntity> entity)
        where TEntity : Entity
    {
        await context.GetAuditLogDbSet<TEntity>().AddAsync(entity);
    }
}
