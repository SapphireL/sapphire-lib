﻿namespace SapphireLibApp.Tools.Application.QueryHandlers.GuidHandlers;

public class GetGuidQueryHandler : CommandHandler<GetGuidQuery, GetGuidQueryResult>
{
    public override async Task<Result<GetGuidQueryResult>> HandleAsync(GetGuidQuery command, CancellationToken cancellationToken = default)
    {
        await Task.CompletedTask;
        return new GetGuidQueryResult
        {
            Guid = Guid.NewGuid()
        };
    }
}
