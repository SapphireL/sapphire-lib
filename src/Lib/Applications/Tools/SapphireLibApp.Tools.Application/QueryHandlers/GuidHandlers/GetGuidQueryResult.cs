﻿namespace SapphireLibApp.Tools.Application.QueryHandlers.GuidHandlers;

public class GetGuidQueryResult
{
    public required Guid Guid { get; set; }
}
