﻿namespace SapphireLibApp.Tools.Application.QueryHandlers.Jwt;

public class DecodeJwtQuery : QueryBase<DecodeJwtQueryResult>
{
    public required string JwtToken { get; set; }
}
