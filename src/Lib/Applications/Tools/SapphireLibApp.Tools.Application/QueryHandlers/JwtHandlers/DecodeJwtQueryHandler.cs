﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SapphireLibApp.Tools.Application.QueryHandlers.Jwt;

public class DecodeJwtQueryHandler : CommandHandler<DecodeJwtQuery, DecodeJwtQueryResult>
{
    public override async Task<Result<DecodeJwtQueryResult>> HandleAsync(DecodeJwtQuery command, CancellationToken cancellationToken = default)
    {
        try
        {
            var tokenString = command.JwtToken;
            var jwtHandler = new JwtSecurityTokenHandler();
            var readableToken = jwtHandler.CanReadToken(tokenString);

            if (readableToken != true)
            {
                return Result.Failure<DecodeJwtQueryResult>(Error.Problem("invalid code", "The token doesn't seem to be in a proper JWT format."));
            }

            var token = jwtHandler.ReadJwtToken(tokenString);

            // Extract the headers of the JWT
            var headers = token.Header;
            var jwtHeader = "{";
            foreach (var h in headers)
            {
                jwtHeader += '"' + h.Key + "\":\"" + h.Value + "\",";
            }

            jwtHeader += "}";

            string header = JToken.Parse(jwtHeader).ToString(Formatting.Indented);

            // Extract the payload of the JWT
            var claims = token.Claims;
            var jwtPayload = "{";
            foreach (Claim c in claims)
            {
                jwtPayload += '"' + c.Type + "\":\"" + c.Value + "\",";
            }

            jwtPayload += "}";
            string payload = JToken.Parse(jwtPayload).ToString(Formatting.Indented);
            var result = new DecodeJwtQueryResult
            {
                Header = header,
                Payload = payload,
                Issuer = token.Issuer,
                IssuerAt = token.IssuedAt,
                ValidFrom = token.ValidFrom,
                ValidTo = token.ValidTo,
                Audiences = token.Audiences
            };
            await Task.CompletedTask;
            return result;
        }
        catch (Exception ex)
        {
            return Result.Failure<DecodeJwtQueryResult>(Error.Problem("invalid code", ex.Message));
        }
    }
}
