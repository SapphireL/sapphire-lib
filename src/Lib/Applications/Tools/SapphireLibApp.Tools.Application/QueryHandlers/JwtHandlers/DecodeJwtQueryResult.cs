﻿namespace SapphireLibApp.Tools.Application.QueryHandlers.Jwt;

public class DecodeJwtQueryResult
{
    public required string Header { get; set; }

    public required string Payload { get; set; }

    public required string Issuer { get; set; }

    public required DateTime IssuerAt { get; set; }

    public required DateTime ValidFrom { get; set; }

    public required DateTime ValidTo { get; set; }

    public required IEnumerable<string> Audiences { get; set; }
}
