﻿using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Extensions;
using SapphireLib.Domain;
using SapphireLib.Module.Core;

using SapphireLibApp.Tools.Application.QueryHandlers.GuidHandlers;
using SapphireLibApp.Tools.Application.QueryHandlers.Jwt;

namespace SapphireLibApp.Tools.Infrastructure;

internal class SappToolsModule : BaseModule
{
    protected override void OnConfiguring(FeaturesBuilder builder)
    {
        builder.AddCommandHandler<ICommandHandler<GetGuidQuery, Result<GetGuidQueryResult>>, GetGuidQueryHandler>();
        builder.AddCommandHandler<ICommandHandler<DecodeJwtQuery, Result<DecodeJwtQueryResult>>, DecodeJwtQueryHandler>();
    }
}
