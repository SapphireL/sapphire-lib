﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Extensions;

namespace SapphireLibApp.Tools.Infrastructure;

public static class WebApplicationBuilderExtensions
{
    public static void AddSappAppTools(this WebApplicationBuilder builder)
    {
        builder.AddSappLib<SappToolsModule>();
    }
}
