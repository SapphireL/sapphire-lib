﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Extensions;
using SapphireLib.Command.Providers;
using SapphireLib.Command.Test.CommandProviderTests.Prepared;

using Xunit;

namespace SapphireLib.Command.Test.CommandProviderTests
{
    public class CommandHandlerProviderTest
    {
        [Fact]
        public void ShouldResolvedCommandHandler()
        {
            var provider = AddCommandHandlers();
            var handler = provider.GetCommandHandler<TestCommand>();
            var handlerWithResult = provider.GetCommandHandler<TestResultCommand, bool>();
            Assert.NotNull(handler);
            Assert.NotNull(handlerWithResult);
            Assert.IsType<TestCommandHandler>(handler);
            Assert.IsType<TestResultCommandHandler>(handlerWithResult);
        }

        [Fact]
        public void UseCommandModuleShouldAddCommandHandler()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLibCommand(moduleBuilder =>
            {
                moduleBuilder.AddCommandHandlerProvider();
                moduleBuilder.AddCommandHandler<ICommandHandler<TestCommand>, TestCommandHandler>();
                moduleBuilder.AddCommandHandler<ICommandHandler<TestResultCommand, bool>, TestResultCommandHandler>();
            });

            var app = builder.Build();

            var provider = app.Services.GetRequiredService<ICommandHandlerProvider>();
            var testCommandHandler = provider.GetCommandHandler<TestCommand>();
            var testCommandWithResultHandler = provider.GetCommandHandler<TestResultCommand, bool>();
            Assert.NotNull(testCommandHandler);
            Assert.NotNull(testCommandWithResultHandler);
            Assert.IsType<TestCommandHandler>(testCommandHandler);
            Assert.IsType<TestResultCommandHandler>(testCommandWithResultHandler);
        }

        private ICommandHandlerProvider AddCommandHandlers()
        {
            var services = new ServiceCollection();
            services.AddScoped<ICommandHandler<TestCommand>, TestCommandHandler>();
            services.AddScoped<ICommandHandler<TestResultCommand, bool>, TestResultCommandHandler>();
            var commandHandlerProvider = new CommandHandlerProvider(services.BuildServiceProvider());
            return commandHandlerProvider;
        }
    }
}
