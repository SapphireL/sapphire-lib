﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Pipeline;
using SapphireLib.Command.Providers;
using SapphireLib.Command.Senders;
using SapphireLib.Command.Test.MiddlewareTests.Prepared;

using Xunit;

namespace SapphireLib.Command.Test.MiddlewareTests
{
    public class MiddlewareTest
    {
        [Fact]
        public async Task MiddlewareShouldExecuteTwiceWithInsideAndOutside()
        {
            var services = new ServiceCollection();
            services.AddSingleton<ICommandHandlerProvider, CommandHandlerProvider>();
            services.AddTransient<ICommandSender, CommandSender>();
            services.AddTransient<ICommandHandler<TestCommand, bool>, TestCommandHandler>();
            services.AddTransient<ICommandPipeline, CommandPipeline>();
            services.AddTransient<ICommandMiddleware, TestMiddleware1>();
            services.AddSingleton<ICommandPipelineMiddlewareProvider, CommandPipelineMiddlewareProvider>();

            var sp = services.BuildServiceProvider();

            var sender = sp.GetRequiredService<ICommandSender>();
            var command = new TestCommand();
            var result = await sender.SendAsync(command);

            Assert.True(result);
        }
    }
}
