﻿using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Test.MiddlewareTests.Prepared
{
    internal class TestCommand : ICommand<bool>
    {
        public int Value { get; set; } = 1;
    }
}
