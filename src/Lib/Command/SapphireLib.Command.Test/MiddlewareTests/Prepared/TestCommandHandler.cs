﻿using SapphireLib.Command.Handlers;

namespace SapphireLib.Command.Test.MiddlewareTests.Prepared
{
    internal class TestCommandHandler : CommandHandler<TestCommand, bool>
    {
        public override async Task<bool> HandleAsync(TestCommand command, CancellationToken cancellationToken = default)
        {
            await Task.CompletedTask;
            return false;
        }
    }
}
