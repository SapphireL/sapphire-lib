﻿using SapphireLib.Validator.Extensions;
using SapphireLib.Validator.Generic;

namespace SapphireLib.Command.Test.MiddlewareTests.Prepared
{
    internal class TestCommandValidator : BaseValidator<TestCommand>
    {
        public override void BuildCheckers()
        {
            CheckFor(x => x.Value).GreaterThan(2);
        }
    }
}
