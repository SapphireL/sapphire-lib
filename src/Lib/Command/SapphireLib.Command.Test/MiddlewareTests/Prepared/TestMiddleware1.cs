﻿using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Handlers;

namespace SapphireLib.Command.Test.MiddlewareTests.Prepared
{
    internal class TestMiddleware1 : ICommandMiddleware
    {
        public async Task InvokeAsync(ICommandPipelineContext context, PipelineDelegate next)
        {
            await next(context);

            context.Result = new CommandResult<bool>(true);
        }
    }
}
