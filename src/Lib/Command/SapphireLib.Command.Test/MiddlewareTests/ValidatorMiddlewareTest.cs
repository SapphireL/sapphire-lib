﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Pipeline;
using SapphireLib.Command.Pipeline.Middlewares;
using SapphireLib.Command.Providers;
using SapphireLib.Command.Senders;
using SapphireLib.Command.Test.MiddlewareTests.Prepared;
using SapphireLib.Logger.Extensions;
using SapphireLib.Module.Core;
using SapphireLib.UnifiedException;
using SapphireLib.Validator.Extensions;
using SapphireLib.Validator.Generic;

using Xunit;

namespace SapphireLib.Command.Test.MiddlewareTests
{
    public class ValidatorMiddlewareTest
    {
        [Fact]
        public async void ShouldThrowException()
        {
            var builder = WebApplication.CreateBuilder();
            var sappBuilder = SappServiceProvider.CreateBuilder();

            sappBuilder.AddFileLoggerFactory(builder.Configuration);
            sappBuilder.Build();
            builder.AddSappLibValidator();
            var services = builder.Services;
            services.AddSingleton<ICommandHandlerProvider, CommandHandlerProvider>();
            services.AddTransient<ICommandSender, CommandSender>();
            services.AddTransient<ICommandHandler<TestCommand, bool>, TestCommandHandler>();
            services.AddTransient<ICommandPipeline, CommandPipeline>();
            services.AddTransient<ICommandMiddleware, CommandValidatorMiddleware>();
            services.AddSingleton<ICommandPipelineMiddlewareProvider, CommandPipelineMiddlewareProvider>();

            services.AddScoped<IValidator<TestCommand>, TestCommandValidator>();
            var sp = services.BuildServiceProvider();

            var sender = sp.GetRequiredService<ICommandSender>();
            var command = new TestCommand();
            await Assert.ThrowsAsync<InvalidParameterException>(() => sender.SendAsync(command));
        }
    }
}
