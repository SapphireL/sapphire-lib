﻿using SapphireLib.Command.Handlers;

namespace SapphireLib.Command.Test.RoutedCommandHandlerTests.Prepared
{
    internal class TestCommandHandler1 : CommandHandler<TestCommand, bool>
    {
        public override async Task<bool> HandleAsync(TestCommand command, CancellationToken cancellationToken = default)
        {
            await Task.CompletedTask;
            return true;
        }
    }
}
