﻿using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Test.RoutedCommandHandlerTests.Prepared
{
    internal class TestCommandHandlerRouteProvider(TestContext route) : ICommandHandlerRouteProvider
    {
        public string GetRoute()
        {
            return route.Route;
        }
    }
}
