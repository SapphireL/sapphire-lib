﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Providers;
using SapphireLib.Command.Test.RoutedCommandHandlerTests.Prepared;

using Xunit;

namespace SapphireLib.Command.Test.RoutedCommandHandlerTests
{
    public class RoutedCommandHandlerTest
    {
        [Fact]
        public async void CommandShouldHandleByRoutedCommandHandler()
        {
            var services = new ServiceCollection();
            services.AddKeyedScoped<ICommandHandler<TestCommand, bool>, TestCommandHandler1>("1");
            services.AddKeyedScoped<ICommandHandler<TestCommand, bool>, TestCommandHandler2>("2");
            var commandHandlerProvider = new CommandHandlerProvider(services.BuildServiceProvider());

            var handler1 = commandHandlerProvider.GetCommandHandler<TestCommand, bool>("1");
            var handler2 = commandHandlerProvider.GetCommandHandler<TestCommand, bool>("2");
            Assert.NotNull(handler1);
            Assert.NotNull(handler2);
            Assert.IsType<TestCommandHandler1>(handler1);
            Assert.IsType<TestCommandHandler2>(handler2);
            var result1 = await handler1.HandleAsync(new TestCommand());
            var result2 = await handler2.HandleAsync(new TestCommand());
            Assert.True(((ICommandResult<bool>)result1).Result);
            Assert.True(((ICommandResult<bool>)result2).Result);
        }

        [Fact]
        public async void CommandShouldHandleByRoutedCommandHandlerWithRouteProvider()
        {
            var services = new ServiceCollection();
            services.AddKeyedScoped<ICommandHandler<TestCommand, bool>, TestCommandHandler1>("1");
            services.AddKeyedScoped<ICommandHandler<TestCommand, bool>, TestCommandHandler2>("2");
            var context = new TestContext();
            var commandHandlerProvider = new CommandHandlerProvider(new TestCommandHandlerRouteProvider(context), services.BuildServiceProvider());
            var handler1 = commandHandlerProvider.GetCommandHandler<TestCommand, bool>();
            context.Route = "2";
            var handler2 = commandHandlerProvider.GetCommandHandler<TestCommand, bool>();
            Assert.NotNull(handler1);
            Assert.NotNull(handler2);
            Assert.IsType<TestCommandHandler1>(handler1);
            Assert.IsType<TestCommandHandler2>(handler2);
            var result1 = await handler1.HandleAsync(new TestCommand());
            var result2 = await handler2.HandleAsync(new TestCommand());
            Assert.True(((ICommandResult<bool>)result1).Result);
            Assert.True(((ICommandResult<bool>)result2).Result);
        }
    }
}
