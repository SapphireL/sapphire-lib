﻿using SapphireLib.Command.Handlers;

namespace SapphireLib.Command.Test.SenderTests.Prepared
{
    internal class TestCommandHandler : CommandHandler<TestCommand>
    {
        public override async Task HandleAsync(TestCommand command, CancellationToken cancellationToken = default)
        {
            Handled = true;
            await Task.CompletedTask;
        }

        public static bool Handled { get; set; } = false;
    }
}
