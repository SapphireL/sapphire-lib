﻿using SapphireLib.Command.Handlers;

namespace SapphireLib.Command.Test.SenderTests.Prepared
{
    internal class TestResultCommandHandler : CommandHandler<TestResultCommand, bool>
    {
        public override async Task<bool> HandleAsync(TestResultCommand command, CancellationToken cancellationToken = default)
        {
            await Task.CompletedTask;
            return true;
        }
    }
}
