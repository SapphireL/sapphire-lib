﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Providers;
using SapphireLib.Command.Senders;
using SapphireLib.Command.Test.SenderTests.Prepared;

using Xunit;

namespace SapphireLib.Command.Test.SenderTests
{
    public class SenderTests
    {
        [Fact]
        public async void ShouldSendCommand()
        {
            var provider = AddCommandHandlers();
            var sender = new CommandSender(provider);
            var command = new TestCommand();
            await sender.SendAsync(command);
            var commandWithResult = new TestResultCommand();
            var result = await sender.SendAsync(commandWithResult);

            Assert.True(TestCommandHandler.Handled);
            Assert.True(result);
        }

        private ICommandHandlerProvider AddCommandHandlers()
        {
            var services = new ServiceCollection();
            services.AddScoped<ICommandHandler<TestCommand>, TestCommandHandler>();
            services.AddScoped<ICommandHandler<TestResultCommand, bool>, TestResultCommandHandler>();
            var commandHandlerProvider = new CommandHandlerProvider(services.BuildServiceProvider());
            return commandHandlerProvider;
        }
    }
}
