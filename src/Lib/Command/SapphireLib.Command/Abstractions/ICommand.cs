﻿namespace SapphireLib.Command.Abstractions
{
    public interface ICommand : IBaseCommand
    {
    }

    public interface ICommand<in TResult> : IBaseCommand
    {
    }
}
