﻿namespace SapphireLib.Command.Abstractions
{
    public interface ICommandHandler
    {
        Task<ICommandResult> HandleAsync(IBaseCommand command, CancellationToken cancellationToken = default);
    }

    public interface ICommandHandler<in TCommand> : ICommandHandler
        where TCommand : ICommand
    {
        Task HandleAsync(TCommand command, CancellationToken cancellationToken = default);
    }

    public interface ICommandHandler<in TCommand, TResult> : ICommandHandler
        where TCommand : ICommand<TResult>
    {
        Task<TResult> HandleAsync(TCommand command, CancellationToken cancellationToken = default);
    }
}
