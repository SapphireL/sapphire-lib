﻿namespace SapphireLib.Command.Abstractions;

/// <summary>
/// 命令处理程序提供者接口，负责提供与特定命令类型相关的命令处理程序。
/// </summary>
/// <remarks>
/// 此接口定义了一组方法，用于获取命令处理程序以处理不同类型的命令。
/// </remarks>
public interface ICommandHandlerProvider
{
    /// <summary>
    /// 获取与指定命令类型关联的命令处理程序。
    /// </summary>
    /// <typeparam name="TCommand">要获取处理程序的命令类型。</typeparam>
    /// <returns>返回对应的命令处理程序。</returns>
    public ICommandHandler GetCommandHandler<TCommand>()
        where TCommand : ICommand;

    /// <summary>
    /// 获取与指定命令类型及其结果类型关联的命令处理程序。
    /// </summary>
    /// <typeparam name="TCommand">要获取处理程序的命令类型。</typeparam>
    /// <typeparam name="TResult">命令执行后返回的结果类型。</typeparam>
    /// <returns>返回对应的命令处理程序。</returns>
    public ICommandHandler GetCommandHandler<TCommand, TResult>()
        where TCommand : ICommand<TResult>;

    /// <summary>
    /// 根据命令类型和结果类型获取命令处理程序。
    /// </summary>
    /// <param name="commandType">要获取处理程序的命令类型。</param>
    /// <param name="resultType">命令执行后返回的结果类型。</param>
    /// <returns>返回对应的命令处理程序。</returns>
    public ICommandHandler GetCommandHandler(Type commandType, Type resultType);

    /// <summary>
    /// 根据命令类型、结果类型和路由获取命令处理程序。
    /// </summary>
    /// <param name="commandType">要获取处理程序的命令类型。</param>
    /// <param name="resultType">命令执行后返回的结果类型。</param>
    /// <param name="route">命令的路由。</param>
    /// <returns>返回对应的命令处理程序。</returns>
    public ICommandHandler GetCommandHandler(Type commandType, Type resultType, string route);

    /// <summary>
    /// 根据命令类型和路由获取命令处理程序。
    /// </summary>
    /// <typeparam name="TCommand">要获取处理程序的命令类型。</typeparam>
    /// <param name="route">命令的路由。</param>
    /// <returns>返回对应的命令处理程序。</returns>
    public ICommandHandler GetCommandHandler<TCommand>(string route)
       where TCommand : ICommand;

    /// <summary>
    /// 根据命令类型、结果类型和路由获取命令处理程序。
    /// </summary>
    /// <typeparam name="TCommand">要获取处理程序的命令类型。</typeparam>
    /// <typeparam name="TResult">命令执行后返回的结果类型。</typeparam>
    /// <param name="route">命令的路由。</param>
    /// <returns>返回对应的命令处理程序。</returns>
    public ICommandHandler GetCommandHandler<TCommand, TResult>(string route)
        where TCommand : ICommand<TResult>;
}