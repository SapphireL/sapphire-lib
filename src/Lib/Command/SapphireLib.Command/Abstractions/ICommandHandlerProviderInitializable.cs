﻿namespace SapphireLib.Command.Abstractions
{
    public interface ICommandHandlerProviderInitializable
    {
        public void Initialize();
    }
}
