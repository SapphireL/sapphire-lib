﻿namespace SapphireLib.Command.Abstractions
{
    /// <summary>
    /// 命令处理器路由提供者.
    /// 请将该服务注册为scoped以保证整个请求的生命周期中的唯一性.
    /// </summary>
    public interface ICommandHandlerRouteProvider
    {
        /// <summary>
        /// 获取路由.
        /// </summary>
        /// <returns>路由.</returns>
        public string GetRoute();
    }
}
