﻿namespace SapphireLib.Command.Abstractions;

public interface ICommandMiddleware<TCommand>
    where TCommand : ICommand
{
    public Task InvokeAsync(ICommandPipelineContext<TCommand> context, PipelineDelegate<TCommand> next);
}

/// <summary>
/// 命令中间件接口.
/// 该接口为单例，在单例模式下，如果需要获取scope服务，请注入<see cref="IServiceScopeFactory"/>.
/// </summary>
public interface ICommandMiddleware
{
    public Task InvokeAsync(ICommandPipelineContext context, PipelineDelegate next);
}
