﻿using SapphireLib.Command.Pipeline;

namespace SapphireLib.Command.Abstractions
{
    public interface ICommandPipeline<TCommand>
        where TCommand : ICommand
    {
        public Task RunAsync(CommandPipelineContext<TCommand> context);
    }

    public interface ICommandPipeline
    {
        public Task RunAsync(ICommandPipelineContext context);
    }

    public delegate Task PipelineDelegate<TCommand>(ICommandPipelineContext<TCommand> context)
        where TCommand : ICommand;

    public delegate Task PipelineDelegate(ICommandPipelineContext context);
}
