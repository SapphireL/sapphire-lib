﻿namespace SapphireLib.Command.Abstractions
{
    public interface ICommandPipelineContext
    {
        ICommandHandler CommandHandler { get; }

        IBaseCommand Command { get; }

        ICommandResult Result { get; set; }
    }

    public interface ICommandPipelineContext<out TCommand>
        where TCommand : ICommand
    {
        TCommand Command { get; }
    }
}
