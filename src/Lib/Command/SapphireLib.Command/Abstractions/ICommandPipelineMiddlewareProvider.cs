﻿namespace SapphireLib.Command.Abstractions
{
    public interface ICommandPipelineMiddlewareProvider<TCommand>
        where TCommand : ICommand
    {
        public void AddMiddleware(ICommandMiddleware<TCommand> commandMiddleware);
    }

    public interface ICommandPipelineMiddlewareProvider
    {
        public List<ICommandMiddleware> Middlewares { get; }

        public void AddMiddleware(ICommandMiddleware commandMiddleware);
    }
}
