﻿namespace SapphireLib.Command.Abstractions
{
    public interface ICommandResult
    {
    }

    public interface ICommandResult<T> : ICommandResult
    {
        public T Result { get; set; }
    }
}
