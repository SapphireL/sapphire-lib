﻿namespace SapphireLib.Command.Abstractions;

/// <summary>
/// 命令发送者接口，负责发送命令到处理程序。
/// </summary>
/// <remarks>
/// 此接口定义了异步发送命令的方法，包括发送命令和返回结果的命令。
/// </remarks>
public interface ICommandSender
{
    /// <summary>
    /// 异步发送命令。
    /// </summary>
    /// <typeparam name="TCommand">要发送的命令类型。</typeparam>
    /// <param name="command">要发送的命令。</param>
    /// <param name="cancellationToken">可选的取消令牌。</param>
    /// <returns>一个表示异步操作的任务。</returns>
    Task SendAsync<TCommand>(TCommand command, CancellationToken cancellationToken = default)
        where TCommand : ICommand;

    /// <summary>
    /// 异步发送返回结果的命令。
    /// </summary>
    /// <typeparam name="TResult">返回结果的类型。</typeparam>
    /// <param name="command">要发送的命令。</param>
    /// <param name="cancellationToken">可选的取消令牌。</param>
    /// <returns>一个表示异步操作的任务，返回结果类型。</returns>
    Task<TResult> SendAsync<TResult>(ICommand<TResult> command, CancellationToken cancellationToken = default);

    /// <summary>
    /// 异步发送命令到指定的路由。
    /// </summary>
    /// <typeparam name="TCommand">要发送的命令类型。</typeparam>
    /// <param name="command">要发送的命令。</param>
    /// <param name="route">要发送的路由。</param>
    /// <param name="cancellationToken">可选的取消令牌。</param>
    /// <returns>一个表示异步操作的任务。</returns>
    Task SendAsync<TCommand>(TCommand command, string route, CancellationToken cancellationToken = default)
         where TCommand : ICommand;

    /// <summary>
    /// 异步发送返回结果的命令到指定的路由。
    /// </summary>
    /// <typeparam name="TResult">返回结果的类型。</typeparam>
    /// <param name="command">要发送的命令。</param>
    /// <param name="route">要发送的路由。</param>
    /// <param name="cancellationToken">可选的取消令牌。</param>
    /// <returns>一个表示异步操作的任务，返回结果类型。</returns>
    Task<TResult> SendAsync<TResult>(ICommand<TResult> command, string route, CancellationToken cancellationToken = default);
}