﻿using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Providers;
using SapphireLib.Command.Senders;
using SapphireLib.Module.Core;

namespace SapphireLib.Command.Definitions;

/// <summary>
/// 命令模块，用于配置应用程序中的命令发送和处理服务。
/// </summary>
/// <remarks>
/// <para>此模块继承自 <see cref="BaseModule"/>，并重写 <see cref="ConfigureServices"/> 方法以注册命令相关的服务：</para>
/// <list type="bullet">
///   <item>
///     <description><see cref="ICommandSender"/>：用于发送命令的接口，允许模块中的命令被执行。</description>
///   </item>
///   <item>
///     <description><see cref="ICommandHandlerProvider"/>：命令处理器提供程序接口，用于获取处理特定命令的处理器。</description>
///   </item>
/// </list>
/// <para>这些服务被注册为 <c>Scoped</c>，确保在每个请求上下文中创建单一实例，适用于命令执行期间的依赖管理。</para>
/// </remarks>
public class CommandModule : BaseModule
{
    /// <summary>
    /// 配置服务的方法，在此方法中为命令发送和处理器提供程序注册依赖项。
    /// </summary>
    /// <param name="context">提供应用程序上下文的 <see cref="BuilderContext"/>。</param>
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.TryAddScoped<ICommandSender, CommandSender>();
        context.Services.TryAddScoped<ICommandHandlerProvider, CommandHandlerProvider>();
    }
}