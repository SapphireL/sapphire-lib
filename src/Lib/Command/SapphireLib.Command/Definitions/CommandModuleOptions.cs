﻿using System.Collections.Concurrent;

using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Definitions
{
    public class CommandModuleOptions
    {
        public ConcurrentDictionary<Type, List<ICommandHandlerDecorator>> CommandHandlerDecorators { get; set; } = new ConcurrentDictionary<Type, List<ICommandHandlerDecorator>>();

        public ICommandHandlerProvider CommandHandlerProvider { get; set; } = default!;
    }
}
