﻿using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Pipeline;
using SapphireLib.Command.Pipeline.Middlewares;
using SapphireLib.Module.Core;

namespace SapphireLib.Command.Definitions;

/// <summary>
/// 命令管道特性，用于配置应用程序中的命令处理管道及其中间件.
/// </summary>
/// <remarks>
/// <para>此特性继承自 <see cref="BaseFeature"/>，并重写 <see cref="ConfigureServices"/> 方法以注册命令管道相关的服务：</para>
/// <list type="bullet">
///   <item>
///     <description><see cref="ICommandPipeline"/>：表示命令处理管道的接口，负责执行命令和中间件.</description>
///   </item>
///   <item>
///     <description><see cref="ICommandMiddleware"/>：表示命令中间件的接口，用于在命令执行过程中处理请求.</description>
///   </item>
///   <item>
///     <description><see cref="ICommandPipelineMiddlewareProvider"/>：命令管道中间件提供程序接口，用于获取特定中间件的实例.</description>
///   </item>
/// </list>
/// <para>这些服务被注册为 <c>Singleton</c>，确保在整个应用程序生命周期中只创建一个实例.</para>
/// </remarks>
public class CommandPipelineFeature : BaseFeature
{
    /// <summary>
    /// 配置服务的方法，在此方法中为命令管道及其中间件注册依赖项.
    /// </summary>
    /// <param name="context">提供应用程序上下文的 <see cref="BuilderContext"/>.</param>
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.TryAddSingleton<ICommandPipeline, CommandPipeline>();
        context.Services.TryAddSingleton<ICommandMiddleware, CommandValidatorMiddleware>();
        context.Services.TryAddSingleton<ICommandPipelineMiddlewareProvider, CommandPipelineMiddlewareProvider>();
    }
}