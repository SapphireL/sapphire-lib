﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Definitions;
using SapphireLib.Command.Pipeline;
using SapphireLib.Module.Core;

namespace SapphireLib.Command.Extensions
{
    public static class FeatureBuilderExtensions
    {
        public static void AddPipeline<TCommand, TMiddleware>(this FeatureBuilder<CommandPipelineFeature> builder, Action<ICommandPipelineMiddlewareProvider<TCommand>> middleware)
            where TMiddleware : ICommandMiddleware<TCommand>
            where TCommand : ICommand
        {
            builder.Context.Services.AddSingleton<ICommandPipelineMiddlewareProvider<TCommand>>(serviceProvider =>
            {
                var provider = new CommandPipelineMiddlewareProvider<TCommand>(serviceProvider);
                middleware(provider);
                return provider;
            });

            builder.Context.Services.AddScoped<ICommandPipeline<TCommand>, CommandPipeline<TCommand>>();
        }

        public static void AddCommandHandler<TInterface, TCommandHandler>(this FeaturesBuilder featuresBuilder)
            where TInterface : class, ICommandHandler
            where TCommandHandler : class, TInterface
        {
            featuresBuilder.Context.Services.AddScoped<TInterface, TCommandHandler>();
        }
    }
}
