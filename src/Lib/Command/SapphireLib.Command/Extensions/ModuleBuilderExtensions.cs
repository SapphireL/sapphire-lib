﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Definitions;
using SapphireLib.Command.Providers;
using SapphireLib.Module.Core;

namespace SapphireLib.Command.Extensions;

public static class ModuleBuilderExtensions
{
    public static void AddCommandHandlerProvider<TCommandHandlerProvider>(this ModuleBuilder<CommandModule> moduleBuilder)
        where TCommandHandlerProvider : class, ICommandHandlerProvider
    {
        moduleBuilder.Context.Services.AddSingleton<ICommandHandlerProvider, TCommandHandlerProvider>();
    }

    public static void AddCommandHandlerProvider(this ModuleBuilder<CommandModule> moduleBuilder)
    {
        moduleBuilder.AddCommandHandlerProvider<CommandHandlerProvider>();
    }

    public static void AddCommandHandler<TInterface, TCommandHandler>(this ModuleBuilder<CommandModule> moduleBuilder)
        where TInterface : class, ICommandHandler
        where TCommandHandler : class, TInterface
    {
        moduleBuilder.Context.Services.AddScoped<TInterface, TCommandHandler>();
    }

    /// <summary>
    /// 扩展方法，用于在命令模块中启用命令处理管道.
    /// </summary>
    /// <param name="moduleBuilder">当前的 <see cref="ModuleBuilder{TModule}"/> 实例，用于构建命令模块.</param>
    /// <remarks>
    /// 该方法会将 <see cref="CommandPipelineFeature"/> 特性添加到模块构建中，
    /// 从而启用命令处理管道功能，为命令的执行提供支持.
    /// </remarks>
    public static void EnablePipeline(this ModuleBuilder<CommandModule> moduleBuilder)
    {
        moduleBuilder.WithFeature<CommandPipelineFeature>();
    }
}
