﻿using SapphireLib.Command.Definitions;
using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;

namespace SapphireLib.Command.Extensions;

using Microsoft.AspNetCore.Builder;

/// <summary>
/// 扩展方法，用于向 <see cref="WebApplicationBuilder"/> 添加 SapphireLib 命令模块.
/// </summary>
public static class WebApplicationBuilderExtensions
{
    /// <summary>
    /// 向 <see cref="WebApplicationBuilder"/> 添加命令模块，允许模块化命令的处理和发送.
    /// </summary>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例.</param>
    /// <param name="moduleBuilder">可选的配置委托，用于自定义命令模块的构建.</param>
    public static void AddSappLibCommand(this WebApplicationBuilder builder, Action<ModuleBuilder<CommandModule>>? moduleBuilder = null)
    {
        builder.AddSappLib(moduleBuilder);
    }
}
