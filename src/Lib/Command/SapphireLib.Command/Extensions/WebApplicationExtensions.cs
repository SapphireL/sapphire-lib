﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Definitions;
using SapphireLib.Module.Extensions;

namespace SapphireLib.Command.Extensions
{
    public static class WebApplicationExtensions
    {
        public static void UseCommandModule(this WebApplication app, Action<ICommandHandlerProvider> commandHandler)
        {
            var provider = app.Services.GetRequiredService<ICommandHandlerProvider>();
            commandHandler(provider);
            app.UseSappLib<CommandModule>();
        }
    }
}
