﻿using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Handlers
{
    public abstract class CommandHandler<TCommand> : ICommandHandler<TCommand>
         where TCommand : ICommand
    {
        public abstract Task HandleAsync(TCommand command, CancellationToken cancellationToken = default);

        public async Task<ICommandResult> HandleAsync(IBaseCommand command, CancellationToken cancellationToken = default)
        {
            await HandleAsync((TCommand)command, cancellationToken);
            return new CommandResult();
        }
    }

    public abstract class CommandHandler<TCommand, TResult> : ICommandHandler<TCommand, TResult>
        where TCommand : ICommand<TResult>
    {
        public async Task<ICommandResult> HandleAsync(IBaseCommand command, CancellationToken cancellationToken = default)
        {
            var result = await HandleAsync((TCommand)command, cancellationToken);
            return new CommandResult<TResult>(result);
        }

        public abstract Task<TResult> HandleAsync(TCommand command, CancellationToken cancellationToken = default);
    }
}
