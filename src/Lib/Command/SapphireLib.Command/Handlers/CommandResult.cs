﻿using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Handlers
{
    internal class CommandResult : ICommandResult
    {
    }

    internal class CommandResult<T> : ICommandResult<T>
    {
        public CommandResult(T result)
        {
            Result = result;
        }

        public T Result { get; set; }
    }
}
