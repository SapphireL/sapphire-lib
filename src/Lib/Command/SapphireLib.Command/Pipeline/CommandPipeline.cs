﻿using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Pipeline
{
    internal class CommandPipeline<TCommand> : ICommandPipeline<TCommand>
        where TCommand : ICommand
    {
        private readonly ICommandPipelineMiddlewareProvider<TCommand> _provider;
        private readonly ICommandHandlerProvider _handlerProvider;
        private readonly List<Func<PipelineDelegate<TCommand>, PipelineDelegate<TCommand>>> _components = new();
        private PipelineDelegate<TCommand> _pipeline = null!;

        public CommandPipeline(ICommandHandlerProvider handlerProvider, ICommandPipelineMiddlewareProvider<TCommand> provider)
        {
            _handlerProvider = handlerProvider;
            _provider = provider;
            Build();
        }

        public void Build()
        {
            PipelineDelegate<TCommand> pipeline = async context =>
            {
                var handler = _handlerProvider.GetCommandHandler<TCommand>();
                await handler.HandleAsync(context.Command);
            };

            // foreach (var middleware in _provider.GetMiddlewares<TCommand>())
            // {
            //    UseMiddleware(middleware);
            // }
            for (var c = _components.Count - 1; c >= 0; c--)
            {
                pipeline = _components[c](pipeline);
            }

            _pipeline = pipeline;
        }

        public async Task RunAsync(CommandPipelineContext<TCommand> context)
        {
            await _pipeline.Invoke(context);
        }

        private void UseMiddleware(ICommandMiddleware<TCommand> middleware)
        {
            Use(next => context => middleware.InvokeAsync(context, next));
        }

        private void Use(Func<PipelineDelegate<TCommand>, PipelineDelegate<TCommand>> middleware)
        {
            _components.Add(middleware);
        }
    }

    internal class CommandPipeline : ICommandPipeline
    {
        // private readonly ICommandHandlerProvider _handlerProvider;
        private readonly ICommandPipelineMiddlewareProvider _provider;
        private readonly List<Func<PipelineDelegate, PipelineDelegate>> _components = new();
        private PipelineDelegate _pipeline = null!;

        public CommandPipeline(/*ICommandHandlerProvider handlerProvider, */ICommandPipelineMiddlewareProvider provider)
        {
            // _handlerProvider = handlerProvider;
            _provider = provider;
            Build();
        }

        public void Build()
        {
            PipelineDelegate pipeline = async context =>
            {
                var result = await context.CommandHandler.HandleAsync(context.Command);
                context.Result = result;
            };

            foreach (var middleware in _provider.Middlewares)
            {
                UseMiddleware(middleware);
            }

            for (var c = _components.Count - 1; c >= 0; c--)
            {
                pipeline = _components[c](pipeline);
            }

            _pipeline = pipeline;
        }

        public async Task RunAsync(ICommandPipelineContext context)
        {
            await _pipeline.Invoke(context);
        }

        private void Use(Func<PipelineDelegate, PipelineDelegate> middleware)
        {
            _components.Add(middleware);
        }

        private void UseMiddleware(ICommandMiddleware middleware)
        {
            Use(next => context => middleware.InvokeAsync(context, next));
        }
    }
}
