﻿using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Pipeline
{
    public class CommandPipelineContext(IBaseCommand command, ICommandHandler commandHandler) : ICommandPipelineContext
    {
        public ICommandHandler CommandHandler => commandHandler;

        public ICommandResult Result { get; set; } = default!;

        public IBaseCommand Command => command;
    }

    public class CommandPipelineContext<TCommand>(TCommand command) : ICommandPipelineContext<TCommand>
        where TCommand : ICommand
    {
        public TCommand Command { get; set; } = command;
    }
}