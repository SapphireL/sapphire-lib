﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Pipeline;

internal class CommandPipelineMiddlewareProvider<TCommand>(IServiceProvider serviceProvider) : ICommandPipelineMiddlewareProvider<TCommand>
      where TCommand : ICommand
{
    private readonly List<ICommandMiddleware<TCommand>> _middlewares = serviceProvider.GetServices<ICommandMiddleware<TCommand>>().ToList();

    public List<ICommandMiddleware<TCommand>> Middlewares => _middlewares;

    public void AddMiddleware(ICommandMiddleware<TCommand> commandMiddleware)
    {
        Middlewares.Add(commandMiddleware);
    }
}

internal class CommandPipelineMiddlewareProvider : ICommandPipelineMiddlewareProvider
{
    public CommandPipelineMiddlewareProvider(IEnumerable<ICommandMiddleware> middlewares)
    {
        _middlewares = middlewares;
    }

    private readonly IEnumerable<ICommandMiddleware> _middlewares;

    public List<ICommandMiddleware> Middlewares => _middlewares.ToList();

    public void AddMiddleware(ICommandMiddleware commandMiddleware)
    {
        Middlewares.Add(commandMiddleware);
    }
}
