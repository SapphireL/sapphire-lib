﻿using SapphireLib.Command.Abstractions;
using SapphireLib.UnifiedException;
using SapphireLib.Validator.Generic;

namespace SapphireLib.Command.Pipeline.Middlewares
{
    /// <summary>
    /// <see cref="ICommandMiddleware"/>.
    /// </summary>
    /// <param name="validatorProvider"><see cref="IValidatorProvider"/>.</param>
    internal class CommandValidatorMiddleware(IValidatorProvider validatorProvider) : ICommandMiddleware
    {
        public async Task InvokeAsync(ICommandPipelineContext context, PipelineDelegate next)
        {
            var validator = validatorProvider.GetValidator(context.Command);
            if (validator != null)
            {
                var result = validator.ValidateDetails(context.Command);
                var exceptions = result
                    .Where(c => c.HasFalse)
                    .Select(results =>
                        new ParameterExceptionDetail
                        {
                            ParameterName = results.CheckeeProperty,
                            Errors = results.CheckResultDetails.Select(r => new UnifiedExceptionErrorDetail
                            {
                                ErrorCode = r.FailureCode,
                                Message = r.FailureMessage
                            })
                        });

                if (exceptions.Any())
                {
                    throw new InvalidParameterException(exceptions);
                }
            }

            await next(context);
        }
    }
}
