﻿using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Pipeline.Middlewares
{
    internal class ExceptionMiddleware : ICommandMiddleware
    {
        public async Task InvokeAsync(ICommandPipelineContext context, PipelineDelegate next)
        {
            await Task.CompletedTask;
        }
    }
}
