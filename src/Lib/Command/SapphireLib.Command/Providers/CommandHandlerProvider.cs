﻿using System.Collections.Concurrent;

using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Command.Abstractions;

namespace SapphireLib.Command.Providers
{
    public sealed class CommandHandlerProvider : ICommandHandlerProvider
    {
        private readonly IServiceProvider _serviceProvider;

        private static readonly ConcurrentDictionary<string, ConcurrentDictionary<string, Type>> _handlersDictionary = new();
        private readonly ICommandHandlerRouteProvider? _routeProvider;

        public CommandHandlerProvider(ICommandHandlerRouteProvider routeProvider, IServiceProvider serviceProvider)
            : this(serviceProvider) => _routeProvider = routeProvider;

        public CommandHandlerProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ICommandHandler GetCommandHandler<TCommand>()
            where TCommand : ICommand
        {
            return GetOrAddHandlerFromDic(typeof(TCommand), null, _routeProvider?.GetRoute());
        }

        public ICommandHandler GetCommandHandler<TCommand, TResult>()
            where TCommand : ICommand<TResult>
        {
            return GetOrAddHandlerFromDic(typeof(TCommand), typeof(TResult), _routeProvider?.GetRoute());
        }

        public ICommandHandler GetCommandHandler<TCommand>(string route)
            where TCommand : ICommand
        {
            return GetOrAddHandlerFromDic(typeof(TCommand), null, route);
        }

        public ICommandHandler GetCommandHandler<TCommand, TResult>(string route)
            where TCommand : ICommand<TResult>
        {
            return GetOrAddHandlerFromDic(typeof(TCommand), typeof(TResult), route);
        }

        public ICommandHandler GetCommandHandler(Type commandType, Type resultType)
        {
            return GetOrAddHandlerFromDic(commandType, resultType, _routeProvider?.GetRoute());
        }

        public ICommandHandler GetCommandHandler(Type commandType, Type resultType, string route)
        {
            return GetOrAddHandlerFromDic(commandType, resultType, route);
        }

        private ICommandHandler GetOrAddHandlerFromDic(Type commandType, Type? resultType, string? route)
        {
            route = route ?? "default";

            var routedDic = _handlersDictionary.GetOrAdd(
               route,
               _ =>
            {
                return new ConcurrentDictionary<string, Type>();
            });
            var handlerType = routedDic.GetOrAdd(
            commandType.Name,
            _ =>
            {
                var ht = resultType == null ? typeof(ICommandHandler<>).MakeGenericType(commandType) : typeof(ICommandHandler<,>).MakeGenericType(commandType, resultType);
                return ht!;
            });

            var handler = route == "default" ? _serviceProvider.GetRequiredService(handlerType) : _serviceProvider.GetRequiredKeyedService(handlerType, route);

            return (handler as ICommandHandler)!;
        }
    }
}
