﻿using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Pipeline;

namespace SapphireLib.Command.Senders
{
    internal class CommandSender(ICommandHandlerProvider handlerProvider) : ICommandSender
    {
        private readonly ICommandPipeline? _commandPipeline;

        public CommandSender(ICommandHandlerProvider handlerProvider, ICommandPipeline commandPipeline)
            : this(handlerProvider)
        {
            _commandPipeline = commandPipeline;
        }

        public async Task<TResult> SendAsync<TResult>(ICommand<TResult> command, CancellationToken cancellationToken = default)
        {
            var handler = handlerProvider.GetCommandHandler(command.GetType(), typeof(TResult));
            if (_commandPipeline != null)
            {
                var context = new CommandPipelineContext(command, handler);
                await _commandPipeline.RunAsync(context);
                var result = context.Result;
                return ((ICommandResult<TResult>)result).Result;
            }
            else
            {
                var result = await handler.HandleAsync(command, cancellationToken);
                return ((ICommandResult<TResult>)result).Result;
            }
        }

        public async Task SendAsync<TCommand>(TCommand command, CancellationToken cancellationToken = default)
            where TCommand : ICommand
        {
            var handler = handlerProvider.GetCommandHandler<TCommand>();
            if (_commandPipeline != null)
            {
                var context = new CommandPipelineContext(command, handler);
                await _commandPipeline.RunAsync(context);
            }
            else
            {
                await handler.HandleAsync(command, cancellationToken);
            }
        }

        public async Task SendAsync<TCommand>(TCommand command, string route, CancellationToken cancellationToken = default)
            where TCommand : ICommand
        {
            await handlerProvider.GetCommandHandler<TCommand>(route).HandleAsync(command, cancellationToken);
        }

        public async Task<TResult> SendAsync<TResult>(ICommand<TResult> command, string route, CancellationToken cancellationToken = default)
        {
            var handler = handlerProvider.GetCommandHandler(command.GetType(), typeof(TResult), route);
            var result = await handler.HandleAsync(command, cancellationToken);
            return ((ICommandResult<TResult>)result).Result;
        }
    }
}
