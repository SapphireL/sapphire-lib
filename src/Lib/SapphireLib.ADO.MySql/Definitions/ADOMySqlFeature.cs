﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.ADO.Abstractions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.MySql.Definitions
{
    internal class ADOMySqlFeature<TClient> : BaseFeature
          where TClient : MySqlConnectionClient
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            var services = context.Services;
            services.AddTransient<ISqlConnectionClient, MySqlConnectionClient>();
            services.AddTransient<TClient>();
        }
    }
}
