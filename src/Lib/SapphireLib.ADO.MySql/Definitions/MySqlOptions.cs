﻿using SapphireLib.ADO.Definitions;

namespace SapphireLib.ADO.MySql.Definitions
{
    public class MySqlOptions : ADOOptions
    {
        public string? Schema { get; set; }
    }
}
