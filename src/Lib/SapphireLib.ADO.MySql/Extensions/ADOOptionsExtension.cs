﻿using SapphireLib.ADO.Definitions;
using SapphireLib.ADO.MySql.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.MySql.Extensions
{
    public static class ADOOptionsExtension
    {
        public static void UseMySql(this ModuleBuilder<ADOModule> builder, Action<MySqlOptions> configure)
        {
            builder.WithFeature<ADOMySqlFeature<MySqlConnectionClient>, MySqlOptions>(configure);
        }

        public static void UseMySql<TClient>(this FeatureBuilder<ADOFeature> builder, Action<MySqlOptions> configure)
           where TClient : MySqlConnectionClient
        {
            builder.WithFeature<ADOMySqlFeature<TClient>, MySqlOptions>(configure);
        }
    }
}
