﻿using System.Data;
using System.Data.Common;

using Microsoft.Extensions.Options;

using MySqlConnector;

using SapphireLib.ADO.Abstractions;
using SapphireLib.ADO.MySql.Definitions;

namespace SapphireLib.ADO.MySql
{
    public class MySqlConnectionClient(IOptions<MySqlOptions> options) : ISqlConnectionClient
    {
        private readonly MySqlOptions _options = options.Value;
        private DbConnection _connection = null!;

        public string DatabaseName => _options.DatabaseName;

        public string? Schema => _options.Schema;

        public string ConnectionString => _options.ConnectionString;

        public void Dispose()
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
            {
                _connection.Dispose();
            }
        }

        public DbConnection CreateNewConnection()
        {
            var connection = new MySqlConnection(_options.ConnectionString);
            connection.Open();
            return connection;
        }

        public string GetConnectionString()
        {
            return _options.ConnectionString;
        }

        public DbConnection GetOpenConnection()
        {
            if (_connection == null || _connection.State != ConnectionState.Open)
            {
                _connection = new MySqlConnection(_options.ConnectionString);
                _connection.Open();
            }

            return _connection;
        }

        public DbTransaction BeginTransaction()
        {
            return _connection.BeginTransaction();
        }

        public async Task<DbTransaction> BeginTransactionAsync()
        {
            return await _connection.BeginTransactionAsync();
        }
    }
}
