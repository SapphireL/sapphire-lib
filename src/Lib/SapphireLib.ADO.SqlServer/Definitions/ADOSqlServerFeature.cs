﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.ADO.Abstractions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.SqlServer.Definitions
{
    public class ADOSqlServerFeature<TClient> : BaseFeature
        where TClient : SqlServerConnectionClient
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<ISqlConnectionClient, SqlServerConnectionClient>();
            context.Services.AddTransient<TClient>();
        }
    }
}
