﻿using SapphireLib.ADO.Definitions;

namespace SapphireLib.ADO.SqlServer.Definitions
{
    public class SqlServerOptions : ADOOptions
    {
        public string Schema { get; set; } = "dbo";
    }

    public class SqlServerOptions<TClient> : SqlServerOptions
        where TClient : SqlServerConnectionClient
    {
    }
}
