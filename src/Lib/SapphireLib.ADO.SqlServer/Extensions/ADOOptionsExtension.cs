﻿using SapphireLib.ADO.Definitions;
using SapphireLib.ADO.SqlServer.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.SqlServer.Extensions
{
    public static class ADOOptionsExtension
    {
        public static void UseSqlServer<TClient>(this FeatureBuilder<ADOFeature> builder, Action<SqlServerOptions> configure)
            where TClient : SqlServerConnectionClient
        {
            builder.WithFeature<ADOSqlServerFeature<TClient>, SqlServerOptions>(configure);
        }

        public static void UseSqlServer<TClient>(this FeatureBuilder<ADOFeature> builder, Action<SqlServerOptions<TClient>> configure)
           where TClient : SqlServerConnectionClient
        {
            builder.WithFeature<ADOSqlServerFeature<TClient>, SqlServerOptions<TClient>>(configure);
        }

        public static void UseSqlServer<TClient>(this FeatureBuilder<ADOFeature> builder, string? section = null, Action<SqlServerOptions<TClient>>? configure = null)
         where TClient : SqlServerConnectionClient
        {
            builder.WithFeature<ADOSqlServerFeature<TClient>, SqlServerOptions<TClient>>(section, configure);
        }

        public static void UseSqlServer<TClient, TOptions>(this FeatureBuilder<ADOFeature> builder, Action<TOptions> configure)
           where TClient : SqlServerConnectionClient
           where TOptions : SqlServerOptions, new()
        {
            builder.WithFeature<ADOSqlServerFeature<TClient>, TOptions>(configure);
        }

        public static void UseSqlServer<TClient>(this FeatureBuilder<ADOFeature> builder)
          where TClient : SqlServerConnectionClient
        {
            builder.WithFeature<ADOSqlServerFeature<TClient>, SqlServerOptions>();
        }
    }
}
