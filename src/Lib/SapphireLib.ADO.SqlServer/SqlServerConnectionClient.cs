﻿using System.Data;
using System.Data.Common;

using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;

using SapphireLib.ADO.Abstractions;
using SapphireLib.ADO.SqlServer.Definitions;

namespace SapphireLib.ADO.SqlServer
{
    public class SqlServerConnectionClient(IOptions<SqlServerOptions> options)
        : ISqlConnectionClient
    {
        private readonly SqlServerOptions _options = options.Value;
        private DbConnection _connection = null!;

        public string DatabaseName => _options.DatabaseName;

        public string Schema => _options.Schema;

        public string ConnectionString => _options.ConnectionString;

        public async Task<DbTransaction> BeginTransactionAsync()
        {
            return await _connection.BeginTransactionAsync();
        }

        public DbTransaction BeginTransaction()
        {
            return _connection.BeginTransaction();
        }

        public DbConnection CreateNewConnection()
        {
            var connection = new SqlConnection(_options.ConnectionString);
            connection.Open();

            return connection;
        }

        public void Dispose()
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
            {
                _connection.Dispose();
            }
        }

        public string GetConnectionString()
        {
            return _options.ConnectionString;
        }

        public DbConnection GetOpenConnection()
        {
            if (_connection == null || _connection.State != ConnectionState.Open)
            {
                _connection = new SqlConnection(_options.ConnectionString);
                _connection.Open();
            }

            return _connection;
        }
    }
}
