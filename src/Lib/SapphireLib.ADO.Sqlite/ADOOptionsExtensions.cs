﻿using SapphireLib.ADO.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.Sqlite
{
    public static class ADOOptionsExtensions
    {
        public static void UseSqlite<TClient>(this FeatureBuilder<ADOFeature> builder, Action<SqliteOptions> configure)
           where TClient : SqliteConnectionClient
        {
            builder.WithFeature<ADOSqliteFeature<TClient>, SqliteOptions>(configure);
        }

        public static void UseSqlite<TClient>(this FeatureBuilder<ADOFeature> builder)
          where TClient : SqliteConnectionClient
        {
            builder.WithFeature<ADOSqliteFeature<TClient>, SqliteOptions>();
        }
    }
}
