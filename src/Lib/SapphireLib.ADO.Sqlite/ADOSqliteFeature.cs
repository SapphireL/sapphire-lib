﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.ADO.Abstractions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.Sqlite
{
    public class ADOSqliteFeature<TClient> : BaseFeature
        where TClient : SqliteConnectionClient
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<ISqlConnectionClient, SqliteConnectionClient>();
            context.Services.AddTransient<TClient>();
        }
    }
}
