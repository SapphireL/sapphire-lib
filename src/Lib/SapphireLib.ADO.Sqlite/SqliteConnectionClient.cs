﻿using System.Data;
using System.Data.Common;

using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;

using SapphireLib.ADO.Abstractions;

namespace SapphireLib.ADO.Sqlite
{
    public class SqliteConnectionClient(IOptions<SqliteOptions> options) : ISqlConnectionClient
    {
        private readonly SqliteOptions _options = options.Value;
        private DbConnection _connection = default!;

        public string DatabaseName => _options.DatabaseName;

        public string ConnectionString => _options.ConnectionString;

        public string? Schema { get; set; }

        public DbConnection CreateNewConnection()
        {
            var connection = new SqliteConnection(_options.ConnectionString);
            return connection;
        }

        public string GetConnectionString()
        {
            return _options.ConnectionString;
        }

        public DbConnection GetOpenConnection()
        {
            if (_connection == null || _connection.State != ConnectionState.Open)
            {
                _connection = new SqliteConnection(_options.ConnectionString);
                _connection.Open();
            }

            return _connection;
        }

        public void Dispose()
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
            {
                _connection.Dispose();
            }
        }

        public DbTransaction BeginTransaction()
        {
            return _connection.BeginTransaction();
        }

        public async Task<DbTransaction> BeginTransactionAsync()
        {
            return await _connection.BeginTransactionAsync();
        }
    }
}
