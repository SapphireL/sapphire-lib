﻿using Microsoft.Extensions.Options;

using SapphireLib.ADO.Extensions;
using SapphireLib.ADO.MySql;
using SapphireLib.ADO.MySql.Definitions;
using SapphireLib.ADO.MySql.Extensions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.Test.MySql.Prepared
{
    internal class TestModule : BaseModule
    {
        public TestModule()
        {
        }
    }

    internal class TestSelfConfiguredModule : BaseModule
    {
        protected override void OnConfiguring(FeaturesBuilder builder)
        {
            builder.AddADOClient(ado =>
            {
                ado.UseMySql<TestClient>(x =>
                {
                    x.Schema = "test1";
                    x.DatabaseName = "test2";
                    x.ConnectionString = "test3";
                });
            });
        }
    }

    internal class TestClient : MySqlConnectionClient
    {
        public TestClient(IOptionsSnapshot<MySqlOptions> options)
            : base(options)
        {
        }
    }
}
