﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.ADO.Abstractions;
using SapphireLib.ADO.Extensions;
using SapphireLib.ADO.MySql;
using SapphireLib.ADO.MySql.Extensions;
using SapphireLib.ADO.Test.MySql.Prepared;
using SapphireLib.Module.Extensions;

namespace SapphireLib.ADO.Test.MySql.Tests
{
    public class WebApplicationBuilderTest
    {
        [Fact]
        public void Add_ADOModule_Should_Add_MySql_Extension()
        {
            // Arrange
            var builder = WebApplication.CreateBuilder();
            builder.AddADOModule(adoBuilder =>
            {
                adoBuilder.UseMySql(
                    options =>
                         {
                             options.Schema = "test1";
                             options.DatabaseName = "test2";
                             options.ConnectionString = "test3";
                         });
            });

            var app = builder.Build();

            // Action
            var service = app.Services.GetRequiredService<ISqlConnectionClient>();

            // Assert
            Assert.NotNull(service);
            Assert.IsType<MySqlConnectionClient>(service);
            var mysqlService = (MySqlConnectionClient)service;
            Assert.Equal("test1", mysqlService.Schema);
            Assert.Equal("test2", mysqlService.DatabaseName);
            Assert.Equal("test3", mysqlService.ConnectionString);
        }

        /// <summary>
        /// 测试为单独模块添加扩展.
        /// 将AOD作为TestModule的扩展.
        /// </summary>
        [Fact]
        public void Add_Module_As_Extension_Should_Add_MySql_Extension()
        {
            // Arrange
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLib<TestModule>(moduleBuilder =>
            {
                moduleBuilder.AddADOClient(ado =>
                {
                    ado.UseMySql<TestClient>(x =>
                    {
                        x.Schema = "test1";
                        x.DatabaseName = "test2";
                        x.ConnectionString = "test3";
                    });
                });
            });
            var app = builder.Build();

            // Action
            var service = app.Services.GetRequiredService<TestClient>();

            // Assert
            Assert.NotNull(service);
            Assert.IsType<TestClient>(service);
            Assert.Equal("test1", service.Schema);
            Assert.Equal("test2", service.DatabaseName);
            Assert.Equal("test3", service.ConnectionString);
        }

        [Fact]
        public void Add_SelfConfiguredModule_Should_Add_MySql_Extension()
        {
            // Arrange
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLib<TestSelfConfiguredModule>();
            var app = builder.Build();

            // Action
            var service = app.Services.GetRequiredService<ISqlConnectionClient>();

            // Assert
            Assert.NotNull(service);
            Assert.IsType<MySqlConnectionClient>(service);
            var mysqlService = (MySqlConnectionClient)service;
            Assert.Equal("test1", mysqlService.Schema);
            Assert.Equal("test2", mysqlService.DatabaseName);
            Assert.Equal("test3", mysqlService.ConnectionString);
        }

        [Fact]
        public void Add_SelfConfiguredModule_Should_Add_MySql_Extension1()
        {
            // Arrange
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLib<TestSelfConfiguredModule>();
            var app = builder.Build();

            // Action
            var service = app.Services.GetRequiredService<ISqlConnectionClient>();

            // Assert
            Assert.NotNull(service);
            Assert.IsType<MySqlConnectionClient>(service);
            var mysqlService = (MySqlConnectionClient)service;
            Assert.Equal("test1", mysqlService.Schema);
            Assert.Equal("test2", mysqlService.DatabaseName);
            Assert.Equal("test3", mysqlService.ConnectionString);
        }
    }
}
