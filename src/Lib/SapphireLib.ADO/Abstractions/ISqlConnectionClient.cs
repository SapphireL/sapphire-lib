﻿using System.Data.Common;

namespace SapphireLib.ADO.Abstractions;

public interface ISqlConnectionClient
{
    DbConnection GetOpenConnection();

    DbConnection CreateNewConnection();

    Task<DbTransaction> BeginTransactionAsync();

    DbTransaction BeginTransaction();

    string GetConnectionString();

    public string DatabaseName { get; }

    public string? Schema { get; }
}
