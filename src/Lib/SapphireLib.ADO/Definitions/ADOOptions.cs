﻿namespace SapphireLib.ADO.Definitions;

public class ADOOptions
{
    public string ConnectionString { get; set; } = default!;

    public string DatabaseName { get; set; } = default!;
}
