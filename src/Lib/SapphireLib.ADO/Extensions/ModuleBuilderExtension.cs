﻿using SapphireLib.ADO.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.ADO.Extensions
{
    public static class ModuleBuilderExtension
    {
        public static void AddADOClient<TModule>(this ModuleBuilder<TModule> builder, Action<FeatureBuilder<ADOFeature>> configure)
            where TModule : BaseModule, new()
        {
            var featureBuilder = new FeatureBuilder<ADOFeature>(builder.Context);
            configure(featureBuilder);
            var extension = featureBuilder.Build();
            builder.WithFeature(extension);
        }

        public static void AddADOClient(this FeaturesBuilder builder, Action<FeatureBuilder<ADOFeature>> configure)
        {
            builder.WithFeature(configure);
        }
    }
}
