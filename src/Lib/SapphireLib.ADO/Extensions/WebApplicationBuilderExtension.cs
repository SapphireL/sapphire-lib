﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.ADO.Definitions;
using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;

namespace SapphireLib.ADO.Extensions;

public static class WebApplicationBuilderExtension
{
    public static void AddADOModule(this WebApplicationBuilder builder, Action<ModuleBuilder<ADOModule>> adoBuilder)
    {
        builder.AddSappLib(adoBuilder);
    }
}
