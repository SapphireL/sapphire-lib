﻿using SapphireLib.Domain;

namespace SapphireLib.Application.Commands
{
    public abstract class CommandHandler<TCommand> : Command.Handlers.CommandHandler<TCommand, Result>
        where TCommand : ICommand
    {
    }

    public abstract class CommandHandler<TCommand, TResult> : Command.Handlers.CommandHandler<TCommand, Result<TResult>>
       where TCommand : ICommand<TResult>
    {
    }
}
