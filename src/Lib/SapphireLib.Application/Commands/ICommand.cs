﻿using SapphireLib.Domain;

namespace SapphireLib.Application.Commands
{
    public interface ICommand : Command.Abstractions.ICommand<Result>
    {
    }

    public interface ICommand<TResult> : Command.Abstractions.ICommand<Result<TResult>>
    {
    }
}
