﻿namespace SapphireLib.Application.Queries
{
    public interface IKeySearchQuery
    {
        public string? Keyword { get; }
    }
}
