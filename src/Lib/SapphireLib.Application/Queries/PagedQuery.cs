﻿namespace SapphireLib.Application.Queries
{
    public class PagedQuery<TResult> : QueryBase<TResult>
    {
        public int Current { get; set; } = 1;

        public int PageSize { get; set; } = 10;

        public virtual string? SortBy { get; set; } = "CreationTime";

        public virtual bool IsDescending { get; set; } = true;

        public virtual int Offset()
        {
            return (Current - 1) * PageSize;
        }

        public virtual int Next()
        {
            return PageSize;
        }
    }
}
