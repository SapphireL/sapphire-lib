﻿namespace SapphireLib.Application.Queries
{
    public class PagedQueryResult<T>
    {
        public List<T> Data { get; set; } = [];

        public int Total { get; set; } = 0;

        public int Current { get; set; } = 1;

        public bool Success { get; set; } = true;

        public int PageSize { get; set; } = 10;
    }
}
