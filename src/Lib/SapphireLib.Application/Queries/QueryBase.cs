﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

using SapphireLib.Application.Commands;

namespace SapphireLib.Application.Queries
{
    public abstract class QueryBase<TResult> : ICommand<TResult>
    {
        [BindNever]
        public virtual Guid Id { get; }

        protected QueryBase()
        {
            Id = Guid.NewGuid();
        }

        protected QueryBase(Guid id)
        {
            Id = id;
        }
    }
}
