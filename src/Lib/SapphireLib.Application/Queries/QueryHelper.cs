﻿using Dapper;

namespace SapphireLib.Application.Queries
{
    public static class QueryHelper
    {
        public static SqlBuilder.Template CreateSqlTemplate(string query)
        {
            var builder = new SqlBuilder();
            return builder.AddTemplate($"{query} /**where**/");
        }
    }
}
