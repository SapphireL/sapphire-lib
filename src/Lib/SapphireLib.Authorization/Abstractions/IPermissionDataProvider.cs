﻿namespace SapphireLib.Authorization.Abstractions
{
    public interface IPermissionDataProvider
    {
        public Task<List<PermissionData>> GetPermissions(string userId);
    }
}
