﻿namespace SapphireLib.Authorization.Abstractions
{
    public interface IResource
    {
        public string? ResourceId { get; set; }

        public string? UserId { get; set; }
    }
}
