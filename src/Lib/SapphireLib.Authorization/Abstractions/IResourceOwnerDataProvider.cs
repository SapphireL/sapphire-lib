﻿namespace SapphireLib.Authorization.Abstractions
{
    public interface IResourceOwnerDataProvider<T>
    {
        Task<bool> GetResourceData(T query);
    }
}
