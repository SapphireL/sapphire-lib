﻿using Microsoft.AspNetCore.Authorization;

namespace SapphireLib.Authorization.Abstractions;

public interface ISuperAuthorizeService
{
    public bool IsSuperAuthorized(AuthorizationHandlerContext context);
}
