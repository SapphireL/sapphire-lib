﻿using SapphireLib.Authorization.Services;

namespace SapphireLib.Authorization.Abstractions
{
    public interface ITokenClaimsService
    {
        TokenServiceDto GenerateJwtToken(string userId, string userName);

        TokenServiceDto GenerateJwtToken(string userId, string userName, string role, bool? hasAllPermissions = null);

        TokenServiceDto GenerateJwtToken(string userId, string userName, string role, string[] permissions);
    }
}
