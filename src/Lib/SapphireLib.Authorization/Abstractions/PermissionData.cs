﻿namespace SapphireLib.Authorization.Abstractions
{
    public class PermissionData
    {
        public string Name { get; set; } = default!;
    }
}
