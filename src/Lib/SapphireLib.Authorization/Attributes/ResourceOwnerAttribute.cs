﻿using Microsoft.AspNetCore.Authorization;

namespace SapphireLib.Authorization.Attributes
{
    public class ResourceOwnerAttribute<T> : AuthorizeAttribute, IAuthorizationRequirement
    {
    }
}