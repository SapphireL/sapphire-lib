﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Authorization.Handlers;

using SapphireLib.Authorization.Services;
using SapphireLib.Module.Core;

namespace SapphireLib.Authorization;

public class AuthorizationModule : BaseModule
{
    protected override void OnRunning(RunningContext context)
    {
        var app = context.WebApplication;
        app.UseMiddleware<CorrelationMiddleware>();
        app.UseAuthentication();
        app.UseAuthorization();
    }

    protected override void ConfigureServices(BuilderContext context)
    {
        var services = context.Services;
        services.AddHttpContextAccessor();
        services.AddAuthorization();

        services.AddTransient<IAuthorizationHandler, PermissionAuthorizationHandler>();
        services.AddTransient<PermissionDataService>();

        services.AddScoped<IUserManager, UserManager>();
    }
}
