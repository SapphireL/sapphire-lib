﻿using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Authorization.Services;
using SapphireLib.Module.Core;

namespace SapphireLib.Authorization.Definitions
{
    internal class JwtFeature : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            var options = context.GetOptions<JwtFeatureOptions>() ?? new JwtFeatureOptions();
            context.Services.AddAuthentication().AddJwtBearer(o =>
            {
                SecurityKey? key = null;

                if (!string.IsNullOrEmpty(options.JWTSecret))
                {
                    switch (options.TokenSigningStyle)
                    {
                        case "Symmetric":
                            key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(options.JWTSecret));

                            break;
                        case "Asymmetric":
                            {
                                var rsa = RSA.Create();
                                if (options.KeyIsPemEncoded)
                                {
                                    rsa.ImportFromPem(options.JWTSecret);
                                }
                                else
                                {
                                    rsa.ImportRSAPublicKey(Convert.FromBase64String(options.JWTSecret), out _);
                                }

                                key = new RsaSecurityKey(rsa);
                                break;
                            }

                        default:
                            throw new InvalidOperationException("没有指定Jwt签名类型");
                    }
                }

                o.TokenValidationParameters.IssuerSigningKey = key;
                o.TokenValidationParameters.ValidateIssuerSigningKey = key is not null;
                o.TokenValidationParameters.ValidateLifetime = options.ValidateLifetime;
                o.TokenValidationParameters.ClockSkew = TimeSpan.FromSeconds(options.ClockSkew);
                o.TokenValidationParameters.ValidAudience = options.Audience;
                o.TokenValidationParameters.ValidateAudience = options.ValidateAudience;
                o.TokenValidationParameters.ValidIssuer = options.Issuer;
                o.TokenValidationParameters.ValidateIssuer = options.ValidateIssuer;

                o.TokenValidationParameters.NameClaimType = ClaimTypes.Name;
                o.TokenValidationParameters.RoleClaimType = ClaimTypes.Role;
            });
            context.Services.AddTransient<ITokenClaimsService, DefaultTokenClaimService>();
        }
    }
}
