﻿namespace SapphireLib.Authorization.Definitions
{
    public class JwtFeatureOptions
    {
        public string JWTSecret { get; set; } = default!;

        public string TokenSigningStyle { get; set; } = "Symmetric";

        public bool KeyIsPemEncoded { get; set; } = false;

        public int ExpiryInterval { get; set; } = 600;

        public int RefreshExpiryInterval { get; set; } = 3600;

        public bool ValidateLifetime { get; set; } = true;

        public bool ValidateIssuer { get; set; } = false;

        public string? Issuer { get; set; }

        public bool ValidateAudience { get; set; } = false;

        public string? Audience { get; set; }

        public string PermissionsClaimType { internal get; set; } = "permissions";

        public int ClockSkew { get; set; } = 600;
    }
}
