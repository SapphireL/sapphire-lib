﻿using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Authorization.Definitions;
using SapphireLib.Authorization.Features;
using SapphireLib.Authorization.Services;
using SapphireLib.Module.Core;

namespace SapphireLib.Authorization.Extensions;

public static class ModuleBuilderExtension
{
    public static ModuleBuilder<AuthorizationModule> AddSuperAdminAutoAllowFeature(this ModuleBuilder<AuthorizationModule> builder, Action<FeatureBuilder<SuperAdminAutoAllowFeature>>? config = null)
    {
        builder.WithFeature(config);
        return builder;
    }

    /// <summary>
    /// 为<see cref="SuperAdminAutoAllowFeature"/>添加基于HttpContext的管理员身份鉴别服务.
    /// </summary>
    /// <param name="builder"><see cref="FeatureBuilder"/>.</param>
    public static void AddSuperAuthorizeHttpContextService(this FeatureBuilder<SuperAdminAutoAllowFeature> builder)
    {
        builder.Context.Services.TryAddTransient<ISuperAuthorizeService, SuperAuthorizeHttpContextService>();
    }

    /// <summary>
    /// 为<see cref="SuperAdminAutoAllowFeature"/>添加自定义的继承了<see cref="ISuperAuthorizeService"/>的管理员身份鉴别服务.
    /// </summary>
    /// <typeparam name="T">服务实例.</typeparam>
    /// <param name="builder"><see cref="FeatureBuilder"/>.</param>
    public static void AddSuperAuthorizeService<T>(this FeatureBuilder<SuperAdminAutoAllowFeature> builder)
        where T : class, ISuperAuthorizeService
    {
        builder.Context.Services.TryAddTransient<ISuperAuthorizeService, T>();
    }

    /// <summary>
    /// 添加Jwt认证功能.
    /// </summary>
    /// <param name="builder"><see cref="FeatureBuilder"/>.</param>
    /// <param name="section">默认读取Authorization:JWT.</param>
    /// <param name="configure"><see cref="JwtFeatureOptions"/>.</param>
    /// <returns><see cref="ModuleBuilder"/>.</returns>
    public static ModuleBuilder<AuthorizationModule> AddJwtFeature(this ModuleBuilder<AuthorizationModule> builder, string? section = "Authorization:JWT", Action<JwtFeatureOptions>? configure = null)
    {
        builder.WithFeature<JwtFeature, JwtFeatureOptions>(section, configure);
        return builder;
    }

    public static ModuleBuilder<AuthorizationModule> AddDataPermissionFeature<TDataProvider>(this ModuleBuilder<AuthorizationModule> builder)
        where TDataProvider : class, IPermissionDataProvider
    {
        var extensionBuilder = new FeatureBuilder<DataPermissionFeature<TDataProvider>>(builder.Context);
        var extension = extensionBuilder.Build();
        builder.WithFeature(extension);
        return builder;
    }

    public static ModuleBuilder<AuthorizationModule> AddDataOwnerResourceFeature<TDataProvider, TResource>(this ModuleBuilder<AuthorizationModule> builder)
        where TDataProvider : class, IResourceOwnerDataProvider<TResource>
        where TResource : IResource, new()
    {
        var extensionBuilder = new FeatureBuilder<DataOwnerResourceFeature<TDataProvider, TResource>>(builder.Context);
        var extension = extensionBuilder.Build();
        builder.WithFeature(extension);
        return builder;
    }
}