﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;

namespace SapphireLib.Authorization.Extensions;

public static class WebApplicationBuilderExtension
{
    public static void AddSappLibAuthorization(this WebApplicationBuilder builder, Action<ModuleBuilder<AuthorizationModule>>? action = null)
    {
        builder.AddSappLib(action);
    }
}
