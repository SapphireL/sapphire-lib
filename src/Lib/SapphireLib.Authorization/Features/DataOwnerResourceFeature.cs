﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Authorization.Handlers;
using SapphireLib.Authorization.Services;
using SapphireLib.Module.Core;

namespace SapphireLib.Authorization.Features
{
    public class DataOwnerResourceFeature<TDataProvider, TResource> : BaseFeature
        where TDataProvider : class, IResourceOwnerDataProvider<TResource>
        where TResource : IResource, new()
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            var services = context.Services;
            services.AddTransient<IResourceOwnerDataProvider<TResource>, TDataProvider>();
            services.AddTransient<IAuthorizationHandler, ResourceOwnerAuthorizationHandler<TResource>>();
            services.AddTransient<ResourceOwnerDataService<TResource>>();
        }
    }
}
