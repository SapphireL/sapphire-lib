﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Module.Core;

namespace SapphireLib.Authorization.Features
{
    internal class DataPermissionFeature<TDataProvider> : BaseFeature
        where TDataProvider : class, IPermissionDataProvider
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<IPermissionDataProvider, TDataProvider>();
        }
    }
}
