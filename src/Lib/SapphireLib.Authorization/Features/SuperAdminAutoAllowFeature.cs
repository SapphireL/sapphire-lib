﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Authorization.Handlers;
using SapphireLib.Authorization.Services;
using SapphireLib.Module.Core;

namespace SapphireLib.Authorization.Features;

public class SuperAdminAutoAllowFeature : BaseFeature
{
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.AddTransient<IAuthorizationHandler, SuperAdminAuthorizationHandler>();
        context.Services.TryAddTransient<ISuperAuthorizeService, SuperAuthorizeHttpContextService>();
    }
}
