﻿using System.Security.Claims;

using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

using SapphireLib.Authorization.Attributes;

using SapphireLib.Authorization.Services;

namespace SapphireLib.Authorization.Handlers
{
    internal class PermissionAuthorizationHandler(ILogger<PermissionAuthorizationHandler> logger, PermissionDataService permissionDataService) : AuthorizationHandler<PermissionAuthorizeAttribute>
    {
        private readonly ILogger<PermissionAuthorizationHandler> _logger = logger;
        private readonly PermissionDataService permissionDataService = permissionDataService;

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizeAttribute requirement)
        {
            _logger.LogWarning(
                "Evaluating authorization requirement for permission equals {permission}", requirement.Permission);

            var userId = context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (userId != null)
            {
                var permissions = await permissionDataService.GetPermissions(userId);

                if (permissions.Any(p => string.Equals(p.Name, requirement.Permission, StringComparison.OrdinalIgnoreCase)))
                {
                    context.Succeed(requirement);
                }
            }
        }
    }
}
