﻿using System.Security.Claims;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Authorization.Attributes;
using SapphireLib.Authorization.Services;

namespace SapphireLib.Authorization.Handlers
{
    internal class ResourceOwnerAuthorizationHandler<TResource>(ILogger<ResourceOwnerAuthorizationHandler<TResource>> logger, ResourceOwnerDataService<TResource> resourceOwnerDataService, IHttpContextAccessor? httpContextAccessor) : AuthorizationHandler<ResourceOwnerAttribute<TResource>>
        where TResource : IResource, new()
    {
        private readonly ILogger<ResourceOwnerAuthorizationHandler<TResource>> _logger = logger;
        private readonly ResourceOwnerDataService<TResource> _resourceOwnerDataService = resourceOwnerDataService;
        private readonly IHttpContextAccessor? _httpContextAccessor = httpContextAccessor;

        public override async Task HandleAsync(AuthorizationHandlerContext context)
        {
            var query = new TResource();
            var userId = context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var resourceId = _httpContextAccessor?.HttpContext?.Request.Query["resourceId"];
            query.ResourceId = resourceId;
            query.UserId = userId;

            await _resourceOwnerDataService.GetResourceData(query);
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ResourceOwnerAttribute<TResource> requirement)
        {
            throw new NotImplementedException();
        }
    }
}