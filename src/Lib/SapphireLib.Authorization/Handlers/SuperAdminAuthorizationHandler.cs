﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Authorization.Attributes;

namespace SapphireLib.Authorization.Handlers;

internal class SuperAdminAuthorizationHandler(ILogger<PermissionAuthorizationHandler> logger, ISuperAuthorizeService service) : AuthorizationHandler<PermissionAuthorizeAttribute>
{
    private readonly ILogger<PermissionAuthorizationHandler> _logger = logger;

    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionAuthorizeAttribute requirement)
    {
        _logger.LogWarning(
           "Evaluating authorization requirement for permission equals {permission}", requirement.Permission);

        if (service.IsSuperAuthorized(context))
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }
}
