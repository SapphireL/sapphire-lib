﻿namespace SapphireLib.Authorization;

public interface IUserManager
{
    string UserId { get; }

    string Username { get; }

    Guid CorrelationId { get; }

    bool IsAvailable { get; }
}