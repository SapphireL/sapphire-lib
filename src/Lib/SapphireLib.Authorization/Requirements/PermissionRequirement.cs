﻿using Microsoft.AspNetCore.Authorization;

namespace SapphireLib.Authorization.Requirements
{
    internal class PermissionRequirement : IAuthorizationRequirement
    {
        public string Permission { get; }

        public PermissionRequirement(string permission)
        {
            Permission = permission;
        }
    }
}
