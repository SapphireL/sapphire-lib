﻿using Microsoft.AspNetCore.Authorization;

namespace SapphireLib.Authorization.Requirements
{
    internal class ResourceOwnerRequirement<TKey> : IAuthorizationRequirement
    {
        public TKey ResourceId { get; }

        public ResourceOwnerRequirement(TKey key)
        {
            ResourceId = key;
        }
    }
}
