﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Authorization.Definitions;

namespace SapphireLib.Authorization.Services
{
    internal class DefaultTokenClaimService : ITokenClaimsService
    {
        private readonly JwtFeatureOptions _configuration;

        public DefaultTokenClaimService(IOptions<JwtFeatureOptions> configuration)
        {
            _configuration = configuration.Value;
        }

        public TokenServiceDto GenerateJwtToken(string userId, string userName)
        {
            return GenTokenServiceDto(userId, userName);
        }

        public TokenServiceDto GenerateJwtToken(string userId, string userName, string role, bool? hasAllPermissions = null)
        {
            return GenTokenServiceDto(userId, userName, role, hasAllPermissions);
        }

        public TokenServiceDto GenerateJwtToken(string userId, string userName, string role, string[] permissions)
        {
            return GenTokenServiceDto(userId, userName, role, false, permissions);
        }

        private static List<Claim> GetClaims(string userId, string userName, string? role = null, bool? hasAllPermissions = null, string[]? permissions = null)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, userName),
                new(ClaimTypes.NameIdentifier, userId),
            };

            if (!string.IsNullOrWhiteSpace(role))
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            if (hasAllPermissions == true)
            {
                claims.Add(new Claim("all-permissions", "1"));
            }

            if (permissions != null && permissions.Length > 0)
            {
                foreach (var permission in permissions)
                {
                    claims.Add(new Claim("permissions", permission));
                }
            }

            return claims;
        }

        private TokenServiceDto GenTokenServiceDto(string userId, string userName, string? role = null, bool? hasAllPermissions = null, string[]? permissions = null)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration.JWTSecret);
            var claims = GetClaims(userId, userName, role, hasAllPermissions, permissions);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims.ToArray()),
                Expires = DateTime.Now.AddSeconds(_configuration.ExpiryInterval),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Issuer = _configuration.Issuer,
                Audience = _configuration.Audience,
            };

            var accessToken = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
            var refreshToken = Guid.NewGuid().ToString("N");
            var refreshTokenExpiry = DateTime.Now.AddSeconds(_configuration.RefreshExpiryInterval);
            var result = new TokenServiceDto(accessToken, refreshToken, tokenDescriptor.Expires ?? DateTime.Now, refreshTokenExpiry, userName);
            return result;
        }
    }
}
