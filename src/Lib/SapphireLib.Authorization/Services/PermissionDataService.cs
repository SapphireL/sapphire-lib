﻿using SapphireLib.Authorization.Abstractions;

namespace SapphireLib.Authorization.Services
{
    public class PermissionDataService(IPermissionDataProvider provider)
    {
        private readonly IPermissionDataProvider _permissionDataProvider = provider;

        public async Task<List<PermissionData>> GetPermissions(string userId)
        {
            return await _permissionDataProvider.GetPermissions(userId);
        }
    }
}
