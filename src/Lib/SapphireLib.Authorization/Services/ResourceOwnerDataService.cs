﻿using SapphireLib.Authorization.Abstractions;

namespace SapphireLib.Authorization.Services
{
    public class ResourceOwnerDataService<T>(IResourceOwnerDataProvider<T> provider)
    {
        private readonly IResourceOwnerDataProvider<T> _permissionDataProvider = provider;

        public async Task<bool> GetResourceData(T query)
        {
            return await _permissionDataProvider.GetResourceData(query);
        }
    }
}
