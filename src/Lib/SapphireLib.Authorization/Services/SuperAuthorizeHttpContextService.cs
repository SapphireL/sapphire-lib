﻿using System.Security.Claims;

using Microsoft.AspNetCore.Authorization;

using SapphireLib.Authorization.Abstractions;

namespace SapphireLib.Authorization.Services;

internal class SuperAuthorizeHttpContextService : ISuperAuthorizeService
{
    public bool IsSuperAuthorized(AuthorizationHandlerContext context)
    {
        var claims = context.User.Claims.ToList();
        var hasSuperAdminClaim = claims.Any(c => string.Equals(c.Type, ClaimTypes.Role, StringComparison.OrdinalIgnoreCase) && string.Equals(c.Value, "SuperAdmin", StringComparison.OrdinalIgnoreCase));
        var hasAllPermissionClaim = claims.Any(c => string.Equals(c.Type, "all-permissions", StringComparison.OrdinalIgnoreCase) && string.Equals(c.Value, "1", StringComparison.OrdinalIgnoreCase));

        if (hasSuperAdminClaim || hasAllPermissionClaim)
        {
            return true;
        }

        return false;
    }
}
