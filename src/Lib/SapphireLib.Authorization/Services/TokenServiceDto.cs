﻿namespace SapphireLib.Authorization.Services
{
    public class TokenServiceDto(string accessToken, string refreshToken, DateTime expires, DateTime refreshExpires, string username)
    {
        public string AccessToken { get; } = accessToken;

        public string RefreshToken { get; } = refreshToken;

        public DateTime Expires { get; } = expires;

        public DateTime RefreshExpires { get; } = refreshExpires;

        public string Username { get; } = username;
    }
}
