﻿using System.Security.Claims;

using Microsoft.AspNetCore.Http;

namespace SapphireLib.Authorization;

internal class UserManager(IHttpContextAccessor httpContextAccessor) : IUserManager
{
    public string UserId
    {
        get
        {
            var httpContext = httpContextAccessor.HttpContext;
            var userId = httpContextAccessor.HttpContext?.User.Claims.Single(
                x => x.Type == ClaimTypes.NameIdentifier).Value;
            if (httpContext == null || userId == null)
            {
                throw new ApplicationException("用户信息获取不到");
            }

            return userId;
        }
    }

    public string Username
    {
        get
        {
            var httpContext = httpContextAccessor.HttpContext;
            var username = httpContextAccessor.HttpContext?.User.Claims.Single(
                x => x.Type == ClaimTypes.Name).Value;
            if (httpContext == null || string.IsNullOrEmpty(username))
            {
                throw new ApplicationException("用户信息获取不到");
            }

            return username;
        }
    }

    public Guid CorrelationId
    {
        get
        {
            if (httpContextAccessor.HttpContext != null)
            {
                if (httpContextAccessor.HttpContext.Request.Headers.Keys.Any(
                    x => x == CorrelationMiddleware.CorrelationHeaderKey))
                {
                    var headerCorrelationId = httpContextAccessor.HttpContext.Request.Headers[CorrelationMiddleware.CorrelationHeaderKey].ToString();
                    return Guid.Parse(
                        headerCorrelationId);
                }
            }

            throw new ApplicationException("Http context and correlation id is not available");
        }
    }

    public bool IsAvailable => httpContextAccessor.HttpContext != null && httpContextAccessor
                    .HttpContext?
                    .User?
                    .Claims?
                    .SingleOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?
                    .Value != null;
}
