﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SapphireLib.Cache.Interface.Inteface
{
    public interface ICacheFactory
    {
        ICacheProtectorService GetCacheProtectoryService();
    }
}
