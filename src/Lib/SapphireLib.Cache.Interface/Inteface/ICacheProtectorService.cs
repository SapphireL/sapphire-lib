﻿using Microsoft.AspNetCore.Http;

namespace SapphireLib.Cache.Interface.Inteface
{
    public interface ICacheProtectorService
    {
        /// <summary>
        ///  启用缓存保护.
        /// </summary>
        /// <param name="context">消息体.</param>
        /// <param name="next">请求委托.</param>
        /// <param name="protectiveMeasures">是否启用计数器保护接口.</param>
        /// <param name="lifeCycle">缓存生命周期.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task CacheProtector(HttpContext context, RequestDelegate next, bool protectiveMeasures, int lifeCycle);
    }
}
