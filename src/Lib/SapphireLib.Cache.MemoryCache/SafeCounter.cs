﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SapphireLib.Cache.MemoryCache
{
    public static class SafeCounter
    {
        private static int _count;
        private static DateTime time; // 上一次重置计数器时间

        public static long Increment()
        {
            Interlocked.Increment(ref _count);
            return _count;
        }

        public static void RestartCount()
        {
            _count = 0;
        }

        /// <summary>
        /// 重置计数时间.
        /// </summary>
        /// <param name="counterResetTime">配置的时间范围.</param>
        public static void RestartTime(int counterResetTime)
        {
            DateTime now = DateTime.UtcNow.AddHours(8);
            int duration = (now - time).Seconds;

            // 当本次计数时间超出配置时间范围
            if (duration > counterResetTime)
            {
                if (time != DateTime.MinValue)
                {
                    Console.WriteLine("重置计数器");
                    RestartCount();
                }

                time = DateTime.UtcNow.AddHours(8);
            }
        }

        /// <summary>
        /// 判断是否超出时间.
        /// </summary>
        /// <param name="counterResetTime">配置的时间范围.</param>
        /// <returns>是否超出时间.</returns>
        public static bool ProtectiveMeasures(int counterResetTime)
        {
            DateTime now = DateTime.UtcNow.AddHours(8);
            int duration = (now - time).Seconds;
            Console.WriteLine("时间:" + duration);
            return duration < counterResetTime;
        }

        public static DateTime GetTime()
        {
            return time;
        }
    }
}
