﻿using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using SapphireLib.Cache.Interface.Inteface;

namespace SapphireLib.Cache.MemoryCache.Service
{
    public class MemoryCacheProtectorService : ICacheProtectorService
    {
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public MemoryCacheProtectorService(IConfiguration configuration, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            _memoryCache = memoryCache;
        }

        public async Task CacheProtector(HttpContext context, RequestDelegate next, bool protectiveMeasures, int lifeCycle)
        {
            int requestQuantityLimit = 0;
            int counterResetTime = 0;
            var requestQuantityLimitStatus = int.TryParse(_configuration.GetSection("Cache:RequestQuantityLimit").Value ?? string.Empty, out requestQuantityLimit);
            var counterResetTimeStatus = int.TryParse(_configuration.GetSection("Cache:CounterResetTime").Value ?? string.Empty, out counterResetTime);
            var endpoint = context.GetEndpoint();
            var key = context.Request + context.User.Identity?.Name;
            key = Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(key)));
            if (!requestQuantityLimitStatus || !counterResetTimeStatus)
            {
                throw new Exception("请正确配置RequestQuantityLimit及CounterResetTime的类型");
            }

            string? type;
            string? value;
            if (protectiveMeasures)
            {
                if (_memoryCache.TryGetValue(key, out value) && _memoryCache.TryGetValue(key + "_Type", out type))
                {
                    context.Response.ContentType = type; // 设置响应头ContentType要在修改body前。
                    await context.Response.WriteAsync(value ?? string.Empty);

                    return;
                }

                long index = SafeCounter.Increment();
                int threadId = Thread.CurrentThread.ManagedThreadId;
                Console.WriteLine("Thread ID: {0}, 计数器: {1}", threadId, index);
                SafeCounter.RestartTime(counterResetTime);

                // 当请求时间超出保护时间且并未达到访问量，则重置计数器。
                if (index < requestQuantityLimit && !SafeCounter.ProtectiveMeasures(counterResetTime))
                {
                    SafeCounter.RestartCount();
                }

                string redisLock = (System.Reflection.Assembly.GetExecutingAssembly()?.GetName()?.Name ?? string.Empty) + "_RedisLock";

                // 当访问量达到访问量且未超出保护时间,增加缓存抵挡并发。
                if (index > requestQuantityLimit && SafeCounter.ProtectiveMeasures(counterResetTime))
                {
                    await Cache(context, next, lifeCycle, key);
                    SafeCounter.RestartCount();
                    SafeCounter.RestartTime(counterResetTime);
                    return;
                }

                await next(context);
                return;
            }
            else
            {
                await Cache(context, next, lifeCycle, key);
                return;
            }
        }

        public async Task Cache(HttpContext context, RequestDelegate next, int lifeCycle, string key)
        {
            string? value;
            string? type;
            if (!_memoryCache.TryGetValue(key, out value))
            {
                await SetCache(context, next, lifeCycle, key);
                return;
            }

            if (_memoryCache.TryGetValue(key, out value) && _memoryCache.TryGetValue(key + "_Type", out type))
            {
                context.Response.ContentType = type; // 设置响应头ContentType要在修改body前。
                await context.Response.WriteAsync(value ?? string.Empty);

                return;
            }
        }

        public async Task SetCache(HttpContext context, RequestDelegate next, int lifeCycle, string key)
        {
            string? value;
            var originalBodyStream = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;

                await next(context);

                context.Response.Body.Seek(0, SeekOrigin.Begin);
                string text = new StreamReader(context.Response.Body).ReadToEnd();
                context.Response.Body.Seek(0, SeekOrigin.Begin);
                if (!_memoryCache.TryGetValue(key, out value))
                {
                    Console.WriteLine("过期时间" + lifeCycle);
                    _memoryCache.Set(key + "_Type", context.Response.ContentType, TimeSpan.FromSeconds(lifeCycle));
                    _memoryCache.Set(key, text, TimeSpan.FromSeconds(lifeCycle));
                }

                context.Response.Body = originalBodyStream;
                await context.Response.WriteAsync(text);
                return;
            }
        }
    }
}
