﻿using StackExchange.Redis;

namespace SapphireLib.Cache.StackExchange.Redis.Helper
{
    public static class CacheHelper
    {
        private static ConnectionMultiplexer? _redis;
        private static IDatabase? _db;

        public static void Initialize(string connection)
        {
            Console.WriteLine("初始化实力");
            _redis = ConnectionMultiplexer.Connect(connection);
            _db = _redis.GetDatabase();
        }

        public static IDatabase GetDB()
        {
            if (_db == null)
            {
                throw new Exception("redis连接失败");
            }

            return _db;
        }

        public static void Set(string key, RedisValue value, int? expireSeconds = null)
        {
            if (_db == null)
            {
                throw new Exception("redis连接失败");
            }

            TimeSpan? timeSpan = null;
            if (expireSeconds != null)
            {
                timeSpan = TimeSpan.FromSeconds(expireSeconds.Value);
            }

            _db.StringSet(key, value, timeSpan);
        }

        public static string? Get(string key)
        {
            if (_db == null)
            {
                throw new Exception("redis连接失败");
            }

            return _db.StringGet(key);
        }

        public static long IncrBy(string key, long value = 1)
        {
            if (_db == null)
            {
                throw new Exception("redis连接失败");
            }

            return _db.StringIncrement(key, value);
        }

        public static bool Exists(string key)
        {
            if (_db == null)
            {
                throw new Exception("redis连接失败");
            }

            return _db.KeyExists(key);
        }
    }
}
