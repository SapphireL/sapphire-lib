﻿using SapphireLib.Cache.StackExchange.Redis.Helper;

namespace SapphireLib.Cache.StackExchange.Redis
{
    public static class SafeCounter
    {
        private static DateTime time;

        public static long Increment()
        {
            return CacheHelper.IncrBy("num", 1);
        }

        public static void RestartCount()
        {
            CacheHelper.Set("num", 0);
        }

        public static void RestartTime(int counterResetTime)
        {
            DateTime now = DateTime.UtcNow.AddHours(8);
            int duration = (now - time).Seconds;
            if (duration > counterResetTime)
            {
                if (time != DateTime.MinValue)
                {
                    Console.WriteLine("重置计数器");
                    RestartCount();
                }

                time = DateTime.UtcNow.AddHours(8);
            }
        }

        public static bool ProtectiveMeasures()
        {
            DateTime now = DateTime.UtcNow.AddHours(8);
            int duration = (now - time).Seconds;
            Console.WriteLine("时间:" + duration);
            return duration < 10;
        }

        public static DateTime GetTime()
        {
            return time;
        }
    }
}
