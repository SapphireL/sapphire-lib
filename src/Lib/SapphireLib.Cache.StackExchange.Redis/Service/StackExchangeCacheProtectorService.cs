﻿using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using SapphireLib.Cache.Interface.Inteface;
using SapphireLib.Cache.StackExchange.Redis.Helper;
using StackExchange.Redis;

namespace SapphireLib.Cache.StackExchange.Redis.Service
{
    public class StackExchangeCacheProtectorService : ICacheProtectorService
    {
        private readonly IConfiguration _configuration;
        private int requestQuantityLimit;
        private int counterResetTime;
        private bool requestQuantityLimitStatus;
        private bool counterResetTimeStatus;

        public StackExchangeCacheProtectorService(IConfiguration configuration)
        {
            _configuration = configuration;
            requestQuantityLimitStatus = int.TryParse(_configuration.GetSection("Cache:RequestQuantityLimit").Value ?? string.Empty, out requestQuantityLimit);
            counterResetTimeStatus = int.TryParse(_configuration.GetSection("Cache:CounterResetTime").Value ?? string.Empty, out counterResetTime);
        }

        public async Task CacheProtector(HttpContext context, RequestDelegate next, bool protectiveMeasures, int lifeCycle)
        {
            var endpoint = context.GetEndpoint();
            var key = context.Request + context.User.Identity?.Name;
            key = Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(key)));
            if (!requestQuantityLimitStatus || !counterResetTimeStatus)
            {
                throw new Exception("请正确配置RequestQuantityLimit及CounterResetTime的类型");
            }

            if (protectiveMeasures)
            {
                if (CacheHelper.Exists(key))
                {
                    await GetCache(context, next, lifeCycle, key);
                    return;
                }

                long index = SafeCounter.Increment();
                int threadId = Thread.CurrentThread.ManagedThreadId;
                Console.WriteLine("Thread ID: {0}, 计数器: {1}", threadId, index);
                SafeCounter.RestartTime(counterResetTime);

                // 当请求时间超出保护时间且并未达到访问量，则重置计数器。
                if (index < requestQuantityLimit && !SafeCounter.ProtectiveMeasures())
                {
                    SafeCounter.RestartCount();
                }

                string redisLock = (System.Reflection.Assembly.GetExecutingAssembly()?.GetName()?.Name ?? string.Empty) + "_RedisLock";

                // 当访问量达到访问量且未超出保护时间,增加缓存抵挡并发。
                if (index > requestQuantityLimit && SafeCounter.ProtectiveMeasures())
                {
                    RedisValue token = Environment.MachineName;
                    var db = CacheHelper.GetDB();
                    if (db.LockTake(redisLock, token, TimeSpan.FromSeconds(3)))
                    {
                        try
                        {
                            await Cache(context, next, lifeCycle, key);
                        }
                        finally
                        {
                            db.LockRelease(redisLock, token);
                        }
                    }
                    else
                    { // 当LockTake分布式锁被占用的时候递归一次从GetCache处获取缓存
                        await CacheProtector(context, next, protectiveMeasures, lifeCycle);
                        return;
                    }

                    return;
                }

                await next(context);
                return;
            }
            else
            {
                await Cache(context, next, lifeCycle, key);
                return;
            }
        }

        public async Task Cache(HttpContext context, RequestDelegate next, int lifeCycle, string key)
        {
            if (CacheHelper.Exists(key))
            {
                await GetCache(context, next, lifeCycle, key);
                return;
            }

            if (!CacheHelper.Exists(key))
            {
                await SetCache(context, next, lifeCycle, key);
                return;
            }
        }

        public async Task SetCache(HttpContext context, RequestDelegate next, int lifeCycle, string key)
        {
            var originalBodyStream = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;

                await next(context);

                context.Response.Body.Seek(0, SeekOrigin.Begin);
                string text = new StreamReader(context.Response.Body).ReadToEnd();
                context.Response.Body.Seek(0, SeekOrigin.Begin);
                if (!CacheHelper.Exists(key))
                {
                    Console.WriteLine("过期时间" + lifeCycle);
                    CacheHelper.Set(key, text, lifeCycle);
                    CacheHelper.Set(key + "_Type", context.Response.ContentType, lifeCycle);
                }

                context.Response.Body = originalBodyStream;
                await context.Response.WriteAsync(text);
                SafeCounter.RestartCount();
                SafeCounter.RestartTime(counterResetTime);
                return;
            }
        }

        public async Task<bool> GetCache(HttpContext context, RequestDelegate next, int lifeCycle, string key)
        {
            try
            {
                string res = CacheHelper.Get(key) ?? string.Empty;
                string type = CacheHelper.Get(key + "_Type") ?? string.Empty;
                if (!string.IsNullOrEmpty(res) && !string.IsNullOrEmpty(type))
                {
                    context.Response.ContentType = type;
                    await context.Response.WriteAsync(res);
                }
                else
                {
                    await SetCache(context, next, lifeCycle, key);
                }

                return true;
            }
            catch (Exception ex)
            {
#pragma warning disable CA2200 // 再次引发以保留堆栈详细信息
                throw ex;
#pragma warning restore CA2200 // 再次引发以保留堆栈详细信息
            }
        }
    }
}
