﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SapphireLib.Cache.CacheAttribute
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CacheProtectorAttribute : Attribute
    {
        /// <summary>
        /// 生命周期(秒).
        /// </summary>
        public int LifeCycle { get; set; }

        /// <summary>
        /// 启用保护模式.
        /// </summary>
        public bool ProtectiveMeasures { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CacheProtectorAttribute"/> class.
        /// 接口响应数据缓存，缓存可能会导致数据一致性问题.
        /// </summary>
        /// <param name="lifeCycle">生命周期(秒).</param>
        /// <param name="protectiveMeasures">启用计数器保护接口(true:启用计数器保护接口,false:直接缓存数据).</param>
        public CacheProtectorAttribute(int lifeCycle, bool protectiveMeasures = false)
        {
            LifeCycle = lifeCycle;
            ProtectiveMeasures = protectiveMeasures;
        }
    }
}
