﻿using CSRedis;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Cache.Factory;
using SapphireLib.Cache.Interface.Inteface;
using SapphireLib.Cache.MemoryCache.Service;
using SapphireLib.Cache.Middleware;
using SapphireLib.Cache.StackExchange.Redis.Helper;
using SapphireLib.Cache.StackExchange.Redis.Service;
using SapphireLib.Module.Core;

namespace SapphireLib.Cache
{
    public class CacheModule : BaseModule
    {
        private IConfiguration? configuration;

        protected override void OnRunning(RunningContext context)
        {
            var app = context.WebApplication;
            app.UseMiddleware<CacheProtectorMiddleware>();

            // base.Use(app);
        }

        protected override void OnConfiguring(FeaturesBuilder builder)
        {
             configuration = builder.Context.Configuration;
        }

        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<ICacheFactory, CacheFactory>();
            var cacheConfig = configuration?.GetSection("Cache");
            if (cacheConfig != null)
            {
                foreach (var each in cacheConfig.GetChildren())
                {
                    switch (each.Key)
                    {
                        case "CsRedisCore": // 初始化CSRedis实例
                            var csRedisConn = each.GetSection("Connection")?.Value;
                            if (!string.IsNullOrEmpty(csRedisConn))
                            {
                                var csRedis = new CSRedisClient(csRedisConn);
                                RedisHelper.Initialization(csRedis);
                            }

                            break;
                        case "Redis": // 初始化 StackExchange.Redis实例
                            context.Services.AddTransient<ICacheProtectorService, StackExchangeCacheProtectorService>();
                            var stackExRedisConn = each.GetSection("Connection").Value;
                            if (!string.IsNullOrEmpty(stackExRedisConn))
                            {
                                CacheHelper.Initialize(stackExRedisConn);
                            }

                            break;

                        case "MemoryCache": // DI注入MemoryCache服务
                            var cacheProtector = each.GetSection("CacheProtector")?.Value;
                            bool cacheProtectorStatus = false;
                            if (!string.IsNullOrEmpty(cacheProtector) && bool.TryParse(cacheProtector, out cacheProtectorStatus) && cacheProtectorStatus)
                            {
                                context.Services.AddMemoryCache();
                                context.Services.AddTransient<ICacheProtectorService, MemoryCacheProtectorService>();
                            }

                            break;
                    }

                    context.Services.AddTransient<Func<string, ICacheProtectorService>>(serviceProvider => key =>
                    {
                        switch (key)
                        {
                            case "Redis":
                                return serviceProvider.GetServices<ICacheProtectorService>()
                                     .First(service => service.GetType() == typeof(StackExchangeCacheProtectorService));
                            case "MemoryCache":
                                return serviceProvider.GetServices<ICacheProtectorService>()
                                     .First(service => service.GetType() == typeof(MemoryCacheProtectorService));
                            default:
                                throw new ArgumentException("Invalid key", nameof(key));
                        }
                    });
                }
            }

            // base.Load(services);
        }
    }

    public static class GlobalConfig
    {
        public static IConfiguration? Configuration { get; set; }
    }
}
