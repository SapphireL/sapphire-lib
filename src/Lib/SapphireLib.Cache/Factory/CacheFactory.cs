﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Cache.Interface.Inteface;

namespace SapphireLib.Cache.Factory
{
    public class CacheFactory : ICacheFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;

        public CacheFactory(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            _configuration = configuration;
        }

        public ICacheProtectorService GetCacheProtectoryService()
        {
            var cacheConfig = _configuration.GetSection("Cache");
            if (cacheConfig != null)
            {
                foreach (var each in cacheConfig.GetChildren())
                {
                    switch (each.Key)
                    {
                        case "Redis": // 获取StackExchange.Redis接口保护器服务
                            var stackRedisConn = each.GetSection("Connection")?.Value;
                            if (!string.IsNullOrEmpty(stackRedisConn))
                            {
                                var res = _serviceProvider.GetRequiredService<Func<string, ICacheProtectorService>>();
                                return res(each.Key);
                            }

                            break;
                        case "MemoryCache":
                            var cacheProtector = each.GetSection("CacheProtector")?.Value;
                            bool cacheProtectorStatus = false;
                            if (!string.IsNullOrEmpty(cacheProtector) && bool.TryParse(cacheProtector, out cacheProtectorStatus) && cacheProtectorStatus)
                            {
                                var res = _serviceProvider.GetRequiredService<Func<string, ICacheProtectorService>>();
                                return res(each.Key);
                            }

                            break;
                    }
                }
            }

            throw new ArgumentException("Invalid type");
        }
    }
}
