﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Cache.Interface.Inteface;

namespace SapphireLib.Cache.Middleware
{
    public class CacheProtectorMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IServiceProvider _serviceProvider;

        public CacheProtectorMiddleware(RequestDelegate next, IServiceProvider serviceProvider)
        {
            _next = next;
            _serviceProvider = serviceProvider;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                var endpoint = context.GetEndpoint();
                if (endpoint != null && context.User.Identity != null)
                { // && !string.IsNullOrEmpty(context.User.Identity.Name)
                    var caching = endpoint.Metadata.GetMetadata<CacheAttribute.CacheProtectorAttribute>();
                    if (caching == null)
                    {
                        await _next(context);
                        return;
                    }

                    var feactory = _serviceProvider.GetRequiredService<ICacheFactory>();
                    ICacheProtectorService cacheProtector = feactory.GetCacheProtectoryService();

                    await cacheProtector.CacheProtector(context, _next, caching.ProtectiveMeasures, caching.LifeCycle);
                    return;
                }

                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
#pragma warning disable CA2200 // 再次引发以保留堆栈详细信息
                throw ex;
#pragma warning restore CA2200 // 再次引发以保留堆栈详细信息
            }
        }
    }
}
