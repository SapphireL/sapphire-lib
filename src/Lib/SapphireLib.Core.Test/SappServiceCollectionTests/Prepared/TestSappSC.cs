﻿using SapphireLib.Core.ServiceCollections;

namespace SapphireLib.Core.Test.SappServiceCollectionTests.Prepared;

internal class TestSappSC : SappSC<TestSappSC, TestSappSCOptions>
{
    protected override void InjectServices(SappSCBuilderContext<TestSappSC, TestSappSCOptions> context)
    {
        var options = context.GetOptions();

        base.InjectServices(context);
    }
}
