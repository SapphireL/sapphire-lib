﻿using SapphireLib.Core.ServiceCollections;

namespace SapphireLib.Core.Test.SappServiceCollectionTests.Prepared;

internal class TestSappSCChildWithOptionsOptions
{
    public string ChildValue { get; set; } = default!;
}
