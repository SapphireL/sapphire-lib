﻿using SapphireLib.Core.ServiceCollections;

namespace SapphireLib.Core.Test.SappServiceCollectionTests.Prepared;

internal class TestSappSCWithChildren : SappSC<TestSappSCWithChildren>
{
    protected override void InjectServices(SappSCBuilderContext<TestSappSCWithChildren> context)
    {
        context.AddChild<TestSappSCChildWithOptions, TestSappSCChildWithOptionsOptions>("Child1");
        context.AddChild<TestSappSCChild>("Child2");
    }
}
