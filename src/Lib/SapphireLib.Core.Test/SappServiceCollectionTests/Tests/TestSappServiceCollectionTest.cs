﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using SapphireLib.Core.Extensions;
using SapphireLib.Core.Test.SappServiceCollectionTests.Prepared;

using Xunit;

namespace SapphireLib.Core.Test.SappServiceCollectionTests.Tests;

public class TestSappServiceCollectionTest
{
    private readonly IConfiguration _configuration;

    public TestSappServiceCollectionTest()
    {
        // 创建配置构建器
        var configurationBuilder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory()) // 设置基目录为当前目录
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true); // 添加配置文件

        // 构建配置
        _configuration = configurationBuilder.Build();
    }

    [Fact]
    public void TestSappSC_Create_With_No_Options()
    {
        var appBuilder = WebApplication.CreateBuilder();
        appBuilder.Configuration.SetBasePath(Directory.GetCurrentDirectory()) // 设置基目录为当前目录
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true); // 添加配置文件

        var collection = appBuilder.AddSappApp<TestSappSCNoOptions>();

        Assert.NotNull(collection);
    }

    [Fact]
    public void TestSappServiceCollection_BindOptions_ShouldSetValueToWeb()
    {
        var appBuilder = WebApplication.CreateBuilder();
        appBuilder.Configuration.SetBasePath(Directory.GetCurrentDirectory()) // 设置基目录为当前目录
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true); // 添加配置文件

        var collection = appBuilder.AddSappApp<TestSappSC, TestSappSCOptions>("Web");

        var sp = appBuilder.Services.BuildServiceProvider();

        var appOptions = sp.GetService<IOptions<TestSappSCOptions>>();

        Assert.NotNull(appOptions);
        Assert.Equal("Web", appOptions!.Value.Value);
    }

    /// <summary>
    /// 测试带有子类的SappSC
    /// </summary>
    [Fact]
    public void TestSappSC_Create_With_Children()
    {
        var appBuilder = WebApplication.CreateBuilder();
        appBuilder.Configuration.SetBasePath(Directory.GetCurrentDirectory()) // 设置基目录为当前目录
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true); // 添加配置文件

        var collection = appBuilder.AddSappApp<TestSappSCWithChildren>("TestSappSCWithChildren");

        Assert.NotNull(collection);
        Assert.Equal(2, collection.Children[0].Children.Count);
    }

    /// <summary>
    /// 测试带有子类的SappSC，子类有选项
    /// </summary>
    [Fact]
    public void TestSappSC_Create_With_Children_With_Options()
    {
        var appBuilder = WebApplication.CreateBuilder();
        appBuilder.Configuration.SetBasePath(Directory.GetCurrentDirectory()) // 设置基目录为当前目录
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true); // 添加配置文件

        var collection = appBuilder.AddSappApp<TestSappSCWithChildren>("TestSappSCWithChildrenOptions");

        var sp = appBuilder.Services.BuildServiceProvider();
        var appOptions = sp.GetService<IOptions<TestSappSCChildWithOptionsOptions>>();

        Assert.NotNull(collection);
        Assert.Equal(2, collection.Children[0].Children.Count);
        Assert.Equal("TestSappSCWithChildrenOptionsChildValue", appOptions!.Value.ChildValue);
    }
}
