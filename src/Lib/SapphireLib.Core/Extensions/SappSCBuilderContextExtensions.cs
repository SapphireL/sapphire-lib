﻿using SapphireLib.Core.ServiceCollections;

namespace SapphireLib.Core.Extensions;

public static class SappSCBuilderContextExtensions
{
    public static SappSCBuilder<TSappSC, TSappSCOptions> CreateBuilder<TSappSC, TSappSCOptions>(this SappSCBuilderContext<TSappSC, TSappSCOptions> context)
        where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
        where TSappSCOptions : class, new()
    {
        return new SappSCBuilder<TSappSC, TSappSCOptions>(context);
    }

    public static SappSCBuilder<TSappSC> CreateBuilder<TSappSC>(this SappSCBuilderContext<TSappSC> context)
       where TSappSC : SappSC<TSappSC>, new()
    {
        return new SappSCBuilder<TSappSC>(context);
    }
}
