﻿using SapphireLib.Core.ServiceCollections;

namespace SapphireLib.Core.Extensions;

public static class SappSCBuilderExtensions
{
    public static SappSCDirector<TSappSC, TSappSCOptions> CreateDirector<TSappSC, TSappSCOptions>(this SappSCBuilder<TSappSC, TSappSCOptions> builder)
        where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
        where TSappSCOptions : class, new()
    {
        return new SappSCDirector<TSappSC, TSappSCOptions>(builder);
    }

    public static SappSCDirector<TSappSC> CreateDirector<TSappSC>(this SappSCBuilder<TSappSC> builder)
      where TSappSC : SappSC<TSappSC>, new()
    {
        return new SappSCDirector<TSappSC>(builder);
    }
}
