﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Core.ServiceCollections;

namespace SapphireLib.Core.Extensions;

public static class WebApplicationBuilderExtensions
{
    /// <summary>
    /// 添加Sapphire App服务。
    /// </summary>
    /// <typeparam name="TSappSC">服务类型。<see cref="SappSC{TSappSC}"/></typeparam>
    /// <param name="webApplicationBuilder"><see cref="WebApplicationBuilder"/></param>
    /// <param name="aliasName">服务别名。</param>
    /// <returns><see cref="SappAppSC"/>。</returns>
    public static SappAppSC AddSappApp<TSappSC>(this WebApplicationBuilder webApplicationBuilder, string? aliasName = null)
      where TSappSC : SappSC<TSappSC>, new()
    {
        // 创建App的Director。
        var appDirector = CreateAppDirector(webApplicationBuilder);
        return appDirector.ConstructWithChild<TSappSC>(aliasName);
    }

    public static SappAppSC AddSappApp<TSappSC, TSappSCOptions>(this WebApplicationBuilder webApplicationBuilder, string? aliasName = null)
        where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
        where TSappSCOptions : class, new()
    {
        var appDirector = CreateAppDirector(webApplicationBuilder);
        return appDirector.ConstructWithChild<TSappSC, TSappSCOptions>(aliasName);
    }

    public static SappSCBuilderContext<TSappSC, TSappSCOptions> CreateSappContext<TSappSC, TSappSCOptions>(this WebApplicationBuilder webApplicationBuilder, string? aliseName)
        where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
        where TSappSCOptions : class, new()
    {
        return new SappSCBuilderContext<TSappSC, TSappSCOptions>(webApplicationBuilder, aliseName);
    }

    public static SappSCBuilderContext<TSappSC> CreateSappContext<TSappSC>(this WebApplicationBuilder webApplicationBuilder, string? aliasName = null)
        where TSappSC : ISappSC<TSappSC>, new()
    {
        return new SappSCBuilderContext<TSappSC>(webApplicationBuilder, aliasName);
    }

    private static SappSCDirector<SappAppSC> CreateAppDirector(WebApplicationBuilder webApplicationBuilder)
    {
        var context = webApplicationBuilder.CreateSappContext<SappAppSC>("SappApp");
        var builder = context.CreateBuilder();
        var director = builder.CreateDirector();
        return director;
    }

    private static SappSCDirector<SappAppSC> CreateLibDirector(WebApplicationBuilder webApplicationBuilder)
    {
        var context = webApplicationBuilder.CreateSappContext<SappAppSC>("SappLib");
        var builder = context.CreateBuilder();
        var director = builder.CreateDirector();
        return director;
    }
}
