﻿namespace SapphireLib.Core.ServiceCollections;

public interface ISappSC
{
    /// <summary>
    /// 获取ServiceProvider
    /// </summary>
    public IServiceProvider ServiceProvider { get; }

    /// <summary>
    /// 获取所有子项。
    /// </summary>
    public List<ISappSC> Children { get; }
}

/// <summary>
/// 定义泛型接口 <see cref="ISappSC{TSappSC}"/>，继承自 <see cref="ISappSC"/>。
/// </summary>
/// <typeparam name="TSappSC">模块类型，必须实现 <see cref="ISappSC{TSappSC}"/> 接口并具有无参构造函数。</typeparam>
public interface ISappSC<TSappSC> : ISappSC
    where TSappSC : ISappSC<TSappSC>, new()
{
    /// <summary>
    /// 获取用于构建模块的上下文 <see cref="ISappSCBuilderContext{TSappSC}"/> 实例。
    /// </summary>
    /// <returns>返回 <see cref="ISappSCBuilderContext{TSappSC}"/> 实例。</returns>
    public ISappSCBuilderContext<TSappSC> GetBuilderContext();
}

/// <summary>
/// 定义泛型接口 <see cref="ISappSC{TSappSC, TSappSCOptions}"/>，继承自 <see cref="ISappSC{TSappSC}"/>。
/// </summary>
/// <typeparam name="TSappSC">模块类型，必须继承 <see cref="SappSC{TSappSC, TSappSCOptions}"/> 并具有无参构造函数。</typeparam>
/// <typeparam name="TSappSCOptions">模块配置选项类型，必须为引用类型并具有无参构造函数。</typeparam>
public interface ISappSC<TSappSC, TSappSCOptions> : ISappSC<TSappSC>
    where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
    where TSappSCOptions : class, new()
{
    /// <summary>
    /// 获取用于构建模块的上下文 <see cref="ISappSCBuilderContext{TSappSC, TSappSCOptions}"/> 实例。
    /// </summary>
    /// <returns>返回 <see cref="ISappSCBuilderContext{TSappSC, TSappSCOptions}"/> 实例。</returns>
    public new ISappSCBuilderContext<TSappSC, TSappSCOptions> GetBuilderContext();
}
