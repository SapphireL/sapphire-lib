﻿namespace SapphireLib.Core.ServiceCollections;

public interface ISappSCBuilder<TSappSC>
{
    public ISappSCBuilder<TSappSC> AddChild(ISappSC sappSC);

    public ISappSCBuilder<TSappSC> BuildSCServices();

    public ISappSCBuilder<TSappSC> BuildChildren();

    public ISappSCBuilder<TSappSC> SetContext();

    public ISappSCBuilderContext<TSappSC> GetContext();

    public TSappSC Build();
}

public interface ISappSCBuilder<TSappSC, TSappSCOptions> : ISappSCBuilder<TSappSC>
{
    public ISappSCBuilder<TSappSC> BindOptions();

    public new ISappSCBuilder<TSappSC, TSappSCOptions> SetContext();
}
