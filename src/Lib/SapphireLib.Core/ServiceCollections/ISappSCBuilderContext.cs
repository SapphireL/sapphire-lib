﻿namespace SapphireLib.Core.ServiceCollections;

public interface ISappSCBuilderContext
{
    /// <summary>
    /// 获取当前服务集合名称。
    /// </summary>
    public string AliasName { get; }

    /// <summary>
    /// 父级构造器上下文。
    /// </summary>
    public ISappSCBuilderContext? Parent { get; }

    /// <summary>
    /// 获取所有子服务集合。
    /// </summary>
    public List<ISappSC> Children { get; }

    /// <summary>
    /// 获取所有服务集合导演。
    /// </summary>
    public List<ISappSCDirector> Directors { get; }

    public ISappSCDirector<TChildSappSC> AddChild<TChildSappSC>(string? aliasName = null)
        where TChildSappSC : SappSC<TChildSappSC>, new();

    public ISappSCDirector<TChildSappSC, TChildSappSCOptions> AddChild<TChildSappSC, TChildSappSCOptions>(string? aliasName = null)
        where TChildSappSC : SappSC<TChildSappSC, TChildSappSCOptions>, new()
        where TChildSappSCOptions : class, new();
}

/// <summary>
/// Sapphire Serice Collection 构造器上下文。
/// </summary>
/// <typeparam name="TSappSC"><see cref="SappSC{TSappSC}"/></typeparam>
public interface ISappSCBuilderContext<TSappSC> : ISappSCBuilderContext
{
}

public interface ISappSCBuilderContext<TSappSC, TSappSCOptions> : ISappSCBuilderContext<TSappSC>
{
    /// <summary>
    /// 绑定配置节。
    /// <remarks>
    /// Sapphire的默认实现 <see cref="SappSCBuilderContext{TSappSC, TSappSCOptions}"/> 如果 <paramref name="section"/> 为 null 则根据类型名自动绑定。
    /// </remarks>
    /// </summary>
    /// <param name="section"> 配置节。</param>
    public void BindOptions();
}