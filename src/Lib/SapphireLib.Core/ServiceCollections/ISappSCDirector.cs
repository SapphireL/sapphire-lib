﻿namespace SapphireLib.Core.ServiceCollections;

public interface ISappSCDirector
{
    public ISappSC Construct();
}

public interface ISappSCDirector<TSappSC, TSappSCOptions> : ISappSCDirector<TSappSC>
    where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
    where TSappSCOptions : class, new()
{
}

public interface ISappSCDirector<TSappSC> : ISappSCDirector
    where TSappSC : SappSC<TSappSC>, new()
{
}
