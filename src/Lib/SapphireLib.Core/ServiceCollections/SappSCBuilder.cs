﻿namespace SapphireLib.Core.ServiceCollections;

public class SappSCBuilder<TSappSC>(SappSCBuilderContext<TSappSC> context) : ISappSCBuilder<TSappSC>
      where TSappSC : SappSC<TSappSC>, new()
{
    private readonly TSappSC _instance = new();

    protected TSappSC Instance => _instance;

    private readonly SappSCBuilderContext<TSappSC> _context = context ?? throw new ArgumentNullException(nameof(context));

    public SappSCBuilderContext<TSappSC> Context => _context;

    public TSappSC Build()
    {
        return _instance;
    }

    public virtual ISappSCBuilder<TSappSC> SetContext()
    {
        _instance.SetBuilderContext(_context);
        return this;
    }

    public ISappSCBuilder<TSappSC> BuildChildren()
    {
        _instance.InjectChildrenServices();
        return this;
    }

    public virtual ISappSCBuilder<TSappSC> BuildSCServices()
    {
        _instance.InjectInternalServices();
        return this;
    }

    public ISappSCBuilder<TSappSC> AddChild(ISappSC sappSC)
    {
        _instance.Children.Add(sappSC);
        return this;
    }

    public virtual ISappSCBuilderContext<TSappSC> GetContext()
    {
        return _context;
    }
}

public class SappSCBuilder<TSappSC, TSappSCOptions>(SappSCBuilderContext<TSappSC, TSappSCOptions> context) : SappSCBuilder<TSappSC>(context), ISappSCBuilder<TSappSC, TSappSCOptions>
    where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
    where TSappSCOptions : class, new()
{
    private readonly SappSCBuilderContext<TSappSC, TSappSCOptions> _context = context ?? throw new ArgumentNullException(nameof(context));

    /// <summary>
    /// 获取当前的构建上下文。
    /// </summary>
    public new SappSCBuilderContext<TSappSC, TSappSCOptions> Context => _context;

    /// <summary>
    /// 获取当前实例。
    /// </summary>
    protected TSappSC SC => Instance;

    public override ISappSCBuilder<TSappSC, TSappSCOptions> SetContext()
    {
        Instance.SetBuilderContext(_context);
        return this;
    }

    public override ISappSCBuilder<TSappSC, TSappSCOptions> BuildSCServices()
    {
        Instance.InjectInternalServices();
        return this;
    }

    public ISappSCBuilder<TSappSC> BindOptions()
    {
        _context.BindOptions();
        return this;
    }

    public override ISappSCBuilderContext<TSappSC, TSappSCOptions> GetContext()
    {
        return _context;
    }
}
