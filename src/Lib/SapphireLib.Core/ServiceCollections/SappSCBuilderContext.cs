﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Core.Extensions;

namespace SapphireLib.Core.ServiceCollections;

/// <summary>
/// Sapphire Serice Collection 构造器上下文。
/// </summary>
/// <typeparam name="TSappSC"><see cref="SappSC{TSappSC}"/></typeparam>
/// <param name="builder"><see cref="WebApplicationBuilder"/></param>
public class SappSCBuilderContext<TSappSC> : ISappSCBuilderContext<TSappSC>
    where TSappSC : ISappSC<TSappSC>, new()
{
    private readonly WebApplicationBuilder _builder;
    private readonly ConfigurationManager _configuration;
    private readonly IWebHostEnvironment _webHostEnvironment;
    private readonly IServiceCollection _internalServices = new ServiceCollection();

    public SappSCBuilderContext(WebApplicationBuilder builder, string? aliasName)
    {
        _aliasName = string.IsNullOrWhiteSpace(aliasName) ? GetType().Name : aliasName;
        _builder = builder ?? throw new ArgumentNullException(nameof(builder));
        _configuration = builder.Configuration ?? throw new ArgumentNullException(nameof(builder));
        _webHostEnvironment = builder.Environment ?? throw new ArgumentNullException(nameof(builder));
    }

    public List<ISappSC> Children => [];

    public List<ISappSCDirector> Directors { get; } = [];

    /// <summary>
    /// 应用程序服务集合。
    /// 应用程序默认使用的 <see cref="IServiceCollection"/>。
    /// </summary>
    public IServiceCollection AppServices => _builder.Services;

    /// <summary>
    /// 应用程序配置管理器。
    /// </summary>
    public ConfigurationManager AppConfiguration => _configuration;

    /// <summary>
    /// 应用程序环境。
    /// </summary>
    public IWebHostEnvironment AppWebHostEnvironment => _webHostEnvironment;

    public WebApplicationBuilder WebApplicationBuilder => _builder;

    /// <summary>
    /// 模块服务集合。
    /// </summary>
    public IServiceCollection SappServices => _internalServices;

    public virtual ISappSCBuilderContext? Parent { get; set; }

    public string OptionsSection { get; set; } = default!;

    private string _aliasName;

    /// <summary>
    /// 获取或设置当前服务集合名称。
    /// <remarks>如果未设置，则返回当前类型名称。</remarks>
    /// </summary>
    public string AliasName => _aliasName;

    public IServiceCollection? AddScoped<TService, TImplementation>(ProviderType providerType = ProviderType.ALL)
        where TService : class
        where TImplementation : class, TService
    {
        return AddService<TService, TImplementation>(ServiceLifetime.Scoped, providerType);
    }

    public IServiceCollection? AddTransient<TService, TImplementation>(ProviderType providerType = ProviderType.ALL)
       where TService : class
       where TImplementation : class, TService
    {
        return AddService<TService, TImplementation>(ServiceLifetime.Transient, providerType);
    }

    public IServiceCollection? AddSingleton<TService, TImplementation>(ProviderType providerType = ProviderType.ALL)
       where TService : class
       where TImplementation : class, TService
    {
        return AddService<TService, TImplementation>(ServiceLifetime.Singleton, providerType);
    }

    public virtual ISappSCDirector<TChildSappSC> AddChild<TChildSappSC>(string? aliasName = null)
     where TChildSappSC : SappSC<TChildSappSC>, new()
    {
        var context = _builder.CreateSappContext<TChildSappSC>(aliasName);
        context.Parent = this;
        var builder = context.CreateBuilder();
        var director = builder.CreateDirector();
        Directors.Add(director);
        return director;
    }

    public virtual ISappSCDirector<TChildSappSC, TChildSappSCOptions> AddChild<TChildSappSC, TChildSappSCOptions>(string? aliasName = null)
        where TChildSappSC : SappSC<TChildSappSC, TChildSappSCOptions>, new()
        where TChildSappSCOptions : class, new()
    {
        var context = _builder.CreateSappContext<TChildSappSC, TChildSappSCOptions>(aliasName);
        context.Parent = this;
        var builder = context.CreateBuilder();
        var director = builder.CreateDirector();
        Directors.Add(director);
        return director;
    }

    private IServiceCollection? AddService<TService, TImplementation>(
       ServiceLifetime lifetime, ProviderType providerType)
       where TService : class
       where TImplementation : class, TService
    {
        switch (providerType)
        {
            case ProviderType.ALL:
                _builder.Services.Add(new ServiceDescriptor(typeof(TService), typeof(TImplementation), lifetime));
                _internalServices.Add(new ServiceDescriptor(typeof(TService), typeof(TImplementation), lifetime));
                return null;
            case ProviderType.App:
                _builder.Services.Add(new ServiceDescriptor(typeof(TService), typeof(TImplementation), lifetime));
                return _builder.Services;
            case ProviderType.SC:
                _internalServices.Add(new ServiceDescriptor(typeof(TService), typeof(TImplementation), lifetime));
                return _internalServices;
        }

        return null;
    }
}

public enum ProviderType
{
    /// <summary>
    /// 服务提供者。
    /// </summary>
    App,

    /// <summary>
    /// 模块服务提供者。
    /// </summary>
    SC,

    /// <summary>
    /// 所有服务提供者。
    /// </summary>
    ALL
}
