﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SapphireLib.Core.ServiceCollections;

public class SappSCBuilderContext<TSappSC, TSappSCOptions>(WebApplicationBuilder builder, string? aliasName)
: SappSCBuilderContext<TSappSC>(builder, aliasName), ISappSCBuilderContext<TSappSC, TSappSCOptions>
    where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
    where TSappSCOptions : class, new()
{
    protected TSappSCOptions? Options { get; set; } = new TSappSCOptions();

    public void BindOptions()
    {
        var section = AliasName;
        var parentNode = Parent;
        while (parentNode != null)
        {
            section = $"{parentNode.AliasName}:{section}";
            parentNode = parentNode?.Parent;
        }

        AppConfiguration.GetSection(section).Bind(Options);
        AppServices.Configure<TSappSCOptions>(AppConfiguration.GetSection(section));
    }

    public TSappSCOptions? GetOptions()
    {
        return Options;
    }
}
