﻿namespace SapphireLib.Core.ServiceCollections;

public class SappSCDirector<TSappSC>(ISappSCBuilder<TSappSC> builder) : ISappSCDirector<TSappSC>
      where TSappSC : SappSC<TSappSC>, new()
{
    private readonly ISappSCBuilder<TSappSC> _builder = builder;

    public virtual ISappSC Construct()
    {
        return _builder
           .SetContext()
           .BuildSCServices() // 构建服务集合
           .BuildChildren() // 构建子项
           .Build();
    }

    /// <summary>
    /// 创建一个 App 的 SappSCDirector
    /// </summary>
    /// <param name="director">子服务的 director</param>
    /// <returns>服务实例。</returns>
    public TSappSC ConstructWithChildDirector(ISappSCDirector director)
    {
        _builder.SetContext().GetContext().Directors.Add(director);
        return _builder
            .BuildSCServices() // 构建服务集合
            .BuildChildren() // 构建子项
            .Build();
    }

    public TSappSC ConstructWithChild<TChildSappSC>(string? aliasName = null)
         where TChildSappSC : SappSC<TChildSappSC>, new()
    {
        _builder.SetContext().GetContext().AddChild<TChildSappSC>(aliasName);
        return _builder
          .BuildSCServices() // 构建服务集合
          .BuildChildren() // 构建子项
          .Build();
    }

    public TSappSC ConstructWithChild<TChildSappSC, TChildSappSCOptions>(string? aliasName = null)
        where TChildSappSC : SappSC<TChildSappSC, TChildSappSCOptions>, new()
        where TChildSappSCOptions : class, new()
    {
        _builder.SetContext().GetContext().AddChild<TChildSappSC, TChildSappSCOptions>(aliasName);
        return _builder
          .BuildSCServices() // 构建服务集合
          .BuildChildren() // 构建子项
          .Build();
    }
}

public sealed class SappSCDirector<TSappSC, TSappSCOptions>(ISappSCBuilder<TSappSC, TSappSCOptions> builder)
    : SappSCDirector<TSappSC>(builder), ISappSCDirector<TSappSC, TSappSCOptions>
    where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
    where TSappSCOptions : class, new()
{
    private readonly ISappSCBuilder<TSappSC, TSappSCOptions> _builder = builder;

    public static SappSCDirector<TSappSC, TSappSCOptions> CreateDirector(ISappSCBuilder<TSappSC, TSappSCOptions> builder)
    {
        return new SappSCDirector<TSappSC, TSappSCOptions>(builder);
    }

    public override ISappSC Construct()
    {
        return _builder
            .SetContext().BindOptions()
            .BuildSCServices() // 构建服务集合
            .BuildChildren() // 构建子项
            .Build();
    }
}