﻿using Microsoft.Extensions.DependencyInjection;

namespace SapphireLib.Core.ServiceCollections;

public abstract class SappSC<TSappSC, TSappSCOptions> : SappSC<TSappSC>, ISappSC<TSappSC, TSappSCOptions>
    where TSappSC : SappSC<TSappSC, TSappSCOptions>, new()
    where TSappSCOptions : class, new()
{
    private SappSCBuilderContext<TSappSC, TSappSCOptions> _builderContext = default!;

    /// <summary>
    /// 获取 <see cref="ISappSCBuilderContext{TSappSC, TSappSCOptions}"/>
    /// </summary>
    /// <returns> <see cref="ISappSCBuilderContext{TSappSC, TSappSCOptions}"/></returns>
    public override ISappSCBuilderContext<TSappSC, TSappSCOptions> GetBuilderContext()
    {
        return _builderContext;
    }

    internal override void InjectInternalServices()
    {
        InjectServices(_builderContext);
        SetServiceProvider(_builderContext.SappServices.BuildServiceProvider());
    }

    internal override void InjectChildrenServices()
    {
        foreach (var director in _builderContext.Directors)
        {
            Children.Add(director.Construct());
        }
    }

    internal void SetBuilderContext(SappSCBuilderContext<TSappSC, TSappSCOptions> context)
    {
        _builderContext = context;
    }

    /// <summary>
    /// 向集合注入服务。
    /// </summary>
    /// <param name="context">构建器上下文<see cref="SappSCBuilderContext">。</param>
    protected virtual void InjectServices(SappSCBuilderContext<TSappSC, TSappSCOptions> context)
    {
    }

    /// <summary>
    /// 配置运行时。
    /// </summary>
    /// <param name="context">运行时上下文<see cref="SappSCRunningContext">。</param>
    protected virtual void ConfigureRuntime(SappSCRunningContext context)
    {
    }
}
