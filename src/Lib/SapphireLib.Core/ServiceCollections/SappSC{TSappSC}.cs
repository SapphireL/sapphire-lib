﻿using Microsoft.Extensions.DependencyInjection;

namespace SapphireLib.Core.ServiceCollections;

public abstract class SappSC<TSappSC> : ISappSC<TSappSC>
    where TSappSC : ISappSC<TSappSC>, new()
{
    /// <summary>
    /// 获取当前实例的子模块集合。
    /// </summary>
    public List<ISappSC> Children => _children;

    public IServiceProvider ServiceProvider => _serviceProvider ?? throw new ArgumentNullException("ServiceProvder 没有被创建");

    protected void SetServiceProvider(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    private readonly SappSCRunningContext _runningContext = default!;
    private readonly List<ISappSC> _children = [];
    private IServiceProvider _serviceProvider = default!;

    private SappSCBuilderContext<TSappSC> _builderContext = default!;

    public SappSCRunningContext GetRunningContext() => _runningContext;

    public virtual ISappSCBuilderContext<TSappSC> GetBuilderContext()
    {
        return _builderContext;
    }

    internal void SetBuilderContext(SappSCBuilderContext<TSappSC> context)
    {
        _builderContext = context;
    }

    internal virtual void InjectChildrenServices()
    {
        foreach (var director in _builderContext.Directors)
        {
            Children.Add(director.Construct());
        }
    }

    internal virtual void InjectInternalServices()
    {
        InjectServices(_builderContext);
        SetServiceProvider(_builderContext.SappServices.BuildServiceProvider());
    }

    protected virtual void InjectServices(SappSCBuilderContext<TSappSC> context)
    {
    }
}
