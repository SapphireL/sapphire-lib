﻿namespace SapphireLib.DataProvider
{
    public interface IUnitWork
    {
        Task<int> SaveChangeAsync();
    }
}
