﻿using SapphireLib.Domain.Entities;

namespace SapphireLib.Domain.AdminUser.Identity;

public class AdminUserBase<TKey> : Entity
    where TKey : IEquatable<TKey>
{
    public TKey Id { get; set; } = default!;

    public virtual string UserName { get; set; } = default!;

    public AdminUserBase(string userName) => UserName = userName;

    public AdminUserBase()
    {
    }
}
