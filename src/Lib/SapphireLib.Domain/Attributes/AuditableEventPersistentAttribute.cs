﻿namespace SapphireLib.Domain.Attributes;

[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public class AuditableEventPersistentAttribute : Attribute
{
}
