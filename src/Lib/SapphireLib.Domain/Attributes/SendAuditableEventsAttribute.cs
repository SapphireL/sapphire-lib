﻿namespace SapphireLib.Domain.Attributes;

[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public class SendAuditableEventsAttribute : Attribute
{
    public bool IsEnabled { get; }

    public SendAuditableEventsAttribute(bool isEnabled = true)
    {
        IsEnabled = isEnabled;
    }
}
