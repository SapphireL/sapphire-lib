﻿namespace SapphireLib.Domain.Attributes;

[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public class SendEntityAddedEventAttribute : Attribute
{
    public bool IsEnabled { get; }

    public SendEntityAddedEventAttribute(bool isEnabled = true)
    {
        IsEnabled = isEnabled;
    }
}
