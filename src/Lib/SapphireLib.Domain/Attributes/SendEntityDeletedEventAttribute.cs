﻿namespace SapphireLib.Domain.Attributes;

[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public class SendEntityDeletedEventAttribute : Attribute
{
    public bool IsEnabled { get; }

    public SendEntityDeletedEventAttribute(bool isEnabled = true)
    {
        IsEnabled = isEnabled;
    }
}
