﻿namespace SapphireLib.Domain.Attributes;

[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public class SendEntityUpdatedEventAttribute : Attribute
{
    public bool IsEnabled { get; }

    public SendEntityUpdatedEventAttribute(bool isEnabled = true)
    {
        IsEnabled = isEnabled;
    }
}
