﻿using System.Reflection;

namespace SapphireLib.Domain.Definitions
{
    public class DomainExtensionOptions
    {
        public Assembly RepositoryAssemblyToScan { get; set; } = default!;
    }
}
