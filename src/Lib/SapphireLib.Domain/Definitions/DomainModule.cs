﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Domain.Events;
using SapphireLib.Module.Core;

namespace SapphireLib.Domain.Definitions;

/// <summary>
/// 域模块，用于配置应用程序中的领域事件总线服务。
/// </summary>
/// <remarks>
/// <para>此模块继承自 <see cref="BaseModule"/>，并重写 <see cref="ConfigureServices"/> 方法以注册领域事件相关的服务：</para>
/// <list type="bullet">
///   <item>
///     <description><see cref="IDomainEventBus"/>：领域事件总线接口，负责在领域事件之间进行传递和处理。</description>
///   </item>
/// </list>
/// <para>这些服务被注册为 <c>Scoped</c>，确保在每个请求上下文中创建单一实例，适用于领域事件的管理。</para>
/// </remarks>
public class DomainModule : BaseModule
{
    /// <summary>
    /// 配置服务的方法，在此方法中为领域事件总线注册依赖项。
    /// </summary>
    /// <param name="context">提供应用程序上下文的 <see cref="BuilderContext"/>。</param>
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.AddScoped<IDomainEventBus, DomainEventBus>();
    }
}
