﻿namespace SapphireLib.Domain.Entities;

/// <summary>
/// 领域实体.
/// </summary>
public abstract class Entity
{
    private List<IDomainEvent> _domainEvents = [];

    /// <summary>
    /// 领域事件.
    /// </summary>
    public IReadOnlyCollection<IDomainEvent> DomainEvents => _domainEvents.ToList();

    public void ClearDomainEvents()
    {
        _domainEvents?.Clear();
    }

    /// <summary>
    /// 添加领域事件.
    /// </summary>
    /// <param name="domainEvent">领域事件.</param>
    protected void AddDomainEvent(IDomainEvent domainEvent)
    {
        _domainEvents ??= [];

        _domainEvents.Add(domainEvent);
    }
}
