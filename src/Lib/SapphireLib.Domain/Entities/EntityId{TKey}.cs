﻿namespace SapphireLib.Domain.Entities;

public abstract class EntityId<TKey> : Entity
{
    public EntityId(TKey id)
        : this()
    {
        Id = id;
    }

    protected EntityId()
    {
    }

    public virtual TKey Id { get; set; } = default!;
}
