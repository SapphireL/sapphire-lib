﻿namespace SapphireLib.Domain.Enums
{
    public enum GenderEnum
    {
        /// <summary>
        /// 未知.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// 男性.
        /// </summary>
        Male = 1,

        /// <summary>
        /// 女性.
        /// </summary>
        Female = 2
    }
}
