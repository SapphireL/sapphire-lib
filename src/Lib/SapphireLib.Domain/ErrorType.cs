﻿namespace SapphireLib.Domain;

public enum ErrorType
{
    /// <summary>
    /// General failure.
    /// </summary>
    Failure = 0,

    /// <summary>
    /// Validation failure.
    /// </summary>
    Validation = 1,

    /// <summary>
    /// Problem failure.
    /// </summary>
    Problem = 2,

    /// <summary>
    /// Not found failure.
    /// </summary>
    NotFound = 3,

    /// <summary>
    /// Conflict failure.
    /// </summary>
    Conflict = 4
}
