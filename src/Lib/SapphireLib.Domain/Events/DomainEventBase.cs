﻿namespace SapphireLib.Domain.Events;

public class DomainEventBase : IDomainEvent
{
    public Guid Id { get; }

    public DateTime OccurredOn { get; }

    public DomainEventBase(Guid? id = null)
    {
        Id = id ?? Guid.NewGuid();
        OccurredOn = DateTime.Now;
    }
}
