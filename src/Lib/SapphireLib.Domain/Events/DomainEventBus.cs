﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.Domain.Events;

internal class DomainEventBus(IEventBus eventbus) : IDomainEventBus
{
    public async Task DispatchDomainEventsAsync(List<IDomainEvent> events, CancellationToken cancellationToken = default)
    {
        foreach (var domainEvent in events)
        {
            await eventbus.PublishAsync(domainEvent, cancellationToken);
        }
    }
}
