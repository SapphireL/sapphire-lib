﻿using SapphireLib.Domain.Entities;

namespace SapphireLib.Domain.Events;

/// <summary>
/// 表示实体添加时发生的事件.
/// </summary>
/// <typeparam name="TEntity">实体类型.</typeparam>
/// <param name="entity">实体.</param>
public class EntityAddedEvent<TEntity>(TEntity entity) : EntityAuditableEvent
     where TEntity : Entity
{
    /// <summary>
    /// 获取实体.
    /// </summary>
    public TEntity Entity { get; } = entity;
}
