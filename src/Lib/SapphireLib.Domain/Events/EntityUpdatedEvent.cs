﻿using SapphireLib.Domain.Entities;

namespace SapphireLib.Domain.Events;

/// <summary>
/// 表示实体更新时发生的事件.
/// </summary>
/// <typeparam name="TEntity">实体类型.</typeparam>
/// <param name="entity">更新后的实体.</param>
/// <param name="previousEntity">更新前的实体.</param>
public class EntityUpdatedEvent<TEntity>(TEntity entity, TEntity previousEntity) : EntityAuditableEvent
       where TEntity : Entity
{
    /// <summary>
    /// 获取实体.
    /// </summary>
    public TEntity Entity { get; } = entity;

    public TEntity PreviousEntity { get; } = previousEntity;
}
