﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Domain.Definitions;
using SapphireLib.Module.Extensions;

namespace SapphireLib.Domain.Extensions;

public static class WebApplicationExtensions
{
    /// <summary>
    /// 扩展方法，用于向 <see cref="WebApplicationBuilder"/> 添加 <see cref="DomainModule"/>。
    /// </summary>
    /// <param name="builder">要扩展的 <see cref="WebApplicationBuilder"/> 实例。</param>
    /// <returns>返回扩展后的 <see cref="WebApplicationBuilder"/> 实例。</returns>
    public static WebApplicationBuilder AddSappLibDomain(this WebApplicationBuilder builder)
    {
        builder.AddSappLib<DomainModule>();
        return builder;
    }
}
