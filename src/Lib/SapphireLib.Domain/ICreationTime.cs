﻿namespace SapphireLib.Domain;

public interface ICreationTime
{
    public DateTime CreationTime { get; }
}
