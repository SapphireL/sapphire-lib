﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.Domain;

public interface IDomainEvent : IEventContent
{
}
