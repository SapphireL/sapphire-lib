﻿namespace SapphireLib.Domain;

public interface IDomainEventBus
{
    Task DispatchDomainEventsAsync(List<IDomainEvent> events, CancellationToken cancellationToken = default);
}
