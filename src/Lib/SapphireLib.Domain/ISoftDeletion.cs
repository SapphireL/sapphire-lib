﻿namespace SapphireLib.Domain;

public interface ISoftDeletion
{
    public bool IsDeleted { get; }
}
