﻿using SapphireLib.Module.Core;

namespace SapphireLib.Module.BusinessModule.Extensions;

public static class ModuleFeatureConfigurationExtension
{
    public static void AddDomainEventBusFeature<TModule>(this ModuleBuilder<TModule> builder)
        where TModule : BaseModule, new()
    {
    }
}
