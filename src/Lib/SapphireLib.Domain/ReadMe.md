# 领域驱动模型

## 概念

### Entity

Entity 是一个聚合根，它具有唯一标识，并且具有生命周期。

1. __唯一标识：__ SapphireLib中以数据库主键为Entity的唯一标识。
    > __注意__ ：有的设计者倾向于使用用户名、电子邮件等，他们认为这些在系统中是唯一的。但是虽然他们是唯一的，但是在以后的业务中可能是会更改的。
2. __状态变化：__ Entity的属性是可变的，但是Entity的唯一标识（Id）是不可变的。
3. __生命周期：__ Entity的生命周期是可变的，Entity可以创建、更新、删除。
    > __注意__ ：在SapphireLib中，可以通过继承接口来实现快速添加属性。

### ValueObject

`ValueObject` 是一种没有唯一标识符的对象，完全由其属性的值来定义。如果两个值对象的所有属性都相同，那么它们被认为是同一个值对象。值对象通常是不可变的，也就是说，一旦创建，不能被修改。

1. __没有唯一标识：__ 值对象不需要唯一标识符来区分它们。它们的身份完全取决于其属性的值。如果两个值对象的属性完全相同，它们就被视为相等。
2. __不可变性：__ 一旦创建就不应该被修改。如果需要改变其属性的值，创建一个新的值对象实例来实现。
3. __用于描述属性：__ 用来描述实体的某些属性或状态。在系统中可以重复使用。
4. __简化比较：__ 不需要考虑唯一标识符，只要比较它们的属性即可。

> __注意__ ：
>
>- 在SapphireLib中，我们约定使用`record`来表示值对象。因为`record`天然的支持不可变性和按值比较。
>- 该如何抉择Entity还是值对象？
>
>    以订单`Address`为例，如果你不关心地址历史变化，且地址的修改只是一个替换行为，可以将 `Address` 作为值对象。如果你需要跟踪地址的变化或保留它的历史记录，`Address` 应该作为实体。实体的身份在生命周期中保持不变，而属性则可以变化。

### 代码规范

1. 实体（派生至`Entity`）的属性`setter`应该为`private`。确保外部实体无法直接更改属性内容。

    如

    ```csharp
    public class Customer: Entity<int>
    {
        public Customer(string name) //通过构造函数创建实体
        {
            Name = name;
        }

        public static Customer Create(string name) //通过静态方法创建实体
        {
            return new Customer(name);
        } 

        public string Name { get; private set; } = string.Empty; //应保持setter为private

        public void UpdateName(string name) //如果需要更改，对外暴露更改方法。
        {
            Name = name;
        }
    }
    ```

2. 定义值对象需要继承`record`。

    如

    ```csharp
    public record Address(string Street, string City, string PostalCode);
    ```

### API

- `Entity`：只包含领域事件集合`IReadOnlyCollection<IDomainEvent>`的实体
  - `AddDomainEvent`：添加领域事件。
  - `ClearDomainEvents` ：清空领域事件。
- `EntityId<TKey>`：包含主键的实体。
- `ISoftDeletion` ：`IsDeleted` 软删除。
- `ICreationTime` ：`CreationTime` 创建时间。
