﻿namespace SapphireLib.EventBus.API.Definitions
{
    public class EventBusAPIFeatureOptions
    {
        public bool DisabledAuthorization { get; set; } = false;
    }
}
