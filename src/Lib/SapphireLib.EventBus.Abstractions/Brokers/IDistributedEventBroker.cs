﻿using SapphireLib.EventBus.Abstractions.Outbox;

namespace SapphireLib.EventBus.Abstractions.Brokers
{
    public interface IDistributedEventBroker
    {
        Task PublishAsync(OutboxMessage message);
    }
}
