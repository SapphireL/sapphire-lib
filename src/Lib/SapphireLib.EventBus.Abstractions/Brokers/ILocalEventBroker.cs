﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.EventBus.Abstractions.Brokers
{
    public interface ILocalEventBroker
    {
        public Task PublishAsync<T>(T @event, CancellationToken cancellationToken = default)
            where T : IEventContent;
    }
}
