﻿namespace SapphireLib.EventBus.Abstractions.Consumers
{
    public sealed class ConsumerMessage
    {
        public Guid MessageId { get; init; }

        public string Name { get; init; }

        public ConsumerMessage(Guid messageId, string name)
        {
            MessageId = messageId;
            Name = name;
        }
    }
}
