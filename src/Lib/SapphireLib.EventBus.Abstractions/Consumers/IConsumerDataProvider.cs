﻿namespace SapphireLib.EventBus.Abstractions.Consumers
{
    public interface IConsumerDataProvider
    {
        Task<bool> ConsumerExistsAsync(ConsumerMessage message);

        Task InsertConsumerAsync(ConsumerMessage message);
    }
}
