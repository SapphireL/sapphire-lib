﻿namespace SapphireLib.EventBus.Abstractions.Consumers;

public interface IConsumerMessageDbTableCreator
{
    public Task CreateTable();
}
