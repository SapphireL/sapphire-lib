﻿namespace SapphireLib.EventBus.Abstractions.EventBus
{
    public interface IEventBusDbCreator
    {
        public Task CreateDbAsync();
    }
}
