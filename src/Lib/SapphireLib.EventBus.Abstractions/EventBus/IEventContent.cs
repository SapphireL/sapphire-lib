﻿namespace SapphireLib.EventBus.Abstractions.EventBus
{
    public interface IEventContent
    {
        public Guid Id { get; }

        public DateTime OccurredOn { get; }
    }
}
