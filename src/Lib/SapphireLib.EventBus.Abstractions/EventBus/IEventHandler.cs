﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.EventBus.EventBus;

public interface IEventHandler<in TEvent> : IEventHandler
    where TEvent : IEventContent
{
    Task HandleAsync(TEvent @event, CancellationToken cancellationToken = default);
}

public interface IEventHandler
{
    Task HandleAsync(IEventContent @event, CancellationToken cancellationToken = default);
}