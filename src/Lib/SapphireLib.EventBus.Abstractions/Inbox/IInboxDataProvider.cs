﻿namespace SapphireLib.EventBus.Abstractions.Inbox
{
    public interface IInboxDataProvider
    {
        Task<IReadOnlyCollection<InboxMessage>> GetInboxMessagesAsync(int limit);

        Task<int> DeleteInboxMessagesAsync(IEnumerable<Guid> ids);

        Task<bool> DeleteInboxMessageAsync(Guid id);

        Task<bool> UpdateInboxMessageAsync(InboxMessage content);

        Task<int> InsertInboxMessageAsync(InboxMessage content);
    }
}
