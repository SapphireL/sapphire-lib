﻿namespace SapphireLib.EventBus.Abstractions.Inbox
{
    public sealed class InboxMessage
    {
        public InboxMessage(Guid id, string type, string content, DateTime occurredOn)
        {
            Id = id;
            Type = type;
            Content = content;
            OccurredOn = occurredOn;
        }

        public Guid Id { get; init; }

        public InboxMessageStatus Status { get; private set; } = InboxMessageStatus.Waiting;

        public InboxMessageResult? Result { get; private set; }

        public string Type { get; init; } = default!;

        public string Content { get; init; } = default!;

        public DateTime OccurredOn { get; init; } = default!;

        public DateTime CreatedOn { get; init; } = DateTime.Now;

        public DateTime? StartedOn { get; private set; }

        public DateTime? FinishedOn { get; private set; }

        public int RetryAttempts { get; set; } = 0;

        public string? Error { get; private set; }

        public void Start()
        {
            StartedOn = DateTime.Now;
            Status = InboxMessageStatus.Processing;
        }

        public void Retry(string error)
        {
            RetryAttempts++;
            Status = InboxMessageStatus.Processing;
            Error = error;
        }

        public void Finished(InboxMessageResult result, string? error = null)
        {
            FinishedOn = DateTime.Now;
            Status = InboxMessageStatus.Finished;
            Result = result;
            Error = error;
        }
    }
}
