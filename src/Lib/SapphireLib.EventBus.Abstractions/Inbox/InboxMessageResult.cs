﻿namespace SapphireLib.EventBus.Abstractions.Inbox
{
    public enum InboxMessageResult
    {
        /// <summary>
        /// 成功.
        /// </summary>
        Success,

        /// <summary>
        /// 失败.
        /// </summary>
        Failed,
    }
}
