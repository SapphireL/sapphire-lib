﻿namespace SapphireLib.EventBus.Abstractions.Inbox
{
    public enum InboxMessageStatus
    {
        /// <summary>
        /// 等待执行.
        /// </summary>
        Waiting = 0,

        /// <summary>
        /// 处理中.
        /// </summary>
        Processing = 1,

        /// <summary>
        /// 完成.
        /// </summary>
        Finished = 3
    }
}
