﻿namespace SapphireLib.EventBus.Abstractions.Inbox
{
    public enum InboxMessageType
    {
        /// <summary>
        /// 本地.
        /// </summary>
        Local = 0,

        /// <summary>
        /// 分布式.
        /// </summary>
        Distributed = 1
    }
}
