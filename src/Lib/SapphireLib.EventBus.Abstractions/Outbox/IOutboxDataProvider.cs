﻿namespace SapphireLib.EventBus.Abstractions.Outbox
{
    public interface IOutboxDataProvider
    {
        Task<IReadOnlyCollection<OutboxMessage>> GetOutboxEventsAsync(int limit);

        Task<int> DeleteOutboxEventsAsync(IEnumerable<Guid> ids);

        Task<bool> DeleteOutboxEventAsync(Guid id);

        Task<bool> UpdateOutboxEventAsync(OutboxMessage content);

        Task<int> InsertOutboxEventAsync(OutboxMessage content);
    }
}
