﻿namespace SapphireLib.EventBus.Abstractions.Outbox;

public interface IOutboxDbTableCreator
{
    public Task CreateTableAsync();
}
