﻿namespace SapphireLib.EventBus.Abstractions.Outbox
{
    public sealed class OutboxMessage
    {
        public OutboxMessage(Guid id, string type, string content, DateTime occurredOn, OutboxMessageType messageType)
        {
            Id = id;
            Type = type;
            Content = content;
            OccurredOn = occurredOn;
            MessageType = messageType;
        }

        public OutboxMessage()
        {
        }

        public Guid Id { get; init; }

        public OutboxMessageStatus Status { get; private set; } = OutboxMessageStatus.Waiting;

        public OutboxMessageResult? Result { get; private set; }

        public string Type { get; init; } = default!;

        public OutboxMessageType MessageType { get; init; }

        public string Content { get; init; } = default!;

        public DateTime OccurredOn { get; init; } = default!;

        public DateTime CreatedOn { get; init; } = DateTime.Now;

        public DateTime? StartedOn { get; private set; }

        public DateTime? FinishedOn { get; private set; }

        public int RetryAttempts { get; set; } = 0;

        public string? Error { get; private set; }

        public void Start()
        {
            StartedOn = DateTime.Now;
            Status = OutboxMessageStatus.Processing;
        }

        public void Retry(string error)
        {
            RetryAttempts++;
            Status = OutboxMessageStatus.Processing;
            Error = error;
        }

        public void Finished(OutboxMessageResult result, string? error = null)
        {
            FinishedOn = DateTime.Now;
            Status = OutboxMessageStatus.Finished;
            Result = result;
            Error = error;
        }
    }
}
