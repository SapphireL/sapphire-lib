﻿namespace SapphireLib.EventBus.Abstractions.Outbox
{
    public enum OutboxMessageResult
    {
        /// <summary>
        /// 成功.
        /// </summary>
        Success,

        /// <summary>
        /// 失败.
        /// </summary>
        Failed,
    }
}
