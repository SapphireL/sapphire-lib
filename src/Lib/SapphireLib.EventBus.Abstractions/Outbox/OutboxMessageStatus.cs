﻿namespace SapphireLib.EventBus.Abstractions.Outbox
{
    public enum OutboxMessageStatus
    {
        /// <summary>
        /// 等待执行.
        /// </summary>
        Waiting = 0,

        /// <summary>
        /// 处理中.
        /// </summary>
        Processing = 1,

        /// <summary>
        /// 完成.
        /// </summary>
        Finished = 3
    }
}
