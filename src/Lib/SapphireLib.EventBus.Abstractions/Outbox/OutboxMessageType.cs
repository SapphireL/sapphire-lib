﻿namespace SapphireLib.EventBus.Abstractions.Outbox
{
    public enum OutboxMessageType
    {
        /// <summary>
        /// 本地.
        /// </summary>
        Local = 0,

        /// <summary>
        /// 分布式.
        /// </summary>
        Distributed = 1
    }
}
