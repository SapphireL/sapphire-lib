﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireLib.EventBus.Abstractions.Consumers;

namespace SapphireLib.EventBus.EFCore.Configurations
{
    internal sealed class ConsumerMessageConfiguration(string tableName, string schema) : IEntityTypeConfiguration<ConsumerMessage>
    {
        public void Configure(EntityTypeBuilder<ConsumerMessage> builder)
        {
            builder.ToTable(tableName, schema);

            builder.HasKey(o => new { o.MessageId, o.Name });

            builder.Property(o => o.Name).HasMaxLength(500);
        }
    }
}
