﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireLib.EventBus.Abstractions.Outbox;

namespace SapphireLib.EventBus.EFCore.Configurations
{
    internal sealed class OutboxEventConfiguration(string tableName, string schema) : IEntityTypeConfiguration<OutboxMessage>
    {
        public void Configure(EntityTypeBuilder<OutboxMessage> builder)
        {
            builder.ToTable(tableName, schema);

            builder.HasKey(o => o.Id);

            builder.Property(o => o.Content);
        }
    }
}
