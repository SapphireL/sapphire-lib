﻿using SapphireLib.Repository.EFCore.Definitions;

namespace SapphireLib.EventBus.EFCore.Definitions
{
    public class EventBusEFCoreOptions : EFCoreOptions<EventBusDbContext>
    {
        public string OutboxEventTableName { get; set; } = "OutboxEvents";

        public string ConsumerMessageTableName { get; set; } = "ConsumerMessages";
    }
}
