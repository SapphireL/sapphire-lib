﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.EFCore.Definitions
{
    public class EventBusPersistenceEFCoreFeature : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            var options = context.GetRequiredOptions<OutboxFeatureOptions>();

            var efCoreOptions = new EventBusEFCoreOptions();

            // var options = context.GetRequiredOptions<EFCoreOptions>();
            context.Services.AddSingleton(efCoreOptions);
            var configOptions = context.GetRequiredOptions<OutboxFeatureOptions>();
            context.Services.AddDbContext<EventBusDbContext>();
        }

        protected override void OnRunning(RunningContext context)
        {
            using var scope = context.WebApplication.Services.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<EventBusDbContext>();
            var s = dbContext.Database.GetConnectionString();
            dbContext.Database.EnsureCreatedAsync().Wait();

            // dbContext.Database.EnsureCreatedAsync().Wait();
        }
    }
}
