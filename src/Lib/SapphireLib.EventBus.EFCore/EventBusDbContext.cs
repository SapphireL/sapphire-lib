﻿using Microsoft.EntityFrameworkCore;

using SapphireLib.EventBus.EFCore.Definitions;
using SapphireLib.EventBus.EFCore.Extensions;
using SapphireLib.Repository.EFCore;

namespace SapphireLib.EventBus.EFCore
{
    public class EventBusDbContext : BaseDbContext<EventBusDbContext>
    {
        private readonly EventBusEFCoreOptions _options;

        public EventBusDbContext(EventBusEFCoreOptions options)
            : base(options)
        {
            _options = options;
        }

        protected EventBusDbContext()
            : base()
        {
            _options = new EventBusEFCoreOptions();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureEventBus(_options.OutboxEventTableName, _options.ConsumerMessageTableName, _options.Schema ?? "SapphireLib");
        }
    }
}
