﻿using SapphireLib.EventBus.EFCore.Definitions;
using SapphireLib.Repository.EFCore;

namespace SapphireLib.EventBus.EFCore;

public class EventBusDesignTimeDbContextFactory : BaseDesignTimeDbContextFactory<EventBusDbContext, EventBusEFCoreOptions>
{
    public override EventBusDbContext CreateDbContext(string[] args)
    {
        var context = base.CreateDbContext(args);

        return context;
    }
}
