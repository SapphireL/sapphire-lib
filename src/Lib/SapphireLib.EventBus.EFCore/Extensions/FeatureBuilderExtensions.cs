﻿using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EFCore.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.EFCore.Extensions
{
    public static class FeatureBuilderExtensions
    {
        /// <summary>
        /// 创建数据库.
        /// 执行数据库迁移文件.
        /// 线上版本请不要开启.
        /// 如需更新数据库，请手动执行迁移命令.
        /// </summary>
        /// <param name="featureBuilder">featureBuilder.</param>
        public static void EnsureDatabaseCreated(this FeatureBuilder<OutboxFeature> featureBuilder)
        {
            featureBuilder.WithFeature<EventBusPersistenceEFCoreFeature>();
        }
    }
}
