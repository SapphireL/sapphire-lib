﻿using Microsoft.EntityFrameworkCore;

using SapphireLib.EventBus.EFCore.Configurations;

namespace SapphireLib.EventBus.EFCore.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void ConfigureEventBus(this ModelBuilder modelBuilder, string eventBusTableName, string consumerMessageTableName, string schema)
        {
            modelBuilder.ApplyConfiguration(new OutboxEventConfiguration(eventBusTableName, schema));
            modelBuilder.ApplyConfiguration(new ConsumerMessageConfiguration(consumerMessageTableName, schema));
        }
    }
}
