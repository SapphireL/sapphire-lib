﻿using Dapper;

using Microsoft.Extensions.Options;

using SapphireLib.EventBus.Abstractions.Consumers;
using SapphireLib.EventBus.SqlServer.Definitions;

namespace SapphireLib.EventBus.SqlServer.Database
{
    internal class ConsumerMessagesDbTableCreator(IOptions<EventBusSqlServerOptions> options, EventBusConnectionClient client) : IConsumerMessageDbTableCreator
    {
        public async Task CreateTable()
        {
            var sql = $@"
            USE [{options.Value.DatabaseName}]
            IF NOT EXISTS (
                SELECT * FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = '{options.Value.Schema}' 
                AND TABLE_NAME = '{options.Value.ConsumerMessageTableName}')
            BEGIN

            SET ANSI_NULLS ON
            SET QUOTED_IDENTIFIER ON
            CREATE TABLE [{options.Value.Schema}].[{options.Value.ConsumerMessageTableName}](
	            [MessageId] [uniqueidentifier] NOT NULL,
	            [Name] [nvarchar](500) NOT NULL,
             CONSTRAINT [PK_ConsumerMessages] PRIMARY KEY CLUSTERED 
            (
	            [MessageId] ASC,
	            [Name] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
            ) ON [PRIMARY]

            END";

            using var connection = client.GetOpenConnection();

            // EventBusModule.Logger.Information("Event bus consumer message table is creating.");
            // EventBusModule.Logger.Information($"database:{options.Value.DatabaseName}");
            // EventBusModule.Logger.Information($"schema:{options.Value.Schema}");
            // EventBusModule.Logger.Information($"table:{options.Value.ConsumerMessageTableName}");
            await connection.ExecuteAsync(sql);

            // EventBusModule.Logger.Information("Event bus outbox table has been created.");
        }
    }
}
