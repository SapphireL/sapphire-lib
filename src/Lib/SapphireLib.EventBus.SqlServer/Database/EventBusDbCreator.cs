﻿using Dapper;

using Microsoft.Extensions.Options;

using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.SqlServer.Definitions;

namespace SapphireLib.EventBus.SqlServer.Database
{
    internal class EventBusDbCreator(IOptions<EventBusSqlServerOptions> options, EventBusConnectionClient client) : IEventBusDbCreator
    {
        public async Task CreateDbAsync()
        {
            var sql = $@"
        IF NOT EXISTS (
            SELECT name FROM master.dbo.sysdatabases WHERE name = '{options.Value.DatabaseName}')
        BEGIN
            CREATE DATABASE [{options.Value.DatabaseName}];
        END

        USE [{options.Value.DatabaseName}];
        IF NOT EXISTS (
            SELECT * 
            FROM sys.schemas 
            WHERE name = '{options.Value.Schema}'
        )
        BEGIN
            EXEC('CREATE SCHEMA [{options.Value.Schema}]');
        END;
        ";

            using var connection = client.GetOpenConnection();

            // module.InternalLogger.Information("Event bus database is creating.");
            // module.InternalLogger.Information($"database:{options.Value.DatabaseName}");
            await connection.ExecuteAsync(sql);

            // module.InternalLogger.Information("Event bus database has been created.");
        }
    }
}
