﻿using Dapper;

using Microsoft.Extensions.Options;

using SapphireLib.EventBus.Abstractions.Outbox;
using SapphireLib.EventBus.SqlServer.Definitions;

namespace SapphireLib.EventBus.SqlServer.Database
{
    internal class OutboxDbTableCreator(IOptions<EventBusSqlServerOptions> options, EventBusConnectionClient client) : IOutboxDbTableCreator
    {
        public async Task CreateTableAsync()
        {
            var sql = $@"
            USE [{options.Value.DatabaseName}]
            IF NOT EXISTS (
                SELECT * FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = '{options.Value.Schema}' 
                AND TABLE_NAME = '{options.Value.OutboxEventTableName}')
            BEGIN

            SET ANSI_NULLS ON
            SET QUOTED_IDENTIFIER ON
            CREATE TABLE [{options.Value.Schema}].[{options.Value.OutboxEventTableName}](
	        [Id] [uniqueidentifier] NOT NULL,
	        [Status] [int] NOT NULL,
	        [Result] [int] NULL,
	        [Type] [nvarchar](max) NOT NULL,
	        [MessageType] [int] NOT NULL,
	        [Content] [nvarchar](max) NOT NULL,
	        [OccurredOn] [datetime2](7) NOT NULL,
	        [CreatedOn] [datetime2](7) NOT NULL,
	        [StartedOn] [datetime2](7) NULL,
	        [FinishedOn] [datetime2](7) NULL,
	        [RetryAttempts] [int] NOT NULL,
	        [Error] [nvarchar](max) NULL,
             CONSTRAINT [PK_OutboxEvents] PRIMARY KEY CLUSTERED 
            (
	            [Id] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
            END";

            using var connection = client.GetOpenConnection();

            // module.InternalLogger.Information("Event bus outbox table is creating.");
            // EventBusModule.Logger.Information($"database:{options.Value.DatabaseName}");
            // EventBusModule.Logger.Information($"schema:{options.Value.Schema}");
            // EventBusModule.Logger.Information($"table:{options.Value.OutboxEventTableName}");
            await connection.ExecuteAsync(sql);

            // EventBusModule.Logger.Information("Event bus outbox table has been created.");
        }
    }
}
