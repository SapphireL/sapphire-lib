﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.ADO.Extensions;
using SapphireLib.ADO.SqlServer.Extensions;
using SapphireLib.EventBus.Abstractions.Consumers;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Abstractions.Outbox;
using SapphireLib.EventBus.SqlServer.Database;
using SapphireLib.EventBus.SqlServer.Providers;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.SqlServer.Definitions
{
    internal class EventBusPersistenceSqlServerFeature : BaseFeature
    {
        protected override void OnConfiguring(FeaturesBuilder builder)
        {
            var options = builder.Context.GetRequiredOptions<EventBusSqlServerOptions>();
            builder.AddADOClient(config =>
            {
                config.UseSqlServer<EventBusConnectionClient, EventBusConnectionClientOptions>(config =>
                {
                    config.ConnectionString = options.ConnectionString;
                    config.DatabaseName = options.DatabaseName;
                    config.Schema = options.Schema;
                });
            });
        }

        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<IOutboxDataProvider, SqlServerOutboxDataProvider>();
            context.Services.AddTransient<IConsumerDataProvider, SqlServerConsumerDataProvider>();
            context.Services.AddScoped<IOutboxDbTableCreator, OutboxDbTableCreator>();
            context.Services.AddScoped<IConsumerMessageDbTableCreator, ConsumerMessagesDbTableCreator>();
            context.Services.AddScoped<IEventBusDbCreator, EventBusDbCreator>();
        }
    }
}
