﻿namespace SapphireLib.EventBus.SqlServer.Definitions;

/// <summary>
/// 事件总线SqlServer数据库配置。
/// </summary>
public class EventBusSqlServerOptions
{
    public string ConnectionString { get; set; } = default!;

    public string DatabaseName { get; set; } = default!;

    public string OutboxEventTableName { get; set; } = "OutboxEvents";

    public string ConsumerMessageTableName { get; set; } = "ConsumerMessages";

    public string Schema { get; set; } = "SapphireLib";
}
