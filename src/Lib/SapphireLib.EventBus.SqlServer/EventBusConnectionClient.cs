﻿using Microsoft.Extensions.Options;

using SapphireLib.ADO.SqlServer;

namespace SapphireLib.EventBus.SqlServer
{
    internal class EventBusConnectionClient(IOptions<EventBusConnectionClientOptions> options) : SqlServerConnectionClient(options)
    {
        public string OutboxEventTableName { get; set; } = "OutboxEvents";

        public string ConsumerMessageTableName { get; set; } = "ConsumerMessages";
    }
}
