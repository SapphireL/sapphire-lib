﻿using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.SqlServer.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.SqlServer.Extensions;

public static class FeatureBuilderExtensions
{
    /// <summary>
    /// 使用 SQL Server 作为事件总线的持久化存储。
    /// </summary>
    /// <param name="builder">特性构建器实例。</param>
    /// <param name="options">SQL Server 选项配置的委托。</param>
    public static void UseSqlServer(this FeatureBuilder<OutboxFeature> builder, Action<EventBusSqlServerOptions> options)
    {
        builder.WithFeature<EventBusPersistenceSqlServerFeature, EventBusSqlServerOptions>(options); // 注册 SQL Server 持久化特性
    }

    /// <summary>
    /// 使用 SQL Server 作为事件总线的持久化存储。
    /// </summary>
    /// <param name="builder">模块构建器实例。</param>
    /// <param name="options">SQL Server 选项配置的委托。</param>
    public static void UseSqlServer(this ModuleBuilder<EventBusModule> builder, Action<EventBusSqlServerOptions> options)
    {
        builder.WithFeature<EventBusPersistenceSqlServerFeature, EventBusSqlServerOptions>(options); // 注册 SQL Server 持久化特性
    }

    /// <summary>
    /// 使用 SQL Server 作为事件总线的持久化存储。
    /// </summary>
    /// <remarks>
    /// 使用配置节点为 <code>EventBus:SqlServer</code>。
    /// </remarks>
    /// <param name="builder">模块构建器实例。</param>
    public static void UseSqlServer(this ModuleBuilder<EventBusModule> builder)
    {
        builder.WithFeature<EventBusPersistenceSqlServerFeature, EventBusSqlServerOptions>("EventBus:SqlServer"); // 注册 SQL Server 持久化特性
    }

    /// <summary>
    /// 使用 SQL Server 作为事件总线的持久化存储。
    /// </summary>
    /// <param name="builder">模块构建器实例。</param>
    /// <param name="section">配置节的名称。</param>
    public static void UseSqlServer(this ModuleBuilder<EventBusModule> builder, string section)
    {
        builder.WithFeature<EventBusPersistenceSqlServerFeature, EventBusSqlServerOptions>(section); // 注册 SQL Server 持久化特性
    }

    /// <summary>
    /// 使用 SQL Server 作为事件总线的持久化存储。
    /// </summary>
    /// <param name="builder">模块构建器实例。</param>
    /// <param name="section">配置节的名称。</param>
    /// <param name="options">SQL Server 选项配置的可选委托。</param>
    public static void UseSqlServer(this ModuleBuilder<EventBusModule> builder, string section = "EventBus:SqlServer", Action<EventBusSqlServerOptions>? options = null)
    {
        builder.WithFeature<EventBusPersistenceSqlServerFeature, EventBusSqlServerOptions>(section, options); // 注册 SQL Server 持久化特性
    }
}