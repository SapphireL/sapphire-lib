﻿using Dapper;

using SapphireLib.EventBus.Abstractions.Consumers;

namespace SapphireLib.EventBus.SqlServer.Providers
{
    internal sealed class SqlServerConsumerDataProvider(EventBusConnectionClient client) : IConsumerDataProvider
    {
        private readonly EventBusConnectionClient _client = client;

        public async Task<bool> ConsumerExistsAsync(ConsumerMessage message)
        {
            const string sql =
            """
            SELECT CASE 
                WHEN EXISTS(
                    SELECT 1
                    FROM message_consumers
                    WHERE MessageId = @MessageId AND
                          Name = @Name
                ) 
                THEN CAST(1 AS BIT) 
                ELSE CAST(0 AS BIT) 
            END AS ExistsResult;
            """;
            using var connection = _client.GetOpenConnection();
            return await connection.ExecuteScalarAsync<bool>(sql, new
            {
                message.MessageId,
                message.Name
            });
        }

        public async Task InsertConsumerAsync(ConsumerMessage message)
        {
            const string sql =
            """
            INSERT INTO message_consumers(MessageId, Name)
            VALUES (@MessageId, @Name)
            """;
            using var connection = _client.GetOpenConnection();
            await connection.ExecuteAsync(sql, new
            {
                message.MessageId,
                message.Name
            });
        }
    }
}
