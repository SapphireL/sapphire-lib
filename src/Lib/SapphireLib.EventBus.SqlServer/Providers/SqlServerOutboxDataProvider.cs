﻿using Dapper;

using SapphireLib.EventBus.Abstractions.Outbox;

namespace SapphireLib.EventBus.SqlServer.Providers
{
    internal sealed class SqlServerOutboxDataProvider(EventBusConnectionClient client) : IOutboxDataProvider
    {
        private readonly EventBusConnectionClient _client = client;

        public async Task<bool> DeleteOutboxEventAsync(Guid id)
        {
            var sql =
               $"""
                DELETE {_client.Schema}.{_client.OutboxEventTableName}
                WHERE id = @id
                """;

            using var connection = _client.GetOpenConnection();
            return await connection.ExecuteAsync(sql, new { id }) > 0;
        }

        public async Task<int> DeleteOutboxEventsAsync(IEnumerable<Guid> ids)
        {
            var sql =
               $"""
                DELETE {_client.Schema}.{_client.OutboxEventTableName}
                WHERE id in @ids
                """;

            using var connection = _client.GetOpenConnection();
            return await connection.ExecuteAsync(sql, new { ids });
        }

        public async Task<IReadOnlyCollection<OutboxMessage>> GetOutboxEventsAsync(int limit)
        {
            var sql =
            $"""
             SELECT TOP {limit}
                [Id],
                [Type],
                [Content],
                [OccurredOn],
                [MessageType],
                [RetryAttempts]
             FROM {_client.Schema}.{_client.OutboxEventTableName}
             WHERE [Status] in (0,1) and [Result] is NULL
             ORDER BY OccurredOn
             """;

            using var connection = _client.GetOpenConnection();
            return (await connection.QueryAsync<OutboxMessage>(sql, new { limit })).ToList();
        }

        public async Task<int> InsertOutboxEventAsync(OutboxMessage content)
        {
            var sql = $"""
                INSERT INTO {_client.Schema}.{_client.OutboxEventTableName}(id, status, type, content, createdOn, occurredOn, messageType, retryAttempts)
                VALUES (@id, @status, @type, @content, @createdOn, @occurredOn, @messageType, 0)
            
                """
            ;

            using var connection = _client.GetOpenConnection();
            return await connection.ExecuteAsync(sql, new
            {
                id = content.Id,
                status = content.Status,
                type = content.Type,
                content = content.Content,
                createdOn = content.CreatedOn,
                occurredOn = content.OccurredOn,
                messageType = content.MessageType
            });
        }

        public async Task<bool> UpdateOutboxEventAsync(OutboxMessage content)
        {
            var sql = $"""
                UPDATE {_client.Schema}.{_client.OutboxEventTableName}
                SET FinishedOn = @finishedOn,
                    RetryAttempts = @retryAttempts,
                    StartedOn = @startedOn,
                    Status = @status,
                    Error = @error,
                    Result = @result
                WHERE id = @id
                """
            ;

            using var connection = _client.GetOpenConnection();
            return await connection.ExecuteAsync(
            sql,
            new
            {
                id = content.Id,
                finishedOn = content.FinishedOn,
                retryAttempts = content.RetryAttempts,
                startedOn = content.StartedOn,
                status = content.Status,
                result = content.Result,
                error = content.Error
            }) > 0;
        }
    }
}
