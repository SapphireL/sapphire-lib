﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.Outbox;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.Sqlite.Definitions
{
    public class EventBusPersistenceSqliteFeature : BaseFeature
    {
        protected override void OnConfiguring(FeaturesBuilder builder)
        {
        }

        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<IOutboxDataProvider, SqlitePersistenceProvider>();
        }
    }
}
