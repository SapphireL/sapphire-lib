﻿using Microsoft.Extensions.Options;

using SapphireLib.ADO.Sqlite;

namespace SapphireLib.EventBus.Sqlite
{
    internal class EventBusConnectionClient(IOptions<SqliteOptions> options) : SqliteConnectionClient(options)
    {
    }
}
