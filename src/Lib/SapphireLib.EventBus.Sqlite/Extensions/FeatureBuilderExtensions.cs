﻿using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.Sqlite.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.Sqlite.Extensions
{
    public static class FeatureBuilderExtensions
    {
        public static void UseSqlite(this FeatureBuilder<OutboxFeature> builder)
        {
            builder.WithFeature<EventBusPersistenceSqliteFeature>();
        }
    }
}
