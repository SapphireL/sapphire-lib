﻿using Dapper;

using SapphireLib.EventBus.Abstractions.Outbox;

namespace SapphireLib.EventBus.Sqlite
{
    internal class SqlitePersistenceProvider(EventBusConnectionClient client) : IOutboxDataProvider
    {
        private readonly EventBusConnectionClient _client = client;

        public async Task<bool> DeleteOutboxEventAsync(Guid id)
        {
            var sql =
               $"""
                DELETE {_client.Schema}outbox_events
                WHERE id = @id
                """;

            using var connection = _client.GetOpenConnection();
            return (await connection.ExecuteAsync(sql, new { id })) > 0;
        }

        public async Task<int> DeleteOutboxEventsAsync(IEnumerable<Guid> ids)
        {
            var sql =
               $"""
                DELETE {_client.Schema}outbox_events
                WHERE id in @ids
                """;

            using var connection = _client.GetOpenConnection();
            return await connection.ExecuteAsync(sql, new { ids });
        }

        public async Task<IReadOnlyCollection<OutboxMessage>> GetOutboxEventsAsync(int limit)
        {
            var sql =
            $"""
             SELECT
                id AS id,
                type AS type,
                content AS content,
                occurredTime AS occurredTime,
                messageType AS messageType
             FROM {_client.Schema}outbox_events
             WHERE status = 0
             ORDER BY OccurredTime
             LIMIT @limit
             """;

            using var connection = _client.GetOpenConnection();
            return (await connection.QueryAsync<OutboxMessage>(sql, new { limit })).ToList();
        }

        public async Task<int> InsertOutboxEventAsync(OutboxMessage content)
        {
            var sql = $"""
                INSERT INTO {_client.Schema}outbox_events(id, status, type, content, addTime, occurredTime, messageType)
                VALUES (@id, @status, @type, @content, @addTime, @occurredTime, @messageType)
            
                """
            ;

            using var connection = _client.GetOpenConnection();
            return await connection.ExecuteAsync(sql, new
            {
                id = content.Id,
                status = content.Status,
                type = content.Type,
                content = content.Content,
                addTime = content.CreatedOn,
                occurredTime = content.OccurredOn,
                messageType = content.MessageType
            });
        }

        public async Task<bool> UpdateOutboxEventAsync(OutboxMessage content)
        {
            var sql = $"""
                UPDATE {_client.Schema}outbox_events
                SET FinishedTime = @finishedTime,
                    RetryAttempts = @retryAttempts,
                    StartTime = @startTime
                    Status = @status,
                    Error = @error,
                    Result = @result
                WHERE id = @id
                """
            ;

            using var connection = _client.GetOpenConnection();
            return (await connection.ExecuteAsync(
            sql,
            new
            {
                id = content.Id,
                finishedTime = content.FinishedOn,
                retryAttempts = content.RetryAttempts,
                startTime = content.StartedOn,
                status = content.Status,
                result = content.Result,
                error = content.Error
            })) > 0;
        }
    }
}
