﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.Extensions;
using SapphireLib.Module.Extensions;

namespace SapphireLib.EventBus.Test.EFCoreTests
{
    public class EFCoreTest
    {
        [Fact]
        public void ShouldCreateDatabase()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLibEventBus(configure =>
            {
                configure.AddOutbox();
            });
            var app = builder.Build();
            app.UseSappLib<EventBusModule>();

            Assert.True(File.Exists("test.db"));
        }
    }
}
