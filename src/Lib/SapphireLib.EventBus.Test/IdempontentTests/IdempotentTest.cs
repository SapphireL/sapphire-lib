﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.Extensions;
using SapphireLib.EventBus.Test.IdempontentTests.Prepared;
using SapphireLib.Module.Extensions;

namespace SapphireLib.EventBus.Test.IdempontentTests
{
    public class IdempotentTest
    {
        [Fact]
        public async Task ShouldOnlyExcuteAtLeastOnce()
        {
            var builder = CreateEventBusBuilder();

            builder.AddIdempotentEventHandler<TestEventHandler, TestEvent>();
            var app = builder.Build();

            app.UseSappLib<EventBusModule>();

            // var service = new OutboxProcessor(app.Services.GetRequiredService<IServiceScopeFactory>(), app.Services.GetRequiredService<IOptions<OutboxFeatureOptions>>());
            // await service.StartAsync(CancellationToken.None);
            var eventBus = app.Services.GetRequiredService<IEventBus>();
            await eventBus.PublishAsync(new TestEvent());

            Thread.Sleep(5000);

            // await service.StopAsync(CancellationToken.None);
            Assert.Equal(2, TimeController.RetryTimes);
        }

        [Fact]
        public async Task ShouldOnlyExcuteAtMostOnce()
        {
            var builder = CreateEventBusBuilder();
            builder.AddIdempotentEventHandler<TestEventHandler, TestEvent>();
            builder.AddIdempotentEventHandler<TestEventHandler1, TestEvent>();
            var app = builder.Build();

            // var service = new OutboxProcessor(app.Services.GetRequiredService<IServiceScopeFactory>(), app.Services.GetRequiredService<IOptions<OutboxFeatureOptions>>());
            // await service.StartAsync(CancellationToken.None);
            var eventBus = app.Services.GetRequiredService<IEventBus>();
            await eventBus.PublishAsync(new TestEvent());

            Thread.Sleep(5000);

            // await service.StopAsync(CancellationToken.None);
            Assert.Equal(2, TimeController.RetryTimes);
            Assert.Equal(3, TestEventHandler.ExecutedTimes);
            Assert.Equal(1, TestEventHandler1.ExecutedTimes);
        }

        private WebApplicationBuilder CreateEventBusBuilder()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLibEventBus(configure =>
            {
                configure.AddOutbox();
            });
            return builder;
        }
    }
}
