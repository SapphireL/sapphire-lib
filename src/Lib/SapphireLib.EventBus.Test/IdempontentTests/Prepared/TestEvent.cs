﻿using SapphireLib.EventBus.Attributes;
using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Test.IdempontentTests.Prepared
{
    [OutboxEvent]
    internal class TestEvent : EventContent
    {
        public TestEvent()
            : base(Guid.NewGuid(), DateTime.Now)
        {
        }
    }
}
