﻿using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Test.IdempontentTests.Prepared
{
    internal class TestEventHandler : BaseEventHandler<TestEvent>
    {
        public override async Task HandleAsync(TestEvent @event, CancellationToken cancellationToken = default)
        {
            ExecutedTimes++;
            await Task.CompletedTask;
            if (!TimeController.ShouldSuccess())
            {
                TimeController.RetryTimes++;
                throw new Exception();
            }
        }

        internal static int ExecutedTimes { get; set; } = 0;
    }
}
