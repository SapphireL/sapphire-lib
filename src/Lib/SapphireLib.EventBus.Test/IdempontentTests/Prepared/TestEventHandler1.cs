﻿using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Test.IdempontentTests.Prepared
{
    internal class TestEventHandler1 : BaseEventHandler<TestEvent>
    {
        public override async Task HandleAsync(TestEvent @event, CancellationToken cancellationToken = default)
        {
            await Task.CompletedTask;
            ExecutedTimes++;
        }

        internal static int ExecutedTimes { get; set; } = 0;
    }
}
