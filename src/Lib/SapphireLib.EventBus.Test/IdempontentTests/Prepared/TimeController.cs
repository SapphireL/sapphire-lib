﻿namespace SapphireLib.EventBus.Test.IdempontentTests.Prepared
{
    public static class TimeController
    {
        public static int RetryTimes { get; set; } = 0;

        public static bool ShouldSuccess() => RetryTimes >= 2;
    }
}
