﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.Brokers;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Brokers;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EventBus;
using SapphireLib.EventBus.Test.LocalEventBusTests.Prepared;

namespace SapphireLib.EventBus.Test.LocalEventBusTests
{
    public class EventHandlersTest
    {
        [Fact]
        public async Task Should_GetHandlers()
        {
            var services = new ServiceCollection();
            services.AddTransient<IEventHandler<TestEvent>, TestEventHandler>();
            services.AddTransient<IEventHandler<TestEvent>, TestEventHandler1>();
            var testEvent = new TestEvent();
            var serviceProvider = services.BuildServiceProvider();
            var handlers = EventHandlersHelper.GetHandlers(testEvent, serviceProvider);
            foreach (var handler in handlers)
            {
                await handler.HandleAsync(testEvent);
            }

            Assert.NotNull(handlers);
            Assert.NotEmpty(handlers);
            Assert.Equal(1, testEvent.Count);
        }

        [Fact]
        public async Task Should_PublishEvent()
        {
            var services = new ServiceCollection();
            services.AddTransient<ILocalEventBroker, LocalEventBroker>();
            services.AddOptions<EventBusConfigOptions>();
            services.AddTransient<IEventHandler<TestEvent>, TestEventHandler>();
            services.AddTransient<IEventHandler<TestEvent>, TestEventHandler1>();
            services.AddTransient<IEventBus, BaseEventBus>();

            var testEvent = new TestEvent();
            var serviceProvider = services.BuildServiceProvider();

            var eventBus = serviceProvider.GetRequiredService<IEventBus>();
            await eventBus.PublishAsync(testEvent);

            Assert.Equal(1, testEvent.Count);
            Assert.Equal("Test", testEvent.Value);
        }
    }
}
