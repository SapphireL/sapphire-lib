﻿using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Test.LocalEventBusTests.Prepared
{
    internal class TestEvent : EventContent
    {
        public TestEvent()
            : base(Guid.NewGuid(), DateTime.Now)
        {
        }

        internal int Count { get; set; }

        internal string Value { get; set; } = default!;
    }
}
