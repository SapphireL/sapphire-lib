﻿using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Test.LocalEventBusTests.Prepared
{
    internal class TestEventHandler : BaseEventHandler<TestEvent>
    {
        public override async Task HandleAsync(TestEvent @event, CancellationToken cancellationToken = default)
        {
            @event.Count++;
            await Task.CompletedTask;
        }
    }

    internal class TestEventHandler1 : BaseEventHandler<TestEvent>
    {
        public override async Task HandleAsync(TestEvent @event, CancellationToken cancellationToken = default)
        {
            @event.Value = "Test";
            await Task.CompletedTask;
        }
    }
}
