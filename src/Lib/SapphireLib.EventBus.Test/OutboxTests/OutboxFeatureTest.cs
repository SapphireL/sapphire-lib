﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EventBus;
using SapphireLib.EventBus.Exceptions;
using SapphireLib.EventBus.Extensions;
using SapphireLib.EventBus.Test.OutboxTests.Prepared;
using SapphireLib.Module.Extensions;

namespace SapphireLib.EventBus.Test.OutboxTests
{
    public class OutboxFeatureTest
    {
        [Fact]
        public void ShouldThrowExceptionWhenNoDataProvider()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLibEventBus(e =>
            {
                e.AddOutbox();
            });
            var app = builder.Build();

            Assert.Throws<EventBusDataProviderException>(app.UseSappLib<EventBusModule>);
        }

        [Fact]
        public void ShouldSaveEvent()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLibEventBus(moduleBuilder => moduleBuilder.AddOutbox());

            builder.Services.AddTransient<IEventHandler<TestOutboxEvent>, TestOutboxEventHandler>();

            var app = builder.Build();
        }
    }
}
