﻿using SapphireLib.EventBus.Attributes;
using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Test.OutboxTests.Prepared
{
    [OutboxEvent]
    internal class TestOutboxEvent : EventContent
    {
        public TestOutboxEvent()
          : base(Guid.NewGuid(), DateTime.Now)
        {
        }

        public string Name { get; set; } = "aaa";
    }
}
