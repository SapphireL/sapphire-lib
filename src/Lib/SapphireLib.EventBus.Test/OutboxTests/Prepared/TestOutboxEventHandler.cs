﻿using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Test.OutboxTests.Prepared
{
    internal class TestOutboxEventHandler : BaseEventHandler<TestOutboxEvent>
    {
        public override async Task HandleAsync(TestOutboxEvent @event, CancellationToken cancellationToken = default)
        {
            @event.Name = "sss";
            await Task.CompletedTask;
        }
    }
}
