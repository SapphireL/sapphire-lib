﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EventBus;
using SapphireLib.EventBus.Extensions;
using SapphireLib.EventBus.Test.PersistenceTests.Prepared;
using SapphireLib.Module.Extensions;

namespace SapphireLib.EventBus.Test.PersistenceTests
{
    public class PersistenceTest
    {
        [Fact]
        public async Task ShouldSaveEvent()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLibEventBus(configure =>
            {
                configure.AddOutbox();
            });

            builder.Services.AddTransient<IEventHandler<TestOutboxEvent>, TestOutboxEventHandler>();

            var app = builder.Build();

            app.UseSappLib<EventBusModule>();

            // var service = new OutboxProcessor(app.Services.GetRequiredService<IServiceScopeFactory>(), app.Services.GetRequiredService<IOptions<OutboxFeatureOptions>>());
            // await service.StartAsync(CancellationToken.None);
            var eventBus = app.Services.GetRequiredService<IEventBus>();
            await eventBus.PublishAsync(new TestOutboxEvent
            {
                Name = "aaa"
            });

            Thread.Sleep(1000);

            // await service.StopAsync(CancellationToken.None);
            Assert.True(File.Exists("test.db"));
        }
    }
}
