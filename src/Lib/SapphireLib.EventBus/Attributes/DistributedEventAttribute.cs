﻿namespace SapphireLib.EventBus.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DistributedEventAttribute : Attribute
    {
    }
}
