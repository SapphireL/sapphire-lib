﻿namespace SapphireLib.EventBus.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class OutboxEventAttribute : Attribute
    {
    }
}
