﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.Brokers;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EventBus;

namespace SapphireLib.EventBus.Brokers
{
    internal class LocalEventBroker(IServiceProvider serviceProvider) : ILocalEventBroker
    {
        public async Task PublishAsync<T>(T @event, CancellationToken cancellationToken = default)
            where T : IEventContent
        {
            var module = serviceProvider.GetRequiredService<EventBusModule>();

            // module.InternalLogger.Information($"Event published: [{@event.GetType()}] \n{JsonConvert.SerializeObject(@event)}");
            var handlers = EventHandlersHelper.GetHandlers(@event.GetType(), serviceProvider);

            // module.InternalLogger.Information($"Handlers found: [{@event.GetType()}]\n[{string.Join(',', handlers.Select(h => h.GetType()))}] ");
            foreach (var handler in handlers)
            {
                await handler.HandleAsync(@event, cancellationToken).ConfigureAwait(false);

                // module.InternalLogger.Information($"Event handled: [{@event.GetType()}]\n [{handler.GetType()}]");
            }

            // var tasks = handlers
            // .Select(handler => handler.HandleAsync(@event, cancellationToken))
            // .ToArray();

            // await Task.WhenAll(tasks);
        }
    }
}
