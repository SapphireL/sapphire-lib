﻿namespace SapphireLib.EventBus.Definitions;

/// <summary>
/// 事件总线配置选项类，用于定义事件总线的配置参数.
/// </summary>
public class EventBusConfigOptions
{
    /// <summary>
    /// 获取或设置一个值，指示是否使用出箱（Outbox）模式.
    /// </summary>
    public bool UseOutbox { get; set; }

    /// <summary>
    /// 获取或设置一个值，指示在数据库不存在时是否创建数据库.
    /// 默认为 <c>false</c>.
    /// </summary>
    public bool CreateDbIfNotExists { get; set; } = false;
}