﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.Brokers;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Abstractions.Outbox;
using SapphireLib.EventBus.Brokers;
using SapphireLib.EventBus.EventBus;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.Definitions;

/// <summary>
/// 事件总线模块，负责配置和管理应用程序中的事件总线功能.
/// </summary>
/// <remarks>
/// 此模块继承自 <see cref="BaseModule"/>，并重写 <see cref="ConfigureServices"/> 方法以注册事件总线和本地事件代理的服务。
/// 该模块还负责在运行时创建数据库和表格（如果需要）.
/// </remarks>
public class EventBusModule : BaseModule
{
    /// <summary>
    /// 配置服务的方法，在此方法中注册事件总线及本地事件代理的依赖项.
    /// </summary>
    /// <param name="context">提供应用程序上下文的 <see cref="BuilderContext"/>.</param>
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.AddTransient<IEventBus, BaseEventBus>();
        context.Services.AddTransient<ILocalEventBroker, LocalEventBroker>();
    }

    /// <summary>
    /// 模块运行时执行的方法，在此方法中根据配置创建数据库和表格.
    /// </summary>
    /// <param name="context">运行时上下文的 <see cref="RunningContext"/>.</param>
    protected override void OnRunning(RunningContext context)
    {
        base.OnRunning(context);

        using var scope = context.WebApplication.Services.CreateScope();
        var options = context.BuilderContext.GetRequiredOptions<EventBusConfigOptions>();
        if (options.CreateDbIfNotExists)
        {
            var dbCreator = scope.ServiceProvider.GetRequiredService<IEventBusDbCreator>();
            dbCreator.CreateDbAsync().Wait();
            var tableCreator = scope.ServiceProvider.GetRequiredService<IOutboxDbTableCreator>();
            tableCreator.CreateTableAsync().Wait();
        }
    }

    /// <summary>
    /// 检查模块健康状态的方法.
    /// </summary>
    /// <param name="context">运行时上下文的 <see cref="RunningContext"/>.</param>
    /// <returns>返回一个布尔值，指示模块是否健康.</returns>
    protected override bool CheckModuleHealthy(RunningContext context)
    {
        var healthy = true;
        using var scope = context.WebApplication.Services.CreateScope();
        var options = context.BuilderContext.GetRequiredOptions<EventBusConfigOptions>();
        if (options.CreateDbIfNotExists)
        {
            var dbCreator = scope.ServiceProvider.GetService<IEventBusDbCreator>();
            var tableCreator = scope.ServiceProvider.GetService<IOutboxDbTableCreator>();

            if (dbCreator == null || tableCreator == null)
            {
                healthy = false;
            }
        }

        return healthy;
    }
}