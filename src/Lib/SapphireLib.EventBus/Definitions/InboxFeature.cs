﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Inbox;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.Definitions
{
    public class InboxFeature : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddHostedService<InboxProcessor>();
        }
    }
}
