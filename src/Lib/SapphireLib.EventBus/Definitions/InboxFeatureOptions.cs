﻿namespace SapphireLib.EventBus.Definitions
{
    public class InboxFeatureOptions
    {
        public string ConnectionString { get; set; } = default!;

        public string DbType { get; set; } = default!;

        public int MaxRetryAttempts { get; set; } = 2;

        public int ProcessDelay { get; set; } = 1000;

        /// <summary>
        /// 发件箱每次最大取出数.
        /// </summary>
        public int Limit { get; set; } = 10;
    }
}
