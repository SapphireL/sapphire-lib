﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.Outbox;
using SapphireLib.EventBus.Exceptions;
using SapphireLib.EventBus.Outbox;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.Definitions
{
    public class OutboxFeature : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddHostedService<OutboxProcessor>();
        }

        protected override void OnRunning(RunningContext context)
        {
            var provider = context.WebApplication.Services.GetService<IOutboxDataProvider>() ?? throw new EventBusDataProviderException($"can not find any implement of {nameof(IOutboxDataProvider)}");
        }
    }
}
