﻿namespace SapphireLib.EventBus.Definitions
{
    public class OutboxFeatureOptions
    {
        public int MaxRetryAttempts { get; set; } = 2;

        public int ProcessDelay { get; set; } = 1000;

        /// <summary>
        /// 发件箱每次最大取出数.
        /// </summary>
        public int Limit { get; set; } = 10;
    }
}
