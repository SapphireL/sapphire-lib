﻿using System.Reflection;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using SapphireLib.EventBus.Abstractions.Brokers;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Abstractions.Outbox;
using SapphireLib.EventBus.Attributes;
using SapphireLib.EventBus.Definitions;

namespace SapphireLib.EventBus.EventBus
{
    internal sealed class BaseEventBus : IEventBus
    {
        private readonly EventBusConfigOptions _options;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILocalEventBroker _localEventBroker;

        public BaseEventBus(IServiceProvider serviceProvider, ILocalEventBroker localEventBroker, IOptionsSnapshot<EventBusConfigOptions> options)
        {
            _serviceProvider = serviceProvider;
            _options = options.Value;
            _localEventBroker = localEventBroker;
        }

        public async Task PublishAsync<T>(T @event, CancellationToken cancellationToken = default)
            where T : IEventContent
        {
            var eventType = @event.GetType();

            var attributes = eventType.GetCustomAttributes();

            var outboxEvent = attributes.Any(x => x.GetType() == typeof(OutboxEventAttribute));
            var distributedEvent = attributes.Any(x => x.GetType() == typeof(DistributedEventAttribute));

            if (outboxEvent && _options.UseOutbox)
            {
                var outBoxEvent = CreateOutboxEvent(@event, distributedEvent ? OutboxMessageType.Distributed : OutboxMessageType.Local);
                var outboxDataProvider = _serviceProvider.GetRequiredService<IOutboxDataProvider>();
                await outboxDataProvider.InsertOutboxEventAsync(outBoxEvent);
            }
            else
            {
                await _localEventBroker.PublishAsync(@event);
            }
        }

        private static OutboxMessage CreateOutboxEvent<T>(T @event, OutboxMessageType type)
            where T : IEventContent
        {
            return new OutboxMessage(
                Guid.NewGuid(),
                @event.GetType().Name,
                JsonConvert.SerializeObject(@event, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead
                }),
                DateTime.Now,
                type);
        }
    }
}
