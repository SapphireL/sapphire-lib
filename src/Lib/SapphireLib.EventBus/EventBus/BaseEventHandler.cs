﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.EventBus.EventBus
{
    public abstract class BaseEventHandler<TEvent> : IEventHandler<TEvent>
        where TEvent : IEventContent
    {
        public abstract Task HandleAsync(TEvent @event, CancellationToken cancellationToken = default);

        public Task HandleAsync(IEventContent @event, CancellationToken cancellationToken = default) => HandleAsync((TEvent)@event, cancellationToken);
    }
}
