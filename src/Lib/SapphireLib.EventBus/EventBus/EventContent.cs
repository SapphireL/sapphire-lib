﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.EventBus.EventBus
{
    public abstract class EventContent : IEventContent
    {
        protected EventContent(Guid id, DateTime occurredOn)
        {
            Id = id;
            OccurredOn = occurredOn;
        }

        public Guid Id { get; init; }

        public DateTime OccurredOn { get; init; }
    }
}
