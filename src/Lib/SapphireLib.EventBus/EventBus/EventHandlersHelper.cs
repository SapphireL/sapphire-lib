﻿using System.Collections.Concurrent;

using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.EventBus.EventBus
{
    internal static class EventHandlersHelper
    {
        private static readonly ConcurrentDictionary<string, Type> _handlers = new();

        internal static IEnumerable<IEventHandler> GetHandlers(Type type, IServiceProvider serviceProvider)
        {
            var handlerType = _handlers.GetOrAdd(
           type.Name,
           _ =>
           {
               var eventHandlerType = typeof(IEventHandler<>).MakeGenericType(type);

               return eventHandlerType;
           });

            var eventHandlers = serviceProvider.GetServices(handlerType);
            return (eventHandlers as IEnumerable<IEventHandler>)!;
        }

        internal static IEnumerable<IEventHandler> GetHandlers(IEventContent @event, IServiceProvider serviceProvider)
        {
            return GetHandlers(@event.GetType(), serviceProvider);
        }
    }
}
