﻿using SapphireLib.EventBus.Abstractions.Consumers;
using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.EventBus.EventBus
{
    internal sealed class IdempotentEventHandler<TEvent> : BaseEventHandler<TEvent>
        where TEvent : IEventContent
    {
        private readonly IEventHandler<TEvent> _decorated;
        private readonly IConsumerDataProvider _consumerDataProvider;

        public IdempotentEventHandler(IEventHandler<TEvent> decorated, IConsumerDataProvider consumerDataProvider)
        {
            _decorated = decorated;
            _consumerDataProvider = consumerDataProvider;
        }

        public override async Task HandleAsync(TEvent @event, CancellationToken cancellationToken = default)
        {
            var consumerMessage = new ConsumerMessage(@event.Id, _decorated.GetType().FullName ?? _decorated.GetType().Name);

            if (await _consumerDataProvider.ConsumerExistsAsync(consumerMessage))
            {
                return;
            }

            await _decorated.HandleAsync(@event, cancellationToken);

            await _consumerDataProvider.InsertConsumerAsync(consumerMessage);
        }
    }
}
