﻿using SystemException = SapphireLib.UnifiedException.SystemException;

namespace SapphireLib.EventBus.Exceptions
{
    internal class EventBusDataProviderException : SystemException
    {
        public EventBusDataProviderException(string message)
        : base(message)
        {
        }

        public EventBusDataProviderException(Exception? innerException)
          : base("EventBus 数据提供程序出现错误", innerException)
        {
        }
    }
}
