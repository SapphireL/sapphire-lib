﻿using SystemException = SapphireLib.UnifiedException.SystemException;

namespace SapphireLib.EventBus.Exceptions
{
    internal class EventPublishException(string message) : SystemException(message)
    {
    }
}
