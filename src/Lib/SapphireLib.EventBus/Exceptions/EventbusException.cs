﻿using SystemException = SapphireLib.UnifiedException.SystemException;

namespace SapphireLib.EventBus.Exceptions
{
    internal class EventbusException : SystemException
    {
        public EventbusException(string message)
           : base(message)
        {
        }

        public EventbusException(string message, Exception? innerException)
           : base(message, innerException)
        {
        }
    }
}
