﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EventBus;
using SapphireLib.Module.Core;

namespace SapphireLib.EventBus.Extensions;

public static class ModuleBuilderExtensions
{
    public static void AddOutbox(this ModuleBuilder<EventBusModule> moduleBuilder, Action<OutboxFeatureOptions> options)
    {
        moduleBuilder.WithFeature<OutboxFeature, OutboxFeatureOptions>(options);
    }

    public static void AddOutbox(this ModuleBuilder<EventBusModule> moduleBuilder, string section)
    {
        moduleBuilder.WithFeature<OutboxFeature, OutboxFeatureOptions>(section);
    }

    public static void AddOutbox(this ModuleBuilder<EventBusModule> moduleBuilder)
    {
        moduleBuilder.WithFeature<OutboxFeature, OutboxFeatureOptions>();
    }

    public static void EnableInbox(this ModuleBuilder<EventBusModule> moduleBuilder, Action<FeatureBuilder<InboxFeature>> configure, Action<InboxFeatureOptions> options)
    {
        moduleBuilder.WithFeature(configure, options);
    }

    public static void AddEventHandler<TEventHandler, TEvent>(this FeaturesBuilder builder)
       where TEventHandler : BaseEventHandler<TEvent>
       where TEvent : IEventContent
    {
        builder.Context.Services.AddScoped<IEventHandler<TEvent>, TEventHandler>();
    }
}
