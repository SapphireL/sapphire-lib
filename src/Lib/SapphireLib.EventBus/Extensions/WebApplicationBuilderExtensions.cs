﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.EventBus.Abstractions.Consumers;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EventBus;
using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;

namespace SapphireLib.EventBus.Extensions;

/// <summary>
/// 扩展方法类，提供向 <see cref="WebApplicationBuilder"/> 添加事件总线功能的方法.
/// </summary>
public static class WebApplicationBuilderExtensions
{
    /// <summary>
    /// 向应用程序添加事件总线模块，并配置选项.
    /// </summary>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例.</param>
    /// <param name="moduleBuilder">用于自定义事件总线模块构建的委托.</param>
    /// <param name="options">用于配置事件总线的选项.</param>
    public static void AddSappLibEventBus(this WebApplicationBuilder builder, Action<ModuleBuilder<EventBusModule>> moduleBuilder, Action<EventBusConfigOptions> options)
    {
        builder.AddSappLib(options, moduleBuilder);
    }

    /// <summary>
    /// 向应用程序添加事件总线模块，并根据配置节设置选项.
    /// </summary>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例.</param>
    /// <param name="moduleBuilder">用于自定义事件总线模块构建的委托.</param>
    /// <param name="section">配置节的名称.</param>
    public static void AddSappLibEventBus(this WebApplicationBuilder builder, Action<ModuleBuilder<EventBusModule>> moduleBuilder, string section)
    {
        builder.AddSappLib<EventBusModule, EventBusConfigOptions>(section, moduleBuilder);
    }

    /// <summary>
    /// 向应用程序添加事件总线模块，使用默认的配置.
    /// </summary>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例.</param>
    /// <param name="moduleBuilder">用于自定义事件总线模块构建的委托.</param>
    public static void AddSappLibEventBus(this WebApplicationBuilder builder, Action<ModuleBuilder<EventBusModule>> moduleBuilder)
    {
        builder.AddSappLib<EventBusModule, EventBusConfigOptions>(moduleBuilder);
    }

    /// <summary>
    /// 向应用程序添加幂等事件处理程序.
    /// </summary>
    /// <typeparam name="TEventHandler">处理事件的类型，必须继承自 <see cref="BaseEventHandler{TEvent}"/>.</typeparam>
    /// <typeparam name="TEvent">事件内容的类型，必须实现 <see cref="IEventContent"/>.</typeparam>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例.</param>
    public static void AddIdempotentEventHandler<TEventHandler, TEvent>(this WebApplicationBuilder builder)
        where TEventHandler : BaseEventHandler<TEvent>
        where TEvent : IEventContent
    {
        builder.Services.AddScoped<TEventHandler>();
        builder.Services.AddScoped<IEventHandler<TEvent>>(provider =>
        {
            var handler = provider.GetRequiredService<TEventHandler>();
            return new IdempotentEventHandler<TEvent>(handler, provider.GetRequiredService<IConsumerDataProvider>());
        });
    }

    /// <summary>
    /// 向应用程序添加事件处理程序.
    /// </summary>
    /// <typeparam name="TEventHandler">处理事件的类型，必须继承自 <see cref="BaseEventHandler{TEvent}"/>.</typeparam>
    /// <typeparam name="TEvent">事件内容的类型，必须实现 <see cref="IEventContent"/>.</typeparam>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例.</param>
    public static void AddEventHandler<TEventHandler, TEvent>(this WebApplicationBuilder builder)
        where TEventHandler : BaseEventHandler<TEvent>
        where TEvent : IEventContent
    {
        builder.Services.AddScoped<IEventHandler<TEvent>, TEventHandler>();
    }
}