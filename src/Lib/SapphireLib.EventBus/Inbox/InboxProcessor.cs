﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Abstractions.Inbox;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.EventBus;
using SapphireLib.EventBus.Exceptions;

namespace SapphireLib.EventBus.Inbox
{
    public class InboxProcessor(IInboxDataProvider inboxDataProvider, IServiceProvider serviceProvider)
        : BackgroundService
    {
        private int interval;

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var options = serviceProvider.GetRequiredService<IOptionsSnapshot<InboxFeatureOptions>>().Value;
            interval = options.ProcessDelay;
            var limit = options.Limit;
            await DoWorkAsync(options.MaxRetryAttempts, limit);

            using PeriodicTimer timer = new(TimeSpan.FromMilliseconds(interval));

            while (await timer.WaitForNextTickAsync(stoppingToken))
            {
                options = serviceProvider.GetRequiredService<IOptionsSnapshot<InboxFeatureOptions>>().Value;
                interval = options.ProcessDelay;
                limit = options.Limit;
                await DoWorkAsync(options.MaxRetryAttempts, limit);
                timer.Period = TimeSpan.FromMilliseconds(interval);
            }
        }

        private async Task DoWorkAsync(int maxRetryAttempts, int limit)
        {
            try
            {
                var messages = await inboxDataProvider.GetInboxMessagesAsync(limit);

                foreach (var message in messages)
                {
                    try
                    {
                        message.Start();

                        await PublishAsync(message);
                    }
                    catch (Exception ex)
                    {
                        if (message.RetryAttempts < maxRetryAttempts)
                        {
                            message.Retry(ex.ToString());
                        }
                        else
                        {
                            message.Finished(InboxMessageResult.Failed, ex.ToString());
                        }

                        await inboxDataProvider.UpdateInboxMessageAsync(message);
                        continue;
                    }

                    message.Finished(InboxMessageResult.Success);
                    await inboxDataProvider.UpdateInboxMessageAsync(message);
                }
            }
            catch (Exception e)
            {
                throw new EventBusDataProviderException(e);
            }
        }

        private async Task PublishAsync(InboxMessage message)
        {
            var eventContent = JsonConvert.DeserializeObject<IEventContent>(
                      message.Content,
                      new JsonSerializerSettings
                      {
                          TypeNameHandling = TypeNameHandling.All,
                          MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead
                      })!;

            var handlers = EventHandlersHelper.GetHandlers(eventContent.GetType(), serviceProvider);

            foreach (var handler in handlers)
            {
                await handler.HandleAsync(eventContent, CancellationToken.None).ConfigureAwait(false);
            }
        }
    }
}
