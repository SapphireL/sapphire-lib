﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using SapphireLib.EventBus.Abstractions.Brokers;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.EventBus.Abstractions.Outbox;
using SapphireLib.EventBus.Definitions;
using SapphireLib.EventBus.Exceptions;

namespace SapphireLib.EventBus.Outbox
{
    /// <summary>
    /// 处理发件箱.
    /// </summary>
    public class OutboxProcessor
        : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IOptions<OutboxFeatureOptions> _options;
        private readonly ILogger<EventBusModule> _logger;
        private int _interval;
        private int _limit;
        private int _maxRetryAttempts;

        public OutboxProcessor(IServiceScopeFactory serviceScopeFactory, IOptions<OutboxFeatureOptions> options, ILogger<EventBusModule> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            using var scope = _serviceScopeFactory.CreateScope();
            _logger = logger;

            _options = options;
            UpdateOptions();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"Outbox processor has been started");
            await DoWorkAsync(stoppingToken);

            using PeriodicTimer timer = new(TimeSpan.FromMilliseconds(_interval));

            while (await timer.WaitForNextTickAsync(stoppingToken))
            {
                await DoWorkAsync(stoppingToken);
                timer.Period = TimeSpan.FromMilliseconds(_interval);
            }
        }

        private void UpdateOptions()
        {
            var options = _options.Value;
            _interval = options.ProcessDelay;
            _limit = options.Limit;
            _maxRetryAttempts = options.MaxRetryAttempts;

            _logger.LogInformation($"Options has been updated to \n{nameof(_interval)}:{options.ProcessDelay}\n{nameof(_limit)}:{options.Limit}\n{nameof(_maxRetryAttempts)}:{options.MaxRetryAttempts}");
        }

        private async Task PublishLocalEvent<T>(IServiceScope scope, T @event)
              where T : IEventContent
        {
            var broker = scope.ServiceProvider.GetRequiredService<ILocalEventBroker>();
            await broker.PublishAsync(@event);
        }

        private async Task PublishDistributedEvent(IServiceScope scope, OutboxMessage message)
        {
            var broker = scope.ServiceProvider.GetRequiredService<IDistributedEventBroker>();
            await broker.PublishAsync(message);
        }

        private async Task DoWorkAsync(CancellationToken stoppingToken)
        {
            try
            {
                using var scope = _serviceScopeFactory.CreateScope();
                var outboxPersistentProvider = scope.ServiceProvider.GetRequiredService<IOutboxDataProvider>();
                var events = await outboxPersistentProvider.GetOutboxEventsAsync(_limit);

                foreach (var @event in events)
                {
                    try
                    {
                        @event.Start();

                        if (@event.MessageType == OutboxMessageType.Local)
                        {
                            var eventContent = JsonConvert.DeserializeObject<IEventContent>(
                        @event.Content,
                        new JsonSerializerSettings
                        {
                            TypeNameHandling = TypeNameHandling.All,
                            MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead
                        })!;
                            await PublishLocalEvent(scope, eventContent);
                        }
                        else
                        {
                            await PublishDistributedEvent(scope, @event);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Handle exception {@event.GetType()}");
                        if (@event.RetryAttempts < _maxRetryAttempts)
                        {
                            @event.Retry(ex.ToString());
                        }
                        else
                        {
                            @event.Finished(OutboxMessageResult.Failed, ex.ToString());
                        }

                        await outboxPersistentProvider.UpdateOutboxEventAsync(@event);
                        continue;
                    }

                    @event.Finished(OutboxMessageResult.Success);
                    await outboxPersistentProvider.UpdateOutboxEventAsync(@event);
                }
            }
            catch (Exception e)
            {
                throw new EventBusDataProviderException(e);
            }
        }
    }
}
