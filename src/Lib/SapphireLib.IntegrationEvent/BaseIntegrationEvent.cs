﻿namespace SapphireLib.IntegrationEvent;

public abstract class BaseIntegrationEvent
{
    public Guid Id { get; }

    public DateTime OccurredOn { get; }

    protected BaseIntegrationEvent(Guid id, DateTime occurredOn)
    {
        this.Id = id;
        this.OccurredOn = occurredOn;
    }
}
