﻿using Microsoft.Extensions.DependencyInjection;

namespace QingtiArchitecture.BuildingBlocks.IntegrationEventBus
{
    public static class IntegrationEventBusMiddlewareExtensions
    {
        public static IServiceCollection AddQingtiIntegrationEventBus(this IServiceCollection services)
        {
            services.AddCap(x =>
            {
                x.UseInMemoryStorage();
                x.UseInMemoryMessageQueue();
            });
            return services;
        }
    }
}
