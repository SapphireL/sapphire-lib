﻿namespace SapphireLib.IntegrationEventBus;

public abstract class BaseIntegrationEvent
{
    public Guid Id { get; }

    public DateTime OccurredOn { get; }

    protected BaseIntegrationEvent(Guid id, DateTime occurredOn)
    {
        Id = id;
        OccurredOn = occurredOn;
    }
}
