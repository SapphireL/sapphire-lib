﻿namespace SapphireLib.IntegrationEventBus
{
    public interface IIntegrationEventHandler<TEvent>
        where TEvent : BaseIntegrationEvent
    {
        public Task HandleAsync(TEvent @event, CancellationToken cancellationToken);
    }
}
