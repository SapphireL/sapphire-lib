﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Modules;

using Savorboard.CAP.InMemoryMessageQueue;

namespace SapphireLib.IntegrationEventBus
{
    public class IntegrationEventBusModule : BaseModule
    {
        public override void Load(IServiceCollection services)
        {
            services.AddCap(x =>
            {
                x.UseInMemoryStorage();
                x.UseInMemoryMessageQueue();
            });
        }
    }
}
