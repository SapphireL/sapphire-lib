﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Modules;

namespace SapphireLib.IntegrationEventBus
{
    public static class ModuleFeatureConfigurationExtension
    {
        public static void AddIntegrationHandler<THandler, TEvent>(this ModuleConfigurationBuilder builder)
            where THandler : IIntegrationEventHandler<TEvent>
            where TEvent : BaseIntegrationEvent
        {
            var services = builder.WebApplicationBuilder.Services;
            var assembly = typeof(THandler).Assembly;
            services.Scan(scan =>
            {
                scan.FromAssemblies(assembly).AddClasses(classes => classes.AssignableTo(typeof(IIntegrationEventHandler<>)))
                            .AsImplementedInterfaces().WithTransientLifetime();
            });
        }
    }
}
