﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;

using SapphireLib.Logger.Providers;
using SapphireLib.Module.Core;

/// <summary>
/// 文件日志记录特性，负责配置应用程序的文件日志记录功能。
/// </summary>
/// <remarks>
/// 此类继承自 <see cref="BaseFeature"/>，用于将文件日志记录提供程序添加到应用程序的日志记录系统。
/// 它会读取应用程序配置文件中的日志设置，并初始化文件日志记录选项，以确保日志信息能够正确写入文件。
/// 文件日志记录器使用 <see cref="FileLoggerProvider"/> 提供日志记录功能。
/// </remarks>
internal class FileLoggerFeature : BaseFeature
{
    /// <summary>
    /// 配置服务的方法，在此方法中进行文件日志记录的服务配置。
    /// </summary>
    /// <param name="context">提供应用程序上下文的 <see cref="BuilderContext"/>。</param>
    /// <remarks>
    /// 此方法主要执行以下操作：
    /// <list type="bullet">
    ///     <item><description>从配置中读取日志设置并将其添加到日志记录服务，以确保日志的配置符合应用程序的要求。</description></item>
    ///     <item><description>将文件日志记录提供程序注册为单例，通过 <see cref="FileLoggerProvider"/> 实现异步日志处理和文件输出。</description></item>
    ///     <item><description>注册文件日志记录选项，允许在运行时动态更新日志记录配置而无需重启应用程序。</description></item>
    /// </list>
    /// </remarks>
    protected override void ConfigureServices(BuilderContext context)
    {
        // 从配置中读取日志设置并添加到日志记录服务
        context.WebApplicationBuilder.Logging.AddConfiguration(context.Configuration.GetSection("Logging"));

        // 注册文件日志记录提供程序为单例
        context.WebApplicationBuilder.Logging.Services.TryAddEnumerable(
            ServiceDescriptor.Singleton<ILoggerProvider, FileLoggerProvider>());

        // 注册文件日志记录选项
        LoggerProviderOptions.RegisterProviderOptions
            <FileLoggerOptions, FileLoggerProvider>(context.Services);
    }
}