﻿using Microsoft.Extensions.Logging;

using SapphireLib.Logger.Formatters;
using SapphireLib.Module.Core;

namespace SapphireLib.Logger.Definitions;

/// <summary>
/// 日志模块，负责配置和管理应用程序的日志记录功能。
/// </summary>
/// <remarks>
/// 此模块默认添加一个控制台日志记录器（console logger），允许开发者配置多种日志选项，例如：
/// <list type="bullet">
///     <item><description>日志级别</description></item>
///     <item><description>日志前缀</description></item>
///     <item><description>日志后缀</description></item>
/// </list>
/// 有关详细配置选项，请参考 <see cref="SappConsoleLoggerOptions"/> 类。
/// </remarks>
public class LoggerModule : BaseModule
{
    /// <summary>
    /// 配置服务的方法，在此方法中进行日志服务的配置。
    /// </summary>
    /// <param name="context">提供应用程序上下文的<see cref="BuilderContext"/>，允许访问和配置 Web 应用程序的服务。</param>
    protected override void ConfigureServices(BuilderContext context)
    {
        // 配置控制台日志记录
        context.WebApplicationBuilder.Logging.AddConsole(options =>
        {
            // 设置控制台日志的格式化程序名称为 "sappconsole"
            // 这将使用自定义的日志格式化程序，以便在控制台中输出日志信息
            options.FormatterName = "sappconsole";
        })

        // 注册自定义控制台日志格式化程序和选项
        .AddConsoleFormatter<SappConsoleLoggerFormatter, SappConsoleLoggerOptions>();
    }
}
