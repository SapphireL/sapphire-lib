﻿using SapphireLib.Logger.Definitions;
using SapphireLib.Module.Core;

namespace SapphireLib.Logger.Extensions;

public static class ModuleBuilderExtensions
{
    /// <summary>
    /// 扩展方法，用于向日志模块添加文件日志记录功能。
    /// </summary>
    /// <param name="moduleBuilder">当前的 <see cref="ModuleBuilder{TModule}"/> 实例，表示要配置的日志模块。</param>
    /// <remarks>
    /// 此方法将文件日志记录特性添加到日志模块中，以便支持将日志信息写入文件。
    /// 具体的文件日志记录实现由 <see cref="FileLoggerFeature"/> 类定义。
    /// 日志写入文件的规则：
    ///
    /// <list type="number">
    ///     <item><description>日志文件会根据日志分类创建不同的文件：
    ///         如果日志分类是 "SapphireLib"，日志将写入到 "SappCore" 目录下，文件名格式为
    ///         &lt;lastCate&gt;_&lt;currentDate&gt;.log，其中 &lt;lastCate&gt; 是分类的最后一部分，如 SapphireLib.Logger 的 &lt;lastCate&gt; 是 Logger。
    ///         &lt;currentDate&gt; 为当前日期，格式为 yyyy-MM-dd。</description></item>
    ///     <item><description>所有日志的通用文件将写入到 "All" 目录，文件名格式为 &lt;currentDate&gt;.log，
    ///         也会按照当前日期命名。</description></item>
    ///     <item><description>所有日志文件均以 UTF-8 编码写入，确保兼容性和可读性。</description></item>
    /// </list>
    /// </remarks>
    public static void AddFileLogger(this ModuleBuilder<LoggerModule> moduleBuilder)
    {
        moduleBuilder.WithFeature<FileLoggerFeature>();
    }
}
