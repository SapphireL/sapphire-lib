﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using Microsoft.Extensions.Options;

using SapphireLib.Logger.Definitions;
using SapphireLib.Logger.Formatters;
using SapphireLib.Logger.Providers;
using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;

namespace SapphireLib.Logger.Extensions;

public static class WebApplicationBuilderExtensions
{
    /// <summary>
    /// 扩展方法，用于向 <see cref="WebApplicationBuilder"/> 添加 SapphireLib 日志模块。
    /// </summary>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例。</param>
    /// <param name="action">可选的配置委托，用于自定义日志模块的构建。</param>
    /// <returns>返回更新后的 <see cref="WebApplicationBuilder"/> 实例，以便进行链式调用。</returns>
    /// <remarks>
    /// 该方法会调用 <see cref="Module.Extensions.WebApplicationBuilderExtensions.AddSappLib{TModule}(WebApplicationBuilder, Action{ModuleBuilder{TModule}}?)"/> 方法，以初始化和配置 SapphireLib 日志模块。
    /// 可以通过提供一个委托来进一步定制日志模块的选项。
    /// 在配置文件中，可以通过设置 "Logging:Console" 来配置日志格式。
    /// <code>
    /// "Logging": {
    ///   "LogLevel": {
    ///     "Default": "Information",
    ///     "Microsoft": "Information",
    ///     "Microsoft.AspNetCore": "Warning",
    ///     "Microsoft.Hosting.Lifetime": "Information"
    ///   },
    ///   "Console": {
    ///     "LogLevel": {
    ///       "Default": "Information",
    ///       "Microsoft": "Information",
    ///       "Microsoft.AspNetCore": "Warning",
    ///       "Microsoft.Hosting.Lifetime": "Information"
    ///     },
    ///     "FormatterName": "sappconsole",
    ///     "FormatterOptions": {
    ///       "IncludeScopes": true,
    ///       "TimestampFormat": "HH:mm:ss ",
    ///       "UseUtcTimestamp": false,
    ///       "Prefix": "[",
    ///       "Suffix": "]",
    ///       "LogLevelColor": {
    ///         "Information": "White",
    ///         "Warning": "Cyan",
    ///         "Error": "Red"
    ///       }
    ///     }
    ///   }
    /// }
    /// </code>
    /// </remarks>
    public static WebApplicationBuilder AddSappLibLogger(this WebApplicationBuilder builder, Action<ModuleBuilder<LoggerModule>>? action = null)
    {
        builder.AddSappLib(action);
        return builder;
    }

    public static SappServiceProviderBuilder AddFormatterLoggerFactory(this SappServiceProviderBuilder sappServiceProviderBuilder, IConfiguration configuration)
    {
        var loggerFactory = LoggerFactory.Create(builder =>
       {
           builder.AddConsole(options =>
           {
               options.FormatterName = "sappconsole";
           }).AddConsoleFormatter<SappConsoleLoggerFormatter, SappConsoleLoggerOptions>();
           builder.AddConfiguration(configuration.GetSection("Logging"));
       });

        sappServiceProviderBuilder.Services.AddSingleton(loggerFactory);
        return sappServiceProviderBuilder;
    }

    public static SappServiceProviderBuilder AddFileLoggerFactory(this SappServiceProviderBuilder sappServiceProviderBuilder, IConfiguration configuration)
    {
        var loggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddConfiguration(configuration.GetSection("Logging"));
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<ILoggerProvider, FileLoggerProvider>());
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IOptionsChangeTokenSource<FileLoggerOptions>, LoggerProviderOptionsChangeTokenSource<FileLoggerOptions, FileLoggerProvider>>());
        });

        sappServiceProviderBuilder.Services.AddSingleton(loggerFactory);
        return sappServiceProviderBuilder;
    }
}
