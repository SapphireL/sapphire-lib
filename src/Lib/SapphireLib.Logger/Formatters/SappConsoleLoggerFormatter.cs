﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;

namespace SapphireLib.Logger.Formatters
{
    public sealed class SappConsoleLoggerFormatter : ConsoleFormatter, IDisposable
    {
        private readonly IDisposable? _optionsReloadToken;
        private SappConsoleLoggerOptions _formatterOptions;

        public SappConsoleLoggerFormatter(IOptionsMonitor<SappConsoleLoggerOptions> options)
            : base("sappconsole") =>
            (_optionsReloadToken, _formatterOptions) =
                (options.OnChange(ReloadLoggerOptions), options.CurrentValue);

        public void Dispose() => _optionsReloadToken?.Dispose();

        public override void Write<TState>(in LogEntry<TState> logEntry, IExternalScopeProvider? scopeProvider, TextWriter textWriter)
        {
            string? message = logEntry.Formatter?.Invoke(
                    logEntry.State, logEntry.Exception);

            if (message is null)
            {
                return;
            }

            WriteColor(textWriter, logEntry.LogLevel);

            textWriter.Write(GetTopInfoMessage(logEntry.LogLevel, logEntry.Category));
            textWriter.Write(Environment.NewLine);
            WritePrefix(textWriter);
            textWriter.Write(message);
            WriteSuffix(textWriter);
        }

        private void ReloadLoggerOptions(SappConsoleLoggerOptions options) => _formatterOptions = options;

        private string GetTopInfoMessage(LogLevel logLevel, string category)
        {
            var logLevelMessage = logLevel switch
            {
                LogLevel.Trace => "TRCE",
                LogLevel.Debug => "DBUG",
                LogLevel.Information => "INFO",
                LogLevel.Warning => "WARN",
                LogLevel.Error => "FAIL",
                LogLevel.Critical => "CRIT",
                _ => throw new ArgumentOutOfRangeException(nameof(logLevel))
            };
            return $"[ {(_formatterOptions.UseUtcTimestamp
              ? DateTime.UtcNow
              : DateTime.Now)} ][ {logLevelMessage} ] {category}";
        }

        private static string GetColorEscapeCode(ConsoleColor color) =>
         color switch
         {
             ConsoleColor.Black => "\x1B[30m",
             ConsoleColor.DarkRed => "\x1B[31m",
             ConsoleColor.DarkGreen => "\x1B[32m",
             ConsoleColor.DarkYellow => "\x1B[33m",
             ConsoleColor.DarkBlue => "\x1B[34m",
             ConsoleColor.DarkMagenta => "\x1B[35m",
             ConsoleColor.DarkCyan => "\x1B[36m",
             ConsoleColor.Gray => "\x1B[37m",
             ConsoleColor.Red => "\x1B[1m\x1B[31m",
             ConsoleColor.Green => "\x1B[1m\x1B[32m",
             ConsoleColor.Yellow => "\x1B[1m\x1B[33m",
             ConsoleColor.Blue => "\x1B[1m\x1B[34m",
             ConsoleColor.Magenta => "\x1B[1m\x1B[35m",
             ConsoleColor.Cyan => "\x1B[1m\x1B[36m",
             ConsoleColor.White => "\x1B[1m\x1B[37m",

             _ => "\x1B[1m\x1B[37m"
         };

        private void WriteColor(TextWriter textWriter, LogLevel logLevel)
        {
            var color = _formatterOptions.LogLevelColor[logLevel];
            textWriter.Write(GetColorEscapeCode(color));
        }

        private void WritePrefix(TextWriter textWriter)
        {
            textWriter.Write($"{_formatterOptions.Prefix} ");
        }

        private void WriteSuffix(TextWriter textWriter) =>
            textWriter.WriteLine($" {_formatterOptions.Suffix}");
    }
}
