﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace SapphireLib.Logger.Formatters;

/// <summary>
/// 自定义控制台日志格式化程序的选项类，允许配置日志输出的前缀、后缀和颜色。
/// </summary>
public sealed class SappConsoleLoggerOptions : ConsoleFormatterOptions
{
    /// <summary>
    /// 获取或设置日志消息的前缀。
    /// </summary>
    /// <remarks>
    /// 该属性用于在日志消息前添加自定义前缀，可以帮助识别日志来源或提供额外的上下文信息。
    /// </remarks>
    public string? Prefix { get; set; }

    /// <summary>
    /// 获取或设置日志消息的后缀。
    /// </summary>
    /// <remarks>
    /// 该属性用于在日志消息后添加自定义后缀，可以用于提供更多信息或标识日志的结束。
    /// </remarks>
    public string? Suffix { get; set; }

    /// <summary>
    /// 获取或设置不同日志级别对应的控制台颜色。
    /// </summary>
    /// <remarks>
    /// 该字典用于定义每个日志级别在控制台输出时的颜色，使得在查看日志信息时能够更容易地区分不同类型的日志消息。
    /// 默认配置如下：
    /// <list type="bullet">
    ///     <item><description>Debug: 白色</description></item>
    ///     <item><description>Information: 深绿色</description></item>
    ///     <item><description>Warning: 青色</description></item>
    ///     <item><description>Error: 红色</description></item>
    ///     <item><description>Critical: 深红色</description></item>
    /// </list>
    /// </remarks>
    public Dictionary<LogLevel, ConsoleColor> LogLevelColor { get; set; } = new()
    {
        [LogLevel.Debug] = ConsoleColor.White,
        [LogLevel.Information] = ConsoleColor.DarkGreen,
        [LogLevel.Warning] = ConsoleColor.Cyan,
        [LogLevel.Error] = ConsoleColor.Red,
        [LogLevel.Critical] = ConsoleColor.DarkRed
    };
}
