﻿using System.Collections.Concurrent;
using System.Text;

using Microsoft.Extensions.Logging;

namespace SapphireLib.Logger.Providers;

/// <summary>
/// 文件日志记录器，负责将日志信息写入文件。
/// 实现了 <see cref="Microsoft.Extensions.Logging.ILogger"/> 接口，并支持异步日志处理。
/// 日志写入文件的规则：
///
/// <list type="number">
///     <item><description>日志文件会根据日志分类创建不同的文件：
///         如果日志分类是 "SapphireLib"，日志将写入到 "SappCore" 目录下，文件名格式为
///         &lt;lastCate&gt;_&lt;currentDate&gt;.log，其中 &lt;lastCate&gt; 是分类的最后一部分，如 SapphireLib.Logger 的 &lt;lastCate&gt; 是 Logger。
///         &lt;currentDate&gt; 为当前日期，格式为 yyyy-MM-dd。</description></item>
///     <item><description>所有日志的通用文件将写入到 "All" 目录，文件名格式为 &lt;currentDate&gt;.log，
///         也会按照当前日期命名。</description></item>
///     <item><description>所有日志文件均以 UTF-8 编码写入，确保兼容性和可读性。</description></item>
/// </list>
/// </summary>
public sealed class FileLogger : ILogger, IDisposable
{
    private readonly string _name; // 日志记录器名称
    private readonly Func<FileLoggerOptions> _getCurrentConfig; // 获取当前配置的函数
    private static readonly BlockingCollection<(string Category, string Message)> _logQueue = new(new ConcurrentQueue<(string, string)>()); // 日志消息队列
    private readonly Task _logTask; // 处理日志的后台任务
    private static readonly CancellationTokenSource _cts = new(); // 取消令牌源

    /// <summary>
    /// 构造函数，初始化 <see cref="FileLogger"/> 实例。
    /// </summary>
    /// <param name="name">日志记录器的名称。</param>
    /// <param name="getCurrentConfig">获取当前日志记录配置的函数。</param>
    public FileLogger(string name, Func<FileLoggerOptions> getCurrentConfig)
    {
        _name = name;
        _getCurrentConfig = getCurrentConfig;

        // 启动后台任务处理日志队列
        _logTask = Task.Factory.StartNew(ProcessQueue, _cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
    }

    /// <summary>
    /// 开始一个新的日志范围（无作用）。
    /// </summary>
    /// <typeparam name="TState">范围状态的类型。</typeparam>
    /// <param name="state">范围状态。</param>
    /// <returns>始终返回默认值。</returns>
    public IDisposable? BeginScope<TState>(TState state)
        where TState : notnull => default!;

    /// <summary>
    /// 确定日志记录器是否启用给定的日志级别。
    /// </summary>
    /// <param name="logLevel">要检查的日志级别。</param>
    /// <returns>如果启用，则返回 true；否则返回 false。</returns>
    public bool IsEnabled(LogLevel logLevel) => logLevel != LogLevel.None;

    /// <summary>
    /// 记录日志信息。
    /// </summary>
    /// <typeparam name="TState">日志状态的类型。</typeparam>
    /// <param name="logLevel">日志级别。</param>
    /// <param name="eventId">事件 ID。</param>
    /// <param name="state">日志状态。</param>
    /// <param name="exception">相关的异常信息（如果有）。</param>
    /// <param name="formatter">格式化日志信息的函数。</param>
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
    {
        if (!IsEnabled(logLevel))
        {
            return; // 如果日志级别不可用，则返回
        }

        // 构建日志消息
        string message = $"[ {DateTime.Now} ][ {logLevel} ] {_name}\n[ {formatter(state, exception)} ]";
        _logQueue.Add((_name, message)); // 将日志消息添加到队列
    }

    /// <summary>
    /// 释放所有资源，并确保所有日志被处理。
    /// </summary>
    public void Dispose()
    {
        _cts.Cancel(); // 取消日志处理
        _logTask.Wait(); // 等待所有日志处理完成
    }

    /// <summary>
    /// 处理日志消息队列的私有方法。
    /// </summary>
    private void ProcessQueue()
    {
        foreach (var (category, logMessage) in _logQueue.GetConsumingEnumerable(_cts.Token))
        {
            var config = _getCurrentConfig(); // 获取当前的日志配置
            string filePath = GetLogFilePath(config.FilePath, category); // 获取分类日志文件路径
            string allFilePath = GetAllLogFilePath(config.FilePath); // 获取所有日志文件路径

            if (!string.IsNullOrWhiteSpace(filePath))
            {
                // 将日志消息写入分类日志文件
                File.AppendAllText(filePath, logMessage + Environment.NewLine, Encoding.UTF8);
            }

            // 将日志消息写入所有日志文件
            File.AppendAllText(allFilePath, logMessage + Environment.NewLine, Encoding.UTF8);
        }
    }

    /// <summary>
    /// 根据基路径和分类获取日志文件路径。
    /// </summary>
    /// <param name="baseFilePath">基文件路径。</param>
    /// <param name="category">日志分类。</param>
    /// <returns>日志文件的完整路径。</returns>
    private static string GetLogFilePath(string baseFilePath, string category)
    {
        string date = DateTime.Now.ToString("yyyy-MM-dd"); // 获取当前日期
        string directory = Path.GetDirectoryName(baseFilePath) ?? string.Empty; // 获取目录

        // 从分类中提取第一个和最后一个部分
        string lastCate = category.Split('.').Last();
        string firstCate = category.Split('.').First();

        // 如果分类是 "SapphireLib"，则将日志写入特定目录
        if (firstCate == "SapphireLib")
        {
            string categoryDirectory = Path.Combine(directory, "SappCore");
            string extension = Path.GetExtension(baseFilePath);

            if (!Directory.Exists(categoryDirectory))
            {
                Directory.CreateDirectory(categoryDirectory); // 创建分类目录
            }

            return Path.Combine(categoryDirectory, $"{lastCate}_{date}{extension}"); // 返回日志文件路径
        }
        else
        {
            return string.Empty; // 其他分类返回空路径
        }
    }

    /// <summary>
    /// 获取包含所有日志的文件路径。
    /// </summary>
    /// <param name="baseFilePath">基文件路径。</param>
    /// <returns>所有日志文件的完整路径。</returns>
    private static string GetAllLogFilePath(string baseFilePath)
    {
        string date = DateTime.Now.ToString("yyyy-MM-dd"); // 获取当前日期
        string directory = Path.GetDirectoryName(baseFilePath) ?? string.Empty; // 获取目录
        string categoryDirectory = Path.Combine(directory, "All"); // 所有日志的目录
        string extension = Path.GetExtension(baseFilePath);

        if (!Directory.Exists(categoryDirectory))
        {
            Directory.CreateDirectory(categoryDirectory); // 创建所有日志目录
        }

        return Path.Combine(categoryDirectory, $"{date}{extension}"); // 返回所有日志文件路径
    }
}
