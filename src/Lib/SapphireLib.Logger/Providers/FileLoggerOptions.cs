﻿namespace SapphireLib.Logger.Providers
{
    public sealed class FileLoggerOptions
    {
        public string FilePath { get; set; } = "logs/app.log";
    }
}
