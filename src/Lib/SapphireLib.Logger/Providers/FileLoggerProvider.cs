﻿using System.Collections.Concurrent;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace SapphireLib.Logger.Providers;

/// <summary>
/// 文件日志提供程序，负责创建 <see cref="FileLogger"/> 实例。
/// 该提供程序支持根据配置选项动态更新日志记录器的配置。
/// </summary>
[ProviderAlias("FileLogger")]
public class FileLoggerProvider : ILoggerProvider
{
    private readonly IDisposable? _onChangeToken; // 监控配置变化的取消令牌
    private readonly ConcurrentDictionary<string, FileLogger> _loggers =
        new(StringComparer.OrdinalIgnoreCase); // 存储分类日志记录器的字典，支持忽略大小写

    private FileLoggerOptions _currentConfig; // 当前日志记录器的配置选项

    /// <summary>
    /// 构造函数，初始化 <see cref="FileLoggerProvider"/> 实例。
    /// </summary>
    /// <param name="config">用于获取和监控日志记录器配置的 <see cref="IOptionsMonitor{T}"/> 实例。</param>
    public FileLoggerProvider(IOptionsMonitor<FileLoggerOptions> config)
    {
        _currentConfig = config.CurrentValue; // 获取当前配置

        // 注册配置变化的回调函数，以便更新当前配置
        _onChangeToken = config.OnChange(updatedConfig => _currentConfig = updatedConfig);
    }

    /// <summary>
    /// 释放所有资源并清理日志记录器。
    /// </summary>
    public void Dispose()
    {
        _loggers.Clear(); // 清除所有日志记录器
        _onChangeToken?.Dispose(); // 释放配置变化的取消令牌
    }

    /// <summary>
    /// 创建一个新的 <see cref="ILogger"/> 实例。
    /// 如果指定的分类日志记录器已存在，则返回现有实例；否则创建一个新的实例。
    /// </summary>
    /// <param name="categoryName">日志记录器的分类名称。</param>
    /// <returns>新创建的或现有的 <see cref="ILogger"/> 实例。</returns>
    public ILogger CreateLogger(string categoryName) =>
        _loggers.GetOrAdd(categoryName, name => new FileLogger(name, GetCurrentConfig));

    /// <summary>
    /// 获取当前的日志记录器配置。
    /// </summary>
    /// <returns>当前的 <see cref="FileLoggerOptions"/> 配置。</returns>
    private FileLoggerOptions GetCurrentConfig() => _currentConfig; // 返回当前配置
}
