﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.Module.BusinessModule.IntegrationEvents;

public abstract class BaseIntegrationEvent : IEventContent
{
    public Guid Id { get; }

    public DateTime OccurredOn { get; }

    protected BaseIntegrationEvent(Guid id, DateTime occurredOn)
    {
        Id = id;
        OccurredOn = occurredOn;
    }
}
