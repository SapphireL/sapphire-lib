﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;
using SapphireLib.Module.Test.BasicModules.Prepared;

namespace SapphireLib.Module.Test.BasicModules.Extensions
{
    public static class TestExtensions
    {
        internal static void AddTestModule(this WebApplicationBuilder builder, Action<ModuleBuilder<BasicModule>>? moduleBuilder = null)
        {
            builder.AddSappLib(moduleBuilder);
        }

        internal static void AddTestExtension(this ModuleBuilder<BasicModule> moduleBuilder)
        {
            moduleBuilder.WithFeature<BasicExtension>();
        }

        internal static void AddTestExtension(this FeaturesBuilder builder)
        {
            builder.WithFeature<BasicExtension>();
        }
    }
}
