﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Core;

namespace SapphireLib.Module.Test.BasicModules.Prepared
{
    internal class BasicExtension : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<ITestExtensionService, TestExtensionService>();
        }
    }
}
