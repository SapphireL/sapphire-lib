﻿using SapphireLib.Module.Core;

namespace SapphireLib.Module.Test.BasicModules.Prepared
{
    internal class BasicExtensionWithExtension : BaseFeature
    {
        protected override void OnConfiguring(FeaturesBuilder builder)
        {
            builder.WithFeature<BasicSubExtension>();
        }
    }
}
