﻿using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Module.Core;

namespace SapphireLib.Module.Test.BasicModules.Prepared
{
    internal class BasicModule : BaseModule
    {
        public BasicModule()
        {
        }

        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<IBasicService, BasicService>();
        }
    }
}
