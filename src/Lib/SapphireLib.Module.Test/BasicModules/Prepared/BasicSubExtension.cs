﻿using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Module.Core;

namespace SapphireLib.Module.Test.BasicModules.Prepared
{
    internal class BasicSubExtension : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<ITestExtensionExtensionService, TestExtensionExtensionService>();
        }
    }
}
