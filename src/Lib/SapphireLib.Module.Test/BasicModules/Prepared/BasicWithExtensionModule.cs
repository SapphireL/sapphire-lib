﻿using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Module.Core;

namespace SapphireLib.Module.Test.BasicModules.Prepared
{
    internal class BasicWithExtensionModule : BaseModule
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<IBasicService, BasicService>();
        }

        protected override void OnConfiguring(FeaturesBuilder builder)
        {
            builder.WithFeature<BasicExtension>();
            builder.WithFeature<BasicExtensionWithExtension>();
        }
    }
}
