﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Extensions;
using SapphireLib.Module.Test.BasicModules.Extensions;
using SapphireLib.Module.Test.BasicModules.Prepared;

namespace SapphireLib.Module.Test.BasicModules.Tests
{
    public class ExtensionTest
    {
        [Fact]
        public void Add_Module_Should_Resolve_Services()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddSappLib<BasicModule>();

            var serviceProvider = builder.Services.BuildServiceProvider();
            var testModule = serviceProvider.GetRequiredService<BasicModule>();
            var serviceInfo = builder.Services.FirstOrDefault(x => x.ServiceType == typeof(BasicModule));
            var resolvedService = serviceProvider.GetRequiredService<IBasicService>();

            Assert.NotNull(resolvedService);
            Assert.NotNull(serviceInfo);
            Assert.True(serviceInfo.Lifetime == ServiceLifetime.Singleton);
            Assert.NotNull(testModule);
        }

        [Fact]
        public void Add_Basic_Module_Should_Resolve_Services()
        {
            var builder = WebApplication.CreateBuilder();
            builder.AddTestModule(moduleBuilder =>
            {
                moduleBuilder.AddTestExtension();
            });

            var serviceProvider = builder.Services.BuildServiceProvider();
            var resolvedService = serviceProvider.GetRequiredService<ITestExtensionService>();

            Assert.NotNull(resolvedService);
        }
    }
}
