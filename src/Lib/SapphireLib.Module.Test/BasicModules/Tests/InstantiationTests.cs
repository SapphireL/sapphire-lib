﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Core;
using SapphireLib.Module.Test.BasicModules.Prepared;

namespace SapphireLib.Module.Test.BasicModules.Tests
{
    public class InstantiationTests
    {
        /// <summary>
        /// 测试实例化模块.
        /// 调用安装方法之后，应该添加模块实例以及模块中注入的方法.
        /// </summary>
        [Fact]
        public void Instantiation_Should_Add_Singleton_And_Add_IBasicService()
        {
            var builder = WebApplication.CreateBuilder();
            var module = new BasicModule();
            module.Initialize(new BuilderContext(builder));

            var serviceProvider = builder.Services.BuildServiceProvider();
            var testModule = serviceProvider.GetRequiredService<BasicModule>();
            var serviceInfo = builder.Services.FirstOrDefault(x => x.ServiceType == typeof(BasicModule));
            var resolvedService = serviceProvider.GetRequiredService<IBasicService>();

            Assert.NotNull(resolvedService);
            Assert.NotNull(serviceInfo);
            Assert.True(serviceInfo.Lifetime == ServiceLifetime.Singleton);
            Assert.NotNull(testModule);
        }

        /// <summary>
        /// 测试实例化模块.
        /// 自配置模型.
        /// <see cref="BasicWithExtensionModule.OnConfiguring(FeaturesBuilder)"/>
        /// 调用安装方法之后，应该安装了扩展服务.
        /// </summary>
        [Fact]
        public void Instantiation_Should_With_Extension_OnConfiguring()
        {
            var builder = WebApplication.CreateBuilder();
            var module = new BasicWithExtensionModule();
            module.Initialize(new BuilderContext(builder));

            var serviceProvider = builder.Services.BuildServiceProvider();
            var resolvedService = serviceProvider.GetRequiredService<ITestExtensionService>();

            Assert.NotNull(resolvedService);
        }

        /// <summary>
        /// 测试实例化模块.
        /// 外部配置.
        /// 调用安装方法之后，应该安装了扩展服务.
        /// </summary>
        [Fact]
        public void Instantiation_Should_With_Extension_External_Configured()
        {
            var builder = WebApplication.CreateBuilder();
            var module = new BasicModule();
            var extension = new BasicExtension();
            module.AddFeature(extension);
            module.Initialize(new BuilderContext(builder));
            var serviceProvider = builder.Services.BuildServiceProvider();
            var resolvedService = serviceProvider.GetRequiredService<ITestExtensionService>();

            Assert.NotNull(resolvedService);
        }

        [Fact]
        public void Instantiation_With_Multi_Layer_Extensions()
        {
            var builder = WebApplication.CreateBuilder();
            var module = new BasicWithExtensionModule();
            module.Initialize(new BuilderContext(builder));

            var serviceProvider = builder.Services.BuildServiceProvider();
            var resolvedService = serviceProvider.GetRequiredService<ITestExtensionExtensionService>();

            Assert.NotNull(resolvedService);
        }
    }
}
