﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using SapphireLib.Module.Core;
using SapphireLib.Module.Test.BasicModules.Prepared;

namespace SapphireLib.Module.Test.BasicModules.Tests
{
    public class ModuleBuilderTest
    {
        [Fact]
        public void Build_Should_Add_Singleton_And_Add_IBasicService()
        {
            var builder = WebApplication.CreateBuilder();
            var moduleBuilder = new ModuleBuilder<BasicModule>(new BuilderContext(builder));
            moduleBuilder.Initialize();

            var serviceProvider = builder.Services.BuildServiceProvider();
            var testModule = serviceProvider.GetRequiredService<BasicModule>();
            var serviceInfo = builder.Services.FirstOrDefault(x => x.ServiceType == typeof(BasicModule));
            var resolvedService = serviceProvider.GetRequiredService<IBasicService>();

            Assert.NotNull(resolvedService);
            Assert.NotNull(serviceInfo);
            Assert.True(serviceInfo.Lifetime == ServiceLifetime.Singleton);
            Assert.NotNull(testModule);
        }

        [Fact]
        public void Build_Should_With_Extension_OnConfiguring()
        {
            var builder = WebApplication.CreateBuilder();
            var moduleBuilder = new ModuleBuilder<BasicWithExtensionModule>(new BuilderContext(builder));
            moduleBuilder.Initialize();

            var serviceProvider = builder.Services.BuildServiceProvider();
            var resolvedService = serviceProvider.GetRequiredService<ITestExtensionService>();

            Assert.NotNull(resolvedService);
        }

        [Fact]
        public void Build_Should_With_Extension_External_Configured()
        {
            var builder = WebApplication.CreateBuilder();
            var moduleBuilder = new ModuleBuilder<BasicWithExtensionModule>(new BuilderContext(builder));
            moduleBuilder.WithFeature<BasicExtension>();
            moduleBuilder.Initialize();

            var serviceProvider = builder.Services.BuildServiceProvider();
            var resolvedService = serviceProvider.GetRequiredService<ITestExtensionService>();

            Assert.NotNull(resolvedService);
        }

        [Fact]
        public void Build_Should_With_Options()
        {
            var builder = WebApplication.CreateBuilder();
            var testValue = "test";
            var moduleBuilder = new ModuleBuilder<BasicModuleWithOptions>(new BuilderContext(builder));
            moduleBuilder.WithOptions<BasicModuleOptions>(configure => configure.TestName = testValue);
            moduleBuilder.Initialize();
            var serviceProvider = builder.Services.BuildServiceProvider();
            var options = serviceProvider.GetRequiredService<IOptions<BasicModuleOptions>>();
            Assert.NotNull(options);
            Assert.Equal(testValue, options.Value.TestName);
        }

        [Fact]
        public void Build_With_Multi_Layer_Extensions()
        {
            var builder = WebApplication.CreateBuilder();
            var moduleBuilder = new ModuleBuilder<BasicWithExtensionModule>(new BuilderContext(builder));
            moduleBuilder.Initialize();

            var serviceProvider = builder.Services.BuildServiceProvider();
            var resolvedService = serviceProvider.GetRequiredService<ITestExtensionExtensionService>();

            Assert.NotNull(resolvedService);
        }
    }
}
