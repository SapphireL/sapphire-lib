﻿using SapphireLib.Module.Core;

namespace SapphireLib.Module.Abstractions;

public interface IBuilder<TInstance>
    where TInstance : class, IExtensible, new()
{
    public BuilderContext Context { get; }

    public IBuilder<TInstance> WithFeature<TFeature>(TFeature extension)
        where TFeature : class, IFeature, new();

    public void Initialize();

    public TInstance Build();
}
