﻿namespace SapphireLib.Module.Abstractions;

public interface IExtensible
{
    public void AddFeature(IFeature feature);
}
