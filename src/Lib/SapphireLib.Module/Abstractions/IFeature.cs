﻿using SapphireLib.Module.Core;

namespace SapphireLib.Module.Abstractions;

public interface IFeature : IExtensible, IInitializable, IRunable
{
    /// <summary>
    /// 配置扩展的扩展.
    /// 在此方法内部应该调用扩展中的<see cref="BaseFeature.OnConfiguring(FeaturesBuilder)"/>.
    /// </summary>
    /// <param name="context"><see cref="BuilderContext"/>.</param>
    public void Configure(BuilderContext context);
}
