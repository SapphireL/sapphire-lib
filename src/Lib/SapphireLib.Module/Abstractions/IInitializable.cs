﻿using SapphireLib.Module.Core;

namespace SapphireLib.Module.Abstractions;

public interface IInitializable
{
    public void Initialize(BuilderContext context);
}
