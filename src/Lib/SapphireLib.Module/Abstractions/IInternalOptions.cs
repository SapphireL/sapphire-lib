﻿namespace SapphireLib.Module.Abstractions;

public interface IInternalOptions<TModule>
    where TModule : IModule
{
}
