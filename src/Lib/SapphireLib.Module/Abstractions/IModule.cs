﻿using SapphireLib.Module.Core;

namespace SapphireLib.Module.Abstractions;

/// <summary>
/// 模块
/// 你需要在应用程序的组合根项目实现该接口，框架会通过反射，在应用程序启动的时候调用InstallModule方法.在该方法中，你需要将所有该业务模块中的服务进行注册。不要在其他地方注册.
/// API服务只可以依赖组合根项目.
/// </summary>
public interface IModule : IExtensible, IInitializable, IRunable
{
    public BuilderContext BuilderContext { get; }

    public RunningContext RunningContext { get; }
}
