﻿using SapphireLib.Module.Core;

namespace SapphireLib.Module.Abstractions;

public interface IRunable
{
    public void Run(RunningContext context);
}
