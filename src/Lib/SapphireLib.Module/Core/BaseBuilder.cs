﻿using SapphireLib.Module.Abstractions;

namespace SapphireLib.Module.Core;

/// <summary>
/// 基础构建器，提供创建和配置扩展实例的功能。
/// </summary>
/// <typeparam name="TInstance">扩展实例的类型，必须实现 <see cref="IExtensible"/> 和 <see cref="IInitializable"/> 接口。</typeparam>
/// <param name="context">构建上下文。</param>
public abstract class BaseBuilder<TInstance>(BuilderContext context) : IBuilder<TInstance>
    where TInstance : class, IExtensible, IInitializable, new()
{
    private readonly BuilderContext _context = context;

    /// <summary>
    /// 获取当前的构建上下文。
    /// </summary>
    public BuilderContext Context => _context;

    private readonly TInstance _instance = new();

    /// <summary>
    /// 获取当前实例。
    /// </summary>
    protected TInstance Instance => _instance;

    /// <summary>
    /// 初始化当前实例。
    /// </summary>
    public virtual void Initialize()
    {
        Instance.Initialize(Context);
    }

    /// <summary>
    /// 添加特性到实例中。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <param name="feature">特性实例。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature>(TFeature feature)
        where TFeature : class, IFeature, new()
    {
        _instance.AddFeature(feature);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并创建特性的构建器。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature>()
      where TFeature : class, IFeature, new()
    {
        var builder = CreateBuilder<TFeature>();
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性的构建器。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <param name="configureBuilder">用于配置特性构建器的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature>(Action<FeatureBuilder<TFeature>>? configureBuilder = null)
       where TFeature : class, IFeature, new()
    {
        var builder = CreateBuilder<TFeature>();
        configureBuilder?.Invoke(builder);
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>()
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions<TOptions>();
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项和选项部分。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="section">配置节的名称。</param>
    /// <param name="configureOptions">配置选项的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>(string? section = null, Action<TOptions>? configureOptions = null)
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions(section, configureOptions);
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项部分。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="section">配置节的名称。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>(string section)
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions<TOptions>(section);
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="configureOptions">配置选项的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>(Action<TOptions>? configureOptions = null)
     where TFeature : class, IFeature, new()
     where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        if (configureOptions != null)
        {
            builder.WithOptions(configureOptions);
        }
        else
        {
            builder.WithOptions<TOptions>();
        }

        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项和构建器。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="configureBuilder">配置特性构建器的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>(Action<FeatureBuilder<TFeature>> configureBuilder)
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions<TOptions>();
        configureBuilder(builder);
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项和构建器。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="configureBuilder">配置特性构建器的可选委托。</param>
    /// <param name="section">配置节的名称。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>(Action<FeatureBuilder<TFeature>> configureBuilder, string section)
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions<TOptions>(section);
        configureBuilder(builder);
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项和构建器。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="configureBuilder">配置特性构建器的可选委托。</param>
    /// <param name="configure">配置选项的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>(Action<FeatureBuilder<TFeature>> configureBuilder, Action<TOptions> configure)
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions(configure);
        configureBuilder(builder);
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项和构建器。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="configureBuilder">配置特性构建器的可选委托。</param>
    /// <param name="section">配置节的名称。</param>
    /// <param name="configureOptions">配置选项的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithFeature<TFeature, TOptions>(Action<FeatureBuilder<TFeature>> configureBuilder, string? section = null, Action<TOptions>? configureOptions = null)
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions(section, configureOptions);
        configureBuilder(builder);
        BuildFeature(builder);
        return this;
    }

    /// <summary>
    /// 配置选项类型。
    /// </summary>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithOptions<TOptions>()
      where TOptions : class
    {
        _context.ConfigureOptions<TOptions>();
        return this;
    }

    /// <summary>
    /// 配置选项类型和选项节。
    /// </summary>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="section">配置节的名称。</param>
    /// <param name="options">配置选项的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithOptions<TOptions>(string? section = null, Action<TOptions>? options = null)
        where TOptions : class
    {
        _context.ConfigureOptions(section, options);
        return this;
    }

    /// <summary>
    /// 配置选项类型和选项节。
    /// </summary>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="section">配置节的名称。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithOptions<TOptions>(string section)
        where TOptions : class
    {
        _context.ConfigureOptions<TOptions>(section);
        return this;
    }

    /// <summary>
    /// 配置选项类型。
    /// </summary>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="options">配置选项的可选委托。</param>
    /// <returns>当前构建器实例。</returns>
    public IBuilder<TInstance> WithOptions<TOptions>(Action<TOptions> options)
       where TOptions : class
    {
        _context.ConfigureOptions(options);
        return this;
    }

    /// <summary>
    /// 构建并返回当前实例。
    /// </summary>
    /// <returns>当前实例。</returns>
    public TInstance Build()
    {
        return Instance;
    }

    /// <summary>
    /// 创建特性的构建器。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <returns>特性的构建器实例。</returns>
    private FeatureBuilder<TFeature> CreateBuilder<TFeature>()
        where TFeature : class, IFeature, new()
    {
        return new FeatureBuilder<TFeature>(_context);
    }

    /// <summary>
    /// 构建特性并将其添加到实例中。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <param name="featureBuilder">特性的构建器实例。</param>
    private void BuildFeature<TFeature>(FeatureBuilder<TFeature> featureBuilder)
        where TFeature : class, IFeature, new()
    {
        var feature = featureBuilder.Build();
        _instance.AddFeature(feature);
    }
}