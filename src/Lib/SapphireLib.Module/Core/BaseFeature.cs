﻿using SapphireLib.Module.Abstractions;

namespace SapphireLib.Module.Core
{
    public abstract class BaseFeature : IFeature
    {
        private readonly List<IFeature> _features = [];

        protected List<IFeature> Features => _features;

        public virtual void Run(RunningContext context)
        {
            OnRunning(context);
            RunExtensions(context);
        }

        public virtual void Configure(BuilderContext context)
        {
            var features = new FeaturesBuilder(context);
            OnConfiguring(features);
            _features.AddRange(features.Features);
            ConfigureExtensions(context);
        }

        public void AddFeature(IFeature feature)
        {
            _features.Add(feature);
        }

        public virtual void Initialize(BuilderContext context)
        {
            ConfigureServices(context);
            InitializeExtensions(context);
        }

        protected virtual void OnRunning(RunningContext context)
        {
            CheckModuleHealthy(context);
        }

        protected virtual bool CheckModuleHealthy(RunningContext context)
        {
            return true;
        }

        protected virtual void OnConfiguring(FeaturesBuilder builder)
        {
        }

        protected virtual void ConfigureServices(BuilderContext context)
        {
        }

        private void ConfigureExtensions(BuilderContext context)
        {
            foreach (var feature in _features)
            {
                feature.Configure(context);
            }
        }

        private void InitializeExtensions(BuilderContext context)
        {
            foreach (var feature in _features)
            {
                feature.Initialize(context);
            }
        }

        private void RunExtensions(RunningContext context)
        {
            foreach (var feature in _features)
            {
                feature.Run(context);
            }
        }
    }
}
