﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using SapphireLib.Module.Abstractions;
using SapphireLib.Tools;

namespace SapphireLib.Module.Core
{
    public abstract class BaseModule : IModule
    {
        private readonly List<IFeature> _features = [];
        private BuilderContext _builderContext = default!;
        private RunningContext _runningContext = default!;

        public BuilderContext BuilderContext => _builderContext;

        public RunningContext RunningContext => _runningContext;

        protected List<IFeature> Features => _features;

        protected ILogger InternalLogger { get; private set; } = default!;

        public virtual void Run(RunningContext context)
        {
            _runningContext = context;
            OnRunning(_runningContext);
            RunFeatures();
        }

        public void AddFeature(IFeature feature)
        {
            _features.Add(feature);
        }

        public virtual void Initialize(BuilderContext context)
        {
            _builderContext = context;
            InternalLogger = GetLogger();
            InternalLogger.LogInformation("Module is loading.");

            // context.Build();
            Configure();
            context.Services.AddSingleton(this).AsSelf();
            InitializeFeatures();
            ConfigureServices(context);
        }

        protected virtual void ConfigureInternalOptions(BuilderContext context)
        {
        }

        protected virtual void ConfigureServices(BuilderContext context)
        {
        }

        protected virtual void OnConfiguring(FeaturesBuilder builder)
        {
        }

        protected virtual void OnRunning(RunningContext context)
        {
            CheckModuleHealthy(context);
        }

        protected virtual bool CheckModuleHealthy(RunningContext context)
        {
            return true;
        }

        private void Configure()
        {
            var builder = new FeaturesBuilder(_builderContext);
            OnConfiguring(builder);
            _features.AddRange(builder.Features);
            ConfigureFeatures();
        }

        private ILogger GetLogger()
        {
            var factory = SappServiceProvider.Services.GetRequiredService<ILoggerFactory>();
            var logger = factory.CreateLogger(GetType());
            _builderContext.Services.AddSingleton(logger);
            return logger;
        }

        private void InitializeFeatures()
        {
            foreach (var feature in _features)
            {
                feature.Initialize(_builderContext);
            }
        }

        private void ConfigureFeatures()
        {
            foreach (var feature in _features)
            {
                feature.Configure(_builderContext);
            }
        }

        private void RunFeatures()
        {
            foreach (var feature in _features)
            {
                feature.Run(_runningContext);
            }
        }
    }
}
