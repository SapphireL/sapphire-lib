﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace SapphireLib.Module.Core;

/// <summary>
/// 构建器上下文。
/// </summary>
/// <param name="builder"><see cref="WebApplicationBuilder"/>。</param>
public class BuilderContext(WebApplicationBuilder builder)
{
    private readonly IServiceCollection _services = builder.Services;
    private readonly ConfigurationManager _configuration = builder.Configuration;
    private readonly IWebHostEnvironment _webHostEnvironment = builder.Environment;

    private readonly IServiceCollection _internalServices = new ServiceCollection();

    public IServiceCollection InternalServices => _internalServices;

    public IServiceCollection Services => _services;

    public WebApplicationBuilder WebApplicationBuilder => builder;

    public IConfiguration Configuration => _configuration;

    public IWebHostEnvironment WebHostEnvironment => _webHostEnvironment;

    public void ConfigureOptions<TOptions>(string? section = null, Action<TOptions>? options = null)
       where TOptions : class
    {
        if (!string.IsNullOrWhiteSpace(section))
        {
            _configuration.GetSection(section);
            ConfigureOptions<TOptions>(section);
        }
        else
        {
            ConfigureOptions<TOptions>();
        }

        if (options != null)
        {
            ServiceConfigureOptions(options);
        }
    }

    public void ConfigureOptions<TOptions>()
        where TOptions : class
    {
        _internalServices.Configure<TOptions>(_configuration);
        _services.Configure<TOptions>(_configuration);
    }

    public void ConfigureOptions<TOptions>(string section)
        where TOptions : class
    {
        var optionsSection = _configuration.GetSection(section);
        ServiceConfigureOptions<TOptions>(optionsSection);
    }

    public void ConfigureOptions<TOptions>(Action<TOptions> options)
       where TOptions : class
    {
        ConfigureOptions<TOptions>();
        ServiceConfigureOptions(options);
    }

    public void ConfigureOptions<TOptions>(Action<TOptions> options, string section)
       where TOptions : class
    {
        ConfigureOptions<TOptions>(section);
        ServiceConfigureOptions(options);
    }

    public TOptions? GetOptions<TOptions>()
        where TOptions : class
    {
        return _internalServices.BuildServiceProvider().GetService<IOptions<TOptions>>()?.Value;
    }

    public TOptions GetRequiredOptions<TOptions>()
       where TOptions : class
    {
        return _internalServices.BuildServiceProvider().GetRequiredService<IOptions<TOptions>>().Value;
    }

    private void ServiceConfigureOptions<TOptions>(Action<TOptions> options)
        where TOptions : class
    {
        _internalServices.Configure(options);
        _services.Configure(options);
    }

    private void ServiceConfigureOptions<TOptions>(IConfigurationSection section)
        where TOptions : class
    {
        _internalServices.Configure<TOptions>(section);
        _services.Configure<TOptions>(section);
    }
}
