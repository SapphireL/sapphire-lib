﻿using SapphireLib.Module.Abstractions;

namespace SapphireLib.Module.Core
{
    public class FeatureBuilder<TFeature>(BuilderContext context) : BaseBuilder<TFeature>(context)
        where TFeature : class, IFeature, new()
    {
    }
}
