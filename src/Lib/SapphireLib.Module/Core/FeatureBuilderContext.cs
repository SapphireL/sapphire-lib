﻿using Microsoft.AspNetCore.Builder;

namespace SapphireLib.Module.Core;

public class FeatureBuilderContext(string moduleName, WebApplicationBuilder builder) : BuilderContext(builder)
{
    public string ModuleName { get; init; } = moduleName;
}
