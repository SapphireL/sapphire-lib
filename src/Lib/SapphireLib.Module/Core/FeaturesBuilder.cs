﻿using SapphireLib.Module.Abstractions;

namespace SapphireLib.Module.Core;

public class FeaturesBuilder(BuilderContext context)
{
    private readonly BuilderContext _context = context;

    private readonly List<IFeature> _features = [];

    public BuilderContext Context => _context;

    public List<IFeature> Features => _features;

    /// <summary>
    /// 添加扩展.
    /// </summary>
    /// <typeparam name="TFeature">扩展类型.</typeparam>
    public virtual void WithFeature<TFeature>()
        where TFeature : class, IFeature, new()
    {
        // 创建扩展
        var builder = new FeatureBuilder<TFeature>(_context);
        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    public virtual void WithFeature<TFeature, TOptions>()
       where TFeature : class, IFeature, new()
       where TOptions : class
    {
        // 创建扩展
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions<TOptions>();
        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    public virtual void WithFeature<TFeature, TOptions>(string section)
        where TFeature : class, IFeature, new()
        where TOptions : class
    {
        // 创建扩展
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions<TOptions>(section);
        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    /// <summary>
    /// 添加特性到实例中，并配置特性选项和选项部分。
    /// </summary>
    /// <typeparam name="TFeature">特性的类型。</typeparam>
    /// <typeparam name="TOptions">选项的类型。</typeparam>
    /// <param name="section">配置节的名称。</param>
    /// <param name="configureOptions">配置选项的可选委托。</param>
    public void WithFeature<TFeature, TOptions>(string? section = null, Action<TOptions>? configureOptions = null)
    where TFeature : class, IFeature, new()
    where TOptions : class, new()
    {
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions(section, configureOptions);
        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    public virtual void WithFeature<TFeature, TOptions>(Action<TOptions>? configureOptions = null)
       where TFeature : class, IFeature, new()
       where TOptions : class
    {
        // 创建扩展
        var builder = CreateBuilder<TFeature>();
        if (configureOptions != null)
        {
            builder.WithOptions(configureOptions);
        }
        else
        {
            builder.WithOptions<TOptions>();
        }

        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    public virtual void WithFeature<TFeature, TOptions>(Action<TOptions> configureOptions, string section)
       where TFeature : class, IFeature, new()
       where TOptions : class
    {
        // 创建扩展
        var builder = CreateBuilder<TFeature>();
        builder.WithOptions(section, configureOptions);
        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    public virtual void WithFeature<TFeature>(Action<FeatureBuilder<TFeature>> configureExtensionBuilder)
        where TFeature : class, IFeature, new()
    {
        // 创建扩展
        var builder = new FeatureBuilder<TFeature>(_context);
        configureExtensionBuilder(builder);
        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    public virtual void WithFeature<TFeature, TOptions>(Action<FeatureBuilder<TFeature>> configureExtensionBuilder, Action<TOptions> configureOptions)
       where TFeature : class, IFeature, new()
       where TOptions : class
    {
        // 创建扩展
        var builder = new FeatureBuilder<TFeature>(_context);
        builder.WithOptions(configureOptions);
        configureExtensionBuilder(builder);
        var feature = builder.Build();

        // 将扩展添加至module
        _features.Add(feature);
    }

    private FeatureBuilder<TFeature> CreateBuilder<TFeature>()
      where TFeature : class, IFeature, new()
    {
        return new FeatureBuilder<TFeature>(_context);
    }
}
