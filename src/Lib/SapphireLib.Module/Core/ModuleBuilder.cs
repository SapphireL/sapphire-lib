﻿using SapphireLib.Module.Abstractions;

namespace SapphireLib.Module.Core
{
    public class ModuleBuilder<TModule>(BuilderContext context) : BaseBuilder<TModule>(context)
         where TModule : class, IModule, new()
    {
    }
}