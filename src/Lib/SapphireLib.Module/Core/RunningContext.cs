﻿using Microsoft.AspNetCore.Builder;

namespace SapphireLib.Module.Core
{
    public class RunningContext
    {
        private readonly BuilderContext _builderContext;
        private readonly WebApplication _webApplication;

        public BuilderContext BuilderContext => _builderContext;

        public WebApplication WebApplication => _webApplication;

        public RunningContext(BuilderContext context, WebApplication webApplication)
        {
            _builderContext = context;
            _webApplication = webApplication;
        }
    }
}
