﻿namespace SapphireLib.Module.Core;

public class SappServiceProvider
{
    public SappServiceProvider(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public static SappServiceProviderBuilder CreateBuilder()
    {
        return new SappServiceProviderBuilder();
    }

    private static IServiceProvider _serviceProvider = default!;

    internal static IServiceProvider Services => _serviceProvider;
}
