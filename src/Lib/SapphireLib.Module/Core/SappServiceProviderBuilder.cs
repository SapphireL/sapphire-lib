﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace SapphireLib.Module.Core
{
    public class SappServiceProviderBuilder
    {
        public SappServiceProviderBuilder()
        {
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddSimpleConsole();
            });

            _services = new ServiceCollection();
            _services.AddSingleton(loggerFactory);
        }

        private IServiceCollection _services;

        public IServiceCollection Services => _services;

        public SappServiceProvider Build()
        {
            return new SappServiceProvider(_services.BuildServiceProvider());
        }
    }
}
