﻿namespace SapphireLib.Module.Exceptions
{
    internal class ModuleCreatingExecption(string message, string moduleName) : Exception($"{moduleName} has thrown exception with message: {message}")
    {
        public string ModuleName { get; } = moduleName;
    }
}
