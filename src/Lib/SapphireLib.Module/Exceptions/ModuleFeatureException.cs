﻿namespace SapphireLib.Module.Exceptions
{
    public class ModuleFeatureException : Exception
    {
        public string FeatureName { get; }

        public ModuleFeatureException(string message, string featureName)
            : base($"{featureName} has thrown exception with message: {message}")
        {
            FeatureName = featureName;
        }
    }
}
