﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Core;

namespace SapphireLib.Module.Extensions;

public static class WebApplicationBuilderExtensions
{
    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappLib<TModule>(this WebApplicationBuilder builder, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
       where TModule : BaseModule, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <typeparam name="TOptions">模块配置.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappLib<TModule, TOptions>(this WebApplicationBuilder builder, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions<TOptions>();
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <typeparam name="TOptions">模块配置.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="section">配置从配置文件中加载的区域.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappLib<TModule, TOptions>(this WebApplicationBuilder builder, string section, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions<TOptions>(GetLibSectionString(section));
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <typeparam name="TOptions">模块配置.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="options">配置.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappLib<TModule, TOptions>(this WebApplicationBuilder builder, Action<TOptions> options, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions(options);
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <typeparam name="TOptions">模块配置.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="options">配置.</param>
    /// <param name="section">配置从配置文件中加载的区域.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappLib<TModule, TOptions>(this WebApplicationBuilder builder, Action<TOptions> options, string section, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions(GetLibSectionString(section), options);
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappApp<TModule>(this WebApplicationBuilder builder, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
       where TModule : BaseModule, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <typeparam name="TOptions">模块配置.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappApp<TModule, TOptions>(this WebApplicationBuilder builder, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions<TOptions>();
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <typeparam name="TOptions">模块配置.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="section">配置从配置文件中加载的区域.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappApp<TModule, TOptions>(this WebApplicationBuilder builder, string section, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions<TOptions>(GetAppSectionString(section));
        moduleBuilder.Initialize();
    }

    /// <summary>
    /// 添加Sapp模块.
    /// </summary>
    /// <typeparam name="TModule">模块类型.</typeparam>
    /// <typeparam name="TOptions">模块配置.</typeparam>
    /// <param name="builder"><see cref="WebApplicationBuilder" />.</param>
    /// <param name="options">配置.</param>
    /// <param name="configureModuleBuilder"><see cref="ModuleBuilder" />.</param>
    public static void AddSappApp<TModule, TOptions>(this WebApplicationBuilder builder, Action<TOptions> options, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions(options);
        moduleBuilder.Initialize();
    }

    public static void AddSappApp<TModule, TOptions>(this WebApplicationBuilder builder, Action<TOptions> options, string section, Action<ModuleBuilder<TModule>>? configureModuleBuilder = null)
     where TModule : BaseModule, new()
     where TOptions : class, new()
    {
        var moduleBuilder = new ModuleBuilder<TModule>(new BuilderContext(builder));
        configureModuleBuilder?.Invoke(moduleBuilder);
        moduleBuilder.WithOptions(GetAppSectionString(section), options);
        moduleBuilder.Initialize();
    }

    private static string GetLibSectionString(string section)
    {
        return "SappLib:" + section;
    }

    private static string GetAppSectionString(string section)
    {
        return "SappApp:" + section;
    }
}
