﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Abstractions;
using SapphireLib.Module.Core;

namespace SapphireLib.Module.Extensions;

public static class WebApplicationExtension
{
    public static void UseSappLib<TModule>(this WebApplication app)
        where TModule : notnull, IModule
    {
        var module = app.Services.GetRequiredService<TModule>();
        module.Run(new RunningContext(module.BuilderContext, app));
    }
}
