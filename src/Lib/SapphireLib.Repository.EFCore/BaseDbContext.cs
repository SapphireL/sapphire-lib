﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using SapphireLib.Repository.EFCore.Definitions;

namespace SapphireLib.Repository.EFCore;

public abstract class BaseDbContext<TContext> : DbContext
    where TContext : BaseDbContext<TContext>
{
    protected virtual EFCoreOptions<TContext> BaseDbConfigOptions { get; set; } = default!;

    public BaseDbContext(EFCoreOptions<TContext> configOptions)
    {
        BaseDbConfigOptions = configOptions;
    }

    protected BaseDbContext()
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var dbType = BaseDbConfigOptions.DbType;
        var connectionString = BaseDbConfigOptions.ConnectionString;
        switch (dbType)
        {
            case "SqlServer":
                optionsBuilder.UseSqlServer(connectionString).EnableSensitiveDataLogging().LogTo(Console.WriteLine, LogLevel.Debug);
                break;
            case "MySql":
                optionsBuilder.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)).EnableSensitiveDataLogging().LogTo(Console.WriteLine, LogLevel.Debug);
                break;
            case "Sqlite":
                optionsBuilder.UseSqlite(connectionString);
                break;
            case "PostgreSQL":
                optionsBuilder.UseNpgsql(connectionString);
                break;
            default:
                // 在异常消息中包含调用堆栈信息
                var stackTrace = Environment.StackTrace;
                throw new ArgumentException($"{typeof(TContext)}不支持的数据库类型: {dbType ?? "null"}\n DbContext 类型:{typeof(TContext)}\n调用堆栈:\n{stackTrace}");
        }

        base.OnConfiguring(optionsBuilder);
    }
}
