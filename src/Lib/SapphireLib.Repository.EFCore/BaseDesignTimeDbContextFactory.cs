﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

using SapphireLib.Repository.EFCore.Definitions;

namespace SapphireLib.Repository.EFCore;

public abstract class BaseDesignTimeDbContextFactory<TContext, TOptions> : IDesignTimeDbContextFactory<TContext>
    where TContext : BaseDbContext<TContext>
    where TOptions : EFCoreOptions<TContext>, new()
{
    public virtual TContext CreateDbContext(string[] args)
    {
        var configurationBuilder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("dbconfig.json", optional: false);

        Console.WriteLine(Directory.GetCurrentDirectory());

        var configuration = configurationBuilder.Build();

        var dbType = configuration["DbType"]?.ToString();
        var schema = configuration["Schema"]?.ToString();

        if (string.IsNullOrEmpty(dbType))
        {
            throw new ArgumentException("请指定数据库类型");
        }

        if (string.IsNullOrEmpty(schema))
        {
            throw new ArgumentException("请指定数据库schema");
        }

        var builder = new DbContextOptionsBuilder<TContext>();
        var connectionString = configuration["ConnectionString"];
        if (string.IsNullOrEmpty(connectionString))
        {
            throw new InvalidOperationException("连接字符串为空");
        }

        var dbConfig = new TOptions()
        {
            DbType = dbType,
            Schema = schema,
            ConnectionString = connectionString
        };

        if (Activator.CreateInstance(typeof(TContext), dbConfig) is not TContext context)
        {
            throw new InvalidOperationException("无法创建数据库上下文实例");
        }

        return context;
    }
}
