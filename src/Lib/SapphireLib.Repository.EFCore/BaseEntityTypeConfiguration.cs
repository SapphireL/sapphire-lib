﻿namespace SapphireLib.Repository.EFCore;

public abstract class BaseEntityTypeConfiguration(string? schema)
{
    protected string? Schema { get; } = schema;
}
