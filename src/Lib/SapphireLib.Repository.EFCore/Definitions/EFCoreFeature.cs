﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Core;

namespace SapphireLib.Repository.EFCore.Definitions
{
    public class EFCoreFeature<TDbContext> : BaseFeature
        where TDbContext : BaseDbContext<TDbContext>
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            var options = context.GetRequiredOptions<EFCoreOptions<TDbContext>>();
            context.Services.AddDbContext<TDbContext>();
            context.Services.AddSingleton(options);
        }
    }
}
