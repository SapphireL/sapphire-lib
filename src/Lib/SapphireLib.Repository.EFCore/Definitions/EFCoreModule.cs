﻿using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Module.Core;

namespace SapphireLib.Repository.EFCore.Definitions
{
    public class EFCoreModule<TDbContext> : BaseModule
        where TDbContext : BaseDbContext<TDbContext>
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddDbContext<TDbContext>();
        }
    }
}
