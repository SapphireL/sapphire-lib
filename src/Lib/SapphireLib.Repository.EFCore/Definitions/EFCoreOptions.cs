﻿namespace SapphireLib.Repository.EFCore.Definitions
{
    public class EFCoreOptions<TContext>
        where TContext : BaseDbContext<TContext>
    {
        public string ConnectionString { get; set; } = default!;

        public string? Schema { get; set; }

        public string DbType { get; set; } = default!;
    }
}
