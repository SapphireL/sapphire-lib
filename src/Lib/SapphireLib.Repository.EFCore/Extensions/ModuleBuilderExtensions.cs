﻿using SapphireLib.Module.Core;
using SapphireLib.Repository.Definitions;
using SapphireLib.Repository.EFCore.Definitions;

namespace SapphireLib.Repository.EFCore.Extensions;

public static class ModuleBuilderExtensions
{
    public static void AddEFCore<TDbContext>(this ModuleBuilder<RepositoryModule> moduleBuilder, string? section = "Repository:EFCore", Action<EFCoreOptions<TDbContext>>? configure = null)
        where TDbContext : BaseDbContext<TDbContext>
    {
        moduleBuilder.WithFeature<EFCoreFeature<TDbContext>, EFCoreOptions<TDbContext>>(section, configure);
    }

    /// <summary>
    /// 扩展方法，用于向 <see cref="FeaturesBuilder"/> 添加 Entity Framework Core 功能。
    /// </summary>
    /// <typeparam name="TDbContext">上下文类型，必须继承自 <see cref="BaseDbContext{TDbContext}"/>。</typeparam>
    /// <param name="builder">要扩展的 <see cref="FeaturesBuilder"/> 实例。</param>
    /// <param name="section">配置节的名称，默认为 "Repository:EFCore"。</param>
    /// <param name="configure">可选的配置方法，用于配置 EFCore 选项。</param>
    public static void AddEFCore<TDbContext>(this FeaturesBuilder builder, string? section = "Repository:EFCore", Action<EFCoreOptions<TDbContext>>? configure = null)
        where TDbContext : BaseDbContext<TDbContext>
    {
        builder.WithFeature<EFCoreFeature<TDbContext>, EFCoreOptions<TDbContext>>(section, configure);
    }

    public static void AddEFCore<TDbContext>(this FeaturesBuilder builder)
       where TDbContext : BaseDbContext<TDbContext>
    {
        builder.WithFeature<EFCoreFeature<TDbContext>, EFCoreOptions<TDbContext>>();
    }

    public static void AddEFCore1<TDbContext, TModule>(this FeaturesBuilder builder)
        where TDbContext : BaseDbContext<TDbContext>
        where TModule : BaseModule, new()
    {
        builder.WithFeature<EFCoreFeature<TDbContext>, EFCoreFeatureOptions<TModule>>();
    }
}
