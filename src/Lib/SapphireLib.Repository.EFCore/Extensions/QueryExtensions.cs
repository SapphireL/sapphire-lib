﻿using System.Linq.Expressions;

namespace SapphireLib.Repository.EFCore.Extensions;

public static class QueryExtensions
{
    public static IQueryable<T> ApplySorting<T>(this IQueryable<T> query, string sortBy, bool isDescending)
    {
        if (string.IsNullOrWhiteSpace(sortBy))
        {
            return query;
        }

        var param = Expression.Parameter(typeof(T));
        var property = Expression.Property(param, sortBy);
        var lambda = Expression.Lambda(property, param);

        string methodName = isDescending ? "OrderByDescending" : "OrderBy";
        var method = typeof(Queryable).GetMethods()
            .First(m => m.Name == methodName && m.GetParameters().Length == 2)
            .MakeGenericMethod(typeof(T), property.Type);

        return (IQueryable<T>)method.Invoke(null, [query, lambda])!;
    }
}
