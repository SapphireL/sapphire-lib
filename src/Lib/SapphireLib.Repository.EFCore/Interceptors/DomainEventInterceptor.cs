﻿using System.Reflection;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

using SapphireLib.Domain;
using SapphireLib.Domain.Attributes;
using SapphireLib.Domain.Entities;
using SapphireLib.Domain.Events;
using SapphireLib.UnitOfWork.Abstractions;

namespace SapphireLib.Repository.EFCore.Interceptors;

internal class DomainEventInterceptor<TDbContext>(IDomainEventBus domainEventBus) : IUnitOfWorkInterceptor<TDbContext>
    where TDbContext : BaseDbContext<TDbContext>
{
    public async ValueTask InterceptSavingChangesAsync(TDbContext context, CancellationToken cancellationToken = default)
    {
        var customEvents = GetDomainEvents(context);
        var auditableEvents = GetAuditableDomainEvents(context);
        await domainEventBus.DispatchDomainEventsAsync(customEvents.Concat(auditableEvents).ToList(), cancellationToken);
    }

    private static List<IDomainEvent> GetDomainEvents(TDbContext ctx)
    {
        var domainEvents = ctx.ChangeTracker
          .Entries<Entity>()
          .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Count != 0)
          .Select(x => x.Entity)
          .SelectMany(entity =>
          {
              IReadOnlyCollection<IDomainEvent> domainEvents = entity.DomainEvents;

              entity.ClearDomainEvents();

              return domainEvents;
          }).ToList();

        return domainEvents;
    }

    private static List<IDomainEvent> GetAuditableDomainEvents(TDbContext ctx)
    {
        var events = new List<IDomainEvent>();

        foreach (var entry in ctx.ChangeTracker.Entries())
        {
            var type = entry.Entity.GetType();

            switch (entry.State)
            {
                case EntityState.Added:
                    AddEventIfNeeded(events, type, CreateDomainEvent(typeof(EntityAddedEvent<>), entry.Entity));
                    break;

                case EntityState.Modified:
                    if (entry.Properties.Any(x => x.IsModified && (x.Metadata.ValueGenerated == ValueGenerated.Never || x.Metadata.ValueGenerated == ValueGenerated.OnAdd)))
                    {
                        var originalEntity = entry.OriginalValues.ToObject();
                        if (entry.Entity is ISoftDeletion softDeletion && softDeletion.IsDeleted)
                        {
                            AddEventIfNeeded(events, type, CreateDomainEvent(typeof(EntityDeletedEvent<>), entry.Entity));
                        }
                        else
                        {
                            AddEventIfNeeded(events, type, CreateDomainEvent(typeof(EntityUpdatedEvent<>), entry.Entity, originalEntity));
                        }
                    }

                    break;

                case EntityState.Deleted:
                    AddEventIfNeeded(events, type, CreateDomainEvent(typeof(EntityDeletedEvent<>), entry.Entity));
                    break;
            }
        }

        return events;
    }

    private static IDomainEvent CreateDomainEvent(Type eventType, object entity, object? originalEntity = null)
    {
        var genericType = eventType.MakeGenericType(entity.GetType());
        return (IDomainEvent)Activator.CreateInstance(genericType, originalEntity == null ? entity : new object[] { entity, originalEntity })!;
    }

    private static void AddEventIfNeeded(List<IDomainEvent> events, Type type, IDomainEvent domainEvent)
    {
        if (HasSendEventAttribute(type, domainEvent.GetType()))
        {
            events.Add(domainEvent);
        }
    }

    private static bool HasSendEventAttribute(Type entityType, Type eventType)
    {
        return HasSendAllEventsAttribute(entityType) ||
               (eventType == typeof(EntityAddedEvent<>) && HasSendAddedEventAttribute(entityType)) ||
               (eventType == typeof(EntityDeletedEvent<>) && HasSendDeletedEventAttribute(entityType)) ||
               (eventType == typeof(EntityUpdatedEvent<>) && HasSendUpdatedEventAttribute(entityType));
    }

    private static bool HasSendAllEventsAttribute(Type entityType)
    {
        var attribute = entityType.GetCustomAttribute<SendAuditableEventsAttribute>();
        return attribute?.IsEnabled == true;
    }

    private static bool HasSendAddedEventAttribute(Type entityType) => CheckEventAttribute<SendEntityAddedEventAttribute>(entityType);

    private static bool HasSendUpdatedEventAttribute(Type entityType) => CheckEventAttribute<SendEntityUpdatedEventAttribute>(entityType);

    private static bool HasSendDeletedEventAttribute(Type entityType) => CheckEventAttribute<SendEntityDeletedEventAttribute>(entityType);

    private static bool CheckEventAttribute<TAttribute>(Type entityType)
        where TAttribute : Attribute
    {
        return entityType.GetCustomAttribute<TAttribute>() != null;
    }
}
