﻿using SapphireLib.Module.Core;
using SapphireLib.Repository.SqlSugar;

namespace SapphireLib.Module.BusinessModule.Extensions;

public static class ModuleFeatureConfigurationExtension
{
    public static void AddSqlSugarCoreFeature<TModule>(this ModuleBuilder<TModule> builder, Action<SqlSugarConfiguration> configure)
        where TModule : BaseModule, new()
    {
        var extensionBuilder = new FeatureBuilder<SqlSugarCoreFeature>(builder.Context);
        extensionBuilder.WithOptions(configure);
        var extension = extensionBuilder.Build();
        builder.WithFeature(extension);
    }

    public static void AddSqlSugarCoreFeature<TModule>(this ModuleBuilder<TModule> builder)
        where TModule : BaseModule, new()
    {
        var extensionBuilder = new FeatureBuilder<SqlSugarCoreFeature>(builder.Context);
        var extension = extensionBuilder.Build();
        builder.WithFeature(extension);
    }
}
