﻿using SqlSugar;

namespace SapphireLib.Repository.SqlSugar
{
    public interface ISqlSugarDbContext
    {
        SqlSugarClient GetDatabase();
    }
}
