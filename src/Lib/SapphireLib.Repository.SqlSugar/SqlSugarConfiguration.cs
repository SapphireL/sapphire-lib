﻿using SqlSugar;

namespace SapphireLib.Repository.SqlSugar
{
    public class SqlSugarConfiguration
    {
        public string Name { get; set; } = "SqlSugarCore";

        public string ConnectionString { get; set; } = default!;

        public string? Schema { get; set; }

        public DbType DbType { get; set; } = default!;
    }
}
