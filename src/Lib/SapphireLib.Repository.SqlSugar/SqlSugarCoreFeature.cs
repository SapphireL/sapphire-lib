﻿using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Module.Core;

namespace SapphireLib.Repository.SqlSugar
{
    public class SqlSugarCoreFeature : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddScoped<ISqlSugarDbContext, SqlSugarDbContext>();
        }
    }
}
