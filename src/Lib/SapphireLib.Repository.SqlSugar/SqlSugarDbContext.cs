﻿using SqlSugar;

namespace SapphireLib.Repository.SqlSugar
{
    public class SqlSugarDbContext : ISqlSugarDbContext
    {
        private readonly SqlSugarClient _db;

        public SqlSugarDbContext(SqlSugarConfiguration config)
        {
            _db = new SqlSugarClient(new ConnectionConfig
            {
                ConnectionString = config.ConnectionString,
                DbType = config.DbType,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute
            });
        }

        public SqlSugarClient GetDatabase() => _db;
    }
}
