﻿using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Module.Core;
using SapphireLib.Repository;

namespace SapphireLib.Module.BusinessModule.Extensions
{
    public static class ModuleBuilderExtensions
    {
        public static void AddRepositories(this FeaturesBuilder builder, Action<RepositoryExtensionOptions> configure)
        {
            builder.WithFeature<RepositoryFeature, RepositoryExtensionOptions>(configure);
        }

        public static void AddRepository<TRepository, TImplement>(this FeaturesBuilder builder)
            where TImplement : class, IRepository, TRepository
            where TRepository : class
        {
            builder.Context.Services.TryAddScoped<IRepository, TImplement>();
            builder.Context.Services.TryAddScoped<TRepository, TImplement>();
        }
    }
}
