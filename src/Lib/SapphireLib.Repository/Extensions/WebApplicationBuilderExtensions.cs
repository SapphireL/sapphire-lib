﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;
using SapphireLib.Repository.Definitions;

namespace SapphireLib.Repository.Extensions;

public static class WebApplicationBuilderExtensions
{
    public static WebApplicationBuilder AddSappLibRepository(this WebApplicationBuilder builder, Action<ModuleBuilder<RepositoryModule>>? moduleBuilder = null)
    {
        builder.AddSappLib(moduleBuilder);
        return builder;
    }
}
