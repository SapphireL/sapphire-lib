﻿namespace SapphireLib.Repository
{
    public interface IDbContextProvider
    {
        public Task<int> SaveEntitiesAsync(CancellationToken cancellationToken = default);
    }
}
