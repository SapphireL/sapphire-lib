﻿namespace SapphireLib.Repository
{
    public abstract class RepositoryBase : IRepository
    {
        private readonly IDbContextProvider _dbContextProvider;

        public RepositoryBase(IDbContextProvider dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }

        protected virtual Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return _dbContextProvider.SaveEntitiesAsync(cancellationToken);
        }
    }
}
