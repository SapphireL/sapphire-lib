﻿using System.Reflection;

namespace SapphireLib.Repository
{
    public class RepositoryExtensionOptions
    {
        public Assembly RepositoryAssemblyToScan { get; set; } = default!;
    }
}
