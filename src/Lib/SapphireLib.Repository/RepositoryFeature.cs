﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Core;

namespace SapphireLib.Repository
{
    internal class RepositoryFeature : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            var options = context.GetRequiredOptions<RepositoryExtensionOptions>();
            context.Services.Scan(scan =>
            {
                scan.FromAssemblies(options.RepositoryAssemblyToScan)
                .AddClasses(classes => classes.AssignableTo<IRepository>())
                .AsImplementedInterfaces().WithScopedLifetime();
            });
        }
    }
}
