﻿using SapphireLib.Module.Exceptions;

namespace SapphireLib.Repository
{
    public class RepositoryFeatureException(string message, Type moduleType) : ModuleFeatureException(message, nameof(moduleType.Name))
    {
    }
}
