using Microsoft.Extensions.DependencyInjection;

namespace SapphireLib.Tools.Test
{
    public class ServiceCollectionHostedServiceExtensionsTest
    {
        [Fact]
        public void AsSelf_Should_Register_Last_Registration_As_Self_With_Service_Transient()
        {
            // Arrange
            var services = new ServiceCollection();

            // Act
            services.AddTransient<IService, ServiceA>().AsSelf();

            // Assert
            var serviceProvider = services.BuildServiceProvider();
            var serviceA1 = serviceProvider.GetService<IService>();
            var serviceA2 = serviceProvider.GetService<ServiceA>();

            Assert.NotNull(serviceA1);
            Assert.NotNull(serviceA2);
            Assert.IsType<ServiceA>(serviceA1);
            Assert.IsType<ServiceA>(serviceA2);
        }

        [Fact]
        public void AsSelf_Should_Register_Last_Registration_As_Self_With_Service_Singleton()
        {
            // Arrange
            var services = new ServiceCollection();

            // Act
            services.AddSingleton<IService, ServiceA>().AsSelf();

            // Assert
            var serviceProvider = services.BuildServiceProvider();
            var serviceA1 = serviceProvider.GetService<IService>();
            var serviceA2 = serviceProvider.GetService<ServiceA>();

            Assert.NotNull(serviceA1);
            Assert.NotNull(serviceA2);
            Assert.IsType<ServiceA>(serviceA1);
            Assert.IsType<ServiceA>(serviceA2);
        }

        [Fact]
        public void AsSelf_Should_Register_Last_Registration_As_Self_With_Service_Scoped()
        {
            // Arrange
            var services = new ServiceCollection();

            // Act
            services.AddScoped<IService, ServiceA>().AsSelf();

            // Assert
            var serviceProvider = services.BuildServiceProvider();
            var serviceA1 = serviceProvider.GetService<IService>();
            var serviceA2 = serviceProvider.GetService<ServiceA>();

            Assert.NotNull(serviceA1);
            Assert.NotNull(serviceA2);
            Assert.IsType<ServiceA>(serviceA1);
            Assert.IsType<ServiceA>(serviceA2);
        }

        [Fact]
        public void AsSelf_Should_Register_Last_Registration_As_Self_With_Service_GenericType()
        {
            // Arrange
            var services = new ServiceCollection();

            // Act
            services.AddTransient<IServiceGeneric<int>, ServiceGenericA>().AsSelf();

            // Assert
            var serviceProvider = services.BuildServiceProvider();
            var serviceA1 = serviceProvider.GetService<IServiceGeneric<int>>();
            var serviceA2 = serviceProvider.GetService<ServiceGenericA>();

            Assert.NotNull(serviceA1);
            Assert.NotNull(serviceA2);
            Assert.IsType<ServiceGenericA>(serviceA1);
            Assert.IsType<ServiceGenericA>(serviceA2);
        }

        [Fact]
        public void AsSelf_Should_Register_Last_Registration_As_Self_With_Service_Instance()
        {
            // Arrange
            var services = new ServiceCollection();
            var serviceA = new ServiceA();

            // Act
            services.AddSingleton<IService>(serviceA).AsSelf();

            // Assert
            var serviceProvider = services.BuildServiceProvider();
            var serviceA1 = serviceProvider.GetService<IService>();
            var serviceA2 = serviceProvider.GetService<ServiceA>();

            Assert.NotNull(serviceA1);
            Assert.NotNull(serviceA2);
            Assert.IsType<ServiceA>(serviceA1);
            Assert.IsType<ServiceA>(serviceA2);
        }

        [Fact]
        public void AsSelf_Should_Register_Last_Registration_As_Self_With_Service_Factory()
        {
            // Arrange
            var services = new ServiceCollection();
            var serviceA = new ServiceA();

            // Act
            services.AddSingleton<IService>(sp => serviceA).AsSelf();

            // Assert
            var serviceProvider = services.BuildServiceProvider();
            var serviceA1 = serviceProvider.GetService<IService>();
            var serviceA2 = serviceProvider.GetService<ServiceA>();

            Assert.NotNull(serviceA1);
            Assert.NotNull(serviceA2);
            Assert.IsType<ServiceA>(serviceA1);
            Assert.IsType<ServiceA>(serviceA2);
        }

        [Fact]
        public void Should_Be_Null_When_No_As_Self()
        {
            // Arrange
            var services = new ServiceCollection();

            // Act
            services.AddTransient<IService, ServiceA>();

            // Assert
            var serviceProvider = services.BuildServiceProvider();
            var serviceA1 = serviceProvider.GetService<IService>();
            var serviceA2 = serviceProvider.GetService<ServiceA>();

            // Act & Assert
            Assert.NotNull(serviceA1);
            Assert.Null(serviceA2);
        }
    }

    public interface IService
    {
        void DoSomething();
    }

    public class ServiceA : IService
    {
        public void DoSomething()
        {
        }
    }

    public class ServiceB : IService
    {
        public void DoSomething()
        {
        }
    }

    public interface IServiceGeneric<T>
    {
    }

    public class ServiceGenericA : IServiceGeneric<int>
    {
    }

    public class ServiceGenericB : IServiceGeneric<string>
    {
    }
}