﻿namespace SapphireLib.Tools
{
    public static class ActionExtensions
    {
        public static Action<TParent>? ToParent<TChild, TParent>(this Action<TChild>? action)
            where TChild : class
        {
            return parent =>
            {
                if (parent != null)
                {
                    var child = parent as TChild;
                    if (child != null)
                    {
                        action?.Invoke(child);
                    }
                }
            };
        }
    }
}
