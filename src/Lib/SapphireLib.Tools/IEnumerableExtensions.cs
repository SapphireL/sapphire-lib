﻿namespace SapphireLib.Tools;

public static class IEnumerableExtensions
{
    /// <summary>
    /// 迭代集合.
    /// </summary>
    /// <typeparam name="T">集合类型.</typeparam>
    /// <param name="collection">集合.</param>
    /// <param name="action">迭代行为.</param>
    public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
    {
        foreach (T element in collection)
        {
            action(element);
        }
    }

    public static async ValueTask<List<TSource>> ToListAsync<TSource>(this IAsyncEnumerable<TSource> source, CancellationToken cancellationToken = default)
    {
        var list = new List<TSource>();

        await foreach (var item in source.WithCancellation(cancellationToken).ConfigureAwait(false))
        {
            list.Add(item);
        }

        return list;
    }
}
