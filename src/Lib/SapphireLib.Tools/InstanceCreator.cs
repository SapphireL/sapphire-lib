﻿using System.Reflection;

namespace SapphireLib.Tools
{
    /// <summary>
    /// 创建对象.
    /// </summary>
    public static class InstanceCreator
    {
        /// <summary>
        /// 从程序集中创建指定类型的对象.
        /// </summary>
        /// <typeparam name="TServiceType">需要创建的对象类型.</typeparam>
        /// <param name="assemblies">需要扫描的程序集.</param>
        /// <returns>从指定程序集中创建的指定类型的对象集合.</returns>
        public static IEnumerable<TServiceType> CreateFromAssemblies<TServiceType>(params Assembly[] assemblies) =>
            assemblies
            .SelectMany(assembly => assembly.DefinedTypes)
            .Where(IsAssignableToType<TServiceType>)
            .Select(Activator.CreateInstance)
            .Cast<TServiceType>();

        /// <summary>
        /// 从程序集中创建指定类型的对象.
        /// </summary>
        /// <typeparam name="TServiceType">需要创建的对象类型.</typeparam>
        /// <typeparam name="TGenericType">对象泛型类型.</typeparam>
        /// <param name="assemblies">需要扫描的程序集.</param>
        /// <returns>从指定程序集中创建的指定类型的对象集合.</returns>
        public static IEnumerable<TServiceType> CreateFromAssemblies<TServiceType, TGenericType>(params Assembly[] assemblies)
             where TServiceType : class
        {
            var serviceType = typeof(TServiceType);
            var genericType = typeof(TGenericType);
            return assemblies
           .SelectMany(assembly => assembly.DefinedTypes)
           .Where(IsAssignableToType<TServiceType, TGenericType>)
           .Select(Activator.CreateInstance)
           .Cast<TServiceType>();
        }

        private static bool IsAssignableToType<TServiceType>(TypeInfo typeInfo)
        {
            return typeof(TServiceType).IsAssignableFrom(typeInfo) && !typeInfo.IsInterface && !typeInfo.IsAbstract;
        }

        private static bool IsAssignableToType<TServiceType, TGenericType>(TypeInfo typeInfo)
        {
            var serviceType = typeof(TServiceType);
            var genericType = typeof(TGenericType);
            return typeInfo.IsClass && !typeInfo.IsAbstract && !typeInfo.IsInterface &&
                          serviceType.IsAssignableFrom(typeInfo) &&
                          typeInfo.GetInterfaces().Any(i => i.IsGenericType &&
                                                         i.GetGenericTypeDefinition() == typeof(TServiceType) &&
                                                         genericType.IsAssignableFrom(i.GenericTypeArguments[0]));
        }
    }
}
