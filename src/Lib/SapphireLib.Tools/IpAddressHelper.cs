﻿using IPTools.Core;

using Microsoft.AspNetCore.Builder;

namespace SapphireLib.Tools;

public static class IpAddressHelper
{
    public static (string IpLocation, double? Longitude, double? Latitude) GetIpAddress(string ip)
    {
        if (string.IsNullOrWhiteSpace(ip))
        {
            return ("未知", null, null);
        }

        // IpToolSettings.LoadInternationalDbToMemory = true;
        var ipInfo = IpTool.SearchWithI18N(ip);

        var validAddress = string.Join(" ", new[] { ipInfo.Country, ipInfo.Province, ipInfo.City, ipInfo.NetworkOperator }
                                        .Where(u => !string.IsNullOrWhiteSpace(u) && u != "0"));

        return (validAddress, ipInfo.Longitude, ipInfo.Latitude);
    }

    public static void ConfigIpHelper(this WebApplicationBuilder builder)
    {
        IpToolSettings.ChinaDbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Assets/ip2region.db");
        IpToolSettings.InternationalDbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Assets/GeoLite2-City.mmdb");
        IpToolSettings.LoadInternationalDbToMemory = true;
        IpToolSettings.DefalutSearcherType = IpSearcherType.International;
    }
}
