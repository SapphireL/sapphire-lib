﻿using System.Security.Cryptography;
using System.Text;

namespace SapphireLib.Tools
{
    public class MD5Generator
    {
        public static string GenerateDoubleSaltedMD5(string input, string? salt)
        {
            // 第一次 MD5 运算
            string firstHash = CalculateMD5Hash(input, salt);

            // 第二次 MD5 运算
            string doubleHash = CalculateMD5Hash(firstHash, salt);
            return doubleHash;
        }

        public static string GenerateSaltedMD5(string input, string? salt)
        {
            return CalculateMD5Hash(input, salt);
        }

        private static string CalculateMD5Hash(string input, string? salt = "")
        {
            // 将输入字符串和盐值连接起来
            string saltedInput = input + salt;

            // 将字符串转换为字节数组
            byte[] bytes = Encoding.UTF8.GetBytes(saltedInput);

            // 使用 MD5 加密算法计算哈希值
            byte[] hashBytes = MD5.HashData(bytes);

            // 将字节数组转换为字符串表示
            StringBuilder sb = new();
            foreach (byte b in hashBytes)
            {
                sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }
    }
}
