﻿using System.Text;

namespace SapphireLib.Tools;

public class RandomStringGenerator
{
    private const string CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static readonly Random _random = new();

    public static string GenerateRandomString(int length, string? candidateString = null)
    {
        var ca = candidateString ?? CHARS;
        var stringBuilder = new StringBuilder(length);
        for (int i = 0; i < length; i++)
        {
            stringBuilder.Append(ca[_random.Next(ca.Length)]);
        }

        return stringBuilder.ToString();
    }
}
