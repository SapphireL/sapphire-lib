﻿using System.Reflection;

using Microsoft.Extensions.DependencyInjection;

namespace SapphireLib.Tools
{
    public static class ServiceCollectionHostedServiceExtensions
    {
        public static IServiceCollection AsSelf(this IServiceCollection services)
        {
            var lastRegistration = services.LastOrDefault();
            if (lastRegistration != null)
            {
                var implementationType = GetImplementationType(lastRegistration);
                if (lastRegistration.ServiceType == implementationType)
                {
                    return services;
                }

                if (lastRegistration.ImplementationInstance != null)
                {
                    services.Add(new ServiceDescriptor(implementationType, lastRegistration.ImplementationInstance));
                }
                else
                {
                    services.Remove(lastRegistration);
                    if (lastRegistration.ImplementationFactory != null)
                    {
                        services.Add(new ServiceDescriptor(implementationType, lastRegistration.ImplementationFactory, lastRegistration.Lifetime));
                    }
                    else
                    {
                        services.Add(new ServiceDescriptor(
                                    implementationType,
                                    implementationType,
                                    lastRegistration.Lifetime));
                    }

                    services.Add(new ServiceDescriptor(
                            lastRegistration.ServiceType,
                            provider =>
                            {
                                return provider.GetService(implementationType) ?? throw new InvalidOperationException("获取服务失败。");
                            },
                            lastRegistration.Lifetime));
                }
            }

            return services;
        }

        public static IServiceCollection AddTransients<TServiceType>(this IServiceCollection services, params Assembly[] assemblies)
        {
            var serviceType = typeof(TServiceType);
            services.AddTransients(serviceType, assemblies);
            return services;
        }

        public static IServiceCollection AddTransients(this IServiceCollection services, Type serviceType, params Assembly[] assemblies)
        {
            var instanceTypes = GetInstanceTypeFrom(serviceType.GetTypeInfo(), assemblies);

            foreach (var instanceType in instanceTypes)
            {
                services.AddTransient(serviceType, instanceType);
            }

            return services;
        }

        public static IServiceCollection AddSingletons<TServiceType>(this IServiceCollection services, params Assembly[] assemblies)
        {
            var serviceType = typeof(TServiceType);
            services.AddSingletons(serviceType, assemblies);
            return services;
        }

        public static IServiceCollection AddSingletons(this IServiceCollection services, Type serviceType, params Assembly[] assemblies)
        {
            var instanceTypes = GetInstanceTypeFrom(serviceType.GetTypeInfo(), assemblies);
            foreach (var instanceType in instanceTypes)
            {
                services.AddSingleton(serviceType, instanceType);
            }

            return services;
        }

        private static Type GetImplementationType(ServiceDescriptor descriptor)
        {
            if (descriptor.ImplementationType != null)
            {
                return descriptor.ImplementationType;
            }

            if (descriptor.ImplementationInstance != null)
            {
                return descriptor.ImplementationInstance.GetType();
            }

            if (descriptor.ImplementationFactory != null)
            {
                return descriptor.ImplementationFactory.Method.ReturnType;
            }

            throw new InvalidOperationException("无法获取服务描述符的实现类型。");
        }

        private static IEnumerable<TypeInfo> GetInstanceTypeFrom(TypeInfo serviceType, params Assembly[] assemblies)
        {
            return assemblies.SelectMany(assembly => assembly.DefinedTypes).Where(t => IsAssignableToType(t, serviceType.GetTypeInfo()));
        }

        private static bool IsAssignableToType(TypeInfo typeInfo, TypeInfo serviceType)
        {
            return serviceType.IsAssignableFrom(typeInfo) && !typeInfo.IsInterface && !typeInfo.IsAbstract;
        }
    }
}
