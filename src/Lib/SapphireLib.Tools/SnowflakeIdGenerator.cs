﻿namespace SapphireLib.Tools;

public static class SnowflakeIdGenerator
{
    private const int MachineIdBits = 5;
    private const int DatacenterIdBits = 5;
    private const int SequenceBits = 12;
    private const long MaxMachineId = -1L ^ -1L << MachineIdBits;
    private const long MaxDatacenterId = -1L ^ -1L << DatacenterIdBits;
    private const int MachineIdShift = SequenceBits;
    private const int DatacenterIdShift = SequenceBits + MachineIdBits;
    private const int TimestampLeftShift = SequenceBits + MachineIdBits + DatacenterIdBits;
    private const long SequenceMask = -1L ^ -1L << SequenceBits;
    private static readonly object _lock = new object();
    private static readonly long _epoch = 1288834974657L; // Twitter的epoch时间戳，可以根据需要调整
    private static long _machineId;
    private static long _datacenterId;
    private static long _sequence;
    private static long _lastTimestamp = -1L;

    public static void Initialize(long machineId, long datacenterId, long sequence = 0L)
    {
        if (machineId > MaxMachineId || machineId < 0)
        {
            throw new ArgumentException($"Machine ID must be between 0 and {MaxMachineId}");
        }

        if (datacenterId > MaxDatacenterId || datacenterId < 0)
        {
            throw new ArgumentException($"Datacenter ID must be between 0 and {MaxDatacenterId}");
        }

        _machineId = machineId;
        _datacenterId = datacenterId;
        _sequence = sequence;
    }

    public static long NextId()
    {
        lock (_lock)
        {
            var timestamp = GetCurrentTimestamp();

            if (timestamp < _lastTimestamp)
            {
                throw new InvalidOperationException("Clock moved backwards. Refusing to generate id");
            }

            if (_lastTimestamp == timestamp)
            {
                _sequence = _sequence + 1 & SequenceMask;
                if (_sequence == 0)
                {
                    timestamp = WaitForNextTimestamp(_lastTimestamp);
                }
            }
            else
            {
                _sequence = 0L;
            }

            _lastTimestamp = timestamp;

            return (timestamp - _epoch) << TimestampLeftShift |
                   _datacenterId << DatacenterIdShift |
                   _machineId << MachineIdShift |
                   _sequence;
        }
    }

    private static long GetCurrentTimestamp()
    {
        return DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
    }

    private static long WaitForNextTimestamp(long lastTimestamp)
    {
        var timestamp = GetCurrentTimestamp();
        while (timestamp <= lastTimestamp)
        {
            timestamp = GetCurrentTimestamp();
        }

        return timestamp;
    }
}