﻿namespace SapphireLib.UnifiedException
{
    public abstract class BaseUnifiedException : Exception
    {
        public BaseUnifiedException()
        {
        }

        public BaseUnifiedException(string message)
            : base(message)
        {
        }

        public BaseUnifiedException(string message, Exception? innerException)
           : base(message, innerException)
        {
        }
    }
}
