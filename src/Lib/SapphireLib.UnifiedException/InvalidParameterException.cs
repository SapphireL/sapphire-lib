﻿namespace SapphireLib.UnifiedException
{
    public sealed class InvalidParameterException(IEnumerable<ParameterExceptionDetail> errors)
        : BaseUnifiedException
    {
        public IEnumerable<ParameterExceptionDetail> Errors { get; set; } = errors;
    }
}
