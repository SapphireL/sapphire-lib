﻿namespace SapphireLib.UnifiedException
{
    public sealed class ParameterExceptionDetail
    {
        public string ParameterName { get; set; } = default!;

        public IEnumerable<UnifiedExceptionErrorDetail> Errors { get; set; } = [];
    }
}
