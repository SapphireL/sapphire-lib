﻿namespace SapphireLib.UnifiedException
{
    public class SystemException : BaseUnifiedException
    {
        public SystemException(string message)
            : base(message)
        {
        }

        public SystemException(string message, Exception? innerException)
           : base(message, innerException)
        {
        }
    }
}
