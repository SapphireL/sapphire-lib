﻿namespace SapphireLib.UnifiedException
{
    public class UnifiedExceptionErrorDetail
    {
        public int? ErrorCode { get; set; }

        public string Message { get; set; } = default!;
    }
}
