﻿using Microsoft.EntityFrameworkCore;

using SapphireLib.UnitOfWork.Abstractions;

namespace SapphireLib.UnitOfWork.EFCore.Core;

internal class UnitOfWork<TDbContext> : IUnitOfWork
    where TDbContext : DbContext
{
    private readonly TDbContext _context;
    private readonly IUnitOfWorkInterceptor<TDbContext>? _interceptor;

    public UnitOfWork(TDbContext context, IUnitOfWorkInterceptor<TDbContext>? interceptor = null)
    {
        _context = context;
        _interceptor = interceptor;
    }

    public TDbContext Context => _context;

    public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        var entries = _context.ChangeTracker.Entries()
     .Where(e => e.State == EntityState.Modified || e.State == EntityState.Added)
     .ToList();

        if (_interceptor != null)
        {
            await _interceptor.InterceptSavingChangesAsync(_context, cancellationToken);
        }

        await _context.SaveChangesAsync(cancellationToken);
    }
}