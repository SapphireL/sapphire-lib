﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Module.Core;
using SapphireLib.UnitOfWork.Abstractions;
using SapphireLib.UnitOfWork.Definitions;
using SapphireLib.UnitOfWork.EFCore.Core;

namespace SapphireLib.UnitOfWork.EFCore.Definitions;

public class SappUnitOfWorkEFCoreFeature<TDbContext> : BaseFeature
      where TDbContext : DbContext
{
    protected override void OnConfiguring(FeaturesBuilder builder)
    {
        var options = builder.Context.GetRequiredOptions<SappLibUnitOfWorkFeatureOptions>();
        builder.Context.Services.TryAddKeyedScoped<IUnitOfWork, UnitOfWork<TDbContext>>(options.DbContextServiceKey);
    }
}
