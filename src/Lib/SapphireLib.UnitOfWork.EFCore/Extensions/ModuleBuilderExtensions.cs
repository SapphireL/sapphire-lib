﻿using Microsoft.EntityFrameworkCore;

using SapphireLib.Module.Core;
using SapphireLib.UnitOfWork.Definitions;
using SapphireLib.UnitOfWork.EFCore.Definitions;

namespace SapphireLib.UnitOfWork.EFCore.Extensions;

public static class ModuleBuilderExtensions
{
    public static void UseEFCore<TDbContext>(this ModuleBuilder<SapphireLibUnitOfWorkModule> moduleBuilder, Action<SappLibUnitOfWorkFeatureOptions> options)
        where TDbContext : DbContext
    {
        moduleBuilder.WithFeature<SappUnitOfWorkEFCoreFeature<TDbContext>, SappLibUnitOfWorkFeatureOptions>(options);
    }

    public static void UseEFCore<TDbContext>(this FeatureBuilder<SappLibUnitOfWorkFeature> featureBuilder)
       where TDbContext : DbContext
    {
        featureBuilder.WithFeature<SappUnitOfWorkEFCoreFeature<TDbContext>>();
    }
}
