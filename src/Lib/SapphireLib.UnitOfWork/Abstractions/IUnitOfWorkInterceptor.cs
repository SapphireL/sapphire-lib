﻿namespace SapphireLib.UnitOfWork.Abstractions;

public interface IUnitOfWorkInterceptor<in TDbContext>
{
    public ValueTask InterceptSavingChangesAsync(TDbContext context, CancellationToken cancellationToken = default);
}
