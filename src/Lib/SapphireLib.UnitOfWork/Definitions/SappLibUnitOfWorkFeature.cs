﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Core;
using SapphireLib.UnitOfWork.Abstractions;

namespace SapphireLib.UnitOfWork.Definitions;

public class SappLibUnitOfWorkFeature : BaseFeature
{
    protected override bool CheckModuleHealthy(RunningContext context)
    {
        using var scope = context.WebApplication.Services.CreateScope();
        var uow = scope.ServiceProvider.GetService<IUnitOfWork>();
        if (uow == null)
        {
            return false;
        }

        return true;
    }

    protected override void OnRunning(RunningContext context)
    {
        base.OnRunning(context);
    }
}
