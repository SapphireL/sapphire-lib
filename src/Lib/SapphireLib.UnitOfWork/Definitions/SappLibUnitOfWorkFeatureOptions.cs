﻿namespace SapphireLib.UnitOfWork.Definitions;

public class SappLibUnitOfWorkFeatureOptions
{
    public string DbContextServiceKey { get; set; } = default!;
}
