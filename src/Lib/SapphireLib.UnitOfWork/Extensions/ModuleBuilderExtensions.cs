﻿using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Module.Core;
using SapphireLib.UnitOfWork.Abstractions;
using SapphireLib.UnitOfWork.Definitions;

namespace SapphireLib.UnitOfWork.Extensions;

public static class ModuleBuilderExtensions
{
    public static void AddInterceptor<TInterceptor, TDbContext>(this ModuleBuilder<SapphireLibUnitOfWorkModule> builder)
        where TInterceptor : class, IUnitOfWorkInterceptor<TDbContext>
    {
        builder.Context.Services.TryAddScoped<IUnitOfWorkInterceptor<TDbContext>, TInterceptor>();
    }

    public static void AddInterceptor<TInterceptor, TDbContext>(this FeatureBuilder<SappLibUnitOfWorkFeature> builder)
       where TInterceptor : class, IUnitOfWorkInterceptor<TDbContext>
    {
        builder.Context.Services.TryAddScoped<IUnitOfWorkInterceptor<TDbContext>, TInterceptor>();
    }

    public static void AddUnitOfWorkFeature(this FeaturesBuilder featuresBuilder, Action<FeatureBuilder<SappLibUnitOfWorkFeature>> config, Action<SappLibUnitOfWorkFeatureOptions> options)
    {
        featuresBuilder.WithFeature(config, options);
    }
}
