﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;
using SapphireLib.UnitOfWork.Definitions;

namespace SapphireLib.UnitOfWork.Extensions;

public static class WebApplicationBuilderExtensions
{
    public static WebApplicationBuilder AddUnitOfWorkModule(this WebApplicationBuilder webApplicationBuilder, Action<ModuleBuilder<SapphireLibUnitOfWorkModule>> configure)
    {
        webApplicationBuilder.AddSappLib(configure);
        return webApplicationBuilder;
    }
}
