﻿using SapphireLib.Validator.Checkers;

namespace SapphireLib.Validator.Test
{
    public class AccountTest
    {
        [Fact]
        public void Account_Should_True()
        {
            // Arrange
            string a = "ajhskajska6566";
            string b = "44656565656";
            string c = "44656565656";
            string patterna = "^[a-zA-Z0-9]+$";
            string patternb = "^[a-zA-Z0-9]+$";

            // Act
            var result1 = new AccountChecker(patterna).IsValid(a);
            var result2 = new AccountChecker(patternb).IsValid(b);
            var result3 = new AccountChecker(null).IsValid(c);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
            Assert.True(result3);
        }

        [Fact]
        public void Account_Should_False()
        {
            // Arrange
            string a = "___)(*&*&";
            string b = "  ";
            string patterna = "^[a-zA-Z0-9]+$";
            string patternb = "^[a-zA-Z0-9]+$";

            // Act
            var result1 = new AccountChecker(patterna).IsValid(a);
            var result2 = new AccountChecker(patternb).IsValid(b);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
        }
    }
}
