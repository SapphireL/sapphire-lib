﻿using SapphireLib.Validator.Rules;

namespace SapphireLib.Validator.Test
{
    public class GreaterThanTest
    {
        [Fact]
        public void GreaterThan_Should_True()
        {
            // Arrange
            int a = 1;
            DateTime b = DateTime.Now;

            // Act
            var result1 = new GreaterThanChecker<int>(0).IsValid(a);
            var result2 = new GreaterThanChecker<DateTime>(DateTime.Now.AddDays(-1)).IsValid(b);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
        }

        [Fact]
        public void GreaterThan_Should_False()
        {
            // Arrange
            int a = 1;
            DateTime b = DateTime.Now;

            // Act
            var result1 = new GreaterThanChecker<int>(2).IsValid(a);
            var result2 = new GreaterThanChecker<DateTime>(DateTime.Now.AddDays(1)).IsValid(b);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
        }
    }
}
