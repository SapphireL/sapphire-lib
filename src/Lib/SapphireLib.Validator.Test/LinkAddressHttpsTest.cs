﻿using SapphireLib.Validator.Checkers;

namespace SapphireLib.Validator.Test
{
    public class LinkAddressHttpsTest
    {
        [Fact]
        public void LinkAddressHttps_Should_True()
        {
            // Arrange
            string a = "https://www.baidu.com/";
            string b = "HTTPS://www.baidu.com/";

            // Act
            var result1 = new LinkAddressHttpsChecker().IsValid(a);
            var result2 = new LinkAddressHttpsChecker().IsValid(b);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
        }

        [Fact]
        public void LinkAddressHttps_Should_False()
        {
            // Arrange
            string a = "http://www.baidu.com/";
            string b = "Http://www.baidu.com/  ";

            // Act
            var result1 = new LinkAddressHttpsChecker().IsValid(a);
            var result2 = new LinkAddressHttpsChecker().IsValid(b);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
        }
    }
}
