using SapphireLib.Validator.Rules;

namespace SapphireLib.Validator.Test
{
    public class NotNullOrEmptyTest
    {
        [Fact]
        public void NotNullOrEmpty_Should_False_WithEmptyEnumerable()
        {
            // Arrange
            var list = new List<string>();
            int[] arr = Array.Empty<int>();
            var dic = new Dictionary<int, int>();
            var has = new HashSet<int>();

            // Act
            var result1 = new NotNullOrEmptyChecker<List<string>>().IsValid(list);
            var result2 = new NotNullOrEmptyChecker<int[]>().IsValid(arr);
            var result3 = new NotNullOrEmptyChecker<Dictionary<int, int>>().IsValid(dic);
            var result4 = new NotNullOrEmptyChecker<HashSet<int>>().IsValid(has);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
            Assert.False(result3);
            Assert.False(result4);
        }

        [Fact]
        public void NotNullOrEmpty_Should_False_WithArrangedEnumerable()
        {
            // Arrange
            var list = new List<string> { "s" };
            int[] arr = [1, 2];
            var dic = new Dictionary<int, int> { { 1, 1 } };
            var has = new HashSet<string>()
            {
                "Alice"
            };

            // Act
            var result1 = new NotNullOrEmptyChecker<List<string>>().IsValid(list);
            var result2 = new NotNullOrEmptyChecker<int[]>().IsValid(arr);
            var result3 = new NotNullOrEmptyChecker<Dictionary<int, int>>().IsValid(dic);
            var result4 = new NotNullOrEmptyChecker<HashSet<string>>().IsValid(has);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
            Assert.True(result3);
            Assert.True(result4);
        }

        [Fact]
        public void NotNullOrEmpty_Should_False_WithObj()
        {
            // Arrange
            A obj = new A();

            // Act
            var result = new NotNullOrEmptyChecker<B>().IsValid(obj.B);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void NotNullOrEmpty_Should_True_WithObj()
        {
            // Arrange
            A obj = new A();

            // Act
            var result = new NotNullOrEmptyChecker<A>().IsValid(obj);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void NotNullOrEmpty_Should_True_WithString()
        {
            // Arrange
            var str = "s";
            var a = new A();
            a.Str = "s";

            // Act
            var result1 = new NotNullOrEmptyChecker<string>().IsValid(str);
            var result2 = new NotNullOrEmptyChecker<string>().IsValid(a.Str);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
        }

        [Fact]
        public void NotNullOrEmpty_Should_False_WithString()
        {
            // Arrange
            var str = string.Empty;
            var a = new A();

            // Act
            var result1 = new NotNullOrEmptyChecker<string>().IsValid(str);
            var result2 = new NotNullOrEmptyChecker<string>().IsValid(a.Str);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
        }
    }

    public class A
    {
        public B B { get; set; } = default!;

        public string Str { get; set; } = default!;
    }

    public class B
    {
    }
}