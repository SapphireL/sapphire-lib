﻿using SapphireLib.Validator.Rules;

namespace SapphireLib.Validator.Test
{
    public class NotNullTest
    {
        [Fact]
        public void NotNull_Should_True()
        {
            // Arrange
            var a = new object();

            // Act
            var result = new NotNullChecker<object>().IsValid(a);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void NotNull_Should_False()
        {
            // Act
            var result = new NotNullChecker<TestObject>().IsValid(null);

            // Assert
            Assert.False(result);
        }
    }
}
