﻿using SapphireLib.Validator.Checkers;

namespace SapphireLib.Validator.Test
{
    public class NumberTest
    {
        [Fact]
        public void Password_Should_True()
        {
            // Arrange
            string a = "321312312";
            string b = "321312312";

            // Act
            var result1 = new NumberChecker().IsValid(a);
            var result2 = new NumberChecker().IsValid(b);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
        }

        [Fact]
        public void Password_Should_False()
        {
            // Arrange
            string a = "___)(*&*&";
            string b = "sasasasasas  ";

            // Act
            var result1 = new NumberChecker().IsValid(a);
            var result2 = new NumberChecker().IsValid(b);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
        }
    }
}
