﻿using SapphireLib.Validator.Checkers;

namespace SapphireLib.Validator.Test
{
    public class PasswordTest
    {
        [Fact]
        public void Password_Should_True()
        {
            // Arrange
            string a = "Ajhskajska6566";
            string b = "Ass4656565656";
            string patterna = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{1,}$";

            // Act
            var result1 = new PasswordChecker(patterna).IsValid(a);
            var result2 = new PasswordChecker(null).IsValid(b);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
        }

        [Fact]
        public void Password_Should_False()
        {
            // Arrange
            string a = "___)(*&*&";
            string b = "sasasasasas  ";
            string patterna = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{1,}$";
            string patternb = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{1,}$";

            // Act
            var result1 = new PasswordChecker(patterna).IsValid(a);
            var result2 = new PasswordChecker(patternb).IsValid(b);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
        }
    }
}
