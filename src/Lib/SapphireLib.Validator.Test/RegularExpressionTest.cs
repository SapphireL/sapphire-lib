﻿using SapphireLib.Validator.Checkers;

namespace SapphireLib.Validator.Test
{
    public class RegularExpressionTest
    {
        [Fact]
        public void RegularExpression_Should_True()
        {
            // Arrange
            string a = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{1,}$";
            string b = "^[0-9]*$";

            // Act
            var result1 = new RegularExpressionChecker().IsValid(a);
            var result2 = new RegularExpressionChecker().IsValid(b);

            // Assert
            Assert.True(result1);
            Assert.True(result2);
        }

        [Fact]
        public void RegularExpression_Should_False()
        {
            // Arrange
            string a = "___)(*&*&";
            string b = "^[0-";

            // Act
            var result1 = new RegularExpressionChecker().IsValid(a);
            var result2 = new RegularExpressionChecker().IsValid(b);

            // Assert
            Assert.False(result1);
            Assert.False(result2);
        }
    }
}
