﻿namespace SapphireLib.Validator.Test
{
    internal class TestObject
    {
        internal TestChildObject TestChildObject { get; set; } = default!;
    }

    internal class TestChildObject
    {
    }
}
