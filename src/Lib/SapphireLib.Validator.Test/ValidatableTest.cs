﻿using SapphireLib.Validator.Extensions;
using SapphireLib.Validator.Generic;

namespace SapphireLib.Validator.Test
{
    public class ValidatableTest
    {
        [Fact]
        public void Validatable_Should_False()
        {
            var testA = new TestA("a", 0);
            Assert.False(new TestAValidator().Validate(testA));
        }

        [Fact]
        public void Validatable_Should_True()
        {
            var testA = new TestA("sss", 2);
            Assert.True(new TestAValidator().Validate(testA));
        }
    }

    public class TestA(string testParam1, int testInt1)
    {
        public string TestParam1 { get; private set; } = testParam1;

        public int TestInt1 { get; private set; } = testInt1;
    }

    public class TestAValidator : BaseValidator<TestA>
    {
        public override void BuildCheckers()
        {
            CheckFor(x => x.TestParam1).NotNullOrEmpty().Valid(param => param == "sss", " equal to sss.", "Equal sss");
            CheckFor(x => x.TestInt1).NotNullOrEmpty().GreaterThan(1);
        }
    }
}
