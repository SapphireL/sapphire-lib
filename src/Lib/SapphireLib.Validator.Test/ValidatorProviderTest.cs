﻿using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Validator.Extensions;
using SapphireLib.Validator.Generic;

namespace SapphireLib.Validator.Test
{
    public class ValidatorProviderTest
    {
        [Fact]
        public void ShouldGetValidator()
        {
            var services = new ServiceCollection();
            services.AddSingleton<IValidatorProvider, ValidatorProvider>();
            services.AddScoped<IValidator<TestVal>, TestValidator>();
            var sp = services.BuildServiceProvider();
            var provider = sp.GetRequiredService<IValidatorProvider>();

            var test = new TestVal();
            var validator = provider.GetValidator(test);

            Assert.NotNull(provider);
            Assert.NotNull(validator);
            Assert.True(validator.Validate(test));
        }
    }

    internal class TestVal
    {
        public int Test { get; set; } = 1;
    }

    internal class TestValidator : BaseValidator<TestVal>
    {
        public override void BuildCheckers()
        {
            CheckFor(x => x.Test).GreaterThan(0);
        }
    }
}
