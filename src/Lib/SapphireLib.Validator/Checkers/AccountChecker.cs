﻿using System.Text.RegularExpressions;

using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers;

internal class AccountChecker(string? pattern) : IChecker<string>
{
    public string GetErrorMessage()
    {
        return $"The account username does not comply with naming conventions.";
    }

    public bool IsValid(string value)
    {
        if (value == null)
        {
            return false;
        }
        else if (pattern != null)
        {
            try
            {
                Regex regex = new(pattern);
                return regex.IsMatch(value);
            }
            catch (ArgumentException)
            {
                return false;
            }
        }
        else
        {
            string pattern = "^[a-zA-Z0-9]+$";
            Regex regex = new(pattern);
            return regex.IsMatch(value);
        }
    }
}
