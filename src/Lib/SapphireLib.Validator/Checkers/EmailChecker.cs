﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Rules
{
    internal class EmailChecker : IChecker<string>
    {
        public string GetErrorMessage()
        {
            return $"be a email address.";
        }

        public bool IsValid(string obj)
        {
            if (obj == null)
            {
                return false;
            }

            var email = obj.ToString();

            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            int index = email.IndexOf('@');

            return
            index > 0 &&
            index != email.Length - 1 &&
            index == email.LastIndexOf('@');
        }
    }
}
