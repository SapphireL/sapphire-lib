﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class EqualToChecker<T>(T? compareTo) : IChecker<T>
    {
        private readonly T? _compareTo = compareTo;

        public string GetErrorMessage()
        {
            return $"be equal to {_compareTo}";
        }

        public bool IsValid(T? value)
        {
            if (value == null && _compareTo == null)
            {
                return true;
            }
            else
            {
                return EqualityComparer<T>.Default.Equals(value, _compareTo);
            }
        }
    }
}
