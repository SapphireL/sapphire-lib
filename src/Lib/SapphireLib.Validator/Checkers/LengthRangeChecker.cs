﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class LengthRangeChecker(int min, int max) : IChecker<string>
    {
        public string GetErrorMessage()
        {
            return $"be greater than {min} and less than {max} in length";
        }

        public bool IsValid(string value)
        {
            return value.Length <= max && value.Length >= min;
        }
    }
}
