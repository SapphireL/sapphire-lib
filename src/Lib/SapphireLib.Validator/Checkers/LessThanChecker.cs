﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Rules
{
    internal class LessThanChecker<T>(T compareTo) : IChecker<T>
         where T : IComparable<T>
    {
        private readonly T _compareTo = compareTo;

        public string GetErrorMessage()
        {
            return $"be less than {_compareTo}";
        }

        public bool IsValid(T value)
        {
            return value.CompareTo(_compareTo) < 0;
        }
    }
}
