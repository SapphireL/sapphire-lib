﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class LinkAddressHttpsChecker() : IChecker<string>
    {
        public string GetErrorMessage()
        {
            return $"The link address does not meet the specified requirements.";
        }

        public bool IsValid(string value)
        {
            if (value == null)
            {
                return false;
            }
            else
            {
                if (value.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
