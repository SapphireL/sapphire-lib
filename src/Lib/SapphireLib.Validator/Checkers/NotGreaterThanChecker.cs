﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class NotGreaterThanChecker<T>(T compareTo) : IChecker<T>
         where T : IComparable<T>
    {
        private readonly T _compareTo = compareTo;

        public string GetErrorMessage()
        {
            return $"be less than or equal to {_compareTo}";
        }

        public bool IsValid(T value)
        {
            return value.CompareTo(_compareTo) <= 0;
        }
    }
}
