﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class NotLessThanChecker<T>(T compareTo) : IChecker<T>
        where T : IComparable<T>
    {
        private readonly T _compareTo = compareTo;

        public string GetErrorMessage()
        {
            return $"be greater than {_compareTo}";
        }

        public bool IsValid(T value)
        {
            return value.CompareTo(_compareTo) >= 0;
        }
    }
}