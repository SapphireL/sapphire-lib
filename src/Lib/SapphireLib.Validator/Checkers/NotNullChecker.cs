﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Rules;

internal class NotNullChecker<T> : IChecker<T>
{
    public string GetErrorMessage()
    {
        return "not be null";
    }

    public bool IsValid(T? value)
    {
        return value != null;
    }
}
