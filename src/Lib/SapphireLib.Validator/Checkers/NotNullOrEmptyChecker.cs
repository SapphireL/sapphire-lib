﻿using System.Collections;

using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Rules
{
    internal class NotNullOrEmptyChecker<T> : IChecker<T>
    {
        public string GetErrorMessage()
        {
            return "not be null or empty";
        }

        public bool IsValid(T? value)
        {
            if (value == null)
            {
                return false;
            }
            else if (value is string)
            {
                return !string.IsNullOrWhiteSpace(value.ToString());
            }
            else if (value is IEnumerable)
            {
                return (value as IEnumerable)?.GetEnumerator().MoveNext() ?? false;
            }

            return true;
        }
    }
}
