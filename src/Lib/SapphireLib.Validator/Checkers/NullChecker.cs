﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class NullChecker<TProperty> : IChecker<TProperty>
    {
        public string GetErrorMessage()
        {
            return $"be null";
        }

        public bool IsValid(TProperty? value)
        {
            return value == null;
        }
    }
}
