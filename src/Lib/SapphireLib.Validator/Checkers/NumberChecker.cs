﻿using System.Text.RegularExpressions;

using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class NumberChecker() : IChecker<string>
    {
        public string GetErrorMessage()
        {
            return $"Only input numbers from 0 to 9.";
        }

        public bool IsValid(string value)
        {
            if (value == null)
            {
                return false;
            }
            else
            {
                string pattern = "^[0-9]+$";
                Regex regex = new(pattern);
                return regex.IsMatch(value);
            }
        }
    }
}
