﻿using System.Text.RegularExpressions;

using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class PasswordChecker(string? pattern) : IChecker<string>
    {
        public string GetErrorMessage()
        {
            return $"The password does not comply with the password input requirements.";
        }

        public bool IsValid(string value)
        {
            if (value == null)
            {
                return false;
            }
            else if (pattern != null)
            {
                Regex regex = new(pattern);
                return regex.IsMatch(value);
            }
            else
            {
                string pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{1,}$";
                Regex regex = new(pattern);
                return regex.IsMatch(value);
            }
        }
    }
}
