﻿using System.Text.RegularExpressions;

using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Checkers
{
    internal class RegularExpressionChecker : IChecker<string>
    {
        public string GetErrorMessage()
        {
            return $"Invalid regular expression.";
        }

        public bool IsValid(string value)
        {
            if (value == null)
            {
                return false;
            }
            else
            {
                try
                {
                    Regex regex = new(value);
                    return true;
                }
                catch (ArgumentException)
                {
                    return false;
                }
            }
        }
    }
}
