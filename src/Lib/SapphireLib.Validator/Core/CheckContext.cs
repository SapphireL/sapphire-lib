﻿using System.Linq.Expressions;

namespace SapphireLib.Validator.Core
{
    public class CheckContext(string checkee, string checkeeProperty)
    {
        public LambdaExpression? Expression { get; set; }

        public string Checkee { get; internal set; } = checkee;

        public string CheckeeProperty { get; internal set; } = checkeeProperty;

        public bool HasFalse { get; internal set; } = false;

        public bool ThrowExceptionWhenFalse { get; private set; } = false;

        public IList<CheckResult> CheckResults { get; private set; } = [];
    }

    public class CheckContext<T, TProperty>(Func<T, TProperty> func, string checkee, string checkeeProperty)
    {
        public T Obj { get; set; } = default!;

        public Func<T, TProperty> PropertyFunc { get; set; } = func;

        public CheckResult CheckResult { get; private set; } = new CheckResult(checkee, checkeeProperty)!;
    }
}
