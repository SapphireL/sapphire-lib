﻿namespace SapphireLib.Validator.Core
{
    public class CheckResult(string checkee, string checkeeProperty)
    {
        public string Checkee { get; internal set; } = checkee;

        public string CheckeeProperty { get; internal set; } = checkeeProperty;

        public bool HasFalse { get; internal set; } = false;

        public bool ThrowExceptionWhenFalse { get; private set; } = false;

        public IList<CheckResultDetail> CheckResultDetails { get; private set; } = [];
    }
}
