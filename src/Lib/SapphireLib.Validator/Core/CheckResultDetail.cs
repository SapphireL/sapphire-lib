﻿namespace SapphireLib.Validator.Core
{
    public class CheckResultDetail(string checkerName, string failureMessage, bool isFalse = false)
    {
        public string CheckerName { get; set; } = checkerName;

        public bool IsFalse { get; set; } = isFalse;

        public string FailureMessage { get; set; } = failureMessage;

        public int? FailureCode { get; set; }
    }
}
