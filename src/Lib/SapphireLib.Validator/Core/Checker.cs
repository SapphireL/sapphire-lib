﻿namespace SapphireLib.Validator.Core
{
    public class Checker<T, TProperty>
    {
        internal Func<TProperty, bool> CheckerFunc { get; set; }

        private readonly string _checkerName;
        private readonly string _message;

        public Checker(Func<TProperty, bool> checkerFunc, string message, string checkerName)
        {
            CheckerFunc = checkerFunc;
            _message = message;
            _checkerName = checkerName;
        }

        internal Checker(IChecker<TProperty> checker)
            : this(checker.IsValid, checker.GetErrorMessage(), checker.GetType().Name)
        {
        }

        public bool IsValid(CheckContext<T, TProperty> context)
        {
            var result = true;

            if (CheckerFunc != null)
            {
                var propertyValue = context.PropertyFunc(context.Obj);
                result = CheckerFunc(propertyValue);
                if (!result)
                {
                    var actualValue = string.IsNullOrWhiteSpace(propertyValue?.ToString()) ? "empty" : propertyValue?.ToString();

                    context.CheckResult.CheckResultDetails.Add(new CheckResultDetail(_checkerName, $"The `{context.CheckResult.CheckeeProperty}` value should {_message}. The actual value is {actualValue}.", true));
                    context.CheckResult.HasFalse = true;
                }

                return result;
            }

            return true;
        }
    }
}
