﻿namespace SapphireLib.Validator.Core;

public class CheckerBuilder<T, TProperty>(CheckContext<T, TProperty> context, bool fullScan) : List<Checker<T, TProperty>>, ICheckerBuilder<T>
{
    public CheckContext<T, TProperty> Context => context;

    // private readonly IList<CheckResult> _results = [];
    private readonly bool? _fullScan = fullScan;

    // public bool Validate()
    // {
    //    if (_fullScan == true)
    //    {
    //        return Count == this.Count(checker => checker.IsValid(Context));
    //    }
    //    return this.All(checker => checker.IsValid(Context));
    // }
    public bool Validate(T value)
    {
        Context.Obj = value;
        if (_fullScan == true)
        {
            return Count == this.Count(checker => checker.IsValid(Context));
        }

        return this.All(checker => checker.IsValid(Context));
    }

    public CheckResult ValidateWithResults(T value)
    {
        Validate(value);
        return Context.CheckResult;
    }

    internal CheckerBuilder<T, TProperty> AddValidator(Checker<T, TProperty> validator)
    {
        Add(validator);
        return this;
    }
}
