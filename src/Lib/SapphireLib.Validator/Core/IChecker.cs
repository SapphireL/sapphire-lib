﻿namespace SapphireLib.Validator.Core
{
    internal interface IChecker<TProperty>
    {
        bool IsValid(TProperty value);

        string GetErrorMessage();
    }
}
