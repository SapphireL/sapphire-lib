﻿namespace SapphireLib.Validator.Core
{
    public interface ICheckerBuilder<T>
    {
        // public bool Validate();
        public bool Validate(T value);

        public CheckResult ValidateWithResults(T value);
    }
}
