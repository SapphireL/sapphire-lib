﻿using Microsoft.Extensions.DependencyInjection.Extensions;

using SapphireLib.Module.Core;
using SapphireLib.Validator.Generic;

namespace SapphireLib.Validator.Definitions;

/// <summary>
/// 验证模块，用于配置和管理应用程序的验证服务。
/// </summary>
/// <remarks>
/// 此模块通过继承自 <see cref="BaseModule"/>，为应用程序添加验证服务。它会向依赖注入容器中注册 <see cref="IValidatorProvider"/> 的单例实例。
/// <para>
/// 该模块的配置包含以下内容：
/// <list type="bullet">
///   <item><description>注册 <see cref="IValidatorProvider"/> 的单例实现 <see cref="ValidatorProvider"/>。</description></item>
/// </list>
/// </para>
/// </remarks>
internal class ValidatorModule : BaseModule
{
    /// <summary>
    /// 配置验证服务的方法，将 <see cref="IValidatorProvider"/> 的单例实例添加到服务容器中。
    /// </summary>
    /// <param name="context">提供应用程序上下文的 <see cref="BuilderContext"/>。</param>
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.TryAddSingleton<IValidatorProvider, ValidatorProvider>();
    }
}
