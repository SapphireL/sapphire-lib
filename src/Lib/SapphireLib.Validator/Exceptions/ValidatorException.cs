﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Exceptions
{
    public class ValidatorException<T>(string message, ICheckerBuilder<T> validating)
                : Exception(message)
    {
        public ICheckerBuilder<T> Validating { get; private set; } = validating;
    }
}
