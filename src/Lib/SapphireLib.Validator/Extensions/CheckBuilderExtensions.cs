﻿using SapphireLib.Validator.Checkers;
using SapphireLib.Validator.Core;
using SapphireLib.Validator.Rules;

namespace SapphireLib.Validator.Extensions;

public static class CheckBuilderExtensions
{
    public static CheckerBuilder<T, TProperty> NotNullOrEmpty<T, TProperty>(this CheckerBuilder<T, TProperty> builder)
    {
        builder.Add(new Checker<T, TProperty>(new NotNullOrEmptyChecker<TProperty>()));
        return builder;
    }

    public static CheckerBuilder<T, string> LengthRange<T>(this CheckerBuilder<T, string> builder, int min, int max)
    {
        LengthRangeChecker lengthChecker = new LengthRangeChecker(min, max);
        var checker = new Checker<T, string>(lengthChecker);
        builder.Add(checker);
        return builder;
    }

    public static CheckerBuilder<T, TProperty> NotNull<T, TProperty>(this CheckerBuilder<T, TProperty> builder)
    {
        builder.Add(new Checker<T, TProperty>(new NotNullChecker<TProperty>()));
        return builder;
    }

    public static CheckerBuilder<T, TProperty> GreaterThan<T, TProperty>(this CheckerBuilder<T, TProperty> builder, TProperty compareTo)
        where TProperty : IComparable<TProperty>
    {
        builder.Add(new Checker<T, TProperty>(new GreaterThanChecker<TProperty>(compareTo)));
        return builder;
    }

    public static CheckerBuilder<T, TProperty> NotLessThan<T, TProperty>(this CheckerBuilder<T, TProperty> builder, TProperty compareTo)
       where TProperty : IComparable<TProperty>
    {
        builder.Add(new Checker<T, TProperty>(new NotLessThanChecker<TProperty>(compareTo)));
        return builder;
    }

    public static CheckerBuilder<T, TProperty> LessThan<T, TProperty>(this CheckerBuilder<T, TProperty> builder, TProperty compareTo)
       where TProperty : IComparable<TProperty>
    {
        builder.Add(new Checker<T, TProperty>(new LessThanChecker<TProperty>(compareTo)));
        return builder;
    }

    /// <summary>
    /// 不大于.
    /// </summary>
    /// <typeparam name="T">类型.</typeparam>
    /// <typeparam name="TProperty">属性.</typeparam>
    /// <param name="builder"><see cref="CheckerBuilder"/>.</param>
    /// <param name="compareTo">对比值.</param>
    /// <returns>查看<see cref="CheckerBuilder"/>.</returns>
    public static CheckerBuilder<T, TProperty> NotGreaterThan<T, TProperty>(this CheckerBuilder<T, TProperty> builder, TProperty compareTo)
       where TProperty : IComparable<TProperty>
    {
        builder.Add(new Checker<T, TProperty>(new NotGreaterThanChecker<TProperty>(compareTo)));
        return builder;
    }

    public static CheckerBuilder<T, TProperty> EqualTo<T, TProperty>(this CheckerBuilder<T, TProperty> builder, TProperty compareTo)
    {
        builder.Add(new Checker<T, TProperty>(new EqualToChecker<TProperty>(compareTo)));
        return builder;
    }

    /// <summary>
    /// 必须为空.
    /// </summary>
    /// <typeparam name="T">类型.</typeparam>
    /// <typeparam name="TProperty">属性.</typeparam>
    /// <param name="builder"><see cref="CheckerBuilder"/>.</param>
    /// <returns>查看<see cref="CheckerBuilder"/>.</returns>
    public static CheckerBuilder<T, TProperty> Null<T, TProperty>(this CheckerBuilder<T, TProperty> builder)
    {
        builder.Add(new Checker<T, TProperty>(new NullChecker<TProperty>()));
        return builder;
    }

    public static CheckerBuilder<T, string> ValidEmailFormat<T>(this CheckerBuilder<T, string> builder)
    {
        builder.Add(new Checker<T, string>(new EmailChecker()));
        return builder;
    }

    public static CheckerBuilder<T, string> ValidRegularExpressionFormat<T>(this CheckerBuilder<T, string> builder)
    {
        builder.Add(new Checker<T, string>(new RegularExpressionChecker()));
        return builder;
    }

    public static CheckerBuilder<T, string> ValidAccountFormat<T>(this CheckerBuilder<T, string> builder, string? pattern = null)
    {
        builder.Add(new Checker<T, string>(new AccountChecker(pattern)));
        return builder;
    }

    public static CheckerBuilder<T, string> ValidPasswordFormat<T>(this CheckerBuilder<T, string> builder, string? pattern = null)
    {
        builder.Add(new Checker<T, string>(new PasswordChecker(pattern)));
        return builder;
    }

    public static CheckerBuilder<T, string> ValidLinkAddressHttpsFormat<T>(this CheckerBuilder<T, string> builder)
    {
        builder.Add(new Checker<T, string>(new LinkAddressHttpsChecker()));
        return builder;
    }

    public static CheckerBuilder<T, string> ValidNumberFormat<T>(this CheckerBuilder<T, string> builder)
    {
        builder.Add(new Checker<T, string>(new NumberChecker()));
        return builder;
    }

    public static CheckerBuilder<T, TProperty> Valid<T, TProperty>(this CheckerBuilder<T, TProperty> builder, Func<TProperty?, bool> validateFunc, string message, string checkerName)
    {
        builder.AddValidator(new Checker<T, TProperty>(validateFunc, message, checkerName));
        return builder;
    }
}
