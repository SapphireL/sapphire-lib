﻿using Microsoft.AspNetCore.Builder;

using SapphireLib.Module.Extensions;
using SapphireLib.Validator.Definitions;

namespace SapphireLib.Validator.Extensions;

public static class WebApplicationBuilderExtension
{
    /// <summary>
    /// 向 <see cref="WebApplicationBuilder"/> 添加 SapphireLib 验证模块。
    /// </summary>
    /// <param name="builder">当前的 <see cref="WebApplicationBuilder"/> 实例。</param>
    /// <remarks>
    /// 该方法通过调用 <see cref="Module.Extensions.WebApplicationBuilderExtensions.AddSappLib{TModule}"/> 来加载并配置 <see cref="ValidatorModule"/>，
    /// 从而为应用程序提供验证器提供程序支持。
    /// 使用该模块后，开发者可以通过依赖注入的方式获取相应的验证器实例并执行验证逻辑。
    /// </remarks>
    public static void AddSappLibValidator(this WebApplicationBuilder builder)
    {
        builder.AddSappLib<ValidatorModule>();
    }
}
