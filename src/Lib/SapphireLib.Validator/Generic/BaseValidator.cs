﻿using System.Linq.Expressions;

using SapphireLib.Tools;
using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Generic
{
    public abstract class BaseValidator<T> : IValidator<T>
    {
        private readonly List<ICheckerBuilder<T>> _builders = [];

        public abstract void BuildCheckers();

        public List<CheckResult> ValidateDetails(T obj)
        {
            BuildCheckers();
            var results = new List<CheckResult>();
            foreach (var builder in _builders)
            {
                var result = builder.ValidateWithResults(obj);
                results.Add(result);
            }

            return results;
        }

        public bool Validate(T obj)
        {
            var results = ValidateDetails(obj);
            return !results.Any(r => r.HasFalse);
        }

        public bool Validate(object obj) => Validate((T)obj);

        public List<CheckResult> ValidateDetails(object obj) => ValidateDetails((T)obj);

        protected CheckerBuilder<T, TProperty> CheckFor<TProperty>(Expression<Func<T, TProperty>> expression, bool fullScan = false)
        {
            var context = new CheckContext<T, TProperty>(expression.Compile(), typeof(T).Name, expression.GetMemberAccess().Name);

            // _checkContexts.Add(context);
            var builder = new CheckerBuilder<T, TProperty>(context, fullScan);
            _builders.Add(builder);
            return builder;
        }
    }
}
