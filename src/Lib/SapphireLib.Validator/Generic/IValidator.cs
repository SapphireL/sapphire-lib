﻿using SapphireLib.Validator.Core;

namespace SapphireLib.Validator.Generic
{
    public interface IValidator
    {
        public List<CheckResult> ValidateDetails(object obj);

        public bool Validate(object obj);
    }

    public interface IValidator<in T> : IValidator
    {
        public List<CheckResult> ValidateDetails(T obj);

        public bool Validate(T obj);
    }
}
