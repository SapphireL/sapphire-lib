﻿namespace SapphireLib.Validator.Generic
{
    /// <summary>
    /// 验证器提供接口.
    /// 负责创建或者获取验证器.
    /// </summary>
    public interface IValidatorProvider
    {
        /// <summary>
        /// 获取验证器.
        /// </summary>
        /// <param name="obj">被验证的对象.</param>
        /// <returns><see cref="IValidator"/>.</returns>
        IValidator? GetValidator(object obj);
    }
}
