﻿using Microsoft.Extensions.DependencyInjection;

namespace SapphireLib.Validator.Generic;

/// <summary>
/// 默认的验证器提供程序，负责从服务容器中获取适用于特定对象类型的验证器实例。
/// </summary>
/// <param name="scopeFactory">用于创建服务作用域的 <see cref="IServiceScopeFactory"/> 实例。</param>
internal class ValidatorProvider(IServiceScopeFactory scopeFactory) : IValidatorProvider
{
    /// <summary>
    /// 根据指定对象的类型获取对应的验证器实例。
    /// </summary>
    /// <param name="obj">要验证的对象，必须实现 <see cref="IValidator"/> 接口并注册到 <see cref="IServiceCollection"/> 中。</param>
    /// <returns>返回与对象类型匹配的 <see cref="IValidator"/> 实例，或在未找到匹配验证器时返回 null。</returns>
    public IValidator? GetValidator(object obj)
    {
        using var scope = scopeFactory.CreateScope();
        return scope.ServiceProvider.GetService(typeof(IValidator<>).MakeGenericType(obj.GetType())) as IValidator;
    }
}
