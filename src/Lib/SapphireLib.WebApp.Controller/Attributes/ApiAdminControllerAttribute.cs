﻿using System.Diagnostics.CodeAnalysis;

using Microsoft.AspNetCore.Mvc.Routing;

namespace SapphireLib.WebApp.Controller.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
public class ApiAdminControllerAttribute([StringSyntax("Route")] string template) : Attribute, IRouteTemplateProvider
{
    [StringSyntax("Route")]
    public string Template { get; } = "api/admin/" + template ?? throw new ArgumentNullException(nameof(template));

    public int? Order => 0;

    public string? Name { get; set; }
}
