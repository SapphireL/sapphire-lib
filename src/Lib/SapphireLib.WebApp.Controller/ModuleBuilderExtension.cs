﻿using SapphireLib.Module.Core;
using SapphireLib.WebApp.Definitions;

namespace SapphireLib.WebApp.Controller;

public static class ModuleBuilderExtension
{
    public static void AddController(this ModuleBuilder<WebAppModule> builder)
    {
        builder.WithFeature<WebAppControllerFeature>();
    }
}
