﻿using System.Text.Json;
using System.Text.Json.Serialization;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

using SapphireLib.Module.Core;

namespace SapphireLib.WebApp.Controller
{
    public class WebAppControllerFeature : BaseFeature
    {
        protected override void OnRunning(RunningContext context)
        {
            context.WebApplication.MapControllers();
        }

        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddControllers()
                 .AddJsonOptions(options =>
                 {
                     options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                     options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
                     options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                 });
        }
    }
}
