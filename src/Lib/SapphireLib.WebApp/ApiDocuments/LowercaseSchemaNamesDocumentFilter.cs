﻿using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace SapphireLib.WebApp.ApiDocuments;

internal class LowercaseSchemaNamesDocumentFilter : IDocumentFilter
{
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        var schemas = swaggerDoc.Components.Schemas.ToList();

        // 遍历所有 schemas，将首字母改为小写
        foreach (var schema in schemas)
        {
            var camelCaseKey = char.ToLowerInvariant(schema.Key[0]) + schema.Key.Substring(1);
            if (camelCaseKey != schema.Key)
            {
                swaggerDoc.Components.Schemas.Remove(schema.Key);
                swaggerDoc.Components.Schemas.Add(camelCaseKey, schema.Value);

                // 2. 更新所有引用到该 schema 的地方
                UpdateReferences(swaggerDoc, schema.Key, camelCaseKey);
            }
        }
    }

    // 更新所有引用 schema 的地方
    private void UpdateReferences(OpenApiDocument swaggerDoc, string originalKey, string camelCaseKey)
    {
        var originalRef = $"#/components/schemas/{originalKey}";
        var camelCaseRef = $"#/components/schemas/{camelCaseKey}";

        // 遍历 paths，更新 requestBody 和 responses 中的 $ref
        foreach (var pathItem in swaggerDoc.Paths.Values)
        {
            foreach (var operation in pathItem.Operations.Values)
            {
                // 更新 requestBody 中的引用
                if (operation.RequestBody?.Content != null)
                {
                    foreach (var content in operation.RequestBody.Content.Values)
                    {
                        if (content.Schema?.Reference?.ReferenceV3 == originalRef)
                        {
                            content.Schema = new OpenApiSchema
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.Schema,
                                    Id = camelCaseKey
                                }
                            };
                        }
                    }
                }

                // 更新 responses 中的引用
                foreach (var response in operation.Responses.Values)
                {
                    if (response.Content != null)
                    {
                        foreach (var content in response.Content.Values)
                        {
                            if (content.Schema?.Reference?.ReferenceV3 == originalRef)
                            {
                                content.Schema = new OpenApiSchema
                                {
                                    Reference = new OpenApiReference
                                    {
                                        Type = ReferenceType.Schema,
                                        Id = camelCaseKey
                                    }
                                };
                            }
                        }
                    }
                }
            }
        }
    }
}