﻿using SapphireLib.Module.Core;

namespace SapphireLib.WebApp.Definitions;

public class WebAppModule : BaseModule
{
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.AddHttpContextAccessor();
        context.Services.AddCors(options =>
        {
            options.AddPolicy("AllowAll", p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
        });
    }

    protected override void OnRunning(RunningContext context)
    {
        context.WebApplication.UseCors("AllowAll");
    }
}
