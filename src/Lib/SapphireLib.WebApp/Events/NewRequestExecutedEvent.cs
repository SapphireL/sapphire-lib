﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.WebApp.Events;

public class NewRequestExecutedEvent(Guid? id = null, DateTime? occurredOn = null) : IEventContent
{
    public Guid Id { get; set; } = id ?? Guid.NewGuid();

    public DateTime OccurredOn { get; set; } = occurredOn ?? DateTime.Now;

    public string? Path { get; set; }

    /// <summary>
    /// 响应头.
    /// </summary>
    public string? Header { get; set; }

    /// <summary>
    /// 响应内容.
    /// </summary>
    public string? Body { get; set; }

    public int StatusCode { get; set; }
}
