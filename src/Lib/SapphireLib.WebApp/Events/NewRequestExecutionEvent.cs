﻿using SapphireLib.EventBus.Abstractions.EventBus;

namespace SapphireLib.WebApp.Events;

public class NewRequestExecutionEvent(Guid? id = null, DateTime? occurredOn = null) : IEventContent
{
    public Guid Id { get; private set; } = id ?? Guid.NewGuid();

    public DateTime OccurredOn { get; private set; } = occurredOn ?? DateTime.Now;

    /// <summary>
    /// 请求接口名称.
    /// </summary>
    public string? Path { get; set; }

    /// <summary>
    /// 请求方法名称.
    /// </summary>
    public string? Method { get; set; }

    public string? DeviceBrand { get; set; }

    public string? Device { get; set; }

    public string? Browser { get; set; }

    public string? BrowserVersion { get; set; }

    public string? Header { get; set; }

    public string? Body { get; set; }

    /// <summary>
    /// 请求IP地址.
    /// </summary>
    public string? ClientIp { get; set; }

    /// <summary>
    /// 请求地址.
    /// </summary>
    public string? Address { get; set; }

    /// <summary>
    /// 请求操作系统.
    /// </summary>
    public string? OS { get; set; }

    /// <summary>
    /// 请求用户代理.
    /// </summary>
    public string? UserAgent { get; set; }

    /// <summary>
    /// 请求用户ID.
    /// </summary>
    public string? UserId { get; set; }

    public string? Username { get; set; }
}
