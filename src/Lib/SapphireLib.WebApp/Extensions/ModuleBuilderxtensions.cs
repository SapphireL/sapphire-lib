﻿using SapphireLib.Module.Core;
using SapphireLib.WebApp.Definitions;
using SapphireLib.WebApp.Features;

namespace SapphireLib.WebApp.Extensions;

public static class ModuleBuilderxtensions
{
    public static ModuleBuilder<WebAppModule> AddApiDocumentFeature(this ModuleBuilder<WebAppModule> builder, string? section = "WebApp:ApiDocument", Action<ApiDocumentFeatureOptions>? configure = null)
    {
        builder.WithFeature<ApiDocumentFeature, ApiDocumentFeatureOptions>(section, configure);
        return builder;
    }

    public static ModuleBuilder<WebAppModule> EnableRequestLog(this ModuleBuilder<WebAppModule> builder)
    {
        builder.WithFeature<RequestLogFeature>();
        return builder;
    }

    public static ModuleBuilder<WebAppModule> AddProblemDetailsFeature(this ModuleBuilder<WebAppModule> builder)
    {
        builder.WithFeature<ProblemDetailsFeature>();
        return builder;
    }
}
