﻿using SapphireLib.Module.Core;
using SapphireLib.Module.Extensions;
using SapphireLib.WebApp.Definitions;

namespace SapphireLib.WebApp.Extensions;

public static class WebApplicationBuilderExtensions
{
    public static void AddSappLibWebApp(this WebApplicationBuilder builder, Action<ModuleBuilder<WebAppModule>>? action = null)
    {
        builder.AddSappLib(action);
    }
}
