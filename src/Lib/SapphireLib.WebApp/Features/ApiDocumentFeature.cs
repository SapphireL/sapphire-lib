﻿using Microsoft.OpenApi.Models;

using SapphireLib.Module.Core;

namespace SapphireLib.WebApp.Features;

internal class ApiDocumentFeature : BaseFeature
{
    protected override void OnRunning(RunningContext context)
    {
        var app = context.WebApplication;
        var options = context.BuilderContext.GetOptions<ApiDocumentFeatureOptions>() ?? new ApiDocumentFeatureOptions();

        app.UseSwagger(c =>
        {
            c.RouteTemplate = "docs/{documentName}/swagger.json";
        });

        if (context.WebApplication.Environment.IsDevelopment() || options.PublicUI)
        {
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/docs/{options.DocumentName}/swagger.json", options.Description);
                c.RoutePrefix = "docs";
            });
        }
    }

    protected override void ConfigureServices(BuilderContext context)
    {
        var options = context.GetOptions<ApiDocumentFeatureOptions>() ?? new ApiDocumentFeatureOptions();

        context.Services.AddSwaggerGen(c =>
        {
            c.DescribeAllParametersInCamelCase();

            // c.DocumentFilter<LowercaseSchemaNamesDocumentFilter>();
            c.SwaggerDoc(options.DocumentName, new OpenApiInfo { Title = options.Title, Version = options.Version });
            c.SupportNonNullableReferenceTypes();
            c.AddSecurityDefinition("bearerAuth", new OpenApiSecurityScheme
            {
                Type = SecuritySchemeType.Http,
                Scheme = "bearer",
                BearerFormat = "JWT",
                Description = "JWT Authorization header using the Bearer scheme."
            });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "bearerAuth" }
                    },

                    Array.Empty<string>()
                }
            });
        });
    }
}
