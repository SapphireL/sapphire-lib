﻿namespace SapphireLib.WebApp.Features;

public class ApiDocumentFeatureOptions
{
    public string DocumentName { get; set; } = "default";

    public string Title { get; set; } = "default";

    public string Version { get; set; } = "v1";

    public string Description { get; set; } = "default";

    public bool PublicUI { get; set; } = false;
}
