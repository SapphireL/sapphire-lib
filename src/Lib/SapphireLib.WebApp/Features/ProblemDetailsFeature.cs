﻿using SapphireLib.Module.Core;

namespace SapphireLib.WebApp.Features;

internal class ProblemDetailsFeature : BaseFeature
{
    protected override void ConfigureServices(BuilderContext context)
    {
        context.Services.AddProblemDetails(x =>
        {
            x.Map<InvalidParameterException>(ex => new InvalidParameterProblemDetails(ex));
            x.IncludeExceptionDetails = (_, _) => context.WebApplicationBuilder.Environment.IsDevelopment();
        });
    }

    protected override void OnRunning(RunningContext context)
    {
        context.WebApplication.UseProblemDetails();
    }
}
