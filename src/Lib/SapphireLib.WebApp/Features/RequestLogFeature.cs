﻿using SapphireLib.Module.Core;
using SapphireLib.WebApp.Middlewares;

namespace SapphireLib.WebApp.Features
{
    internal class RequestLogFeature : BaseFeature
    {
        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddTransient<RequestLogMiddleware>();
        }

        protected override void OnRunning(RunningContext context)
        {
            base.OnRunning(context);
            context.WebApplication.UseMiddleware<RequestLogMiddleware>();
        }
    }
}
