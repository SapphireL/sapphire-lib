﻿using Newtonsoft.Json;

using SapphireLib.Authorization;
using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.Tools;
using SapphireLib.WebApp.Events;

using UAParser;

namespace SapphireLib.WebApp.Middlewares;

public class RequestLogMiddleware(IEventBus eventBus, IUserManager userManager) : IMiddleware
{
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        var traceId = Guid.NewGuid();
        var requestDetails = await CaptureRequestDetailsAsync(context);
        var originalBodyStream = context.Response.Body;

        using var responseBodyStream = new MemoryStream();
        context.Response.Body = responseBodyStream;

        try
        {
            // Publish the request start event
            await PublishRequestEventAsync(eventBus, traceId, requestDetails);

            // Proceed with the request
            await next(context);

            // Read and publish the response details
            await PublishResponseEventAsync(eventBus, traceId, context, responseBodyStream);
        }
        catch (Exception)
        {
            // Log and publish the exception details
            await PublishResponseEventAsync(eventBus, traceId, context, responseBodyStream);
            throw;
        }
        finally
        {
            // Restore the original response body stream
            context.Response.Body = originalBodyStream;
            await responseBodyStream.CopyToAsync(originalBodyStream);
        }
    }

    private async Task<RequestDetails> CaptureRequestDetailsAsync(HttpContext context)
    {
        var userAgent = context.Request.Headers["User-Agent"].ToString();
        var uaParser = Parser.GetDefault();
        var clientInfo = uaParser.Parse(userAgent);

        context.Request.EnableBuffering();
        var bodyContent = await new StreamReader(context.Request.Body).ReadToEndAsync();
        context.Request.Body.Position = 0;

        var address = IpAddressHelper.GetIpAddress(context!.Connection!.RemoteIpAddress!.ToString());

        return new RequestDetails
        {
            Path = context.Request.Path.Value,
            Method = context.Request.Method,
            UserAgent = userAgent,
            OS = clientInfo.OS.ToString(),
            Device = clientInfo.Device.Family,
            DeviceBrand = clientInfo.Device.Brand,
            Browser = clientInfo.Browser.Family,
            BrowserVersion = clientInfo.Browser.Version,
            Header = JsonConvert.SerializeObject(context.Request.Headers),
            Body = bodyContent,
            ClientIp = context.Connection.RemoteIpAddress?.ToString(),
            Address = address.IpLocation,
            Username = userManager.IsAvailable ? userManager.Username : null,
            UserId = userManager.IsAvailable ? userManager.UserId : null
        };
    }

    private async Task PublishRequestEventAsync(IEventBus eventBus, Guid traceId, RequestDetails requestDetails)
    {
        await eventBus.PublishAsync(new NewRequestExecutionEvent(traceId, DateTime.Now)
        {
            Path = requestDetails.Path,
            Method = requestDetails.Method,
            UserAgent = requestDetails.UserAgent,
            OS = requestDetails.OS,
            Device = requestDetails.Device,
            DeviceBrand = requestDetails.DeviceBrand,
            Browser = requestDetails.Browser,
            BrowserVersion = requestDetails.BrowserVersion,
            Header = requestDetails.Header,
            Body = requestDetails.Body,
            Address = requestDetails.Address,
            ClientIp = requestDetails.ClientIp,
            UserId = requestDetails.UserId,
            Username = requestDetails.Username
        });
    }

    private async Task PublishResponseEventAsync(IEventBus eventBus, Guid traceId, HttpContext context, MemoryStream responseBodyStream)
    {
        // Read the response body
        context.Response.Body.Seek(0, SeekOrigin.Begin);
        var responseBody = await new StreamReader(context.Response.Body).ReadToEndAsync();
        context.Response.Body.Seek(0, SeekOrigin.Begin);

        await eventBus.PublishAsync(new NewRequestExecutedEvent(traceId, DateTime.Now)
        {
            Header = JsonConvert.SerializeObject(context.Response.Headers),
            Body = responseBody,
            StatusCode = context.Response.StatusCode,
            Path = context.Request.Path.Value
        });
    }

    private class RequestDetails
    {
        public string? Path { get; set; }

        public required string Method { get; set; }

        public required string UserAgent { get; set; }

        public required string OS { get; set; }

        public required string Device { get; set; }

        public required string DeviceBrand { get; set; }

        public required string Browser { get; set; }

        public required string BrowserVersion { get; set; }

        public required string Header { get; set; }

        public required string Body { get; set; }

        public string? ClientIp { get; set; }

        public string? Address { get; set; }

        public required string? Username { get; set; }

        public required string? UserId { get; set; }
    }
}
