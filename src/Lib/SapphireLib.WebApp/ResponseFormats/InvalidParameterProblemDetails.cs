﻿namespace SapphireLib.WebApp.ResponseFormats
{
    internal class InvalidParameterProblemDetails : ProblemDetails
    {
        public InvalidParameterProblemDetails(InvalidParameterException exception)
        {
            Title = "Parameter validation error";
            Status = StatusCodes.Status400BadRequest;
            Errors = exception.Errors;
        }

        public IEnumerable<ParameterExceptionDetail> Errors { get; }
    }
}
