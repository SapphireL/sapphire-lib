﻿using Microsoft.Extensions.DependencyInjection;
using SapphireLib.Module.Core;

namespace SapphireLib.Workflow
{
    public class WorkflowModule(WorkflowModuleConfigOptions configOptions) : BaseModule
    {
        private readonly WorkflowModuleConfigOptions _configOptions = configOptions;

        protected override void ConfigureServices(BuilderContext context)
        {
            context.Services.AddWorkflow(cfg =>
            {
                switch (_configOptions.DbType)
                {
                    case "SqlServer":
                        cfg.UseSqlServer(_configOptions.ConnectionString, _configOptions.CanCreateDb, _configOptions.CanMigratedDb);
                        break;
                    default:
                        break;
                }
            });
        }
    }
}
