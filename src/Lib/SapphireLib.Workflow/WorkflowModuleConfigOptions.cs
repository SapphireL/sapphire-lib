﻿namespace SapphireLib.Workflow
{
    public class WorkflowModuleConfigOptions
    {
        public string DbType { get; set; } = default!;

        public string ConnectionString { get; set; } = default!;

        public bool CanCreateDb { get; set; } = false;

        public bool CanMigratedDb { get; set; } = false;
    }
}
