import { toPermissions } from './permissions/mapRoutesPermissions';

/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
export default function access(initialState: { userAccess: API.GetUserAccessQueryResult } | undefined) {
  const { userAccess } = initialState ?? {};
  return {
    hasAccess: (route: any) => userAccess?.hasAllPermissions || userAccess?.permissions?.includes(toPermissions(route.name)),
    hasAccessByPermission: (permission: string) => (userAccess?.hasAllPermissions || userAccess?.permissions?.includes(permission)) || false,
  };
}
