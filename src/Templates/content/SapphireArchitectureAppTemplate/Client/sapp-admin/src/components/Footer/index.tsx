import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-components';
import React from 'react';

const Footer: React.FC = () => {
  return (
    <DefaultFooter
      style={{
        background: 'none',
      }}
      copyright="Powered by Sapphire Lib"
      links={[
        {
          key: 'Sapphire Lib',
          title: 'Sapphire Lib',
          href: 'https://pro.ant.design',
          blankTarget: true,
        },
        {
          key: 'Sapphire App',
          title: <GithubOutlined />,
          href: 'https://github.com/ant-design/ant-design-pro',
          blankTarget: true,
        },
        {
          key: 'Sapphire App',
          title: 'Sapphire App',
          href: 'https://ant.design',
          blankTarget: true,
        },
      ]}
    />
  );
};

export default Footer;
