export default {
    'tables.cancel': 'Cancel',
    'tables.confirm': 'Confirm',
    'tables.confirm.delete': 'Confirm delete {name} ?',
    'tables.delete': 'Delete',
    'tables.edit': 'Edit',
    'tables.new': 'New',
    'tables.permissions': 'Permissions',
    'tables.query': 'Query',
    'tables.reset': 'Reset',
    'tables.resetPassword': 'Reset Password',
}