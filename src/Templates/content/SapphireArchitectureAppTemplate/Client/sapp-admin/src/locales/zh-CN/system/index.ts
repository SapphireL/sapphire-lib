import roles from "./roles";
import logins from "./monitor/login";
import system from "./monitor/system";

export default {
    'system.message.creating': "正在创建",
    'system.message.yes': "是",
    'system.message.no': "否",
    'system.table.action': '操作',
    ...roles,
    ...logins,
    ...system
};
