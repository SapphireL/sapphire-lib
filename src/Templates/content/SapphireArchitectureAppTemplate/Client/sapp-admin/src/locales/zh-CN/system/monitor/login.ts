export default {
    'system.monitor.login.columns.username': '用户名',
    'system.monitor.login.columns.ipAddress': 'IP地址',
    'system.monitor.login.columns.address': '用户地址',
    'system.monitor.login.columns.os': '操作系统',
    'system.monitor.login.columns.userAgent': '用户代理',
    'system.monitor.login.columns.isSuccess': '是否成功',
    'system.monitor.login.columns.failureReason': '失败原因',
    'system.monitor.login.columns.loginAt': '登录时间',
}