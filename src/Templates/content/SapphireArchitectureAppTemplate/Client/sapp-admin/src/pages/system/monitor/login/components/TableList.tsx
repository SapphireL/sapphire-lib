import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { FormattedMessage } from '@umijs/max';

import { getAdminSystemLogsLoginLogs as loginLogsApi } from '@/services/admin/systemLogs';
import { forwardRef, useRef } from 'react';

const getLoginLogsRequest = async (params: any, sort: any) => {
  const modifiedParams: API.getAdminSystemLogsLoginLogsParams = {
    ...params,
    sortBy: Object.keys(sort).length > 0 ? Object.keys(sort)[0] : undefined,
    isDescending: Object.keys(sort).length > 0 ? Object.values(sort)[0] === 'descend' : undefined,
  };
  const response = await loginLogsApi(modifiedParams);
  return response;
};

// ref 类型定义

const TableList = forwardRef(() => {
  const actionRef = useRef<ActionType>();

  const columns: ProColumns<API.GetUserLoginLogsQueryResult>[] = [
    {
      dataIndex: 'index',
      valueType: 'index',
      width: 48,
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.username"></FormattedMessage>,
      dataIndex: 'username',
      valueType: 'text',
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.ipAddress"></FormattedMessage>,
      dataIndex: 'ipAddress',
      valueType: 'text',
      search: false,
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.isSuccess"></FormattedMessage>,
      dataIndex: 'isSuccess',
      sorter: true,
      valueEnum: {
        false: {
          text: <FormattedMessage id="system.message.no"></FormattedMessage>,
          status: 'Default',
        },
        true: {
          text: <FormattedMessage id="system.message.yes"></FormattedMessage>,
          status: 'Success',
        },
      },
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.address"></FormattedMessage>,
      dataIndex: 'address',
      valueType: 'text',
      search: false,
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.os"></FormattedMessage>,
      dataIndex: 'os',
      valueType: 'text',
      search: false,
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.userAgent"></FormattedMessage>,
      dataIndex: 'userAgent',
      valueType: 'text',
      search: false,
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.failureReason"></FormattedMessage>,
      dataIndex: 'failureReason',
      valueType: 'text',
      search: false,
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.loginAt"></FormattedMessage>,
      dataIndex: 'loginAt',
      valueType: 'dateTime',
      search: false,
      sorter: true,
    },
    {
      title: <FormattedMessage id="system.monitor.login.columns.loginAt"></FormattedMessage>,
      valueType: 'dateTimeRange',
      hideInTable: true,
      search: {
        transform: (value) => {
          return {
            startTime: value ? value[0] : null,
            endTime: value ? value[1] : null,
          };
        },
      },
    },
  ];

  return (
    <ProTable<API.GetUserLoginLogsQueryResult, API.GetUserLoginLogsQueryResultPagedQueryResult>
      actionRef={actionRef}
      rowKey="id"
      pagination={{
        showSizeChanger: true,
      }}
      request={getLoginLogsRequest}
      columns={columns}
    />
  );
});

export default TableList;
