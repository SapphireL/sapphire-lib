import { ProDescriptions } from '@ant-design/pro-components';
import { Button, Modal } from 'antd';
import React from 'react';

interface DetailsModalProps {
  visible: boolean;
  details: API.GetRequestLogQueryResult;
  onClose: () => void;
}

const DetailsModal: React.FC<DetailsModalProps> = ({ visible, details, onClose }) => {
  return (
    <Modal
      title={`Trace Id (${details.id})`}
      open={visible}
      onCancel={onClose}
      maskClosable={true}
      width={'80%'}
      footer={[
        <Button key="ok" type="primary" onClick={onClose}>
          确定
        </Button>,
      ]}
    >
      <ProDescriptions bordered={true} column={1}>
        <ProDescriptions.Item
          valueType="text"
          label={<span style={{ whiteSpace: 'nowrap' }}>请求header</span>}
        >
          <div
            style={{
              padding: '16px',
              overflow: 'auto',
              fontSize: '85%',
              backgroundColor: 'rgba(150, 150, 150, 0.1)',
              color: 'rgba(0, 0, 0, 0.65)',
              lineHeight: 1.45,
              fontFamily: 'SFMono-Regular, Consolas, "Liberation Mono", Menlo, Courier, monospace',
              borderRadius: '3px',
              whiteSpace: 'pre-wrap', // 更改为 pre-line
            }}
          >
            {details.requestHeader
              ? JSON.stringify(JSON.parse(details.requestHeader.replace(/&nbsp;/g, ' ')), null, 2)
              : '请求头为空'}
          </div>
        </ProDescriptions.Item>
        <ProDescriptions.Item valueType="jsonCode" label="请求body">
          {details.requestBody}
        </ProDescriptions.Item>
        <ProDescriptions.Item valueType="jsonCode" label="响应header">
          {details.responseHeader}
        </ProDescriptions.Item>
        <ProDescriptions.Item valueType="text" label="响应body">
          <div
            style={{
              padding: '16px',
              overflow: 'auto',
              fontSize: '85%',
              backgroundColor: 'rgba(150, 150, 150, 0.1)',
              color: 'rgba(0, 0, 0, 0.65)',
              lineHeight: 1.45,
              fontFamily: 'SFMono-Regular, Consolas, "Liberation Mono", Menlo, Courier, monospace',
              borderRadius: '3px',
              whiteSpace: 'pre-wrap', // 更改为 pre-line
            }}
          >
            {details.responseBody
              ? JSON.stringify(JSON.parse(details.responseBody.replace(/&nbsp;/g, ' ')), null, 2)
              : '请求头为空'}
          </div>
        </ProDescriptions.Item>
      </ProDescriptions>
    </Modal>
  );
};

export default DetailsModal;
