import type { ActionType, ColumnsState, ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { FormattedMessage } from '@umijs/max';
import { useState } from 'react';

import { getAdminSystemLogsSystemLogs as systemLogsApi } from '@/services/admin/systemLogs';
import { Tag, theme } from 'antd';
import { forwardRef, useRef } from 'react';

const getSystemLogsRequest = async (params: any, sort: any) => {
  const modifiedParams: API.getAdminSystemLogsLoginLogsParams = {
    ...params,
    sortBy: Object.keys(sort).length > 0 ? Object.keys(sort)[0] : undefined,
    isDescending: Object.keys(sort).length > 0 ? Object.values(sort)[0] === 'descend' : undefined,
  };
  const response = await systemLogsApi(modifiedParams);
  return response;
};

interface TableListProps {
  onDetailsClick: (record: API.GetRequestLogQueryResult) => void;
}

const { useToken } = theme;

const TableList = forwardRef<TableListProps, TableListProps>(({ onDetailsClick }) => {
  const actionRef = useRef<ActionType>();
  const { token } = useToken();
  const [columnsStateMap, setColumnsStateMap] = useState<Record<string, ColumnsState>>({
    deviceBrand: {
      show: false,
    },
    device: {
      show: false,
    },
    browser: {
      show: false,
    },
    browserVersion: {
      show: false,
    },
    clientIp: {
      show: false,
    },
    address: {
      show: false,
    },
    os: {
      show: false,
    },
    userAgent: {
      show: false,
    },
  });

  const columns: ProColumns<API.GetRequestLogQueryResult>[] = [
    {
      dataIndex: 'index',
      valueType: 'index',
      width: 48,
      fixed: 'left',
    },
    {
      title: <FormattedMessage id="system.monitor.system.columns.id"></FormattedMessage>,
      dataIndex: 'id',
      valueType: 'text',
      fixed: 'left',
      ellipsis: false,
      render: (text, record) => (
        <a
          key="details"
          onClick={() => {
            onDetailsClick(record);
          }}
          // 使用动态主题色
          style={{ color: token.colorInfo }}
        >
          <div style={{ whiteSpace: 'nowrap' }}>{text}</div>
        </a>
      ),
    },
    {
      title: <FormattedMessage id="system.monitor.system.columns.userId"></FormattedMessage>,
      dataIndex: 'userId',
      valueType: 'text',
      fixed: 'left',
      ellipsis: false,
      render: (text) => <div style={{ whiteSpace: 'nowrap' }}>{text}</div>,
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.username"></FormattedMessage>
        </div>
      ),
      dataIndex: 'username',
      valueType: 'text',
    },
    {
      title: <FormattedMessage id="system.monitor.system.columns.path"></FormattedMessage>,
      dataIndex: 'path',
      valueType: 'code',
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.method"></FormattedMessage>
        </div>
      ),
      dataIndex: 'method',
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.statusCode"></FormattedMessage>
        </div>
      ),
      dataIndex: 'statusCode',
      valueType: 'text',
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.consumingMilliseconds"></FormattedMessage>
        </div>
      ),
      dataIndex: 'consumingMilliseconds',
      valueType: 'text',
      render: (text) => {
        if ((text as number) <= 200) {
          return <Tag color="green">{text}ms</Tag>;
        } else if ((text as number) <= 1000) {
          return <Tag color="orange">{text}ms</Tag>;
        } else {
          return <Tag color="red">{text}ms</Tag>;
        }
      },
    },
    {
      title: <FormattedMessage id="system.monitor.system.columns.creationTime"></FormattedMessage>,
      dataIndex: 'creationTime',
      valueType: 'dateTime',
      render: (text) => <div style={{ whiteSpace: 'nowrap' }}>{text}</div>,
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.deviceBrand"></FormattedMessage>
        </div>
      ),
      dataIndex: 'deviceBrand',
      valueType: 'text',
      search: false,
    },

    {
      title: <FormattedMessage id="system.monitor.system.columns.device"></FormattedMessage>,
      dataIndex: 'device',
      valueType: 'text',
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.browser"></FormattedMessage>
        </div>
      ),
      dataIndex: 'browser',
      valueType: 'text',
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.browserVersion"></FormattedMessage>
        </div>
      ),
      dataIndex: 'browserVersion',
      valueType: 'text',
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.clientIp"></FormattedMessage>
        </div>
      ),
      dataIndex: 'clientIp',
      valueType: 'text',
    },
    {
      title: () => (
        <div style={{ whiteSpace: 'nowrap' }}>
          {' '}
          <FormattedMessage id="system.monitor.system.columns.address"></FormattedMessage>
        </div>
      ),
      dataIndex: 'address',
      valueType: 'text',
    },
    {
      title: <FormattedMessage id="system.monitor.system.columns.os"></FormattedMessage>,
      dataIndex: 'os',
      valueType: 'text',
    },
    {
      title: <FormattedMessage id="system.monitor.system.columns.userAgent"></FormattedMessage>,
      dataIndex: 'userAgent',
      valueType: 'text',
    },
  ];

  return (
    <ProTable<API.GetRequestLogQueryResult, API.GetRequestLogQueryResultPagedQueryResult>
      actionRef={actionRef}
      rowKey="id"
      pagination={{
        showSizeChanger: true,
      }}
      request={getSystemLogsRequest}
      columns={columns}
      columnsState={{
        value: columnsStateMap,
        onChange: setColumnsStateMap,
      }}
    />
  );
});

export default TableList;
