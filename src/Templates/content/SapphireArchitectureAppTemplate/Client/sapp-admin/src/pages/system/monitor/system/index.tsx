import { PageContainer } from '@ant-design/pro-components';
import { useState } from 'react';
import DetailsModal from './components/Details';
import Table from './components/TableList';
const SystemMonitorList: React.FC = () => {
  const [detailsVisible, setDetailsVisible] = useState<boolean>(false);
  const [details, setDetails] = useState<NonNullable<API.GetRequestLogQueryResult>>(
    {} as API.GetRequestLogQueryResult,
  );

  const handleDetailsClick = (details: API.GetRequestLogQueryResult) => {
    setDetailsVisible(true);
    setDetails(details);
  };

  return (
    <PageContainer>
      <Table onDetailsClick={handleDetailsClick}></Table>
      <DetailsModal
        details={details}
        visible={detailsVisible}
        onClose={() => {
          setDetailsVisible(false);
        }}
      />
    </PageContainer>
  );
};

export default SystemMonitorList;
