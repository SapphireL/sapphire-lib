import { PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { FormattedMessage } from '@umijs/max';
import { Access, useAccess } from 'umi';

import { SystemManagement as permissions } from '@/permissions/mapRoutesPermissions';

import {
  deleteAdminManagementRole as deleteRoleApi,
  getAdminManagementRoleList as roleListApi,
} from '@/services/admin/roleManagement';
import { Button, message, Popconfirm, theme } from 'antd';
import { forwardRef, useImperativeHandle, useRef } from 'react';

export enum RoleActionType {
  Add = 'Add',
  Update = 'Update',
}

/**
 * 确认删除角色
 *
 * @param value - 要删除的角色的信息，预期包含一个有效的 id 属性
 */
const confirm = async (value: API.GetRolesQueryResult) => {
  await deleteRoleApi({ id: value.id });
  message.success('删除成功');
};

/**
 * 异步请求角色列表的函数
 *
 * @param params 请求角色列表的参数，具体结构由API定义
 * @param sort 排序条件对象
 * @returns 返回角色列表的API响应数据
 */
const getRolesRequest = async (params: any, sort: any) => {
  const modifiedParams: API.getAdminManagementUserListParams = {
    ...params,
    sortBy: sort ? Object.keys(sort)[0] : undefined,
    isDescending: sort ? Object.values(sort)[0] === 'descend' : undefined,
  };
  const response = await roleListApi(modifiedParams);
  return response;
};

// Props 的类型定义
interface TableListProps {
  openRolePermissionsForm: (id: string, name: string) => void;
  openRoleForm: (ActionType: RoleActionType, role?: API.UpdateRoleCommand | undefined) => void;
}

// ref 类型定义
interface TableListRef {
  reloadTable: () => void;
}

const { useToken } = theme;

const TableList = forwardRef<TableListRef, TableListProps>(
  ({ openRolePermissionsForm, openRoleForm }, ref) => {
    const actionRef = useRef<ActionType>();
    const access = useAccess();

    const { token } = useToken();

    // 暴露刷新表格的方法给外部组件
    useImperativeHandle(ref, () => ({
      reloadTable: () => {
        actionRef.current?.reload();
      },
    }));

    const columns: ProColumns<API.GetRolesQueryResult>[] = [
      {
        dataIndex: 'index',
        valueType: 'index',
        width: 48,
      },
      {
        title: <FormattedMessage id="system.roles.columns.name"></FormattedMessage>,
        dataIndex: 'name',
        sorter: true,
      },
      {
        title: <FormattedMessage id="system.roles.columns.description"></FormattedMessage>,
        dataIndex: 'description',
        valueType: 'textarea',
      },
      {
        title: <FormattedMessage id="system.roles.columns.hasAllPermissions"></FormattedMessage>,
        dataIndex: 'hasAllPermissions',
        sorter: true,
        valueEnum: {
          false: {
            text: (
              <FormattedMessage id="system.roles.columns.hasAllPermissions.no"></FormattedMessage>
            ),
            status: 'Default',
          },
          true: {
            text: (
              <FormattedMessage id="system.roles.columns.hasAllPermissions.yes"></FormattedMessage>
            ),
            status: 'Success',
          },
        },
      },
      {
        title: <FormattedMessage id="system.roles.columns.creationTime"></FormattedMessage>,
        dataIndex: 'creationTime',
        valueType: 'dateTime',
      },
      {
        title: <FormattedMessage id="system.roles.columns.actions"></FormattedMessage>,
        dataIndex: 'option',
        valueType: 'option',
        render: (_, record) => [
          <a
            key="update"
            onClick={() => {
              const { id, ...rest } = record;
              openRoleForm(RoleActionType.Update, { roleId: id, ...rest });
            }}
            // 使用动态主题色
            style={{ color: token.colorInfo }}
          >
            <FormattedMessage id="tables.edit"></FormattedMessage>
          </a>,
          !record.hasAllPermissions && (
            <a
              key="permissions"
              onClick={() => {
                openRolePermissionsForm(record.id, record.name);
              }}
              // 使用动态主题色
              style={{ color: token.colorInfo }}
            >
              <FormattedMessage id="tables.permissions"></FormattedMessage>
            </a>
          ),
          <Access
            key="deleteAccess"
            accessible={access.hasAccessByPermission(permissions.Role.Delete)}
          >
            <Popconfirm
              key="delete"
              title={<FormattedMessage id="tables.delete"></FormattedMessage>}
              description={
                <FormattedMessage id="tables.confirm.delete" values={{ name: record.name }} />
              }
              onConfirm={async () => {
                await confirm(record);
                actionRef.current?.reloadAndRest?.();
              }}
              okText={<FormattedMessage id="tables.confirm"></FormattedMessage>}
              cancelText={<FormattedMessage id="tables.cancel"></FormattedMessage>}
            >
              <a // 使用动态主题色
                style={{ color: token.colorError }}
              >
                <FormattedMessage id="tables.delete"></FormattedMessage>
              </a>
            </Popconfirm>
          </Access>,
        ],
      },
    ];

    return (
      <ProTable<API.GetRolesQueryResult, API.GetRolesQueryResultPagedQueryResult>
        actionRef={actionRef}
        rowKey="id"
        options={{
          search: true,
        }}
        search={false}
        pagination={{
          showSizeChanger: true,
        }}
        request={getRolesRequest}
        columns={columns}
        headerTitle={
          <Access accessible={access.hasAccessByPermission(permissions.Role.Create)}>
            <Button
              key="primary"
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => {
                openRoleForm(RoleActionType.Add);
              }}
            >
              <FormattedMessage id="pages.searchTable.new"></FormattedMessage>
            </Button>
          </Access>
        }
      />
    );
  },
);

export default TableList;
