import { ModalForm, ProFormSwitch, ProFormText, ProFormTextArea } from '@ant-design/pro-components';

import {
  postAdminManagementRole as addRoleApi,
  putAdminManagementRole as updateRoleApi,
} from '@/services/admin/roleManagement';
import { FormattedMessage } from '@umijs/max';
import { message } from 'antd';
import React from 'react';
import { RoleActionType } from './TableList';

interface RoleModalProps {
  visible: boolean;
  role: Partial<API.UpdateRoleCommand> | undefined;
  onClose: () => void;
  actionType: RoleActionType;
  children?: React.ReactNode;
  onSuccess: () => void;
}

const handleAdd = async (fields: API.AddRoleCommand) => {
  const hide = message.loading(<FormattedMessage id="system.message.creating"></FormattedMessage>);

  try {
    await addRoleApi({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};

const handleUpdate = async (fields: API.UpdateRoleCommand) => {
  const hide = message.loading('正在修改');

  try {
    await updateRoleApi({ ...fields });
    hide();
    message.success('修改成功');
    return true;
  } catch (error) {
    hide();
    message.error('修改失败请重试！');
    return false;
  }
};

const UpdateRoleForm: React.FC<RoleModalProps> = ({
  visible,
  onClose,
  role,
  actionType,
  children,
  onSuccess,
}) => {
  return (
    <ModalForm<API.UpdateRoleCommand>
      title={
        actionType === RoleActionType.Add ? (
          <FormattedMessage id="system.roles.form.new.title"></FormattedMessage>
        ) : (
          <FormattedMessage id="system.roles.form.edit.title"></FormattedMessage>
        )
      }
      width="400px"
      open={visible}
      initialValues={role}
      trigger={<>{children}</>}
      onOpenChange={(open) => {
        if (!open) {
          onClose();
        }
      }}
      onFinish={async (value) => {
        switch (actionType) {
          case RoleActionType.Add:
            await handleAdd({ ...value } as API.AddRoleCommand);
            break;
          case RoleActionType.Update:
            await handleUpdate({
              ...value,
              roleId: role?.roleId,
            } as API.UpdateRoleCommand);
            break;
        }
        onSuccess();
        return true;
      }}
      modalProps={{
        destroyOnClose: true,
      }}
    >
      <ProFormText
        rules={[
          {
            required: true,
            message: <FormattedMessage id="system.roles.form.title.rule"></FormattedMessage>,
          },
        ]}
        width="md"
        label={<FormattedMessage id="system.roles.form.title"></FormattedMessage>}
        name="name"
      />
      <ProFormTextArea
        width="md"
        name="description"
        label={<FormattedMessage id="system.roles.form.description"></FormattedMessage>}
      />
      <ProFormSwitch
        name="hasAllPermissions"
        label={<FormattedMessage id="system.roles.form.allPermissions"></FormattedMessage>}
      />
    </ModalForm>
  );
};

export default UpdateRoleForm;
