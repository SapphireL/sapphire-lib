import { ModalForm } from '@ant-design/pro-components';

import { NotInServerPermissions } from '@/permissions/mapRoutesPermissions';
import { permissionsTreeData } from '../permissions';

import {
  getAdminManagementRolePermissions as rolePermissionsApi,
  putAdminManagementRolePermissions as updateRolePermissionsApi,
} from '@/services/admin/roleManagement';
import { FormattedMessage } from '@umijs/max';
import { Tree, TreeProps } from 'antd';
import React, { useEffect, useState } from 'react';

interface RoleModalProps {
  visible: boolean;
  roleId: string;
  roleName: string;
  onClose: () => void;
}

const updateRolePermissionsRequest = async (roleId: string, permissions: string[]) => {
  await updateRolePermissionsApi({
    roleId,
    claims: permissions,
  });
};

const loadPermissions = async (roleName: string) => {
  const permissions = await rolePermissionsApi({
    roleName: roleName,
  });

  return permissions.permissions ? permissions.permissions : [];
};

const PermissionsModalForm: React.FC<RoleModalProps> = ({ visible, roleId, roleName, onClose }) => {
  const [checkedPermissions, setCheckedPermissions] = useState<React.Key[]>([]);
  const [expandedKeys, setExpandedKeys] = useState<React.Key[]>([]); // 维护展开的节点
  const [autoExpandParent, setAutoExpandParent] = useState<boolean>(true);

  useEffect(() => {
    const fetchPermissions = async () => {
      if (visible && roleName) {
        const permissions = await loadPermissions(roleName);
        setCheckedPermissions(permissions); // 将返回的权限数据存储到状态中

        // 更新展开的节点
        const keysToExpand = permissions.map((permission) => permission); // 假设权限与树节点的 key 相同
        console.log(keysToExpand);
        setExpandedKeys(keysToExpand);
      }
    };

    fetchPermissions();

    // 清理副作用
    return () => {
      setCheckedPermissions([]); // 清空选中的权限
      setAutoExpandParent(true);
    };
  }, [visible, roleName]);

  const onExpand: TreeProps['onExpand'] = (expandedKeysValue) => {
    setExpandedKeys(expandedKeysValue);
    setAutoExpandParent(false);
  };

  return (
    <ModalForm
      title={<FormattedMessage id="system.roles.permissions.form.title"></FormattedMessage>}
      width="750px"
      open={visible}
      onFinish={async () => {
        const result = checkedPermissions.filter(
          (item) => !NotInServerPermissions.includes(item.toString()),
        );

        await updateRolePermissionsRequest(
          roleId,
          result.map((item) => String(item)),
        );

        setCheckedPermissions([]);
        return true;
      }}
      onOpenChange={(open) => {
        if (!open) {
          onClose();
        }
      }}
    >
      <Tree
        checkable
        treeData={permissionsTreeData}
        checkedKeys={checkedPermissions}
        autoExpandParent={autoExpandParent}
        onExpand={onExpand}
        expandedKeys={expandedKeys}
        onCheck={(checkedKeysValue) => {
          const checkedKeys =
            'checked' in checkedKeysValue ? checkedKeysValue.checked : checkedKeysValue;
          setCheckedPermissions(checkedKeys);
        }}
      ></Tree>
    </ModalForm>
  );
};

export default PermissionsModalForm;
