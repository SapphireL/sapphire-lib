import type { ActionType } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-components';

import Table, { RoleActionType } from './components/TableList';
import UpdateRoleForm from './components/UpdateRoleForm';
import UpdateRolePermissions from './components/UpdateRolePermissionsForm';

import React, { useRef, useState } from 'react';

const RoleList: React.FC = () => {
  const tableListRef = useRef<{ reloadTable: () => void }>(null);
  /** 新建窗口的弹窗 */
  const [updateRolePermissionsVisible, handleUpdateRolePermissionsVisible] =
    useState<boolean>(false);
  const [updateRoleFormVisible, setUpdateRoleFormVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [roleId, setRoleId] = useState<string>('');
  const [roleName, setRoleName] = useState<string>('');
  const [role, setRole] = useState<API.UpdateRoleCommand>();
  const [actionType, setActionType] = useState<RoleActionType>(RoleActionType.Add);

  const openRolePermissionsForm = (id: string, name: string) => {
    setRoleId(id);
    setRoleName(name);
    handleUpdateRolePermissionsVisible(true);
  };

  const openRoleForm = (actionType: RoleActionType, role?: API.UpdateRoleCommand | undefined) => {
    setUpdateRoleFormVisible(true);
    setRole(role);
    setActionType(actionType);
  };

  const handleRefreshTable = () => {
    // 调用子组件的 reloadTable 方法来刷新表格数据
    tableListRef.current?.reloadTable();
  };

  return (
    <PageContainer>
      <Table
        ref={tableListRef}
        openRolePermissionsForm={openRolePermissionsForm}
        openRoleForm={openRoleForm}
      ></Table>
      <UpdateRoleForm
        visible={updateRoleFormVisible}
        onClose={() => {
          setUpdateRoleFormVisible(false);
          actionRef.current?.reloadAndRest?.();
        }}
        role={role}
        actionType={actionType}
        onSuccess={() => {
          handleRefreshTable();
        }}
      ></UpdateRoleForm>
      <UpdateRolePermissions
        visible={updateRolePermissionsVisible}
        roleId={roleId}
        roleName={roleName}
        onClose={() => handleUpdateRolePermissionsVisible(false)}
      ></UpdateRolePermissions>
    </PageContainer>
  );
};

export default RoleList;
