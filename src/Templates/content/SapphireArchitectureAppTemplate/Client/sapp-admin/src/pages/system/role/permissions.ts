
import { SystemManagement } from '@/permissions/mapRoutesPermissions'
export const permissionsTreeData = [
    {
        title: '系统管理',
        key: SystemManagement.System,
        value: SystemManagement.System,
        children: [
            {
                title: '角色管理',
                key: SystemManagement.Role.Role,
                value: SystemManagement.Role.Role,
                children: [
                    {
                        title: '角色列表',
                        key: SystemManagement.Role.List,
                        value: SystemManagement.Role.List,
                    },
                    {
                        title: '新增角色',
                        key: SystemManagement.Role.Create,
                        value: SystemManagement.Role.Create,
                    },
                    {
                        title: '编辑角色',
                        key: SystemManagement.Role.Update,
                        value: SystemManagement.Role.Update,
                    },
                    {
                        title: '角色权限管理',
                        key: SystemManagement.Role.UpdateClaims,
                        value: SystemManagement.Role.UpdateClaims,
                    },
                    {
                        title: '删除角色',
                        key: SystemManagement.Role.Delete,
                        value: SystemManagement.Role.Delete,
                    }
                ]
            },
            {
                title: '用户管理',
                key: SystemManagement.User.User,
                value: SystemManagement.User.User,
                children: [
                    {
                        title: '用户列表',
                        key: SystemManagement.User.List,
                        value: SystemManagement.User.List,
                    },
                    {
                        title: '新增用户',
                        key: SystemManagement.User.Create,
                        value: SystemManagement.User.Create,
                    },
                    {
                        title: '编辑用户',
                        key: SystemManagement.User.Update,
                        value: SystemManagement.User.Update,
                    },
                    {
                        title: '删除用户',
                        key: SystemManagement.User.Delete,
                        value: SystemManagement.User.Delete,
                    }
                ]
            }
        ]
    },
]