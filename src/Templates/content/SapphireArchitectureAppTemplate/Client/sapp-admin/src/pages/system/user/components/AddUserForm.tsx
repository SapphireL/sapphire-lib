import { ModalForm, ProFormDependency, ProFormText } from '@ant-design/pro-components';

import { postAdminManagementUser as addUserApi } from '@/services/admin/userManagement';
import { message } from 'antd';
import React from 'react';

interface UserModalProps {
  visible: boolean;
  onClose: () => void;
  children?: React.ReactNode;
  onSuccess: () => void;
}

const handleAdd = async (fields: API.AddUserCommand) => {
  const hide = message.loading('正在添加');

  try {
    await addUserApi({ ...fields });
    hide();
    message.success('添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('添加失败请重试！');
    return false;
  }
};

const AddUserForm: React.FC<UserModalProps> = ({ visible, onClose, children, onSuccess }) => {
  return (
    <ModalForm<API.AddUserCommand>
      title="新建用户"
      width="400px"
      open={visible}
      trigger={<>{children}</>}
      onOpenChange={(open) => {
        if (!open) {
          onClose();
        }
      }}
      onFinish={async (value) => {
        await handleAdd({ ...value } as API.AddUserCommand);
        onSuccess();
        return true;
      }}
      modalProps={{
        destroyOnClose: true,
      }}
    >
      <ProFormText
        rules={[
          {
            required: true,
            message: '用户名为必填项',
          },
          {
            min: 6,
            max: 18,
            message: '用户名长度必须在6-18位之间',
          },
        ]}
        width="md"
        label="用户名"
        name="username"
      />
      <ProFormText.Password
        rules={[
          {
            required: true,
            message: '用户密码为必填项',
          },
          {
            pattern: new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{1,}$'),
            message: '密码必须且只能包含大写字母、小写字母和数字',
          },
        ]}
        width="md"
        label="密码"
        name="password"
      ></ProFormText.Password>

      <ProFormDependency name={['password']}>
        {({ password }) => {
          return (
            <ProFormText.Password
              width="md"
              name="password2"
              label="密码"
              rules={[
                {
                  required: true,
                },
                () => ({
                  validator(_, value) {
                    if (!value || password === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('两次输入的密码不一致'));
                  },
                }),
              ]}
            />
          );
        }}
      </ProFormDependency>
    </ModalForm>
  );
};

export default AddUserForm;
