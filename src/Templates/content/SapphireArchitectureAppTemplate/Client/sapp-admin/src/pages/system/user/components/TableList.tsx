import { PlusOutlined } from '@ant-design/icons';
import type { ActionType, ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';

import { SystemManagement as permissions } from '@/permissions/mapRoutesPermissions';
import { getAdminManagementUserList as userListApi } from '@/services/admin/userManagement';
import { FormattedMessage } from '@umijs/max';
import { Button, theme } from 'antd';
import { forwardRef, useImperativeHandle, useRef } from 'react';
import { Access, useAccess } from 'umi';

interface TableListProps {
  onAddUserClick: () => void;
  onUpdatePasswordClick: (user: API.GetUsersQueryResult) => void;
  onUserRoleClick: (user: API.GetUsersQueryResult) => void;
}

// ref 类型定义
interface TableListRef {
  reloadTable: () => void;
}

const { useToken } = theme;

const TableList = forwardRef<TableListRef, TableListProps>(
  ({ onAddUserClick, onUpdatePasswordClick, onUserRoleClick }, ref) => {
    const actionRef = useRef<ActionType>();
    const access = useAccess();

    const { token } = useToken();

    // 暴露刷新表格的方法给外部组件
    useImperativeHandle(ref, () => ({
      reloadTable: () => {
        actionRef.current?.reload();
      },
    }));

    const columns: ProColumns<API.GetUsersQueryResult>[] = [
      {
        dataIndex: 'index',
        valueType: 'index',
        width: 48,
      },
      {
        title: '用户名',
        dataIndex: 'username',
        sorter: true,
      },
      {
        title: '昵称',
        dataIndex: 'nickname',
      },
      {
        title: '头像',
        dataIndex: 'avatarUrl',
        valueType: 'image',
      },
      {
        title: '手机号',
        dataIndex: 'phoneNumber',
      },
      {
        title: '创建时间',
        dataIndex: 'creationTime',
        valueType: 'dateTime',
      },
      {
        title: '操作',
        dataIndex: 'option',
        valueType: 'option',
        render: (_, record) => [
          <Access
            key="deleteAccess"
            accessible={access.hasAccessByPermission(permissions.User.Delete)}
          >
            <a
              key="update"
              onClick={() => {
                onUpdatePasswordClick(record);
              }}
              // 使用动态主题色
              style={{ color: token.colorInfo }}
            >
              修改密码
            </a>
          </Access>,
          <Access key="userRoles" accessible={access.hasAccessByPermission(permissions.User.Roles)}>
            <a
              key="roles"
              onClick={() => {
                onUserRoleClick(record);
              }}
              // 使用动态主题色
              style={{ color: token.colorInfo }}
            >
              分配角色
            </a>
          </Access>,
        ],
      },
    ];

    return (
      <ProTable<API.GetUserQueryResult>
        headerTitle={
          <Access
            key="addUserAccess"
            accessible={access.hasAccessByPermission(permissions.User.Create)}
          >
            <Button
              key="primary"
              icon={<PlusOutlined />}
              type="primary"
              onClick={() => {
                onAddUserClick();
              }}
            >
              <FormattedMessage id="pages.searchTable.new"></FormattedMessage>
            </Button>
          </Access>
        }
        actionRef={actionRef}
        rowKey="id"
        options={{
          search: true,
        }}
        search={false}
        request={async (params, sorter) => {
          // 删除 keyword 属性
          const { current, pageSize, ...restParams } = params;
          const modifiedParams: API.getAdminManagementUserListParams = {
            current, // 将 current 转换为 page
            pageSize,
            ...restParams,
          };
          if (sorter) {
            // ProTable 中的 sorter 格式 { field: 'columnKey', order: 'ascend' | 'descend' }
            modifiedParams.sortBy = Object.keys(sorter)[0];
            modifiedParams.isDescending = Object.values(sorter)[0] === 'descend';
          }
          // 发送请求时，只传递 restParams
          const response = await userListApi(modifiedParams, sorter);
          return response;
        }}
        columns={columns}
      />
    );
  },
);

export default TableList;
