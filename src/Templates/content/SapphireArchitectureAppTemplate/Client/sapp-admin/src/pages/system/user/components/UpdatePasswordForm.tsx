import { ModalForm, ProFormDependency, ProFormText } from '@ant-design/pro-components';

import { putAdminManagementUserPassword as update } from '@/services/admin/userManagement';
import { message } from 'antd';
import React from 'react';

interface UserModalProps {
  visible: boolean;
  onClose: () => void;
  children?: React.ReactNode;
  onSuccess: () => void;
  user: API.GetUsersQueryResult;
}

const handleUpdate = async (fields: API.UpdatePasswordCommand) => {
  const hide = message.loading('正在修改');

  try {
    await update({ ...fields });
    hide();
    message.success('修改成功');
    return true;
  } catch (error) {
    hide();
    message.error('修改失败请重试！');
    return false;
  }
};

const UpdatePasswordForm: React.FC<UserModalProps> = ({
  visible,
  onClose,
  children,
  user,
  onSuccess,
}) => {
  return (
    <ModalForm<API.UpdatePasswordCommand>
      title={`修改${user.username}密码`}
      width="400px"
      open={visible}
      trigger={<>{children}</>}
      onOpenChange={(open) => {
        if (!open) {
          onClose();
        }
      }}
      onFinish={async (value) => {
        await handleUpdate({ ...value } as API.UpdatePasswordCommand);
        onSuccess();
        return true;
      }}
      modalProps={{
        destroyOnClose: true,
      }}
    >
      <ProFormText hidden={true} name="userId" initialValue={user.id}></ProFormText>
      <ProFormText.Password
        rules={[
          {
            required: true,
            message: '用户密码为必填项',
          },
          {
            pattern: new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{1,}$'),
            message: '密码必须且只能包含大写字母、小写字母和数字',
          },
        ]}
        width="md"
        label="密码"
        name="password"
      ></ProFormText.Password>

      <ProFormDependency name={['password']}>
        {({ password }) => {
          return (
            <ProFormText.Password
              width="md"
              name="password2"
              label="密码"
              rules={[
                {
                  required: true,
                },
                () => ({
                  validator(_, value) {
                    if (!value || password === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('两次输入的密码不一致'));
                  },
                }),
              ]}
            />
          );
        }}
      </ProFormDependency>
    </ModalForm>
  );
};

export default UpdatePasswordForm;
