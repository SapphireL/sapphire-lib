import { getAdminManagementRoleList as rolesApi } from '@/services/admin/roleManagement';
import {
  getAdminManagementUserRoles as userRolesApi,
  putAdminManagementUserRoles as updateUserRolesApi,
} from '@/services/admin/userManagement';
import type { ProColumns } from '@ant-design/pro-components';
import { ModalForm, ProTable } from '@ant-design/pro-components';
import { message } from 'antd';
import type { Key } from 'react';
import React, { useState } from 'react';

interface UserRoleFormProps {
  visible: boolean;
  onClose: () => void;
  user: API.GetUsersQueryResult;
  onSuccess: () => void;
}

const getUserRolesRequest = async (userId: string) => {
  return await userRolesApi({ userId });
};

const getRolesRequest = async (params: any, sort: any) => {
  const modifiedParams: API.getAdminManagementUserListParams = {
    ...params,
    sortBy: sort ? Object.keys(sort)[0] : undefined,
    isDescending: sort ? Object.values(sort)[0] === 'descend' : undefined,
  };
  return await rolesApi(modifiedParams);
};

const UserRoleForm: React.FC<UserRoleFormProps> = ({ visible, onClose, user, onSuccess }) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
  const rowSelection = {
    selectedRowKeys: selectedRowKeys,
    type: 'radio' as const,
    onChange: (keys: Key[]) => {
      setSelectedRowKeys(keys);
    },
  };

  const initData = async (params: any, sort: any, userId: string) => {
    const roles = await getUserRolesRequest(userId);
    console.log(roles);
    setSelectedRowKeys(roles.map(({ id }) => id));
    return await getRolesRequest(params, sort);
  };

  const columns: ProColumns<API.GetRolesQueryResult>[] = [
    {
      dataIndex: 'index',
      valueType: 'index',
      width: 48,
    },
    {
      title: '名称',
      dataIndex: 'name',
      sorter: true,
    },
    {
      title: '描述',
      dataIndex: 'description',
      valueType: 'textarea',
    },
    {
      title: '全部权限',
      dataIndex: 'hasAllPermissions',
      sorter: true,
      valueEnum: {
        false: {
          text: '否',
          status: 'Default',
        },
        true: {
          text: '是',
          status: 'Success',
        },
      },
    },
    {
      title: '创建时间',
      dataIndex: 'creationTime',
      valueType: 'dateTime',
    },
  ];

  return (
    <ModalForm
      open={visible}
      onOpenChange={(open) => {
        if (!open) {
          onClose();
        }
      }}
      onFinish={async () => {
        console.log(selectedRowKeys);
        if (selectedRowKeys.length === 0) {
          console.log(selectedRowKeys);
          message.warning('请选择一个角色');
          return false;
        }
        await updateUserRolesApi({
          userId: user.id,
          roleIds: selectedRowKeys as string[],
          defaultRoleId: selectedRowKeys.join(),
        });
        onSuccess();
        return true;
      }}
      title={`更改用户 ${user.username} 的角色`}
      modalProps={{
        destroyOnClose: true,
      }}
    >
      <ProTable<API.GetRolesQueryResult>
        rowKey="id"
        columns={columns}
        pagination={{
          showSizeChanger: true,
        }}
        search={false}
        request={async (params, sort) => {
          const result = await initData(params, sort, user.id);
          return result;
        }}
        options={false}
        rowSelection={rowSelection}
      ></ProTable>
    </ModalForm>
  );
};

export default UserRoleForm;
