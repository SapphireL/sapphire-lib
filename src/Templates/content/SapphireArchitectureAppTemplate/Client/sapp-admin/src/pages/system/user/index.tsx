import { PageContainer } from '@ant-design/pro-components';

import React, { useRef, useState } from 'react';

import AddUserForm from './components/AddUserForm';
import Table from './components/TableList';
import UpdatePasswordForm from './components/UpdatePasswordForm';
import UserRoleForm from './components/UserRoleForm';

const UserList: React.FC = () => {
  const tableListRef = useRef<{ reloadTable: () => void }>(null);
  const [addUserFormVisible, setAddUserFormVisible] = useState<boolean>(false);
  const [updatePasswordFormVisble, setUpdatePasswordFormVisble] = useState<boolean>(false);
  const [userRoleFormVisble, setUserRoleFormVisble] = useState<boolean>(false);
  const [user, setUser] = useState<NonNullable<API.GetUsersQueryResult>>(
    {} as API.GetUsersQueryResult,
  );

  const handleAddUser = () => {
    setAddUserFormVisible(true);
  };

  const handlerUpdatePassword = (user: API.GetUsersQueryResult) => {
    setUpdatePasswordFormVisble(true);
    setUser(user);
  };

  const handleUserRoleForm = (user: API.GetUsersQueryResult) => {
    setUserRoleFormVisble(true);
    setUser(user);
  };

  const handleRefreshTable = () => {
    // 调用子组件的 reloadTable 方法来刷新表格数据
    tableListRef.current?.reloadTable();
  };

  return (
    <PageContainer>
      <Table
        ref={tableListRef}
        onAddUserClick={handleAddUser}
        onUpdatePasswordClick={handlerUpdatePassword}
        onUserRoleClick={handleUserRoleForm}
      ></Table>
      <AddUserForm
        visible={addUserFormVisible}
        onClose={() => {
          setAddUserFormVisible(false);
        }}
        onSuccess={() => {
          handleRefreshTable();
        }}
      ></AddUserForm>
      <UpdatePasswordForm
        visible={updatePasswordFormVisble}
        user={user}
        onClose={() => {
          setUpdatePasswordFormVisble(false);
        }}
        onSuccess={() => {
          handleRefreshTable();
        }}
      ></UpdatePasswordForm>
      <UserRoleForm
        visible={userRoleFormVisble}
        user={user}
        onClose={() => {
          setUserRoleFormVisble(false);
        }}
        onSuccess={() => {
          handleRefreshTable();
        }}
      ></UserRoleForm>
    </PageContainer>
  );
};

export default UserList;
