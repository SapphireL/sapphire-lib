import { getToolsGuidGenerateGuid as getGuidApi } from '@/services/admin/guid';
import { PageContainer, ProDescriptions } from '@ant-design/pro-components';
import { Button } from 'antd';
import React, { useState } from 'react';

import { ReloadOutlined } from '@ant-design/icons';
import type { ProDescriptionsActionType } from '@ant-design/pro-components';
import { useRef } from 'react';
const GuidToolPage: React.FC = () => {
  const actionRef = useRef<ProDescriptionsActionType>();
  const [loading, setLoading] = useState<boolean>(false);
  return (
    <PageContainer>
      <ProDescriptions
        actionRef={actionRef}
        column={2}
        contentStyle={{
          minHeight: '100vh',
          maxWidth: '80%',
          lineHeight: '1.5',
        }}
        size="middle"
        loading={false}
        request={async () => {
          const data = await getGuidApi();
          return Promise.resolve({
            success: true,
            data: data,
          });
        }}
      >
        <ProDescriptions.Item valueType="option">
          <Button
            icon={<ReloadOutlined />}
            type="primary"
            loading={loading}
            onClick={async () => {
              setLoading(true);
              await actionRef.current?.reload();
              setLoading(false);
            }}
            key="reload"
          >
            刷新
          </Button>
        </ProDescriptions.Item>
        <ProDescriptions.Item
          contentStyle={{
            fontSize: '60px',
          }}
          dataIndex="guid"
          valueType="text"
          copyable={true}
        ></ProDescriptions.Item>
      </ProDescriptions>
    </PageContainer>
  );
};

export default GuidToolPage;
