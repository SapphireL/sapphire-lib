
const SystemManagementPrefix = "SystemManagement_";

const RolePrefix = `${SystemManagementPrefix}Role_`;
const UserPrefix = `${SystemManagementPrefix}User_`;

export const RolePermissions = {
    CreateRole: `${RolePrefix}Create`,
    DeleteRole: `${RolePrefix}Delete`,
    GetRole: `${RolePrefix}Get`,
    GetRoles: `${RolePrefix}GetList`,
    UpdateRole: `${RolePrefix}Update`,
    UpdateRoleClaims: `${RolePrefix}UpdateClaims`,
} as const;

export const SystemManagement = {
    System: `${SystemManagementPrefix}SystemManagement`,
    Role: {
        Role: `${RolePrefix}Role`,
        List: `${RolePrefix}GetList`,
        Create: `${RolePrefix}Create`,
        Update: `${RolePrefix}Update`,
        Delete: `${RolePrefix}Delete`,
        Get: `${RolePrefix}Get`,
        UpdateClaims: `${RolePrefix}UpdateClaims`,
    },
    User: {
        User: `${UserPrefix}User`,
        List: `${UserPrefix}GetList`,
        Create: `${UserPrefix}Create`,
        Update: `${UserPrefix}Update`,
        Delete: `${UserPrefix}Delete`,
        Get: `${UserPrefix}Get`,
        Roles: `${UserPrefix}Roles`,
    }
}

export const NotInServerPermissions = [SystemManagement.System, SystemManagement.Role.Role, SystemManagement.User.User];

export function toPermissions(routeName: string): string {
    console.log(routeName)

    if (routeName === "system" || routeName === "role") {
        return SystemManagement.Role.List;
    }

    if (routeName === "user") {
        return SystemManagement.User.List;
    }
    return "admin";
}
