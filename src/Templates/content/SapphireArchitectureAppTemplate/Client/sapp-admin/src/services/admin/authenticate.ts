// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 POST /api/admin/authenticate/authenticate */
export async function postAdminAuthenticateAuthenticate(
  body: API.AuthenticateRequest,
  options?: { [key: string]: any },
) {
  return request<API.AuthenticateResult>('/api/admin/authenticate/authenticate', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 POST /api/admin/authenticate/refresh-token */
export async function postAdminAuthenticateRefreshToken(
  body: API.RefreshTokenRequest,
  options?: { [key: string]: any },
) {
  return request<API.RefreshTokenCommandResult>('/api/admin/authenticate/refresh-token', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
