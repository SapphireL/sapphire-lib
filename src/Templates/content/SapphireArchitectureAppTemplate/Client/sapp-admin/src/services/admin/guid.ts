// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 GET /api/tools/guid/decode-jwt */
export async function getToolsGuidDecodeJwt(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getToolsGuidDecodeJwtParams,
  options?: { [key: string]: any },
) {
  return request<API.DecodeJwtQuery>('/api/tools/guid/decode-jwt', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/tools/guid/generate-guid */
export async function getToolsGuidGenerateGuid(options?: { [key: string]: any }) {
  return request<API.GetGuidQueryResult>('/api/tools/guid/generate-guid', {
    method: 'GET',
    ...(options || {}),
  });
}
