// @ts-ignore
/* eslint-disable */
// API 更新时间：
// API 唯一标识：
import * as authenticate from './authenticate';
import * as guid from './guid';
import * as menu from './menu';
import * as permission from './permission';
import * as roleManagement from './roleManagement';
import * as systemLogs from './systemLogs';
import * as user from './user';
import * as userManagement from './userManagement';
export default {
  authenticate,
  guid,
  menu,
  permission,
  roleManagement,
  systemLogs,
  user,
  userManagement,
};
