// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 GET /api/admin/management/menu */
export async function getAdminManagementMenu(options?: { [key: string]: any }) {
  return request<API.GetMenuResult>('/api/admin/management/menu', {
    method: 'GET',
    ...(options || {}),
  });
}
