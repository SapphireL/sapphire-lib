// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 GET /api/admin/permission/permissions */
export async function getAdminPermissionPermissions(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getAdminPermissionPermissionsParams,
  options?: { [key: string]: any },
) {
  return request<API.GetRolePermissionsQueryResult>('/api/admin/permission/permissions', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
