// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 PUT /api/admin/management/role */
export async function putAdminManagementRole(
  body: API.UpdateRoleCommand,
  options?: { [key: string]: any },
) {
  return request<any>('/api/admin/management/role', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 POST /api/admin/management/role */
export async function postAdminManagementRole(
  body: API.AddRoleCommand,
  options?: { [key: string]: any },
) {
  return request<any>('/api/admin/management/role', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 DELETE /api/admin/management/role */
export async function deleteAdminManagementRole(
  body: API.DeleteRoleCommand,
  options?: { [key: string]: any },
) {
  return request<any>('/api/admin/management/role', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/admin/management/role/list */
export async function getAdminManagementRoleList(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getAdminManagementRoleListParams,
  options?: { [key: string]: any },
) {
  return request<API.GetRolesQueryResultPagedQueryResult>('/api/admin/management/role/list', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/admin/management/role/permissions */
export async function getAdminManagementRolePermissions(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getAdminManagementRolePermissionsParams,
  options?: { [key: string]: any },
) {
  return request<API.GetRolePermissionsQueryResult>('/api/admin/management/role/permissions', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 PUT /api/admin/management/role/permissions */
export async function putAdminManagementRolePermissions(
  body: API.UpdateRoleClaimsCommand,
  options?: { [key: string]: any },
) {
  return request<any>('/api/admin/management/role/permissions', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
