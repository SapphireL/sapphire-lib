// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 GET /api/admin/system/logs/login-logs */
export async function getAdminSystemLogsLoginLogs(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getAdminSystemLogsLoginLogsParams,
  options?: { [key: string]: any },
) {
  return request<API.GetUserLoginLogsQueryResultPagedQueryResult>(
    '/api/admin/system/logs/login-logs',
    {
      method: 'GET',
      params: {
        ...params,
      },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/admin/system/logs/system-logs */
export async function getAdminSystemLogsSystemLogs(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getAdminSystemLogsSystemLogsParams,
  options?: { [key: string]: any },
) {
  return request<API.GetRequestLogQueryResultPagedQueryResult>(
    '/api/admin/system/logs/system-logs',
    {
      method: 'GET',
      params: {
        ...params,
      },
      ...(options || {}),
    },
  );
}
