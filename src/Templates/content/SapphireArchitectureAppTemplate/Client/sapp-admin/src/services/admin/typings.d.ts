declare namespace API {
  type AddRoleCommand = {
    name?: string;
    description?: string;
    hasAllPermissions?: boolean;
  };

  type AddUserCommand = {
    id?: string;
    userName?: string;
    password?: string;
  };

  type AuthenticateRequest = {
    username?: string;
    password?: string;
  };

  type AuthenticateResult = {
    error?: string;
    isAuthenticated?: boolean;
    accessToken?: string;
    refreshToken?: string;
    expires?: string;
    refreshTokenExpires?: string;
    username?: string;
  };

  type DecodeJwtQuery = {
    id?: string;
    jwtToken: string;
  };

  type DeleteRoleCommand = {
    id?: string;
  };

  type getAdminManagementRoleListParams = {
    sortBy?: string;
    isDescending?: boolean;
    keyword?: string;
    current?: number;
    pageSize?: number;
  };

  type getAdminManagementRolePermissionsParams = {
    roleName?: string;
  };

  type getAdminManagementUserListParams = {
    sortBy?: string;
    isDescending?: boolean;
    keyword?: string;
    current?: number;
    pageSize?: number;
  };

  type getAdminManagementUserRolesParams = {
    userId: string;
  };

  type getAdminPermissionPermissionsParams = {
    roleName?: string;
  };

  type getAdminSystemLogsLoginLogsParams = {
    username?: string;
    isSuccess?: boolean;
    startTime?: string;
    endTime?: string;
    sortBy?: string;
    current?: number;
    pageSize?: number;
    isDescending?: boolean;
  };

  type getAdminSystemLogsSystemLogsParams = {
    path?: string;
    clientIp?: string;
    startTime?: string;
    endTime?: string;
    statusCode?: number;
    username?: string;
    method?: string;
    consumingMillisecondsStart?: number;
    consumingMillisecondsEnd?: number;
    sortBy?: string;
    current?: number;
    pageSize?: number;
    isDescending?: boolean;
  };

  type GetGuidQueryResult = {
    guid: string;
  };

  type GetMenuResult = {
    id: string;
    name: string;
    parentId?: string;
    redirect?: string;
    component?: string;
    path: string;
    children?: GetMenuResult[];
    meta?: MenuMetaResult;
  };

  type GetRequestLogQueryResult = {
    id?: string;
    path?: string;
    method?: string;
    deviceBrand?: string;
    device?: string;
    browser?: string;
    browserVersion?: string;
    requestHeader?: string;
    requestBody?: string;
    responseHeader?: string;
    responseBody?: string;
    clientIp?: string;
    address?: string;
    os?: string;
    userAgent?: string;
    userId?: string;
    username?: string;
    statusCode?: number;
    creationTime?: string;
    consumingMilliseconds?: number;
  };

  type GetRequestLogQueryResultPagedQueryResult = {
    data?: GetRequestLogQueryResult[];
    total?: number;
    current?: number;
    success?: boolean;
    pageSize?: number;
  };

  type GetRolePermissionsQueryResult = {
    permissions?: string[];
    hasAllPermissions?: boolean;
  };

  type GetRolesQueryResult = {
    id: string;
    name: string;
    description: string;
    hasAllPermissions: boolean;
    creationTime?: string;
  };

  type GetRolesQueryResultPagedQueryResult = {
    data?: GetRolesQueryResult[];
    total?: number;
    current?: number;
    success?: boolean;
    pageSize?: number;
  };

  type getToolsGuidDecodeJwtParams = {
    jwtToken: string;
  };

  type GetUserAccessQueryResult = {
    currentRole?: string;
    hasAllPermissions?: boolean;
    permissions?: string[];
  };

  type GetUserLoginLogsQueryResult = {
    username: string;
    ipAddress: string;
    address?: string;
    os: string;
    userAgent: string;
    isSuccess?: boolean;
    failureReason?: string;
    loginAt?: string;
  };

  type GetUserLoginLogsQueryResultPagedQueryResult = {
    data?: GetUserLoginLogsQueryResult[];
    total?: number;
    current?: number;
    success?: boolean;
    pageSize?: number;
  };

  type GetUserQueryResult = {
    id: string;
    username: string;
    nickname?: string;
    creationTime?: string;
    avatarUrl?: string;
    phoneNumber?: string;
  };

  type GetUserRolesQueryResult = {
    id: string;
    name: string;
    description: string;
    hasAllPermissions: boolean;
  };

  type GetUsersQueryResult = {
    id: string;
    username: string;
    avatarUrl?: string;
    phoneNumber?: string;
    nickname?: string;
    isDeleted?: boolean;
    creationTime?: string;
  };

  type GetUsersQueryResultPagedQueryResult = {
    data?: GetUsersQueryResult[];
    total?: number;
    current?: number;
    success?: boolean;
    pageSize?: number;
  };

  type MenuMetaResult = {
    metaId: string;
    title: string;
    icon?: string;
    extraIcon?: string;
    showLink?: boolean;
    showParent?: boolean;
    rank?: number;
    auths?: string[];
    roles?: string[];
    keepAlive?: boolean;
    frameSrc?: string;
    frameLoading?: boolean;
    hiddenTag?: boolean;
    dynamicLevel?: number;
    activePath?: string;
  };

  type ProblemDetails = {
    type?: string;
    title?: string;
    status?: number;
    detail?: string;
    instance?: string;
  };

  type RefreshTokenCommandResult = {
    accessToken: string;
    refreshToken: string;
    expires: string;
    refreshTokenExpires: string;
  };

  type RefreshTokenRequest = {
    refreshToken: string;
    username: string;
  };

  type UpdatePasswordCommand = {
    userId?: string;
    password?: string;
  };

  type UpdateRoleClaimsCommand = {
    roleId?: string;
    claims?: string[];
  };

  type UpdateRoleCommand = {
    roleId?: string;
    name?: string;
    description?: string;
    hasAllPermissions?: boolean;
  };

  type UpdateUserRolesCommand = {
    userId: string;
    roleIds: string[];
    defaultRoleId: string;
  };
}
