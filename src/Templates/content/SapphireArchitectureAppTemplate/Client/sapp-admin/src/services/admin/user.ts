// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 GET /api/admin/user */
export async function getAdminUser(options?: { [key: string]: any }) {
  return request<API.GetUserQueryResult>('/api/admin/user', {
    method: 'GET',
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/admin/user/access */
export async function getAdminUserAccess(options?: { [key: string]: any }) {
  return request<API.GetUserAccessQueryResult>('/api/admin/user/access', {
    method: 'GET',
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 PUT /api/admin/user/password */
export async function putAdminUserPassword(body: string, options?: { [key: string]: any }) {
  return request<any>('/api/admin/user/password', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
