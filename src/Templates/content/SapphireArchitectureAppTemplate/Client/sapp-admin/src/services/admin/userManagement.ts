// @ts-ignore
/* eslint-disable */
import { request } from '@umijs/max';

/** 此处后端没有提供注释 POST /api/admin/management/user */
export async function postAdminManagementUser(
  body: API.AddUserCommand,
  options?: { [key: string]: any },
) {
  return request<any>('/api/admin/management/user', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/admin/management/user/list */
export async function getAdminManagementUserList(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getAdminManagementUserListParams,
  options?: { [key: string]: any },
) {
  return request<API.GetUsersQueryResultPagedQueryResult>('/api/admin/management/user/list', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 PUT /api/admin/management/user/password */
export async function putAdminManagementUserPassword(
  body: API.UpdatePasswordCommand,
  options?: { [key: string]: any },
) {
  return request<any>('/api/admin/management/user/password', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/admin/management/user/roles */
export async function getAdminManagementUserRoles(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getAdminManagementUserRolesParams,
  options?: { [key: string]: any },
) {
  return request<API.GetUserRolesQueryResult[]>('/api/admin/management/user/roles', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 PUT /api/admin/management/user/roles */
export async function putAdminManagementUserRoles(
  body: API.UpdateUserRolesCommand,
  options?: { [key: string]: any },
) {
  return request<any>('/api/admin/management/user/roles', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
