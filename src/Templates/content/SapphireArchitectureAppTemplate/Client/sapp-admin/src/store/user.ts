const accessTokenKey: string = "access-token-key";
const refreshTokenKey: string = "refresh-token-key";
const usernameKey: string = "username-key";
const slidingWindow = 2 * 60 * 1000; // 5 分钟

const authenticateStore = {
    setAccessToken(token: string, expireDate: Date) {
        const expires = expireDate.getTime();
        const data = { token, expires };

        localStorage.setItem(accessTokenKey, JSON.stringify(data));
    },

    getAccessToken(): string | null {

        const data = localStorage.getItem(accessTokenKey);
        if (!data) return null;

        try {
            const parsedData = JSON.parse(data);
            if (parsedData.expires) {
                const now = new Date();
                if (now.getTime() > (parsedData.expires - slidingWindow)) {
                    localStorage.removeItem(accessTokenKey);
                    return null;
                }
            }
            return parsedData.token;
        } catch (e) {
            return null;
        }
    },

    setRefreshToken(token: string, expireDate: Date) {
        const expires = expireDate.getTime();
        const data = { token, expires };
        const now = new Date().getTime();
        data.expires = now + expires;

        localStorage.setItem(refreshTokenKey, JSON.stringify(data));
    },

    getRefreshToken(): string | null {
        const data = localStorage.getItem(refreshTokenKey);
        if (!data) return null;

        try {
            const parsedData = JSON.parse(data);
            if (parsedData.expires) {
                const now = new Date();
                if (now.getTime() > parsedData.expires - slidingWindow) {
                    localStorage.removeItem(refreshTokenKey);
                    return null;
                }
            }
            return parsedData.token;
        } catch (e) {
            return null;
        }
    },
    clearTokens() {
        localStorage.removeItem(accessTokenKey);
        localStorage.removeItem(refreshTokenKey);
    },

    getUsername(): string | null {
        return localStorage.getItem(usernameKey);
    },

    setUsername(username: string) {
        localStorage.setItem(usernameKey, username);
    },
};

export default authenticateStore;