﻿using Mapster;

using SapphireArchitecture.Application.CommandHandlers.AccessControls.GetUserPermission;

using SapphireLib.Authorization.Abstractions;
using SapphireLib.Command.Abstractions;

namespace SapphireArchitecture.API.Authorization;

public class PermissionDataProvider(ICommandSender sender) : IPermissionDataProvider
{
    public async Task<List<PermissionData>> GetPermissions(string userId)
    {
        var result = await sender.SendAsync(new UserPermissionQuery
        {
            UserId = new Guid(userId)
        });

        return result.Value.Adapt<List<PermissionData>>();
    }
}
