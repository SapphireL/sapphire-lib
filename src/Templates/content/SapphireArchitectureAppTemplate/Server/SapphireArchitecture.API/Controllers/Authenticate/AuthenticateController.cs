﻿using UAParser;

namespace SapphireArchitecture.API.Controllers.Authenticate;

[ApiAdminController("authenticate")]
public class AuthenticateController(ICommandSender sender) : ControllerBase
{
    /// <summary>
    /// 用户登录接口.
    /// </summary>
    /// <param name="request"><see cref="AuthenticateRequest"/>.</param>
    /// <returns><see cref="AuthenticateResult"/>.</returns>
    [HttpPost("authenticate")]
    [ProducesResponseType(typeof(AuthenticateResult), StatusCodes.Status200OK)]
    public async Task<IActionResult> AuthenticateUser([FromBody] AuthenticateRequest request)
    {
        var clientIp = HttpContext.Connection.RemoteIpAddress?.ToString();
        var userAgent = HttpContext.Request.Headers["User-Agent"].ToString();
        var uaParser = Parser.GetDefault();
        var clientInfo = uaParser.Parse(userAgent);

        var command = new AuthenticateCommand
        {
            Username = request.Username,
            Password = request.Password,
            IpAddress = clientIp,
            OS = clientInfo.OS.ToString(),
            UserAgent = userAgent
        };

        var result = await sender.SendAsync(command);
        return result.Match(Ok, ActionResults.Problem);
    }

    [HttpPost("refresh-token")]
    [ProducesResponseType(typeof(RefreshTokenCommandResult), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest request)
    {
        var command = new RefreshTokenCommand
        {
            RefreshToken = request.RefreshToken,
            Username = request.Username
        };

        var result = await sender.SendAsync(command);
        return result.Match(Ok, ActionResults.Problem);
    }
}
