﻿namespace SapphireArchitecture.API.Controllers.Authenticate;

[ApiAdminController("permission")]
public class PermissionController(ICommandSender sender) : ControllerBase
{
    [HttpGet("permissions")]
    [ProducesResponseType(typeof(GetRolePermissionsQueryResult), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetRolePermissions(string roleName)
    {
        return (await sender.SendAsync(new GetRolePermissionsQuery(roleName))).Match(Ok, ActionResults.Problem);
    }
}
