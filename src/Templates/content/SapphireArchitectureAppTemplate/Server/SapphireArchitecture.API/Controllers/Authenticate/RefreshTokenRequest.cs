﻿namespace SapphireArchitecture.API.Controllers.Authenticate;

public record RefreshTokenRequest
{
    public required string RefreshToken { get; set; }

    public required string Username { get; set; }
}
