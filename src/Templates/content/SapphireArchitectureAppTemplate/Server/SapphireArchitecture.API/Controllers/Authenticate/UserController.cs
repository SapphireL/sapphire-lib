﻿namespace SapphireArchitecture.API.Controllers.Authenticate;

[ApiAdminController("user")]
public class UserController(ICommandSender sender, IUserManager userManager) : ControllerBase
{
    [Authorize]
    [HttpGet]
    [ProducesResponseType(typeof(GetUserQueryResult), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetUser()
    {
        var result = await sender.SendAsync(new GetUserQuery());
        return result.Match(Ok, ActionResults.Problem);
    }

    [Authorize]
    [HttpPut("password")]
    public async Task<IActionResult> UpdatePassword([FromBody] string password)
    {
        var result = await sender.SendAsync(new UpdatePasswordCommand
        {
            Password = password,
            UserId = new Guid(userManager.UserId)
        });

        return result.Match<IActionResult>(Ok, ActionResults.Problem);
    }

    [Authorize]
    [HttpGet("access")]
    [ProducesResponseType(typeof(GetUserAccessQueryResult), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetUserAccess()
    {
        var result = await sender.SendAsync(new GetUserAccessQuery());
        return result.Match(Ok, ActionResults.Problem);
    }
}
