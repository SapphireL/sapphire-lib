﻿namespace SapphireArchitecture.API.Controllers.SystemLogs;

[ApiAdminController("system/logs")]
public class SystemLogsController(ICommandSender commandSender) : ControllerBase
{
    [Authorize]
    [HttpGet("login-logs")]
    [ProducesResponseType(typeof(PagedQueryResult<GetUserLoginLogsQueryResult>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetUserLoginLogs([FromQuery] GetUserLoginLogsQuery query)
    {
        var result = await commandSender.SendAsync(query);
        return result.Match(Ok, ActionResults.Problem);
    }

    [Authorize]
    [HttpGet("system-logs")]
    [ProducesResponseType(typeof(PagedQueryResult<GetRequestLogQueryResult>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetSystemLogs([FromQuery] GetRequestLogQuery query)
    {
        var result = await commandSender.SendAsync(query);
        return result.Match(Ok, ActionResults.Problem);
    }
}
