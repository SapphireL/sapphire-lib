﻿namespace SapphireArchitecture.API.Controllers.SystemManagement;

/// <summary>
/// MenuController.
/// </summary>
[ApiAdminController("management/menu")]
public class MenuController(ICommandSender sender) : ControllerBase
{
    [HttpGet]
    [ProducesResponseType(typeof(GetMenuResult), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public async Task<IActionResult> GetMenus()
    {
        var result = await sender.SendAsync(new GetMenuCommand());
        return result.Match(Ok, ActionResults.Problem);
    }
}
