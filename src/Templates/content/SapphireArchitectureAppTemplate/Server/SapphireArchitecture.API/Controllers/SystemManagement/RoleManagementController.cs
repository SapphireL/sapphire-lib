﻿namespace SapphireArchitecture.API.Controllers.SystemManagement;

[ApiAdminController("management/role")]
public class RoleManagementController(ICommandSender sender) : ControllerBase
{
    [HttpGet("list")]
    [ProducesResponseType(typeof(PagedQueryResult<GetRolesQueryResult>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [PermissionAuthorize(SystemManagementPermissions.RolePermissions.GetRoles)]
    public async Task<IActionResult> List([FromQuery] GetRolesQuery command)
    {
        var result = await sender.SendAsync(command);
        return result.Match(Ok, ActionResults.Problem);
    }

    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [PermissionAuthorize(AccessControlPermissions.UpdateRole)]
    [PermissionAuthorize(SystemManagementPermissions.RolePermissions.UpdateRole)]
    public async Task<IActionResult> UpdateRole([FromBody] UpdateRoleCommand command)
    {
        var result = await sender.SendAsync(command);
        return result.Match<IActionResult>(Ok, ActionResults.Problem);
    }

    [HttpPut("permissions")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]

    [PermissionAuthorize(SystemManagementPermissions.RolePermissions.UpdateRoleClaims)]
    public async Task<IActionResult> UpdateRoleClaims([FromBody] UpdateRoleClaimsCommand command)
    {
        var result = await sender.SendAsync(command);
        return result.Match<IActionResult>(Ok, ActionResults.Problem);
    }

    [HttpGet("permissions")]
    [ProducesResponseType(typeof(GetRolePermissionsQueryResult), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status403Forbidden)]

    [PermissionAuthorize(SystemManagementPermissions.RolePermissions.UpdateRoleClaims)]
    public async Task<IActionResult> GetRolePermissions(string roleName)
    {
        return (await sender.SendAsync(new GetRolePermissionsQuery(roleName))).Match(Ok, ActionResults.Problem);
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [PermissionAuthorize(SystemManagementPermissions.RolePermissions.CreateRole)]
    public async Task<IActionResult> AddRole([FromBody] AddRoleCommand command)
    {
        var result = await sender.SendAsync(command);
        return result.Match<IActionResult>(Created, ActionResults.Problem);
    }

    [HttpDelete]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [PermissionAuthorize(SystemManagementPermissions.RolePermissions.DeleteRole)]
    public async Task<IActionResult> DeleteRole([FromBody] DeleteRoleCommand command)
    {
        var result = await sender.SendAsync(command);
        return result.Match<IActionResult>(Ok, ActionResults.Problem);
    }
}
