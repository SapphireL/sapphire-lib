﻿namespace SapphireArchitecture.API.Controllers.SystemManagement;

[ApiAdminController("management/user")]
public class UserManagementController(ICommandSender sender) : ControllerBase
{
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> AddUser([FromBody] AddUserCommand command)
    {
        var result = await sender.SendAsync(command);
        return result.Match<IActionResult>(Created, ActionResults.Problem);
    }

    [HttpPut("roles")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateUserRoles([FromBody] UpdateUserRolesCommand command)
    {
        var result = await sender.SendAsync(command);
        return result.Match<IActionResult>(Created, ActionResults.Problem);
    }

    [HttpPut("password")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdateUserPassword([FromBody] UpdatePasswordCommand command)
    {
        var result = await sender.SendAsync(command);
        return result.Match<IActionResult>(Ok, ActionResults.Problem);
    }

    [HttpGet("list")]
    [ProducesResponseType(typeof(PagedQueryResult<GetUsersQueryResult>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetUsers([FromQuery] GetUsersQuery query)
    {
        var result = await sender.SendAsync(query);
        return result.Match(Ok, ActionResults.Problem);
    }

    [HttpGet("roles")]
    [ProducesResponseType(typeof(List<GetUserRolesQueryResult>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> GetUserRoles([FromQuery] GetUserRolesQuery query)
    {
        var result = await sender.SendAsync(query);
        return result.Match(Ok, ActionResults.Problem);
    }
}
