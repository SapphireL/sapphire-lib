﻿namespace SapphireArchitecture.API.Controllers.Tools;

[Route("api/tools/guid")]
[ApiController]
public class GuidController(ICommandSender sender) : ControllerBase
{
    [HttpGet("generate-guid")]
    [ProducesResponseType(typeof(GetGuidQueryResult), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetGuid()
    {
        var query = new GetGuidQuery();
        var result = await sender.SendAsync(query);
        return result.Match(Ok, ActionResults.Problem);
    }

    [HttpGet("decode-jwt")]
    [ProducesResponseType(typeof(DecodeJwtQueryResult), StatusCodes.Status200OK)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]
    public async Task<IActionResult> DecodeJwt([FromQuery] DecodeJwtQuery query)
    {
        var result = await sender.SendAsync(query);
        return result.Match(Ok, ActionResults.Problem);
    }
}
