﻿namespace SapphireArchitecture.API.Permissions;

public class SystemManagementPermissions
{
    public class RolePermissions
    {
        public const string CreateRole = RolePrefix + "Create";
        public const string DeleteRole = RolePrefix + "Delete";
        public const string GetRole = RolePrefix + "Get";
        public const string GetRoles = RolePrefix + "GetList";
        public const string UpdateRole = RolePrefix + "Update";
        public const string UpdateRoleClaims = RolePrefix + "UpdateClaims";

        private const string RolePrefix = SystemManagementPrefix + "Role_";
    }

    private const string SystemManagementPrefix = "SystemManagement_";
}
