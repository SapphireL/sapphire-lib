var builder = WebApplication.CreateBuilder(args);

builder.ConfigIpHelper();

var sappBuilder = SappServiceProvider.CreateBuilder();
sappBuilder.AddFileLoggerFactory(builder.Configuration);
sappBuilder.Build();

builder.AddSappLib();
builder.AddSappApp();

var app = builder.Build();
app.UseSapp();
app.Run();