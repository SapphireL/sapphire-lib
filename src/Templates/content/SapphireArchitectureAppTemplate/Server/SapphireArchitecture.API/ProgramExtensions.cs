﻿namespace SapphireArchitecture.API;

public static class ProgramExtensions
{
    public static void AddSappLib(this WebApplicationBuilder builder)
    {
        builder.AddSappLibLogger(action => action.AddFileLogger());
        builder.AddSappLibValidator();
        builder.AddSappLibCommand(action => action.EnablePipeline());
        builder.AddSappAppTools();

        builder.AddSappLibAuthorization(action =>
        {
            action.AddJwtFeature();
            action.AddSuperAdminAutoAllowFeature(config => config.AddSuperAuthorizeHttpContextService());
            action.AddDataPermissionFeature<PermissionDataProvider>();
        });
        builder.AddSappLibWebApp(action =>
        {
            action.AddController();
            action.AddApiDocumentFeature();
            action.EnableRequestLog();
            action.AddProblemDetailsFeature();
        });
        builder.AddSappLibEventBus(
            moduleBuilder =>
            {
                moduleBuilder.AddOutbox();
                moduleBuilder.UseSqlServer();
            },
            options =>
            {
                options.UseOutbox = true;
                options.CreateDbIfNotExists = true;
            });

        builder.AddSappLibDomain();
    }

    public static void AddSappApp(this WebApplicationBuilder builder)
    {
        builder.AddSappAppRequestLogs();
        builder.AddSappAppAuditable();
        builder.AddSappApp<SapphireArchitectureModule>();
    }

    public static void UseSapp(this WebApplication app)
    {
        app.UseSappLib<AuthorizationModule>();
        app.UseSappLib<WebAppModule>();
        app.UseSappLib<SapphireArchitectureModule>();
        app.UseSappLib<EventBusModule>();
        app.UseSappLib<SappAppAuditableEntityModule>();
    }
}
