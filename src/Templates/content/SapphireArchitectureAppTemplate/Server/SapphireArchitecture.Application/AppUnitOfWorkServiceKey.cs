﻿namespace SapphireArchitecture.Application;

public class AppUnitOfWorkServiceKey
{
    public const string Key = "AppUnitOfWorkService";
}
