﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;

public class AuthenticateCommand : ICommand<AuthenticateResult>
{
    public required string Username { get; set; }

    public required string Password { get; set; }

    public required string? IpAddress { get; set; }

    public required string OS { get; set; }

    public required string UserAgent { get; set; }

    // public override void BuildCheckers()
    // {
    //    CheckFor(x => x.UserName, UserName).NotNullOrEmpty();
    //    CheckFor(x => x.Password, Password).NotNullOrEmpty();
    // }
}
