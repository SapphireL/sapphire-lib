﻿using SapphireArchitecture.Domain.AccessControls.Events;

using SapphireLib.EventBus.Abstractions.EventBus;
using SapphireLib.Tools;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;

internal class AuthenticateHandler(ITokenClaimsService tokenClaimsService, IAccessControlUserRepository userRepository, IEventBus eventBus) : CommandHandler<AuthenticateCommand, AuthenticateResult>
{
    public override async Task<Result<AuthenticateResult>> HandleAsync(AuthenticateCommand command, CancellationToken cancellationToken = default)
    {
        var user = await userRepository.GetAsync(command.Username);
        if (user == null || !user.VerifyPassword(command.Password))
        {
            await eventBus.PublishAsync(UserLoginDomainEvent.CreateFailureEvent(command.Username, command.IpAddress ?? "未知", "未知", command.OS, command.UserAgent, DateTime.Now, "用户名或密码错误"));

            return AuthenticateResult.AuthenticateFailedResult("用户名或密码错误");
        }

        var defaultRole = user.UserRoles.SingleOrDefault(u => u.IsDefault)?.Role;

        if (defaultRole == null)
        {
            return AuthenticateResult.AuthenticateFailedResult("用户未分配角色");
        }

        var token = tokenClaimsService.GenerateJwtToken(user.Id.ToString(), user.UserName, defaultRole.Name, defaultRole.HasAllPermissions);
        await userRepository.UpdateRefreshTokenAsync(user.Id, token.RefreshToken, token.RefreshExpires);

        await eventBus.PublishAsync(UserLoginDomainEvent.CreateSuccessEvent(command.Username, command.IpAddress ?? "未知", IpAddressHelper.GetIpAddress(command.IpAddress!).IpLocation, command.OS, command.UserAgent, DateTime.Now));

        return AuthenticateResult.AuthenticateSucceedResult(token.AccessToken, token.RefreshToken, token.Expires, token.RefreshExpires, token.Username);
    }
}