﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;

public class AuthenticateResult
{
    private AuthenticateResult(bool isAuthenticated, string accessToken, string refreshToken, DateTime expires, DateTime refreshTokenExpires, string username)
        : this(isAuthenticated, null)
    {
        AccessToken = accessToken;
        RefreshToken = refreshToken;
        Expires = expires;
        RefreshTokenExpires = refreshTokenExpires;
        Username = username;
    }

    private AuthenticateResult(bool isAuthenticated, string? error)
    {
        IsAuthenticated = isAuthenticated;
        Error = error;
    }

    public string? Error { get; }

    public bool IsAuthenticated { get; }

    public string? AccessToken { get; }

    public string? RefreshToken { get; }

    public DateTime? Expires { get; }

    public DateTime? RefreshTokenExpires { get; }

    public string? Username { get; }

    public static AuthenticateResult AuthenticateSucceedResult(string accessToken, string refreshToken, DateTime expires, DateTime refreshTokenExpires, string username)
    {
        return new AuthenticateResult(true, accessToken, refreshToken, expires, refreshTokenExpires, username);
    }

    public static AuthenticateResult AuthenticateFailedResult(string error)
    {
        return new AuthenticateResult(false, error);
    }
}
