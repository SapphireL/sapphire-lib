﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;

public class RefreshTokenCommand : ICommand<RefreshTokenCommandResult>
{
    public required string RefreshToken { get; set; }

    public required string Username { get; set; }
}
