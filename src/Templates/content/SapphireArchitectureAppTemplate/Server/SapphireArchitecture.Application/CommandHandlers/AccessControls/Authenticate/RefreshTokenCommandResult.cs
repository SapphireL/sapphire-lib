﻿using System.ComponentModel.DataAnnotations;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;

public class RefreshTokenCommandResult(string accessToken, string refreshToken, DateTime expires, DateTime refreshTokenExpires)
{
    [Required]
    public string AccessToken { get; } = accessToken;

    [Required]
    public string RefreshToken { get; } = refreshToken;

    [Required]
    public DateTime Expires { get; } = expires;

    [Required]
    public DateTime RefreshTokenExpires { get; } = refreshTokenExpires;
}
