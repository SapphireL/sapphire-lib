﻿using Dapper;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;

internal class RefreshTokenHandler(SapphireArchitectureQueryClient sapphireArchitectureQueryClient, IAccessControlUserRepository userRepository, ITokenClaimsService tokenClaimsService) : CommandHandler<RefreshTokenCommand, RefreshTokenCommandResult>
{
    public override async Task<Result<RefreshTokenCommandResult>> HandleAsync(RefreshTokenCommand command, CancellationToken cancellationToken = default)
    {
        var sql = GetRefreshToken(sapphireArchitectureQueryClient.Schema);
        using var connection = sapphireArchitectureQueryClient.GetOpenConnection();
        var userId = await connection.ExecuteScalarAsync<string>(sql, new
        {
            command.Username,
            command.RefreshToken
        });

        if (!string.IsNullOrWhiteSpace(userId))
        {
            var user = await userRepository.GetAsync(command.Username);

            var defaultRole = user!.UserRoles.SingleOrDefault(u => u.IsDefault)?.Role;

            var token = tokenClaimsService.GenerateJwtToken(user.Id.ToString(), user.UserName, defaultRole!.Name, defaultRole.HasAllPermissions);

            await userRepository.UpdateRefreshTokenAsync(new Guid(userId), token.RefreshToken, token.RefreshExpires);

            return new RefreshTokenCommandResult(token.AccessToken, token.RefreshToken, token.Expires, token.RefreshExpires);
        }
        else
        {
            return Result.Failure<RefreshTokenCommandResult>(AccessControlErrors.RefreshTokenInvalid);
        }
    }

    private string GetRefreshToken(string? schema)
    {
        schema = string.IsNullOrEmpty(schema) ? string.Empty : $"{schema}.";
        return $@"
                SELECT CAST(Id AS VARCHAR(36)) AS Id FROM {schema}AccessControl_Users WHERE Username = @Username AND RefreshToken_RefreshToken = @RefreshToken AND RefreshToken_Expiry>'{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}'
                ";
    }
}
