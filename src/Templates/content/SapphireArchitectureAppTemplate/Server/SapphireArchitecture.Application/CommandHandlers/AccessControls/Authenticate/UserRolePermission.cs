﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;

internal class UserRolePermission
{
    public string UserName { get; set; } = null!;

    public Guid Id { get; set; } = default!;

    public List<string> Roles { get; set; } = null!;

    public List<string> Permissions { get; set; } = null!;
}
