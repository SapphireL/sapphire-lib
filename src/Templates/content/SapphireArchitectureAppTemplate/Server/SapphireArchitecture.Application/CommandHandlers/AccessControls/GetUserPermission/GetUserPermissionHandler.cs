﻿using Dapper;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.GetUserPermission;

internal class GetUserPermissionHandler(SapphireArchitectureQueryClient sqlConnectionFactory) : CommandHandler<UserPermissionQuery, List<UserPermissionResult>>
{
    public override async Task<Result<List<UserPermissionResult>>> HandleAsync(UserPermissionQuery command, CancellationToken cancellationToken = default)
    {
        using var connection = sqlConnectionFactory.GetOpenConnection();
        var parameters = new DynamicParameters();
        parameters.Add(nameof(UserPermissionQuery.UserId), command.UserId);

        var result = await connection.QueryAsync<UserPermissionResult>(GetUserRolesSql(sqlConnectionFactory.Schema), parameters);

        return result.AsList();
    }

    private string GetUserRolesSql(string? schema)
    {
        return $@"
                SELECT RoleClaims.ClaimValue as Name
                FROM {schema}.AccessControl_Users as Users 
                LEFT JOIN {schema}.AccessControl_UserRoles as UserRoles ON Users.Id = UserRoles.UserId
                LEFT JOIN {schema}.AccessControl_Roles as Roles ON UserRoles.RoleId = Roles.Id
                INNER JOIN {schema}.AccessControl_RoleClaims as RoleClaims ON Roles.Id = RoleClaims.RoleId AND RoleClaims.ClaimType = 'permission'
                WHERE Users.Id = @userId ";
    }
}
