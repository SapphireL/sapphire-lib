﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.GetUserPermission;

public class UserPermissionQuery : QueryBase<List<UserPermissionResult>>
{
    public Guid UserId { get; set; }
}
