﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.GetUserPermission;

public class UserPermissionResult
{
    public string Name { get; set; } = default!;
}
