﻿using Mapster;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Menu;

internal class GetMenuHandler(IAccessControlMenuRepository repository) : CommandHandler<GetMenuCommand, List<GetMenuResult>>
{
    public override async Task<Result<List<GetMenuResult>>> HandleAsync(GetMenuCommand command, CancellationToken cancellationToken = default)
    {
        return (await repository.GetMenus()).Adapt<List<GetMenuResult>>();
    }
}
