﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Menu
{
    public class GetMenuResult
    {
        public required string Id { get; set; }

        public required string Name { get; set; }

        public string? ParentId { get; set; }

        public string? Redirect { get; set; }

        public string? Component { get; set; }

        public required string Path { get; set; }

        public List<GetMenuResult>? Children { get; set; }

        public MenuMetaResult? Meta { get; set; }
    }

    public class MenuMetaResult
    {
        public required string MetaId { get; set; }

        public required string Title { get; set; }

        public string? Icon { get; set; }

        /// <summary>
        /// 菜单名称右侧的额外图标.
        /// </summary>
        public string? ExtraIcon { get; set; }

        public bool? ShowLink { get; set; }

        /// <summary>
        /// 是否显示父级菜单.
        /// </summary>
        public bool? ShowParent { get; set; }

        public int? Rank { get; set; }

        public List<string>? Auths { get; set; }

        public List<string>? Roles { get; set; }

        public bool? KeepAlive { get; set; }

        public string? FrameSrc { get; set; }

        public bool? FrameLoading { get; set; }

        public bool? HiddenTag { get; set; }

        public int? DynamicLevel { get; set; }

        public string? ActivePath { get; set; }

        [Column("Auths")]
        private string? AuthsString
        {
            get { return string.Join(",", Auths ?? Enumerable.Empty<string>()); }
            set { Auths = value?.Replace("[", string.Empty).Replace("]", string.Empty).Replace("\"", string.Empty).Split(',').ToList(); }
        }

        [Column("Roles")]
        private string? RolesString
        {
            get { return string.Join(",", Roles ?? Enumerable.Empty<string>()); }
            set { Roles = value?.Replace("[", string.Empty).Replace("]", string.Empty).Replace("\"", string.Empty).Split(',').ToList(); }
        }
    }
}
