﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles;

public class AddRoleCommand : ICommand
{
    public string Name { get; set; } = default!;

    public string Description { get; set; } = default!;

    public bool HasAllPermissions { get; set; }
}
