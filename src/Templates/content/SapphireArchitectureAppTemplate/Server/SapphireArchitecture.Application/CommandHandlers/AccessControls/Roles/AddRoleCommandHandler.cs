﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles;

internal class AddRoleCommandHandler(IAccessControlRoleRepository repository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : CommandHandler<AddRoleCommand>
{
    public override async Task<Result> HandleAsync(AddRoleCommand command, CancellationToken cancellationToken = default)
    {
        var role = new AccessControlRole(Guid.NewGuid(), command.Name)
        {
            Description = command.Description,
            HasAllPermissions = command.HasAllPermissions
        };

        await repository.AddAsync(role);
        await unitOfWork.SaveChangesAsync(cancellationToken);
        return Result.Success();
    }
}
