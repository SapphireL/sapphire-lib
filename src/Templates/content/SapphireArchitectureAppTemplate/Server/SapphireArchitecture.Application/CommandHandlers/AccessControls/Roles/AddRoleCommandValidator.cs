﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles;

internal class AddRoleCommandValidator : BaseValidator<AddRoleCommand>
{
    public override void BuildCheckers()
    {
        CheckFor(x => x.Name).NotNullOrEmpty().LengthRange(1, 20);
        CheckFor(x => x.Description).NotNullOrEmpty().LengthRange(1, 100);
    }
}
