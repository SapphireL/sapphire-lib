﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles.DeleteRole;

public class DeleteRoleCommand : ICommand
{
    public Guid Id { get; set; }
}
