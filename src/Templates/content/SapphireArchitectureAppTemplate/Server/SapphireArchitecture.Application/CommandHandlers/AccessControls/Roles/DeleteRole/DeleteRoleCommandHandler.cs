﻿using Ardalis.GuardClauses;

using SapphireArchitecture.Application.Guards;
using SapphireArchitecture.Domain.AccessControls.ErrorCodes;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles.DeleteRole;

internal class DeleteRoleCommandHandler(IAccessControlRoleRepository repository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : CommandHandler<DeleteRoleCommand>
{
    public override async Task<Result> HandleAsync(DeleteRoleCommand command, CancellationToken cancellationToken = default)
    {
        var role = await repository.FindAsync(command.Id, AccessControlRoleRepositoryIncludeOptions.Users);
        if (role == null)
        {
            return Result.Failure(Error.NotFound(RoleErrors.RoleNotFound.Code, RoleErrors.RoleNotFound.Description));
        }

        if (!Guard.Against.RoleDeletable(role))
        {
            return Result.Failure(Error.Conflict(RoleErrors.RoleHasRelatedToUser.Code, RoleErrors.RoleHasRelatedToUser.Description));
        }

        repository.Delete(role);
        await unitOfWork.SaveChangesAsync(cancellationToken);
        return Result.Success();
    }
}
