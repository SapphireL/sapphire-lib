﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles;

public record UpdateRoleClaimsCommand : ICommand
{
    public Guid RoleId { get; set; }

    public List<string> Claims { get; set; } = [];
}
