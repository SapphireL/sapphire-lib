﻿using SapphireArchitecture.Domain.AccessControls.ErrorCodes;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles;

internal class UpdateRoleClaimsCommandHandler(IAccessControlRoleRepository roleRepository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : CommandHandler<UpdateRoleClaimsCommand>
{
    public override async Task<Result> HandleAsync(UpdateRoleClaimsCommand command, CancellationToken cancellationToken = default)
    {
        var role = await roleRepository.FindWithClaimsAsync(command.RoleId);
        if (role == null)
        {
            return Result.Failure(Error.NotFound(RoleErrors.RoleNotFound.Code, RoleErrors.RoleNotFound.Description));
        }

        role.Claims.Clear();
        foreach (var claim in command.Claims)
        {
            role.Claims.Add(new AccessControlRoleClaim("permission", claim));
        }

        await unitOfWork.SaveChangesAsync(cancellationToken);
        return Result.Success();
    }
}
