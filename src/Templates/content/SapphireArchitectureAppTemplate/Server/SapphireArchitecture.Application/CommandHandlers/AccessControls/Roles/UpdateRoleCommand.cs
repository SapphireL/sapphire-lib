﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles
{
    public class UpdateRoleCommand : ICommand
    {
        public Guid RoleId { get; set; }

        public string Name { get; set; } = default!;

        public string Description { get; set; } = default!;

        public bool HasAllPermissions { get; set; }
    }
}
