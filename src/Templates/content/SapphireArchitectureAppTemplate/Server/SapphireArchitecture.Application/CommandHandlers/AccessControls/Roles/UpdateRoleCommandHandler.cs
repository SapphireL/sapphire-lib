﻿using SapphireArchitecture.Domain.AccessControls.ErrorCodes;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles
{
    public class UpdateRoleCommandHandler(IAccessControlRoleRepository repository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : CommandHandler<UpdateRoleCommand>
    {
        public override async Task<Result> HandleAsync(UpdateRoleCommand command, CancellationToken cancellationToken = default)
        {
            var role = await repository.FindAsync(command.RoleId);
            if (role is not null)
            {
                role.Name = command.Name;
                role.Description = command.Description;
                role.HasAllPermissions = command.HasAllPermissions;
                await repository.UpdateAsync(role);
                await unitOfWork.SaveChangesAsync(cancellationToken);
                return Result.Success();
            }
            else
            {
                return Result.Failure(Error.NotFound(RoleErrors.RoleNotFound.Code, RoleErrors.RoleNotFound.Description));
            }
        }
    }
}
