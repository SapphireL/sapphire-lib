﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles;

internal class UpdateRoleCommandValidator : BaseValidator<UpdateRoleCommand>
{
    public override void BuildCheckers()
    {
        CheckFor(x => x.Name).NotNullOrEmpty().LengthRange(1, 20);
        CheckFor(x => x.Description).NotNullOrEmpty().LengthRange(1, 100);
    }
}
