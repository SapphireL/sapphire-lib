﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Users;

public class UpdatePasswordCommand : ICommand
{
    public Guid UserId { get; set; } = default!;

    public string Password { get; set; } = default!;
}
