﻿using Ardalis.GuardClauses;

using SapphireArchitecture.Application.Guards;
using SapphireArchitecture.Application.QueryHandlers.Basics.Users;
using SapphireArchitecture.Domain.AccessControls.ErrorCodes;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Users
{
    internal class UpdatePasswordCommandHandler(IAccessControlUserRepository repository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : CommandHandler<UpdatePasswordCommand>
    {
        public override async Task<Result> HandleAsync(UpdatePasswordCommand command, CancellationToken cancellationToken = default)
        {
            var user = await repository.FindAsync(command.UserId);

            var guard = Guard.Against.UserAvailable(user);

            if (!guard)
            {
                return Result.Failure<GetUserQueryResult>(Error.NotFound(UserErrors.UserNotFound.Code, UserErrors.UserNotFound.Description));
            }

            user!.UpdatePassword(command.Password);

            await repository.UpdateUserAsync(user);
            await unitOfWork.SaveChangesAsync(cancellationToken);
            return Result.Success();
        }
    }
}
