﻿namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Users;

internal class UpdatePasswordCommandValidator : BaseValidator<UpdatePasswordCommand>
{
    public override void BuildCheckers()
    {
        CheckFor(x => x.Password).ValidPasswordFormat();
    }
}
