﻿using System.ComponentModel.DataAnnotations;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Users;

public record UpdateUserRolesCommand : ICommand
{
    [Required]
    public Guid UserId { get; set; }

    [Required]
    public List<Guid> RoleIds { get; set; } = [];

    [Required]
    public Guid DefaultRoleId { get; set; }
}
