﻿using SapphireArchitecture.Domain.AccessControls.ErrorCodes;

namespace SapphireArchitecture.Application.CommandHandlers.AccessControls.Users;

internal class UpdateUserRolesCommandHandler(IAccessControlUserRepository userRepository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : CommandHandler<UpdateUserRolesCommand>
{
    public override async Task<Result> HandleAsync(UpdateUserRolesCommand command, CancellationToken cancellationToken = default)
    {
        var user = await userRepository.FindWithRolesAsync(command.UserId);
        if (user == null)
        {
            return Result.Failure(Error.NotFound(UserErrors.UserNotFound.Code, UserErrors.UserNotFound.Description));
        }

        // var roles = await roleRepository.GetRolesByIdAsync(command.RoleIds);
        // 1. 找出需要删除的角色
        var rolesToRemove = user.UserRoles
            .Where(ur => !command.RoleIds.Any(r => r == ur.RoleId))
            .ToList();

        foreach (var roleToRemove in rolesToRemove)
        {
            user.UserRoles.Remove(roleToRemove);
        }

        // 2. 添加新角色或更新现有角色
        foreach (var role in command.RoleIds)
        {
            var existingUserRole = user.UserRoles.FirstOrDefault(ur => ur.RoleId == role);

            if (existingUserRole != null)
            {
                // 角色已存在，更新 DefaultRole 标识（如果需要）
                existingUserRole.IsDefault = role == command.DefaultRoleId;
            }
            else
            {
                // 角色不存在，添加新角色
                user.UserRoles.Add(new AccessControlUserRole(role == command.DefaultRoleId, command.UserId, role));
            }
        }

        await unitOfWork.SaveChangesAsync(cancellationToken);
        return Result.Success();
    }
}
