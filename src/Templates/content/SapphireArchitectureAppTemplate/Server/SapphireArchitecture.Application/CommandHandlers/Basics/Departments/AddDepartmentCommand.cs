﻿namespace SapphireArchitecture.Application.CommandHandlers.Basics.Departments;

/// <summary>
/// 添加部门.
/// </summary>
public class AddDepartmentCommand : ICommand
{
    /// <summary>
    /// 部门名称.
    /// </summary>
    public required string Name { get; set; }

    /// <summary>
    /// 部门主管.
    /// </summary>
    public string? Director { get; set; }

    /// <summary>
    /// 部门电话.
    /// </summary>
    public string? PhoneNumber { get; set; }

    /// <summary>
    /// 部门邮箱.
    /// </summary>
    public string? Email { get; set; }

    /// <summary>
    /// 排序.
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 是否启用.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// 父级部门.
    /// </summary>
    public int? ParentId { get; set; }

    /// <summary>
    /// 备注.
    /// </summary>
    public string? Remarks { get; set; }
}
