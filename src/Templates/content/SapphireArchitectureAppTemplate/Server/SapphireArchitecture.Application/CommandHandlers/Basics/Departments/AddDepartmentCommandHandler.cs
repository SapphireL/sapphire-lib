﻿namespace SapphireArchitecture.Application.CommandHandlers.Basics.Departments;

/// <summary>
/// 新增部门.
/// </summary>
/// <param name="repository"><see cref="IBasicDepartmentRepository"/>.</param>
/// <param name="unitOfWork"><see cref="IUnitOfWork"/>.</param>
public class AddDepartmentCommandHandler(IBasicDepartmentRepository repository, IUnitOfWork unitOfWork) : CommandHandler<AddDepartmentCommand>
{
    public override async Task<Result> HandleAsync(AddDepartmentCommand command, CancellationToken cancellationToken = default)
    {
        var department = new BasicDepartment(command.Name, command.Director, command.PhoneNumber, command.Email, command.Sort, command.Remarks, command.ParentId, command.Enabled);
        await repository.AddAsync(department);
        await unitOfWork.SaveChangesAsync(cancellationToken);
        return Result.Success();
    }
}
