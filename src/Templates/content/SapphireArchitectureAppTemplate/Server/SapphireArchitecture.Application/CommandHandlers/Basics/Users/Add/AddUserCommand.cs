﻿namespace SapphireArchitecture.Application.CommandHandlers.Basics.Users.Add;

/// <summary>
/// 注册新用户命令.
/// </summary>
public class AddUserCommand : CommandBase
{
    /// <summary>
    /// Initializes a new instance of the <see cref="AddUserCommand"/> class.
    /// </summary>
    /// <param name="userName">用户名.</param>
    /// <param name="password">密码.</param>
    public AddUserCommand(string userName, string password)
    {
        UserName = userName;
        Password = password;
    }

    public string UserName { get; private set; }

    public string Password { get; private set; }
}
