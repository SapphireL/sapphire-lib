﻿using SapphireArchitecture.Domain.Basics.ErrorCodes;
using SapphireArchitecture.Domain.Users;

namespace SapphireArchitecture.Application.CommandHandlers.Basics.Users.Add;

internal class AddUserCommandHandler(IBasicUserRepository userRepository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : CommandHandler<AddUserCommand>
{
    public override async Task<Result> HandleAsync(AddUserCommand command, CancellationToken cancellationToken = default)
    {
        if (await userRepository.ExistsAsync(command.UserName))
        {
            return Result.Failure(Error.Conflict(UserErrors.UsernameAlreadyExists.Code, UserErrors.UsernameAlreadyExists.Description));
        }

        var user = new BasicUser(command.UserName, command.Password);
        await userRepository.AddAsync(user);
        await unitOfWork.SaveChangesAsync();
        return Result.Success();
    }
}
