﻿namespace SapphireArchitecture.Application.CommandHandlers.Basics.Users.Add
{
    internal class AddUserCommandValidator : BaseValidator<AddUserCommand>
    {
        public override void BuildCheckers()
        {
            CheckFor(x => x.UserName).LengthRange(6, 18).ValidAccountFormat();
            CheckFor(x => x.Password).ValidPasswordFormat();
        }
    }
}
