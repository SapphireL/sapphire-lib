﻿namespace SapphireArchitecture.Application.CommandHandlers.Basics.Users.Update
{
    public class UpdateUserCommand : CommandBase
    {
        public required string Nickname { get; set; } = default!;

        public required string AvatarUrl { get; set; } = default!;

        public required string PhoneNumber { get; set; } = default!;
    }
}
