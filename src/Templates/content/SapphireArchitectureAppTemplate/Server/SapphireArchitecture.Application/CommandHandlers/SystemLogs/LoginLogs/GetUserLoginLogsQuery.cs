﻿using SapphireLib.Application.Queries;

namespace SapphireArchitecture.Application.CommandHandlers.SystemLogs.LoginLogs;

public class GetUserLoginLogsQuery : PagedQuery<PagedQueryResult<GetUserLoginLogsQueryResult>>
{
    public string? Username { get; set; }

    public bool? IsSuccess { get; set; }

    public DateTime? StartTime { get; set; }

    public DateTime? EndTime { get; set; }

    public override string? SortBy { get; set; } = "LoginAt";
}
