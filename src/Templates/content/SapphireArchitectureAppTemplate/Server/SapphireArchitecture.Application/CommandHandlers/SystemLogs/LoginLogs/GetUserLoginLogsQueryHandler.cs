﻿using Dapper;

namespace SapphireArchitecture.Application.CommandHandlers.SystemLogs.LoginLogs;

internal class GetUserLoginLogsQueryHandler(SapphireArchitectureQueryClient client) : CommandHandler<GetUserLoginLogsQuery, PagedQueryResult<GetUserLoginLogsQueryResult>>
{
    public override async Task<Result<PagedQueryResult<GetUserLoginLogsQueryResult>>> HandleAsync(GetUserLoginLogsQuery command, CancellationToken cancellationToken = default)
    {
        // var result = await repository.GetAllAsync(command.Username, command.IsSuccess, command.StartTime, command.EndTime, command.Page, command.PageSize, command.SortBy, command.IsDescending);
        using var conn = client.GetOpenConnection();

        var builder = new SqlBuilder();

        var selectTemplate = builder.AddTemplate(
            $@"SELECT [Id]
              ,[Username]
              ,[IpAddress]
              ,[Address]
              ,[OS]
              ,[UserAgent]
              ,[IsSuccess]
              ,[FailureReason]
              ,[LoginAt]
          FROM {client.Schema}.[SystemLog_UserLogins] 
            /**where**/
            /**orderby**/
          OFFSET @Offset ROWS FETCH NEXT @NEXT ROWS ONLY",
            new
            {
                Offset = command.Offset(),
                Next = command.Next()
            });

        var countTemplate = builder.AddTemplate(@$"select count(1) from {client.Schema}.[SystemLog_UserLogins] /**where**/");

        if (!string.IsNullOrWhiteSpace(command.Username))
        {
            var username = $"%{command.Username.Trim()}%";
            builder.Where("Username like @Username", new { Username = username });
        }

        if (command.IsSuccess != null)
        {
            builder.Where("IsSuccess = @IsSuccess", new { command.IsSuccess });
        }

        if (command.StartTime != null)
        {
            builder.Where("LoginAt >= @StartTime", new { command.StartTime });
        }

        if (command.EndTime != null)
        {
            builder.Where("LoginAt <= @EndTime", new { command.EndTime });
        }

        if (!string.IsNullOrWhiteSpace(command.SortBy))
        {
            var desc = command.IsDescending ? "desc" : "asc";
            builder.OrderBy($"{command.SortBy} {desc}");
        }

        var data = await conn.QueryAsync<GetUserLoginLogsQueryResult>(selectTemplate.RawSql, selectTemplate.Parameters);
        var total = conn.ExecuteScalar<int>(countTemplate.RawSql, countTemplate.Parameters);

        var result = new PagedQueryResult<GetUserLoginLogsQueryResult>
        {
            Total = total,
            Current = command.Current,
            PageSize = command.PageSize,
            Data = data.ToList()
        };

        return result;
    }
}
