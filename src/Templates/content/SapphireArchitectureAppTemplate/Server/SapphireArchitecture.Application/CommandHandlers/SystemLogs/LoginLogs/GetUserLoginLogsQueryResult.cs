﻿namespace SapphireArchitecture.Application.CommandHandlers.SystemLogs.LoginLogs;

public class GetUserLoginLogsQueryResult
{
    public required string Username { get; set; }

    public required string IpAddress { get; set; }

    public string? Address { get; set; }

    public required string OS { get; set; }

    public required string UserAgent { get; set; }

    public bool IsSuccess { get; set; }

    public string? FailureReason { get; set; }

    public DateTime LoginAt { get; set; }
}
