﻿using SapphireArchitecture.Domain.Basics.Events;

using SapphireLib.EventBus.EventBus;

namespace SapphireArchitecture.Application.EventHandlers.Basics.Users
{
    internal class BasicUserAddedDomainEventHandler(IAccessControlUserRepository accessControlUserRepository, [FromKeyedServices(AppUnitOfWorkServiceKey.Key)] IUnitOfWork unitOfWork) : BaseEventHandler<BasicUserAddedDomainEvent>
    {
        public override async Task HandleAsync(BasicUserAddedDomainEvent @event, CancellationToken cancellationToken = default)
        {
            var user = new AccessControlUser(@event.UserId, @event.UserName, @event.Password);
            await accessControlUserRepository.AddAsync(user);
            await unitOfWork.SaveChangesAsync(cancellationToken);
        }
    }
}
