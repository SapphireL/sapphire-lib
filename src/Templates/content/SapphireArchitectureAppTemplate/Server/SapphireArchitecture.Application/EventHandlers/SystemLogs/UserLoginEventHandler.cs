﻿using SapphireArchitecture.Domain.AccessControls.Events;
using SapphireArchitecture.Domain.SystemLogs;

using SapphireLib.EventBus.EventBus;

namespace SapphireArchitecture.Application.EventHandlers.SystemLogs;

public class UserLoginEventHandler(ISystemLogUserLoginRepository logUserLoginRepository) : BaseEventHandler<UserLoginDomainEvent>
{
    public override async Task HandleAsync(UserLoginDomainEvent @event, CancellationToken cancellationToken = default)
    {
        var log = @event.IsSuccess ? SystemLogUserLogin.CreateSuccessLog(@event.Username, @event.IpAddress, @event.Address, @event.OS, @event.UserAgent, @event.LoginAt) : SystemLogUserLogin.CreateFailureLog(@event.Username, @event.IpAddress, @event.Address, @event.OS, @event.UserAgent, @event.LoginAt, @event.FailureReason);

        await logUserLoginRepository.InsertAsync(log);
    }
}
