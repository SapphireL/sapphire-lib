﻿global using Mapster;

global using Microsoft.Extensions.DependencyInjection;

global using SapphireArchitecture.Application.CommandHandlers.AccessControls.Authenticate;
global using SapphireArchitecture.Application.CommandHandlers.AccessControls.Menu;
global using SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles;
global using SapphireArchitecture.Application.CommandHandlers.SystemLogs.LoginLogs;
global using SapphireArchitecture.Application.QueryHandlers.AccessControls;
global using SapphireArchitecture.Domain.AccessControls;
global using SapphireArchitecture.Domain.Basics;

global using SapphireLib.Application.Commands;
global using SapphireLib.Application.Queries;
global using SapphireLib.Authorization.Abstractions;
global using SapphireLib.Domain;
global using SapphireLib.Module.Core;
global using SapphireLib.UnitOfWork.Abstractions;
global using SapphireLib.Validator.Extensions;
global using SapphireLib.Validator.Generic;
