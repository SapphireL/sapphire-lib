﻿using Ardalis.GuardClauses;

namespace SapphireArchitecture.Application.Guards;

internal static class AccessControlGuards
{
    public static bool UserAvailable(this IGuardClause guardClause, AccessControlUser? user)
    {
        if (user == null)
        {
            return false;
        }

        return true;
    }

    public static bool RoleDeletable(this IGuardClause guardClause, AccessControlRole role)
    {
        if (role.Users.Count != 0)
        {
            return false;
        }

        return true;
    }
}
