﻿using Ardalis.GuardClauses;

using SapphireArchitecture.Domain.Users;

namespace SapphireArchitecture.Application.Guards;

internal static class BasicGuards
{
    public static bool UserAvailable(this IGuardClause guardClause, BasicUser? user)
    {
        if (user == null || user.IsDeleted)
        {
            return false;
        }

        return true;
    }
}
