﻿using SapphireArchitecture.Application.CommandHandlers.AccessControls.GetUserPermission;
using SapphireArchitecture.Application.CommandHandlers.AccessControls.Roles.DeleteRole;
using SapphireArchitecture.Application.CommandHandlers.AccessControls.Users;
using SapphireArchitecture.Application.CommandHandlers.Basics.Users.Add;
using SapphireArchitecture.Application.EventHandlers.Basics.Users;
using SapphireArchitecture.Application.EventHandlers.SystemLogs;
using SapphireArchitecture.Application.QueryHandlers.AccessControls.GetUserRoles;
using SapphireArchitecture.Application.QueryHandlers.Basics.Users;
using SapphireArchitecture.Domain.AccessControls.Events;
using SapphireArchitecture.Domain.Basics.Events;

using SapphireLib.Command.Abstractions;
using SapphireLib.Command.Extensions;
using SapphireLib.EventBus.Extensions;

namespace SapphireArchitecture.Application;

public static class InfrastructureExtensions
{
    public static void AddValidators(this BuilderContext builderContext)
    {
        builderContext.Services.AddScoped<IValidator<UpdateRoleCommand>, UpdateRoleCommandValidator>();
        builderContext.Services.AddScoped<IValidator<AddRoleCommand>, AddRoleCommandValidator>();
        builderContext.Services.AddScoped<IValidator<AddUserCommand>, AddUserCommandValidator>();
        builderContext.Services.AddScoped<IValidator<GetRolePermissionsQuery>, GetRolePermissionsQueryValidator>();
        builderContext.Services.AddScoped<IValidator<UpdatePasswordCommand>, UpdatePasswordCommandValidator>();
    }

    public static void AddHandlers(this FeaturesBuilder builder)
    {
        builder.AddCommandHandler<ICommandHandler<AuthenticateCommand, Result<AuthenticateResult>>, AuthenticateHandler>();
        builder.AddCommandHandler<ICommandHandler<GetMenuCommand, Result<List<GetMenuResult>>>, GetMenuHandler>();
        builder.AddCommandHandler<ICommandHandler<RefreshTokenCommand, Result<RefreshTokenCommandResult>>, RefreshTokenHandler>();

        builder.AddCommandHandler<ICommandHandler<GetUserLoginLogsQuery, Result<PagedQueryResult<GetUserLoginLogsQueryResult>>>, GetUserLoginLogsQueryHandler>();

        builder.AddCommandHandler<ICommandHandler<GetRolesQuery, Result<PagedQueryResult<GetRolesQueryResult>>>, GetRolesQueryHandler>();

        builder.AddCommandHandler<ICommandHandler<UpdateRoleCommand, Result>, UpdateRoleCommandHandler>();
        builder.AddCommandHandler<ICommandHandler<AddRoleCommand, Result>, AddRoleCommandHandler>();
        builder.AddCommandHandler<ICommandHandler<AddUserCommand, Result>, AddUserCommandHandler>();

        builder.AddCommandHandler<ICommandHandler<UpdateUserRolesCommand, Result>, UpdateUserRolesCommandHandler>();
        builder.AddCommandHandler<ICommandHandler<UserPermissionQuery, Result<List<UserPermissionResult>>>, GetUserPermissionHandler>();

        builder.AddCommandHandler<ICommandHandler<UpdateRoleClaimsCommand, Result>, UpdateRoleClaimsCommandHandler>();
        builder.AddCommandHandler<ICommandHandler<GetRolePermissionsQuery, Result<GetRolePermissionsQueryResult>>, GetRolePermissionsQueryHandler>();

        builder.AddCommandHandler<ICommandHandler<GetUserQuery, Result<GetUserQueryResult>>, GetUserQueryHandler>();
        builder.AddCommandHandler<ICommandHandler<UpdatePasswordCommand, Result>, UpdatePasswordCommandHandler>();
        builder.AddCommandHandler<ICommandHandler<GetUserAccessQuery, Result<GetUserAccessQueryResult>>, GetUserAccessQueryHandler>();
        builder.AddCommandHandler<ICommandHandler<DeleteRoleCommand, Result>, DeleteRoleCommandHandler>();
        builder.AddCommandHandler<ICommandHandler<GetUsersQuery, Result<PagedQueryResult<GetUsersQueryResult>>>, GetUsersQueryHandler>();

        builder.AddCommandHandler<ICommandHandler<GetUserRolesQuery, Result<List<GetUserRolesQueryResult>>>, GetUserRolesQueryHandler>();
    }

    public static void AddEventHandlers(this FeaturesBuilder builder)
    {
        builder.AddEventHandler<UserLoginEventHandler, UserLoginDomainEvent>();
        builder.AddEventHandler<BasicUserAddedDomainEventHandler, BasicUserAddedDomainEvent>();
    }
}
