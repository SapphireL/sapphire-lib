﻿namespace SapphireArchitecture.Application.Permissions;

public class AccessControlPermissions
{
    public const string CreateRole = Prefix + "CreateRole";
    public const string DeleteRole = Prefix + "DeleteRole";
    public const string GetRole = Prefix + "GetRole";
    public const string GetRoles = Prefix + "GetRoles";
    public const string UpdateRole = Prefix + "UpdateRole";

    private const string Prefix = "AccessControl_";
}
