﻿namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

public class GetRolePermissionsQuery(string roleName) : QueryBase<GetRolePermissionsQueryResult>
{
    public string RoleName { get; } = roleName;
}
