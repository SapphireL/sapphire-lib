﻿using Dapper;

using SapphireArchitecture.Domain.AccessControls.ErrorCodes;

namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

internal class GetRolePermissionsQueryHandler(SapphireArchitectureQueryClient client) : CommandHandler<GetRolePermissionsQuery, GetRolePermissionsQueryResult>
{
    public override async Task<Result<GetRolePermissionsQueryResult>> HandleAsync(GetRolePermissionsQuery command, CancellationToken cancellationToken = default)
    {
        using var conn = client.GetOpenConnection();

        var sql = HasAllPermissionsSql(client.Schema);
        var role = await conn.QueryFirstOrDefaultAsync<AccessControlRole>(sql, new
        {
            command.RoleName
        });

        if (role == null)
        {
            return Result.Failure<GetRolePermissionsQueryResult>(Error.NotFound(RoleErrors.RoleNotFound.Code, RoleErrors.RoleNotFound.Description));
        }

        if (role.HasAllPermissions == true)
        {
            return new GetRolePermissionsQueryResult
            {
                HasAllPermissions = true
            };
        }

        var builder = new SqlBuilder();
        var selectTemplate = builder.AddTemplate(
         $@"SELECT [ClaimValue]
          FROM {client.Schema}.[AccessControl_Roles] INNER JOIN {client.Schema}.[AccessControl_RoleClaims]
          ON [AccessControl_Roles].[Id] = [AccessControl_RoleClaims].[RoleId]
            /**where**/");

        builder.Where($"{client.Schema}.[AccessControl_Roles].[Name] = @RoleName", new { command.RoleName });

        var data = await conn.QueryAsync<string>(selectTemplate.RawSql, selectTemplate.Parameters);
        return new GetRolePermissionsQueryResult
        {
            HasAllPermissions = false,
            Permissions = data.ToArray()
        };
    }

    private static string HasAllPermissionsSql(string schema)
    {
        return @$"SELECT [HasAllPermissions] FROM {schema}.[AccessControl_Roles] WHERE [NAME] = @RoleName";
    }
}
