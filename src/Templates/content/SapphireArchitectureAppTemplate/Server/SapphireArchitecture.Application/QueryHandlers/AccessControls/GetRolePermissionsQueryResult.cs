﻿namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

public class GetRolePermissionsQueryResult
{
    public string[] Permissions { get; set; } = [];

    public bool HasAllPermissions { get; set; } = false;
}
