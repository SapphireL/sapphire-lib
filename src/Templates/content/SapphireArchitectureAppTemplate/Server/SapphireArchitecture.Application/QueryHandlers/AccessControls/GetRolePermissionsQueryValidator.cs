﻿namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

internal class GetRolePermissionsQueryValidator : BaseValidator<GetRolePermissionsQuery>
{
    public override void BuildCheckers()
    {
        CheckFor(x => x.RoleName).NotNullOrEmpty();
    }
}
