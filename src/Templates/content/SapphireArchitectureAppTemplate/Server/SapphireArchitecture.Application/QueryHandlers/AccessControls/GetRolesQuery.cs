﻿namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

public class GetRolesQuery : PagedQuery<PagedQueryResult<GetRolesQueryResult>>, IKeySearchQuery
{
    public override string? SortBy { get; set; } = "Name";

    public override bool IsDescending { get; set; } = false;

    public string? Keyword { get; set; }
}
