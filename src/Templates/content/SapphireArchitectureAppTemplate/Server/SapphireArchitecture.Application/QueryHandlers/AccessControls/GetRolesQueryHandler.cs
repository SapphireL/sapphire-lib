﻿using Dapper;

namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

internal class GetRolesQueryHandler(SapphireArchitectureQueryClient client) : CommandHandler<GetRolesQuery, PagedQueryResult<GetRolesQueryResult>>
{
    public override async Task<Result<PagedQueryResult<GetRolesQueryResult>>> HandleAsync(GetRolesQuery command, CancellationToken cancellationToken = default)
    {
        using var conn = client.GetOpenConnection();
        var builder = new SqlBuilder();

        var selectTemplate = builder.AddTemplate(
           $@"SELECT [Id]
              ,[Name]
              ,[Description]
              ,[HasAllPermissions]
              ,[CreationTime]
              ,[NormalizedName]
          FROM {client.Schema}.[AccessControl_Roles] 
            /**where**/
            /**orderby**/
          OFFSET @Offset ROWS FETCH NEXT @NEXT ROWS ONLY",
           new
           {
               Offset = command.Offset(),
               Next = command.Next()
           });

        var countTemplate = builder.AddTemplate(@$"select count(1) from {client.Schema}.[AccessControl_Roles] /**where**/");

        if (!string.IsNullOrWhiteSpace(command.Keyword))
        {
            var searchKey = $"%{command.Keyword.Trim()}%";
            builder.OrWhere("Name like @SearchKey", new { SearchKey = searchKey });
            builder.OrWhere("Description like @SearchKey", new { SearchKey = searchKey });
        }

        if (!string.IsNullOrWhiteSpace(command.SortBy))
        {
            var desc = command.IsDescending ? "desc" : "asc";
            builder.OrderBy($"{command.SortBy} {desc}");
        }

        var data = await conn.QueryAsync<GetRolesQueryResult>(selectTemplate.RawSql, selectTemplate.Parameters);
        var total = conn.ExecuteScalar<int>(countTemplate.RawSql, countTemplate.Parameters);

        var result = new PagedQueryResult<GetRolesQueryResult>
        {
            Total = total,
            Current = command.Current,
            PageSize = command.PageSize,
            Data = data.ToList()
        };

        return result;
    }
}
