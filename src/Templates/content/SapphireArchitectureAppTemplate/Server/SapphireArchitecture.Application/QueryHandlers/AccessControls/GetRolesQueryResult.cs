﻿namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

public class GetRolesQueryResult
{
    public required Guid Id { get; set; }

    public required string Name { get; set; }

    public required string Description { get; set; }

    public required bool HasAllPermissions { get; set; }

    public DateTime CreationTime { get; set; }
}
