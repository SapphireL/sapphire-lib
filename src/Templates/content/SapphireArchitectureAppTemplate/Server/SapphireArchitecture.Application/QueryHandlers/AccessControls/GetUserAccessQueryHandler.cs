﻿using Dapper;

using SapphireArchitecture.Domain.AccessControls.ErrorCodes;

using SapphireLib.Authorization;

namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

internal class GetUserAccessQueryHandler(SapphireArchitectureQueryClient client, IUserManager userManager) : CommandHandler<GetUserAccessQuery, GetUserAccessQueryResult>
{
    public override async Task<Result<GetUserAccessQueryResult>> HandleAsync(GetUserAccessQuery command, CancellationToken cancellationToken = default)
    {
        using var conn = client.GetOpenConnection();

        var sql = HasAllPermissionsSql(client.Schema);
        var role = await conn.QueryFirstOrDefaultAsync<AccessControlRole>(sql, new
        {
            userManager.UserId
        });

        if (role == null)
        {
            return Result.Failure<GetUserAccessQueryResult>(Error.NotFound(RoleErrors.RoleNotFound.Code, RoleErrors.RoleNotFound.Description));
        }

        if (role.HasAllPermissions == true)
        {
            return new GetUserAccessQueryResult
            {
                HasAllPermissions = true,
                CurrentRole = role.Name
            };
        }

        var builder = new SqlBuilder();
        var selectTemplate = builder.AddTemplate(
         $@"SELECT [ClaimValue]
          FROM {client.Schema}.[AccessControl_Roles] LEFT JOIN {client.Schema}.[AccessControl_RoleClaims]
          ON [AccessControl_Roles].[Id] = [AccessControl_RoleClaims].[RoleId]
            /**where**/");

        builder.Where($"{client.Schema}.[AccessControl_Roles].[Id] = @Id", new { role.Id });

        var data = await conn.QueryAsync<string>(selectTemplate.RawSql, selectTemplate.Parameters);
        return new GetUserAccessQueryResult
        {
            HasAllPermissions = false,
            Permissions = data.ToArray(),
            CurrentRole = role.Name
        };
    }

    private static string HasAllPermissionsSql(string schema)
    {
        return @$"SELECT [Role].[Id], [Role].[Name],
  [HasAllPermissions] 
  FROM {schema}.[AccessControl_UserRoles] AS [User]  LEFT JOIN  
  {schema}.[AccessControl_Roles] AS [Role] ON [User].[RoleId] = [Role].[Id]
  WHERE [User].[UserId] = @UserId AND [User].IsDefault = 1;";
    }
}
