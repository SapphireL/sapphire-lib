﻿namespace SapphireArchitecture.Application.QueryHandlers.AccessControls;

public class GetUserAccessQueryResult
{
    public string CurrentRole { get; set; } = default!;

    public bool HasAllPermissions { get; set; } = false;

    public string[] Permissions { get; set; } = [];
}
