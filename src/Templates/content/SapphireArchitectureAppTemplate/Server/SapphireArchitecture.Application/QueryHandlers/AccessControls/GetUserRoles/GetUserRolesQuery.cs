﻿using System.ComponentModel.DataAnnotations;

namespace SapphireArchitecture.Application.QueryHandlers.AccessControls.GetUserRoles
{
    public class GetUserRolesQuery : QueryBase<List<GetUserRolesQueryResult>>
    {
        [Required]
        public Guid UserId { get; set; }
    }
}
