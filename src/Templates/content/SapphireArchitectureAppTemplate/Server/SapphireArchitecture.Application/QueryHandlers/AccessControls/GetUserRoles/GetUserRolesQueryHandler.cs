﻿using Dapper;

namespace SapphireArchitecture.Application.QueryHandlers.AccessControls.GetUserRoles;

internal class GetUserRolesQueryHandler(SapphireArchitectureQueryClient client) : CommandHandler<GetUserRolesQuery, List<GetUserRolesQueryResult>>
{
    public override async Task<Result<List<GetUserRolesQueryResult>>> HandleAsync(GetUserRolesQuery command, CancellationToken cancellationToken = default)
    {
        var sql = @"SELECT [Role].[Id],[Role].[Name],[Role].[Description],[Role].[HasAllPermissions]
  FROM [SapphireArchitecture].[SapphireArchitecture].[AccessControl_Users]
  AS [Users] 
	INNER JOIN [SapphireArchitecture].[AccessControl_UserRoles] AS [UserRoles]
		ON [Users].[Id] = [UserRoles].[UserId]
	INNER JOIN [SapphireArchitecture].[AccessControl_Roles] AS [Role] 
		ON [UserRoles].[RoleId] = [Role].[Id]
	WHERE [Users].[Id] = @UserId
	ORDER BY [Role].[CreationTime] DESC";

        using var conn = client.GetOpenConnection();
        var result = await conn.QueryAsync<GetUserRolesQueryResult>(sql, new { command.UserId });
        return result.ToList();
    }
}
