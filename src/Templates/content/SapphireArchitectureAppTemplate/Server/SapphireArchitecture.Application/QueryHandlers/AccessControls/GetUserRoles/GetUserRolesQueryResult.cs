﻿namespace SapphireArchitecture.Application.QueryHandlers.AccessControls.GetUserRoles;

public class GetUserRolesQueryResult
{
    public required Guid Id { get; set; }

    public required string Name { get; set; }

    public required string Description { get; set; }

    public required bool HasAllPermissions { get; set; }
}
