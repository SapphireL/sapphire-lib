﻿namespace SapphireArchitecture.Application.QueryHandlers.Basics.Departments;

public class GetDepartmentsQuery : PagedQuery<PagedQueryResult<GetDepartmentsQueryResult>>, IKeySearchQuery
{
    /// <summary>
    /// 是否启用.
    /// </summary>
    public bool? Enabled { get; set; }

    /// <summary>
    /// 关键字.
    /// </summary>
    public string? Keyword { get; set; }
}
