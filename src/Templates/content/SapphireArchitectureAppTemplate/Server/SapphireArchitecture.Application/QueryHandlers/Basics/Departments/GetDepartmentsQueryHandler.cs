﻿using Dapper;

namespace SapphireArchitecture.Application.QueryHandlers.Basics.Departments;

internal class GetDepartmentsQueryHandler(SapphireArchitectureQueryClient client) : CommandHandler<GetDepartmentsQuery, PagedQueryResult<GetDepartmentsQueryResult>>
{
    public override async Task<Result<PagedQueryResult<GetDepartmentsQueryResult>>> HandleAsync(GetDepartmentsQuery command, CancellationToken cancellationToken = default)
    {
        using var conn = client.GetOpenConnection();
        var builder = new SqlBuilder();

        var selectTemplate = builder.AddTemplate(
           $@"SELECT [Id]
              ,[Director]
              ,[PhoneNumber]
              ,[Email]
              ,[Sort]
              ,[Remarks]
              ,[Enabled]
              ,[Name]
              ,[ParentId]
              ,[IsDeleted]
              ,[CreationTime]
          FROM {client.Schema}.[Basic_Departments]
            /**where**/
            /**orderby**/
          OFFSET @Offset ROWS FETCH NEXT @NEXT ROWS ONLY",
           new
           {
               Offset = command.Offset(),
               Next = command.Next()
           });

        var countTemplate = builder.AddTemplate(@$"select count(1) from {client.Schema}.[Basic_Users] /**where**/");

        if (!string.IsNullOrWhiteSpace(command.Keyword))
        {
            var searchKey = $"%{command.Keyword.Trim()}%";
            builder.OrWhere("[UserName] like @SearchKey", new { SearchKey = searchKey });
            builder.OrWhere("[PhoneNumber] like @SearchKey", new { SearchKey = searchKey });
            builder.OrWhere("[Nickname] like @SearchKey", new { SearchKey = searchKey });
        }

        if (!string.IsNullOrWhiteSpace(command.SortBy))
        {
            var desc = command.IsDescending ? "desc" : "asc";
            builder.OrderBy($"{command.SortBy} {desc}");
        }

        var data = await conn.QueryAsync<GetDepartmentsQueryResult>(selectTemplate.RawSql, selectTemplate.Parameters);
        var total = conn.ExecuteScalar<int>(countTemplate.RawSql, countTemplate.Parameters);

        var result = new PagedQueryResult<GetDepartmentsQueryResult>
        {
            Total = total,
            Current = command.Current,
            PageSize = command.PageSize,
            Data = data.ToList()
        };

        return result;
    }
}
