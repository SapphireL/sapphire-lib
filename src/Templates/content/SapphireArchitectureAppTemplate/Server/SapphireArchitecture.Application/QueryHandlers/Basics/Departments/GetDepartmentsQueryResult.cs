﻿namespace SapphireArchitecture.Application.QueryHandlers.Basics.Departments;

/// <summary>
/// 部门查询结果.
/// </summary>
public class GetDepartmentsQueryResult
{
    /// <summary>
    /// 部门ID.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 排序.
    /// </summary>
    public int Sort { get; set; }

    /// <summary>
    /// 是否启用.
    /// </summary>
    public bool Enabled { get; set; }

    /// <summary>
    /// 创建时间.
    /// </summary>
    public DateTime CreationTime { get; set; }

    /// <summary>
    /// 备注.
    /// </summary>
    public string? Remarks { get; set; }
}
