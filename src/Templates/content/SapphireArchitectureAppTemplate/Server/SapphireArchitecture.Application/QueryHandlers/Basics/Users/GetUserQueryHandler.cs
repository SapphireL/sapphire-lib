﻿using Ardalis.GuardClauses;

using Dapper;

using SapphireArchitecture.Application.Guards;
using SapphireArchitecture.Domain.AccessControls.ErrorCodes;
using SapphireArchitecture.Domain.Users;

using SapphireLib.Authorization;

namespace SapphireArchitecture.Application.QueryHandlers.Basics.Users;

internal class GetUserQueryHandler(SapphireArchitectureQueryClient client, IUserManager userManager) : CommandHandler<GetUserQuery, GetUserQueryResult>
{
    public override async Task<Result<GetUserQueryResult>> HandleAsync(GetUserQuery command, CancellationToken cancellationToken = default)
    {
        var userId = userManager.UserId;
        using var conn = client.GetOpenConnection();
        var sql = $@"SELECT [Id]
      ,[UserName]
      ,[Nickname]
      ,[AvatarUrl]
      ,[PhoneNumber]
      ,[IsDeleted]
      ,[CreationTime]
        FROM {client.Schema}.[Basic_Users] WHERE [Id] = @Id";

        var user = await conn.QueryFirstOrDefaultAsync<BasicUser>(sql, new
        {
            Id = userId
        });

        var guard = Guard.Against.UserAvailable(user);

        if (!guard)
        {
            return Result.Failure<GetUserQueryResult>(Error.NotFound(UserErrors.UserNotFound.Code, UserErrors.UserNotFound.Description));
        }

        return new GetUserQueryResult(user!.Id, user!.Username, user!.Nickname, user.CreationTime, user.AvatarUrl, user.PhoneNumber);
    }
}
