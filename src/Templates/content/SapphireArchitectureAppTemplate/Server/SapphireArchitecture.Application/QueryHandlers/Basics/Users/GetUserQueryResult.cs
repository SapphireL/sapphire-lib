﻿using System.ComponentModel.DataAnnotations;

namespace SapphireArchitecture.Application.QueryHandlers.Basics.Users;

public class GetUserQueryResult(Guid id, string userName, string? nickname, DateTime creationTime, string? avatarUrl = null, string? phoneNumber = null)
{
    [Required]
    public Guid Id { get; set; } = id;

    [Required]
    public string Username { get; set; } = userName;

    public string? Nickname { get; set; } = nickname;

    public DateTime CreationTime { get; set; } = creationTime;

    public string? AvatarUrl { get; set; } = avatarUrl;

    public string? PhoneNumber { get; set; } = phoneNumber;
}
