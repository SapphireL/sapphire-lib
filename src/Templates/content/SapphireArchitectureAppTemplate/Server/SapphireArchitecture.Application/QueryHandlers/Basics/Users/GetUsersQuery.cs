﻿namespace SapphireArchitecture.Application.QueryHandlers.Basics.Users
{
    public class GetUsersQuery : PagedQuery<PagedQueryResult<GetUsersQueryResult>>, IKeySearchQuery
    {
        public override string? SortBy { get; set; } = "CreationTime";

        public override bool IsDescending { get; set; } = false;

        public string? Keyword { get; set; }
    }
}
