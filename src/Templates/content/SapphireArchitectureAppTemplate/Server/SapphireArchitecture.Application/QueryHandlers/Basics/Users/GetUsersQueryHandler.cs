﻿using Dapper;

namespace SapphireArchitecture.Application.QueryHandlers.Basics.Users
{
    internal class GetUsersQueryHandler(SapphireArchitectureQueryClient client) : CommandHandler<GetUsersQuery, PagedQueryResult<GetUsersQueryResult>>
    {
        public override async Task<Result<PagedQueryResult<GetUsersQueryResult>>> HandleAsync(GetUsersQuery command, CancellationToken cancellationToken = default)
        {
            using var conn = client.GetOpenConnection();
            var builder = new SqlBuilder();

            var selectTemplate = builder.AddTemplate(
           $@"SELECT [Id]
                ,[UserName]
                ,[IsDeleted]
                ,[CreationTime]
                ,[AvatarUrl]
                ,[PhoneNumber]
                ,[Nickname]
          FROM {client.Schema}.[Basic_Users]
            /**where**/
            /**orderby**/
          OFFSET @Offset ROWS FETCH NEXT @NEXT ROWS ONLY",
           new
           {
               Offset = command.Offset(),
               Next = command.Next()
           });

            var countTemplate = builder.AddTemplate(@$"select count(1) from {client.Schema}.[Basic_Users] /**where**/");

            if (!string.IsNullOrWhiteSpace(command.Keyword))
            {
                var searchKey = $"%{command.Keyword.Trim()}%";
                builder.OrWhere("[UserName] like @SearchKey", new { SearchKey = searchKey });
                builder.OrWhere("[PhoneNumber] like @SearchKey", new { SearchKey = searchKey });
                builder.OrWhere("[Nickname] like @SearchKey", new { SearchKey = searchKey });
            }

            if (!string.IsNullOrWhiteSpace(command.SortBy))
            {
                var desc = command.IsDescending ? "desc" : "asc";
                builder.OrderBy($"{command.SortBy} {desc}");
            }

            var data = await conn.QueryAsync<GetUsersQueryResult>(selectTemplate.RawSql, selectTemplate.Parameters);
            var total = conn.ExecuteScalar<int>(countTemplate.RawSql, countTemplate.Parameters);

            var result = new PagedQueryResult<GetUsersQueryResult>
            {
                Total = total,
                Current = command.Current,
                PageSize = command.PageSize,
                Data = data.ToList()
            };

            return result;
        }
    }
}
