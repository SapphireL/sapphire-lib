﻿namespace SapphireArchitecture.Application.QueryHandlers.Basics.Users
{
    public class GetUsersQueryResult
    {
        public required Guid Id { get; set; }

        public required string Username { get; set; }

        public string? AvatarUrl { get; set; }

        public string? PhoneNumber { get; set; }

        public string? Nickname { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
