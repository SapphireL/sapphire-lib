﻿using Microsoft.Extensions.Options;

using SapphireLib.ADO.SqlServer;
using SapphireLib.ADO.SqlServer.Definitions;

namespace SapphireArchitecture.Application;

public class SapphireArchitectureQueryClient(IOptionsSnapshot<SqlServerOptions> options) : SqlServerConnectionClient(options)
{
}
