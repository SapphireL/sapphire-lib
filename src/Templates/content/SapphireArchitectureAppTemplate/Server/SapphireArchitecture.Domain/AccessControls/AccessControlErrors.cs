﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls
{
    public static class AccessControlErrors
    {
        public static readonly Error RefreshTokenInvalid = Error.Problem("AC.InvalidRefreshToken", "Then refresh token is invalid.");
    }
}
