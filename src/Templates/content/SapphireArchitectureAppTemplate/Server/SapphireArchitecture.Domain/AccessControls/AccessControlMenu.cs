﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls
{
    public class AccessControlMenu : EntityId<Guid>
    {
        public AccessControlMenu(string path, string name)
        : this()
        {
            Path = path;
            Name = name;
        }

        public AccessControlMenu(string path, string name, Guid parentId)
        : this(path, name)
        {
            ParentId = parentId;
        }

        protected AccessControlMenu()
        {
            Id = Guid.NewGuid();
        }

        public string Path { get; set; } = default!;

        public string Name { get; set; } = default!;

        public string? Redirect { get; set; }

        public string? Component { get; set; }

        public virtual List<AccessControlMenu>? Children { get; set; }

        public AccessControlMenuMeta? Meta { get; set; }

        public Guid? ParentId { get; set; }
    }
}
