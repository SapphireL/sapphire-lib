﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls
{
    public class AccessControlMenuMeta : EntityId<Guid>
    {
        public AccessControlMenuMeta(string title, Guid menuId)
            : this()
        {
            Title = title;
            MenuId = menuId;
        }

        public AccessControlMenuMeta(string title, Guid menuId, List<string> roles)
           : this(title, menuId)
        {
            Roles = roles;
        }

        protected AccessControlMenuMeta()
        {
            Id = Guid.NewGuid();
        }

        public string Title { get; set; } = default!;

        public string? Icon { get; set; }

        /// <summary>
        /// 菜单名称右侧的额外图标.
        /// </summary>
        public string? ExtraIcon { get; set; }

        public bool ShowLink { get; set; } = true;

        /// <summary>
        /// 是否显示父级菜单.
        /// </summary>
        public bool ShowParent { get; set; } = false;

        public int? Rank { get; set; }

        public List<string> Auths { get; set; } = [];

        public List<string> Roles { get; set; } = [];

        public bool KeepAlive { get; set; } = false;

        public string? FrameSrc { get; set; }

        public bool FrameLoading { get; set; } = false;

        public AccessControlMenuMetaTransition? Transition { get; set; }

        public bool HiddenTag { get; set; }

        public int? DynamicLevel { get; set; }

        public string? ActivePath { get; set; }

        public Guid MenuId { get; set; }
    }
}