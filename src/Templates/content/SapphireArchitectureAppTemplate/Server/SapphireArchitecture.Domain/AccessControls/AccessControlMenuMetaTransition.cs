﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls
{
    public class AccessControlMenuMetaTransition : EntityId<Guid>
    {
        public AccessControlMenuMetaTransition(string enterTransition, string leaveTransition, Guid metaId)
           : this(metaId)
        {
            EnterTransition = enterTransition;
            LeaveTransition = leaveTransition;
        }

        public AccessControlMenuMetaTransition(string name, Guid metaId)
            : this(metaId)
        {
            Name = name;
        }

        protected AccessControlMenuMetaTransition()
        {
            Id = Guid.NewGuid();
        }

        protected AccessControlMenuMetaTransition(Guid metaId)
            : this()
        {
            MetaId = metaId;
        }

        public string? Name { get; set; }

        public string? EnterTransition { get; set; }

        public string? LeaveTransition { get; set; }

        public Guid MetaId { get; set; }
    }
}
