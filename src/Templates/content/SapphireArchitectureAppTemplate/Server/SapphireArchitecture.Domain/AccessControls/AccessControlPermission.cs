﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls;

public class AccessControlPermission : EntityId<Guid>
{
    public AccessControlPermission(int menuType, string title, string permissionName)
        : this()
    {
        MenuType = menuType;
        Title = title;
        PermissionName = permissionName;
    }

    protected AccessControlPermission()
    {
    }

    public ICollection<AccessControlPermission> Children { get; } = [];

    public Guid? ParentId { get; set; }

    public int MenuType { get; private set; }

    public string Title { get; private set; } = default!;

    public string PermissionName { get; private set; } = default!;
}
