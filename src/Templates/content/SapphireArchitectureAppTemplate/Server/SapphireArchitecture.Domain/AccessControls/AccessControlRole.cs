﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls;

public class AccessControlRole : Entity, IAggregateRoot, ISoftDeletion, ICreationTime
{
    public AccessControlRole(Guid id, string name)
        : this()
    {
        Id = id;
        Name = name;
        NormalizedName = name.ToUpper();
    }

    public AccessControlRole(string name)
       : this(Guid.NewGuid(), name)
    {
    }

    protected AccessControlRole()
    {
    }

    public Guid Id { get; private set; }

    public string Name { get; set; } = default!;

    public string? Description { get; set; }

    public bool HasAllPermissions { get; set; } = false;

    public string NormalizedName { get; set; } = default!;

    public List<AccessControlUser> Users { get; } = [];

    public List<AccessControlUserRole> UserRoles { get; } = [];

    public ICollection<AccessControlRoleClaim> Claims { get; } = [];

    public bool IsDeleted { get; set; } = false!;

    public DateTime CreationTime { get; set; } = DateTime.Now;
}
