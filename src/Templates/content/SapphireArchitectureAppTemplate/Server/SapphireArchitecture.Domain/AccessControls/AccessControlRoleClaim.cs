﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls;

public class AccessControlRoleClaim : Entity
{
    public Guid Id { get; private set; }

    public string ClaimType { get; set; } = default!;

    public string ClaimValue { get; set; } = default!;

    public Guid RoleId { get; set; }

    public AccessControlRoleClaim(string claimType, string claimValue)
    {
        ClaimType = claimType;
        ClaimValue = claimValue;
    }

    protected AccessControlRoleClaim()
    {
    }
}
