﻿namespace SapphireArchitecture.Domain.AccessControls;

[Flags]
public enum AccessControlRoleRepositoryIncludeOptions
{
    /// <summary>
    /// 不包含任何关联.
    /// </summary>
    None = 0,

    /// <summary>
    /// 包含角色的权限关联.
    /// </summary>
    Permissions = 1,

    /// <summary>
    /// 包含角色的关联用户.
    /// </summary>
    Users = 2
}
