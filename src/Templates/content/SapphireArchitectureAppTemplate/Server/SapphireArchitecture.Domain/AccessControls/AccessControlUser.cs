﻿using SapphireLib.Domain;
using SapphireLib.Domain.AdminUser.Identity;
using SapphireLib.Tools;

namespace SapphireArchitecture.Domain.AccessControls;

public class AccessControlUser : AdminUserBase<Guid>, IAggregateRoot
{
    /// <summary>
    /// Initializes a new instance of the <see cref="AccessControlUser"/> class.
    /// </summary>
    /// <param name="id">用户id.</param>
    /// <param name="userName">用户名.</param>
    /// <param name="password">密码.</param>
    /// <remarks>
    /// 实例化用户会根据密码创建随机加盐后的MD5密码.
    /// </remarks>
    public AccessControlUser(Guid id, string userName, string password)
        : this()
    {
        Id = id;
        UserName = userName;
        Salt = RandomStringGenerator.GenerateRandomString(5);
        PasswordHash = MD5Generator.GenerateDoubleSaltedMD5(password, Salt);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AccessControlUser"/> class.
    /// </summary>
    protected AccessControlUser()
    {
    }

    /// <summary>
    /// 被加密存储的密码.加密方式是加盐和哈希.
    /// </summary>
    public string PasswordHash { get; set; } = null!;

    public string Salt { get; set; } = null!;

    public List<AccessControlRole> Roles { get; } = [];

    public List<AccessControlUserRole> UserRoles { get; } = [];

    public AccessControlUserRefreshToken RefreshToken { get; set; } = default!;

    public bool VerifyPassword(string password)
    {
        return MD5Generator.GenerateDoubleSaltedMD5(password, Salt) == PasswordHash;
    }

    public void UpdatePassword(string password)
    {
        Salt = RandomStringGenerator.GenerateRandomString(5);
        PasswordHash = MD5Generator.GenerateDoubleSaltedMD5(password, Salt);
    }
}