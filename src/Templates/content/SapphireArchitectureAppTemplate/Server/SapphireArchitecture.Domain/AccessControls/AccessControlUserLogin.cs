﻿namespace SapphireArchitecture.Domain.AccessControls;

public class AccessControlUserLogin
{
    /// <summary>
    /// 登录的方式（例如，微信、支付宝登录）.
    /// </summary>
    public string LoginProvider { get; set; } = default!;

    /// <summary>
    /// 登录方式的唯一主键，例如微信的openid.
    /// </summary>
    public string ProviderKey { get; set; } = default!;

    /// <summary>
    /// 登录方式的名称.
    /// </summary>
    public string? ProviderDisplayName { get; set; }

    /// <summary>
    /// 用户id.
    /// </summary>
    public Guid UserId { get; set; } = default!;

    public AccessControlUserLogin(Guid userId, string loginProvider, string providerKey, string providerDisplayName)
    {
        UserId = userId;
        LoginProvider = loginProvider;
        ProviderKey = providerKey;
        ProviderDisplayName = providerDisplayName;
    }

    protected AccessControlUserLogin()
    {
    }
}
