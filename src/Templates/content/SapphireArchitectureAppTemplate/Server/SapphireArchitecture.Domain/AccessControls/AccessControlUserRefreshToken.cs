﻿namespace SapphireArchitecture.Domain.AccessControls
{
    public class AccessControlUserRefreshToken
    {
        public string? RefreshToken { get; set; }

        public DateTime? Expiry { get; set; }
    }
}
