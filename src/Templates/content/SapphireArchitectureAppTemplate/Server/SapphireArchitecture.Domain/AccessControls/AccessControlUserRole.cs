﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls;

public class AccessControlUserRole : Entity
{
    public bool IsDefault { get; set; } = false;

    public Guid UserId { get; set; }

    public Guid RoleId { get; set; }

    public AccessControlRole Role { get; set; } = default!;

    public AccessControlUser User { get; set; } = default!;

    public AccessControlUserRole(bool isDefault, Guid userId, Guid roleId)
    {
        IsDefault = isDefault;
        UserId = userId;
        RoleId = roleId;
    }

    protected AccessControlUserRole()
    {
    }
}
