﻿namespace SapphireArchitecture.Domain.AccessControls.Dto;

public class UserRolePermission
{
    public Guid Id { get; set; } = default!;

    public List<string> Roles { get; set; } = [];

    public List<string> Permissions { get; set; } = [];
}
