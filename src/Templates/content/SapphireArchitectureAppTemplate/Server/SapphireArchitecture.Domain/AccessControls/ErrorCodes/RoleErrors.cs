﻿namespace SapphireArchitecture.Domain.AccessControls.ErrorCodes;

public static class RoleErrors
{
    public static (string Code, string Description) RoleNotFound => new($"{ErrorCodesPrefix.AccessControl.Role}_1", "角色未找到");

    public static (string Code, string Description) RoleHasRelatedToUser => new($"{ErrorCodesPrefix.AccessControl.Role}_2", "角色已经关联用户");
}
