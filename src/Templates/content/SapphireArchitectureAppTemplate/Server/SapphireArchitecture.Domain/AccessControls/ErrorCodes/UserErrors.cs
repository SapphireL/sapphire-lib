﻿namespace SapphireArchitecture.Domain.AccessControls.ErrorCodes;

public static class UserErrors
{
    public static (string Code, string Description) UserNotFound => new($"{ErrorCodesPrefix.AccessControl.User}_1", "用户未找到");
}
