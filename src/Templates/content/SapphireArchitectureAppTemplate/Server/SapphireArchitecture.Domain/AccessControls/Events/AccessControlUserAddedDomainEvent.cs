﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.AccessControls.Events;

public class AccessControlUserAddedDomainEvent : DomainEventBase
{
    public AccessControlUserAddedDomainEvent(Guid userId, string userName, string password)
    {
        UserId = userId;
        UserName = userName;
        Password = password;
    }

    public Guid UserId { get; }

    public string UserName { get; }

    public string Password { get; }
}
