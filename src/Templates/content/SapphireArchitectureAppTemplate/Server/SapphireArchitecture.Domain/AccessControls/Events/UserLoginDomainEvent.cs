﻿using SapphireLib.Domain;
using SapphireLib.EventBus.Attributes;

namespace SapphireArchitecture.Domain.AccessControls.Events;

[OutboxEvent]
public class UserLoginDomainEvent(string username, string ipAddress, string address, string os, string userAgent, bool isSuccess, DateTime loginAt, string? failureReason = null) : DomainEventBase
{
    public string Username { get; set; } = username;

    public string IpAddress { get; set; } = ipAddress;

    public string? Address { get; set; } = address;

    public string OS { get; set; } = os;

    public string UserAgent { get; set; } = userAgent;

    public bool IsSuccess { get; set; } = isSuccess;

    public string? FailureReason { get; set; } = failureReason;

    public DateTime LoginAt { get; set; } = loginAt;

    public static UserLoginDomainEvent CreateSuccessEvent(string username, string ipAddress, string address, string os, string userAgent, DateTime loginAt)
    {
        return new UserLoginDomainEvent(username, ipAddress, address, os, userAgent, true, loginAt);
    }

    public static UserLoginDomainEvent CreateFailureEvent(string username, string ipAddress, string address, string os, string userAgent, DateTime loginAt, string failureReason)
    {
        return new UserLoginDomainEvent(username, ipAddress, address, os, userAgent, false, loginAt, failureReason);
    }
}
