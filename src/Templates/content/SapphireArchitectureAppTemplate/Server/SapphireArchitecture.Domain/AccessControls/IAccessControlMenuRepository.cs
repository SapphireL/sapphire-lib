﻿namespace SapphireArchitecture.Domain.AccessControls
{
    public interface IAccessControlMenuRepository
    {
        Task<List<AccessControlMenu>> GetMenus();
    }
}
