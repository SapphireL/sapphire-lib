﻿namespace SapphireArchitecture.Domain.AccessControls;

public interface IAccessControlRoleRepository
{
    Task<AccessControlRole?> FindAsync(Guid id);

    Task<AccessControlRole?> FindAsync(Guid id, AccessControlRoleRepositoryIncludeOptions options = AccessControlRoleRepositoryIncludeOptions.None);

    Task<AccessControlRole?> FindWithClaimsAsync(Guid id);

    Task AddAsync(AccessControlRole aCRole);

    Task UpdateAsync(AccessControlRole aCRole);

    Task<List<AccessControlRole>> GetRolesByIdAsync(List<Guid> ids);

    void Delete(AccessControlRole aCRole);
}
