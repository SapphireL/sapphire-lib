﻿using SapphireArchitecture.Domain.AccessControls.Dto;

namespace SapphireArchitecture.Domain.AccessControls;

public interface IAccessControlUserRepository
{
    Task AddAsync(AccessControlUser user);

    Task<AccessControlUser?> GetAsync(string userName);

    Task<UserRolePermission> GetUserRolePermissionsAsync(Guid userId);

    Task UpdateRefreshTokenAsync(Guid userId, string refreshToken, DateTime expiry);

    Task UpdateUserAsync(AccessControlUser user);

    Task<AccessControlUser?> FindAsync(Guid id);

    Task<AccessControlUser?> FindWithRolesAsync(Guid id);
}
