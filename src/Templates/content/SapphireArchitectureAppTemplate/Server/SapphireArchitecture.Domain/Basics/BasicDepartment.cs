﻿using SapphireArchitecture.Domain.Basics.Events;

namespace SapphireArchitecture.Domain.Basics
{
    public class BasicDepartment(string name) : EntityId<int>, IAggregateRoot, ISoftDeletion, ICreationTime
    {
        public BasicDepartment(string name, string? director, string? phoneNumber, string? email, int sort, string? remarks, int? parentId, bool enabled)
            : this(name)
        {
            Director = director;
            PhoneNumber = phoneNumber;
            Email = email;
            Sort = sort;
            Remarks = remarks;
            Enabled = enabled;
            ParentId = parentId;
            _isDeleted = false;
            AddDomainEvent(new BasicDepartmentAddedDomainEvent(name, director, phoneNumber, email, sort, remarks, enabled));
        }

        public void Update(string name, string? director, string? phoneNumber, string? email, int sort, string? remarks, int? parentId, bool enabled)
        {
            Name = name;
            Director = director;
            PhoneNumber = phoneNumber;
            Email = email;
            Sort = sort;
            Remarks = remarks;
            Enabled = enabled;
            ParentId = parentId;
            AddDomainEvent(new BasicDepartmentUpdatedDomainEvent(Id, name, director, phoneNumber, email, sort, remarks, enabled));
        }

        public string? Director { get; private set; }

        public string? PhoneNumber { get; private set; }

        public string? Email { get; private set; }

        public int Sort { get; private set; }

        public string? Remarks { get; private set; }

        public bool Enabled { get; private set; }

        public string Name { get; private set; } = name;

        public virtual List<BasicDepartment>? Children { get; private set; }

        public int? ParentId { get; private set; }

        private bool _isDeleted;

        public void Delete()
        {
            _isDeleted = true;
            AddDomainEvent(new BasicDepartmentDeletedDomainEvent(Id, Name));
        }

        public DateTime CreationTime { get; set; } = DateTime.Now;

        public bool IsDeleted => _isDeleted;
    }
}
