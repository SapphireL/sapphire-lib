﻿using SapphireLib.Domain.Attributes;

namespace SapphireArchitecture.Domain.Users;

[SendEntityAddedEvent]
[AuditableEventPersistent]
public class BasicUser : EntityId<Guid>, IAggregateRoot, ISoftDeletion, ICreationTime
{
    public BasicUser(string username, string password)
        : this()
    {
        Username = username;
        AddDomainEvent(new BasicUserAddedDomainEvent(Id, username, password));
    }

    protected BasicUser()
    {
        Id = Guid.NewGuid();
    }

    public string Username { get; set; } = default!;

    public string? AvatarUrl { get; set; }

    public string? PhoneNumber { get; set; }

    public string? Nickname { get; set; }

    public bool IsDeleted { get; set; } = false!;

    public DateTime CreationTime { get; set; } = DateTime.Now;
}
