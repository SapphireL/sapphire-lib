﻿namespace SapphireArchitecture.Domain.Basics.ErrorCodes;

public static class UserErrors
{
    public static (string Code, string Description) UsernameAlreadyExists => new($"{ErrorCodesPrefix.Basic.User}_1", "用户名已存在");

    public static (string Code, string Description) UserNotFound => new($"{ErrorCodesPrefix.Basic.User}_2", "用户未找到");
}
