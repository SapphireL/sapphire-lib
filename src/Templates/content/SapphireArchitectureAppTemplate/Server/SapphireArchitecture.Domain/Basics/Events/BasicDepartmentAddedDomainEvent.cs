﻿namespace SapphireArchitecture.Domain.Basics.Events;

[OutboxEvent]
public class BasicDepartmentAddedDomainEvent(string name, string? director, string? phoneNumber, string? email, int sort, string? remarks, bool enabled) : DomainEventBase
{
    public string Name { get; } = name;

    public string? Director { get; } = director;

    public string? PhoneNumber { get; } = phoneNumber;

    public string? Email { get; } = email;

    public int Sort { get; } = sort;

    public string? Remarks { get; } = remarks;

    public bool Enabled { get; } = enabled;
}
