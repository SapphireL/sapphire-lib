﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.Basics.Events;

public class BasicDepartmentDeletedDomainEvent(int id, string name) : DomainEventBase
{
    public string Name { get; } = name;

    public int DepartmentId { get; } = id;
}
