﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.Basics.Events;

public class BasicDepartmentUpdatedDomainEvent(int id, string name, string? director, string? phoneNumber, string? email, int sort, string? remarks, bool enabled) : DomainEventBase
{
    public int DepartmentId { get; } = id;

    public string Name { get; } = name;

    public string? Director { get; } = director;

    public string? PhoneNumber { get; } = phoneNumber;

    public string? Email { get; } = email;

    public int Sort { get; } = sort;

    public string? Remarks { get; } = remarks;

    public bool Enabled { get; } = enabled;
}