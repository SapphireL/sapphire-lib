﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.Basics.Events;

public class BasicUserAddedDomainEvent(Guid userId, string userName, string password) : DomainEventBase
{
    public Guid UserId { get; } = userId;

    public string UserName { get; } = userName;

    public string Password { get; } = password;
}
