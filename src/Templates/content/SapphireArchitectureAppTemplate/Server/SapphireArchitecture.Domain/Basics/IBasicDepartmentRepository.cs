﻿namespace SapphireArchitecture.Domain.Basics
{
    public interface IBasicDepartmentRepository
    {
        Task<BasicDepartment?> GetAsync(int id);

        Task UpdateAsync(BasicDepartment department);

        Task AddAsync(BasicDepartment department);

        Task<List<BasicDepartment>> GetDepartmentsAsync(CancellationToken cancellationToken);
    }
}
