﻿namespace SapphireArchitecture.Domain.Users;

public interface IBasicUserRepository
{
    public Task AddAsync(BasicUser user);

    public Task<bool> ExistsAsync(string username);
}
