﻿namespace SapphireArchitecture.Domain;

public class ErrorCodesPrefix
{
    public class Basic
    {
        public const string User = $"{Prefix}_001";
        private const string Prefix = "100";
    }

    public class AccessControl
    {
        public const string User = $"{Prefix}_001";

        public const string Role = $"{Prefix}_002";
        private const string Prefix = "200";
    }
}
