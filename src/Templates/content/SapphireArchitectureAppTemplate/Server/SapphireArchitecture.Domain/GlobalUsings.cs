﻿global using SapphireArchitecture.Domain.Basics.Events;

global using SapphireLib.Domain;
global using SapphireLib.Domain.Entities;
global using SapphireLib.Domain.Events;
global using SapphireLib.EventBus.Attributes;
