﻿namespace SapphireArchitecture.Domain.HRM.HRMOrganizations;

public record HRMAddress(string Country, string City, string Street, string Detail, string? PostalCode)
{
}
