﻿namespace SapphireArchitecture.Domain.HRM.HRMOrganizations
{
    public sealed class HRMEmployee : EntityId<int>, ICreationTime, ISoftDeletion
    {
        /// <summary>
        /// 员工编号.
        /// </summary>
        public string Code { get; private set; } = default!;

        /// <summary>
        /// 员工姓名.
        /// </summary>
        public string Name { get; private set; } = default!;

        /// <summary>
        /// 用户Id.
        /// </summary>
        public Guid UserId { get; private set; } = default!;

        /// <summary>
        /// 员工手机号.
        /// </summary>
        public string PhoneNumber { get; private set; } = default!;

        /// <summary>
        /// 员工地址.
        /// </summary>
        public HRMAddress? Address { get; private set; }

        public bool IsDeleted => throw new NotImplementedException();

        public DateTime CreationTime => throw new NotImplementedException();
    }
}
