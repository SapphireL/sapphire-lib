﻿namespace SapphireArchitecture.Domain.HRM.HRMOrganizations;

/// <summary>
/// HRM组织.
/// </summary>
public sealed class HRMOrganization(string name, string code, HRMOrganization? parent = null) : EntityId<int>, IAggregateRoot, ISoftDeletion, ICreationTime, IAuditableEntity
{
    public bool IsDeleted { get; set; } = false;

    public DateTime CreationTime { get; set; } = DateTime.Now;

    /// <summary>
    /// 组织名称.
    /// </summary>
    public string Name { get; private set; } = name;

    /// <summary>
    /// 组织编码.
    /// </summary>
    public string Code { get; private set; } = code;

    /// <summary>
    /// 组织类型.
    /// </summary>
    public HRMOrganizationType Type { get; private set; } = HRMOrganizationType.Company;

    private readonly List<HRMOrganization> _children = [];

    /// <summary>
    /// 子组织.
    /// </summary>
    public IReadOnlyCollection<HRMOrganization> Children => _children;

    /// <summary>
    /// 父组织.
    /// </summary>
    public HRMOrganization? Parent { get; private set; } = parent;

    public int? ParentId { get; private set; }

    /// <summary>
    /// 主管员工.
    /// </summary>
    public HRMEmployee? Director { get; private set; }
}
