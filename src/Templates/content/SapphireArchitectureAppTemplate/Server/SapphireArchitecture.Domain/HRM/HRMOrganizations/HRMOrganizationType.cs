﻿namespace SapphireArchitecture.Domain.HRM.HRMOrganizations;

/// <summary>
/// 组织类型.
/// </summary>
public enum HRMOrganizationType
{
    /// <summary>
    /// 公司.
    /// </summary>
    Company = 0,

    /// <summary>
    /// 部门.
    /// </summary>
    Department = 1,

    /// <summary>
    /// 组织.
    /// </summary>
    Organization = 2
}
