﻿namespace SapphireArchitecture.Domain.SystemLogs
{
    public interface ISystemLogOperationRepository
    {
        public Task InsertAsync(SystemLogOperationLog logUserLogin);
    }
}
