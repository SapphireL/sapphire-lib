﻿namespace SapphireArchitecture.Domain.SystemLogs
{
    public interface ISystemLogUserLoginRepository
    {
        public Task InsertAsync(SystemLogUserLogin logUserLogin);

        public Task<List<SystemLogUserLogin>> GetAllAsync(string? username, bool? isSuccess, DateTime? startTime, DateTime? endTime, int pageNumber, int pageSize, string sortBy, bool isDescending);
    }
}
