﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.SystemLogs
{
    public class SystemLogOperationLog : EntityId<int>
    {
        public SystemLogOperationLog(string @operator, string operatorId, string belongTo, string summary, string ipAddress, string address, string os, string userAgent, bool isSuccess, DateTime operationAt, string remarks)
        {
            Operator = @operator;
            OperatorId = operatorId;
            BelongTo = belongTo;
            Summary = summary;
            IpAddress = ipAddress;
            Address = address;
            Os = os;
            UserAgent = userAgent;
            IsSuccess = isSuccess;
            OperationAt = operationAt;
            Remarks = remarks;
        }

        public string Operator { get; private set; }

        public string OperatorId { get; private set; }

        public string BelongTo { get; private set; }

        public string Summary { get; private set; }

        public string IpAddress { get; private set; }

        public string Address { get; private set; }

        public string Os { get; private set; }

        public string UserAgent { get; private set; }

        public bool IsSuccess { get; private set; }

        public DateTime OperationAt { get; private set; }

        public string Remarks { get; private set; }
    }
}
