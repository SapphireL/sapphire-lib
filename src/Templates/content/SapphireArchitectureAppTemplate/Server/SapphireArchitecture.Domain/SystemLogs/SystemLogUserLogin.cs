﻿using SapphireLib.Domain;

namespace SapphireArchitecture.Domain.SystemLogs
{
    public class SystemLogUserLogin : EntityId<int>
    {
        public required string Username { get; set; }

        public required string IpAddress { get; set; }

        public string? Address { get; set; }

        public required string OS { get; set; }

        public required string UserAgent { get; set; }

        public bool IsSuccess { get; set; }

        public string? FailureReason { get; set; }

        public DateTime LoginAt { get; set; }

        public static SystemLogUserLogin CreateSuccessLog(string username, string ipAddress, string? address, string os, string userAgent, DateTime loginAt)
        {
            return new SystemLogUserLogin
            {
                Username = username,
                IpAddress = ipAddress,
                Address = address,
                OS = os,
                UserAgent = userAgent,
                IsSuccess = true,
                LoginAt = loginAt
            };
        }

        public static SystemLogUserLogin CreateFailureLog(string username, string ipAddress, string? address, string os, string userAgent, DateTime loginAt, string? failureReason)
        {
            return new SystemLogUserLogin
            {
                Username = username,
                IpAddress = ipAddress,
                Address = address,
                OS = os,
                UserAgent = userAgent,
                IsSuccess = false,
                FailureReason = failureReason,
                LoginAt = loginAt
            };
        }
    }
}
