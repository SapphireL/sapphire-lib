﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using SapphireArchitecture.Application;
using SapphireArchitecture.Repository.Database;
using SapphireArchitecture.Repository.Interceptors;

using SapphireLib.ADO.Extensions;
using SapphireLib.ADO.SqlServer.Extensions;
using SapphireLib.Module.BusinessModule.Extensions;
using SapphireLib.Module.Core;
using SapphireLib.Repository.EFCore.Extensions;
using SapphireLib.UnitOfWork.EFCore.Extensions;
using SapphireLib.UnitOfWork.Extensions;

namespace SapphireArchitecture.Infrastructure
{
    public class SapphireArchitectureModule : BaseModule
    {
        protected override bool CheckModuleHealthy(RunningContext context)
        {
            return true;
        }

        protected override void ConfigureServices(BuilderContext context)
        {
            context.AddValidators();
        }

        protected override void OnConfiguring(FeaturesBuilder builder)
        {
            builder.AddEFCore<SapphireArchitectureDbContext>();
            builder.AddEFCore1<SapphireArchitectureDbContext, SapphireArchitectureModule>();

            builder.AddUnitOfWorkFeature(
                config =>
            {
                config.AddInterceptor<DomainEventInterceptor, SapphireArchitectureDbContext>();
                config.UseEFCore<SapphireArchitectureDbContext>();
            },
                options =>
            {
                options.DbContextServiceKey = AppUnitOfWorkServiceKey.Key;
            });

            builder.AddHandlers();

            builder.AddEventHandlers();

            builder.AddADOClient(configure =>
            {
                configure.UseSqlServer<SapphireArchitectureQueryClient>();
            });

            builder.AddRepositories(configure =>
            {
                configure.RepositoryAssemblyToScan = Repository.AssemblyReference.Assembly;
            });
        }

        protected override void OnRunning(RunningContext context)
        {
            using var scope = context.WebApplication.Services.CreateScope();
            using var dbContext = scope.ServiceProvider.GetRequiredService<SapphireArchitectureDbContext>();

            dbContext.Database.Migrate();
        }
    }
}
