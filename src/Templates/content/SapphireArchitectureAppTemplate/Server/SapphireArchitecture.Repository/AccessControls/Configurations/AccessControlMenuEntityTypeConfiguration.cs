﻿namespace SapphireArchitecture.Repository.AccessControls.Configurations;

internal class AccessControlMenuEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlMenu>
{
    public void Configure(EntityTypeBuilder<AccessControlMenu> builder)
    {
        builder.ToTable($"{AccessControlTablePrefix.Prefix}Menus", Schema);

        builder.HasMany<AccessControlMenu>().WithOne().HasForeignKey(ur => ur.ParentId).IsRequired();
        builder.HasMany(x => x.Children).WithOne().HasForeignKey(x => x.ParentId).IsRequired(false);
        builder.HasOne(x => x.Meta).WithOne().HasForeignKey<AccessControlMenuMeta>(m => m.MenuId).IsRequired();
        builder.HasKey(x => x.Id);
        builder.Ignore(b => b.DomainEvents);

        var menus = AccessControlSeedData.GetAccessControlMenuSeedData();
        var children = AccessControlSeedData.GetAccessControlMenuChildrenSeedData();
        menus.AddRange(children);
        builder.HasData(menus);
    }
}
