﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireArchitecture.Domain.AccessControls;
using SapphireArchitecture.Repository.Database;

using SapphireLib.Repository.EFCore;

namespace SapphireArchitecture.Repository.AccessControls.Configurations
{
    internal class AccessControlMenuMetaEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlMenuMeta>
    {
        public void Configure(EntityTypeBuilder<AccessControlMenuMeta> builder)
        {
            builder.ToTable($"{AccessControlTablePrefix.Prefix}MenuMetas", Schema);
            builder.HasOne(x => x.Transition).WithOne().HasForeignKey<AccessControlMenuMetaTransition>(m => m.MetaId).IsRequired();

            builder.HasKey(x => x.Id);
            builder.Ignore(b => b.DomainEvents);

            var list = AccessControlSeedData.GetAccessControlMenuMetaSeedData();
            list.AddRange(AccessControlSeedData.GetAccessControlMenuChildMetaSeedData());
            builder.HasData(list);
        }
    }
}
