﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireArchitecture.Domain.AccessControls;

using SapphireLib.Repository.EFCore;

namespace SapphireArchitecture.Repository.AccessControls.Configurations
{
    internal class AccessControlMenuMetaTransitionEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlMenuMetaTransition>
    {
        public void Configure(EntityTypeBuilder<AccessControlMenuMetaTransition> builder)
        {
            builder.ToTable($"{AccessControlTablePrefix.Prefix}MenuChildMetaTransitions", Schema);

            builder.HasKey(x => x.Id);
            builder.Ignore(b => b.DomainEvents);
        }
    }
}
