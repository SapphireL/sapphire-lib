﻿namespace SapphireArchitecture.Repository.AccessControls.Configurations;

internal class AccessControlPermissionEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlPermission>
{
    public void Configure(EntityTypeBuilder<AccessControlPermission> builder)
    {
        builder.ToTable($"{AccessControlTablePrefix.Prefix}Permissions", Schema);

        builder.HasMany<AccessControlMenu>().WithOne().HasForeignKey(ur => ur.ParentId).IsRequired();
        builder.HasMany(x => x.Children).WithOne().HasForeignKey(x => x.ParentId).IsRequired(false);
        builder.HasKey(x => x.Id);
        builder.Ignore(b => b.DomainEvents);
    }
}
