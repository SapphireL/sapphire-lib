﻿namespace SapphireArchitecture.Repository.AccessControls.Configurations;

internal class AccessControlRoleClaimEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlRoleClaim>
{
    public void Configure(EntityTypeBuilder<AccessControlRoleClaim> builder)
    {
        builder.HasKey(rc => rc.Id);
        builder.ToTable($"{AccessControlTablePrefix.Prefix}RoleClaims", Schema);
        builder.Ignore(b => b.DomainEvents);
    }
}
