﻿namespace SapphireArchitecture.Repository.AccessControls.Configurations;

internal class AccessControlRoleEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlRole>
{
    public void Configure(EntityTypeBuilder<AccessControlRole> builder)
    {
        builder.ToTable($"{AccessControlTablePrefix.Prefix}Roles", Schema);
        builder.HasKey(x => x.Id);
        builder.HasIndex(r => r.NormalizedName).IsUnique();

        builder.Property(u => u.Name).HasMaxLength(256);
        builder.Property(u => u.NormalizedName).HasMaxLength(256);

        builder.HasMany(r => r.Claims).WithOne().HasForeignKey(x => x.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);

        builder.Ignore(b => b.DomainEvents);

        builder.HasData(AccessControlSeedData.GetRoleSeedData());
    }
}
