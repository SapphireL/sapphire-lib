﻿namespace SapphireArchitecture.Repository.AccessControls.Configurations
{
    internal class AccessControlTablePrefix
    {
        internal const string Prefix = "AccessControl_";
    }
}
