﻿namespace SapphireArchitecture.Repository.AccessControls.Configurations;

internal class AccessControlUserEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlUser>
{
    public void Configure(EntityTypeBuilder<AccessControlUser> builder)
    {
        builder.ToTable($"{AccessControlTablePrefix.Prefix}Users", Schema);

        // 配置多对多。更多配置查看https://learn.microsoft.com/zh-cn/ef/core/modeling/relationships/many-to-many
        builder.HasMany(x => x.Roles).WithMany(x => x.Users).UsingEntity<AccessControlUserRole>(
            l => l.HasOne(e => e.Role).WithMany(e => e.UserRoles).HasForeignKey(e => e.RoleId),
            r => r.HasOne(e => e.User).WithMany(e => e.UserRoles).HasForeignKey(e => e.UserId));

        builder.HasMany<AccessControlUserLogin>().WithOne().HasForeignKey(ur => ur.UserId).IsRequired();

        builder.ComplexProperty(u => u.RefreshToken);

        builder.HasKey(x => x.Id);
        builder.Ignore(b => b.DomainEvents);

        builder.HasData(AccessControlSeedData.GetUserSeedData());
    }
}
