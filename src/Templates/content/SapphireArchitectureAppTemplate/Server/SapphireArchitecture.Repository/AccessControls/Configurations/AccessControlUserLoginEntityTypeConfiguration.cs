﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireArchitecture.Domain.AccessControls;

using SapphireLib.Repository.EFCore;

namespace SapphireArchitecture.Repository.AccessControls.Configurations;

internal class AccessControlUserLoginEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlUserLogin>
{
    public void Configure(EntityTypeBuilder<AccessControlUserLogin> builder)
    {
        builder.ToTable($"{AccessControlTablePrefix.Prefix}UserLogins", Schema);
        builder.HasKey(l => new { l.LoginProvider, l.ProviderKey });
    }
}
