﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SapphireArchitecture.Domain.AccessControls;
using SapphireArchitecture.Repository.Database;

using SapphireLib.Repository.EFCore;

namespace SapphireArchitecture.Repository.AccessControls.Configurations;

internal class AccessControlUserRoleEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<AccessControlUserRole>
{
    public void Configure(EntityTypeBuilder<AccessControlUserRole> builder)
    {
        builder.ToTable($"{AccessControlTablePrefix.Prefix}UserRoles", Schema);
        builder.Property(ur => ur.IsDefault).HasDefaultValue(false);

        builder.HasData(AccessControlSeedData.GetAccessControlUserRoleSeedData());
    }
}
