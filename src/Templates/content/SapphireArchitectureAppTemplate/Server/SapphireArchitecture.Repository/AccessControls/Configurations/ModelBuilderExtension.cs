﻿using Microsoft.EntityFrameworkCore;

namespace SapphireArchitecture.Repository.AccessControls.Configurations
{
    internal static class ModelBuilderExtension
    {
        internal static void ApplyAccessControlEntityTypeConfiguration(this ModelBuilder modelBuilder, string? schema)
        {
            modelBuilder.ApplyConfiguration(new AccessControlUserEntityTypeConfiguration(schema));
            modelBuilder.ApplyConfiguration(new AccessControlUserLoginEntityTypeConfiguration(schema));
            modelBuilder.ApplyConfiguration(new AccessControlRoleEntityTypeConfiguration(schema));
            modelBuilder.ApplyConfiguration(new AccessControlRoleClaimEntityTypeConfiguration(schema));
            modelBuilder.ApplyConfiguration(new AccessControlUserRoleEntityTypeConfiguration(schema));

            modelBuilder.ApplyConfiguration(new AccessControlMenuEntityTypeConfiguration(schema));
            modelBuilder.ApplyConfiguration(new AccessControlMenuMetaEntityTypeConfiguration(schema));
            modelBuilder.ApplyConfiguration(new AccessControlMenuMetaTransitionEntityTypeConfiguration(schema));
        }
    }
}
