﻿using Microsoft.EntityFrameworkCore;

using SapphireArchitecture.Domain.AccessControls;
using SapphireArchitecture.Repository.Database;

using SapphireLib.Repository;

namespace SapphireArchitecture.Repository.AccessControls.Repositories
{
    internal class MenuRepository(SapphireArchitectureDbContext context) : IAccessControlMenuRepository, IRepository
    {
        public async Task<List<AccessControlMenu>> GetMenus()
        {
            var menus = await context.AccessControlMenus.Include(x => x.Meta).ToListAsync();

            return menus.Where(e => !e.ParentId.HasValue).ToList();
        }
    }
}
