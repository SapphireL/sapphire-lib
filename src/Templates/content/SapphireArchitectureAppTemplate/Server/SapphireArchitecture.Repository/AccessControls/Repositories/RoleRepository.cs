﻿namespace SapphireArchitecture.Repository.AccessControls.Repositories;

public class RoleRepository(SapphireArchitectureDbContext context) : IAccessControlRoleRepository, IRepository
{
    public async Task AddAsync(AccessControlRole aCRole)
    {
        await context.AddAsync(aCRole);
    }

    public void Delete(AccessControlRole aCRole)
    {
        context.AccessControlRoles.Remove(aCRole);
    }

    public async Task<AccessControlRole?> FindAsync(Guid id)
    {
        return await context.AccessControlRoles.FindAsync(id);
    }

    public async Task<AccessControlRole?> FindAsync(Guid id, AccessControlRoleRepositoryIncludeOptions options = AccessControlRoleRepositoryIncludeOptions.None)
    {
        var query = context.AccessControlRoles.AsQueryable();

        // 根据需要的导航属性来动态构建查询
        if (options.HasFlag(AccessControlRoleRepositoryIncludeOptions.Permissions))
        {
            query = query.Include(r => r.Claims);
        }

        if (options.HasFlag(AccessControlRoleRepositoryIncludeOptions.Users))
        {
            query = query.Include(r => r.Users);
        }

        return await query.FirstOrDefaultAsync(r => r.Id == id);
    }

    public async Task<AccessControlRole?> FindWithClaimsAsync(Guid id)
    {
        var role = await context.AccessControlRoles.Include(r => r.Claims).FirstOrDefaultAsync(r => r.Id == id);
        return role;
    }

    public async Task<List<AccessControlRole>> GetRolesByIdAsync(List<Guid> ids)
    {
        return await context.AccessControlRoles
        .Where(r => ids.Contains(r.Id)).ToListAsync();
    }

    public async Task UpdateAsync(AccessControlRole aCRole)
    {
        context.AccessControlRoles.Update(aCRole);
        await Task.CompletedTask;
    }
}
