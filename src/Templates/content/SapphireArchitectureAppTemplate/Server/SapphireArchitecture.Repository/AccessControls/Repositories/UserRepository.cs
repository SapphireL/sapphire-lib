﻿using System.Data;

using Dapper;

using SapphireArchitecture.Domain.AccessControls.Dto;

namespace SapphireArchitecture.Repository.AccessControls.Repositories;

public class UserRepository(SapphireArchitectureDbContext context) : IAccessControlUserRepository, IRepository
{
    private readonly SapphireArchitectureDbContext _context = context;

    public async Task AddAsync(AccessControlUser user)
    {
        user.RefreshToken = new AccessControlUserRefreshToken();
        await _context.AddAsync(user);
        await _context.SaveChangesAsync();
    }

    public async Task<AccessControlUser?> FindAsync(Guid id)
    {
        return await _context.AccessControlUsers.FindAsync(id);
    }

    public async Task<AccessControlUser?> FindWithRolesAsync(Guid id)
    {
        return await _context.AccessControlUsers.Include(u => u.UserRoles).FirstOrDefaultAsync(u => u.Id == id);
    }

    public async Task<AccessControlUser?> GetAsync(string username)
    {
        return await _context.AccessControlUsers.Include(user => user.UserRoles).ThenInclude(ur => ur.Role).SingleOrDefaultAsync(x => x.UserName == username);
    }

    public async Task<UserRolePermission> GetUserRolePermissionsAsync(Guid userId)
    {
        var dbConnection = _context.Database.GetDbConnection();
        if (dbConnection.State == ConnectionState.Closed)
        {
            await dbConnection.OpenAsync();
        }

        var userIdString = userId.ToString().ToUpper();

        var lookup = new Dictionary<Guid, UserRolePermission>();
        var schema = _context.AccessControlUsers.EntityType.GetSchema();
        var user = (await dbConnection.QueryAsync<UserRolePermission, string, string, UserRolePermission>(
               GetUserRolesSql(schema),
               (userRole, role, permission) =>
               {
                   if (!lookup.TryGetValue(userRole.Id, out UserRolePermission? found))
                   {
                       lookup.Add(userRole.Id, found = userRole);
                   }

                   if (!found.Roles.Contains(role))
                   {
                       found.Roles.Add(role);
                   }

                   if (!string.IsNullOrEmpty(permission))
                   {
                       found.Permissions.Add(permission);
                   }

                   return found;
               },
               new { userIdString },
               splitOn: "RoleName,Permission")).Distinct().FirstOrDefault();

        if (user != null)
        {
            return user;
        }

        throw new InvalidOperationException("user cannot be null");
    }

    public async Task UpdateRefreshTokenAsync(Guid userId, string refreshToken, DateTime expiry)
    {
        var user = await _context.AccessControlUsers.FindAsync(userId);
        if (user != null)
        {
            if (user.RefreshToken != null)
            {
                user.RefreshToken.RefreshToken = refreshToken;
                user.RefreshToken.Expiry = expiry;
            }
            else
            {
                user.RefreshToken = new AccessControlUserRefreshToken { Expiry = expiry, RefreshToken = refreshToken };
            }

            _context.AccessControlUsers.Update(user);
            await _context.SaveChangesAsync();
        }
    }

    public async Task UpdateUserAsync(AccessControlUser user)
    {
        _context.AccessControlUsers.Update(user);
        await Task.CompletedTask;
    }

    private string GetUserRolesSql(string? schema)
    {
        schema = string.IsNullOrEmpty(schema) ? string.Empty : $"{schema}.";
        return $@"
                SELECT Users.Id as Id, Roles.Name as RoleName, RoleClaims.ClaimValue as Permission
                FROM {schema}{AccessControlTablePrefix.Prefix}Users as Users 
                LEFT JOIN {schema}{AccessControlTablePrefix.Prefix}UserRoles as UserRoles ON Users.Id = UserRoles.UserId
                LEFT JOIN {schema}{AccessControlTablePrefix.Prefix}Roles as Roles ON UserRoles.RoleId = Roles.Id
                LEFT JOIN {schema}{AccessControlTablePrefix.Prefix}RoleClaims as RoleClaims ON Roles.Id = RoleClaims.RoleId AND RoleClaims.ClaimType = 'permission'
                WHERE Users.Id = @userIdString ";
    }
}
