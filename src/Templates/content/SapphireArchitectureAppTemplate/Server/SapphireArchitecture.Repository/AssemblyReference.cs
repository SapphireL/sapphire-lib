﻿using System.Reflection;

namespace SapphireArchitecture.Repository
{
    public static class AssemblyReference
    {
#pragma warning disable SA1401 // Fields should be private
        public static Assembly Assembly = typeof(AssemblyReference).Assembly;
#pragma warning restore SA1401 // Fields should be private
    }
}
