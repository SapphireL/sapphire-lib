﻿namespace SapphireArchitecture.Repository.Basics.Configurations;

internal class BasicDepartmentEntityTypeConfiguration(string? schema)
    : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<BasicDepartment>
{
    public void Configure(EntityTypeBuilder<BasicDepartment> builder)
    {
        builder.ToTable($"{BasicTablePrefix.Prefix}Departments", Schema);
        builder.HasKey(u => u.Id);
        builder.HasMany(x => x.Children).WithOne().HasForeignKey(x => x.ParentId).IsRequired(false);
        builder.Property(u => u.IsDeleted).HasDefaultValue(false);
        builder.Property(u => u.CreationTime).HasDefaultValue(DateTime.Now);
        builder.Ignore(u => u.DomainEvents);
    }
}
