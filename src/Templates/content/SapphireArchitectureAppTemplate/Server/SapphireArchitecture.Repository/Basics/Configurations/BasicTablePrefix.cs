﻿namespace SapphireArchitecture.Repository.Basics.Configurations;

internal class BasicTablePrefix
{
    internal const string Prefix = "Basic_";
}
