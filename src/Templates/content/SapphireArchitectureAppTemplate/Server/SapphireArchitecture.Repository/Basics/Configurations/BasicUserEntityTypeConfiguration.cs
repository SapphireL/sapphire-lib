﻿namespace SapphireArchitecture.Repository.Basics.Configurations;

internal class BasicUserEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<BasicUser>
{
    public void Configure(EntityTypeBuilder<BasicUser> builder)
    {
        builder.ToTable($"{BasicTablePrefix.Prefix}Users", Schema);
        builder.HasKey(u => u.Id);
        builder.Property(u => u.IsDeleted).HasDefaultValue(false);
        builder.Property(u => u.Username);
        builder.Property(u => u.CreationTime).HasDefaultValue(DateTime.Now);
        builder.Ignore(u => u.DomainEvents);
    }
}