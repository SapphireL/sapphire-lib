﻿using Microsoft.EntityFrameworkCore;

namespace SapphireArchitecture.Repository.Basics.Configurations;

internal static class ModelBuilderExtension
{
    internal static void ApplyBasicEntityTypeConfiguration(this ModelBuilder modelBuilder, string? schema)
    {
        modelBuilder.ApplyConfiguration(new BasicUserEntityTypeConfiguration(schema));
        modelBuilder.ApplyConfiguration(new BasicDepartmentEntityTypeConfiguration(schema));
    }
}
