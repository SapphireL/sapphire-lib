﻿using Microsoft.EntityFrameworkCore;

using SapphireArchitecture.Domain.Basics;
using SapphireArchitecture.Repository.Database;

using SapphireLib.Repository;

namespace SapphireArchitecture.Repository.Basics.Repositories
{
    internal class DepartmentRepository(SapphireArchitectureDbContext context) : IBasicDepartmentRepository, IRepository
    {
        public async Task AddAsync(BasicDepartment department)
        {
            await context.AddAsync(department);
        }

        public async Task<BasicDepartment?> GetAsync(int id)
        {
            return await context.BasicDepartments.FindAsync(id);
        }

        public async Task<List<BasicDepartment>> GetDepartmentsAsync(CancellationToken cancellationToken = default)
        {
            var departments = await context.BasicDepartments.ToListAsync(cancellationToken);

            return departments.Where(e => !e.ParentId.HasValue).ToList();
        }

        public async Task UpdateAsync(BasicDepartment department)
        {
            context.Update(department);
            await Task.CompletedTask;
        }
    }
}
