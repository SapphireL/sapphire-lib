﻿namespace SapphireArchitecture.Repository.Basics.Repositories;

public class UserRepository(SapphireArchitectureDbContext context) : IBasicUserRepository, IRepository
{
    public async Task AddAsync(BasicUser user)
    {
        await context.BasicUsers.AddAsync(user);
    }

    public async Task<bool> ExistsAsync(string username)
    {
        return await context.BasicUsers.AnyAsync(x => x.Username == username);
    }
}
