﻿namespace SapphireArchitecture.Repository.Database
{
    public static class AccessControlSeedData
    {
        private static Guid _userId = Guid.NewGuid();
        private static Guid _roleId = Guid.NewGuid();

        public static AccessControlUser GetUserSeedData()
        {
            return new AccessControlUser(_userId, "admin", "admin123");
        }

        public static AccessControlRole GetRoleSeedData()
        {
            return new AccessControlRole(_roleId, "SuperAdmin");
        }

        public static AccessControlUserRole GetAccessControlUserRoleSeedData()
        {
            return new AccessControlUserRole(true, _userId, _roleId);
        }

        private static readonly AccessControlMenu Menu1 = new AccessControlMenu("/system", "system");
        private static readonly AccessControlMenu Menu2 = new AccessControlMenu("/monitor", "monitor");
        private static readonly AccessControlMenu Menu3 = new AccessControlMenu("/permission", "permission");
        private static readonly AccessControlMenu Menu4 = new AccessControlMenu("/iframe", "iframe");
        private static readonly AccessControlMenu Menu5 = new AccessControlMenu("/tabs", "tabs");

        private static readonly AccessControlMenu Child1 = new AccessControlMenu("/system/user/index", "SystemUser", Menu1.Id);
        private static readonly AccessControlMenu Child2 = new AccessControlMenu("/system/role/index", "SystemRole", Menu1.Id);
        private static readonly AccessControlMenu Child3 = new AccessControlMenu("/system/menu/index", "SystemMenu", Menu1.Id);
        private static readonly AccessControlMenu Child4 = new AccessControlMenu("/system/dept/index", "SystemDept", Menu1.Id);

        private static readonly AccessControlMenu Child5 = new AccessControlMenu("/monitor/online-user", "OnlineUser", Menu2.Id)
        {
            Component = "monitor/online/index"
        };

        private static readonly AccessControlMenu Child6 = new AccessControlMenu("/monitor/login-logs", "LoginLog", Menu2.Id)
        {
            Component = "monitor/logs/login/index"
        };

        private static readonly AccessControlMenu Child7 = new AccessControlMenu("/monitor/operation-logs", "OperationLog", Menu2.Id)
        {
            Component = "monitor/logs/operation/index"
        };

        private static readonly AccessControlMenu Child8 = new AccessControlMenu("/monitor/system-logs", "SystemLog", Menu2.Id)
        {
            Component = "monitor/logs/system/index"
        };

        private static readonly AccessControlMenu Child9 = new AccessControlMenu("/permission/page/index", "PermissionPage", Menu3.Id);
        private static readonly AccessControlMenu Child10 = new AccessControlMenu("/permission/button/index", "PermissionButton", Menu3.Id);

        private static readonly AccessControlMenu Child11 = new AccessControlMenu("/iframe/embedded", "Embedded", Menu4.Id);
        private static readonly AccessControlMenu Child111 = new AccessControlMenu("/iframe/colorhunt", "FrameColorHunt", Child11.Id);
        private static readonly AccessControlMenu Child112 = new AccessControlMenu("/iframe/uigradients", "FrameUiGradients", Child11.Id);
        private static readonly AccessControlMenu Child113 = new AccessControlMenu("/iframe/ep", "FrameEp", Child11.Id);
        private static readonly AccessControlMenu Child114 = new AccessControlMenu("/iframe/tailwindcss", "FrameTailwindcss", Child11.Id);
        private static readonly AccessControlMenu Child115 = new AccessControlMenu("/iframe/vue3", "FrameVue", Child11.Id);
        private static readonly AccessControlMenu Child116 = new AccessControlMenu("/iframe/vite", "FrameVite", Child11.Id);
        private static readonly AccessControlMenu Child117 = new AccessControlMenu("/iframe/pinia", "FramePinia", Child11.Id);
        private static readonly AccessControlMenu Child118 = new AccessControlMenu("/iframe/vue-router", "FrameRouter", Child11.Id);

        private static readonly AccessControlMenu Child12 = new AccessControlMenu("/iframe/external", "External", Menu4.Id);
        private static readonly AccessControlMenu Child121 = new AccessControlMenu("/external", "https://pure-admin.github.io/pure-admin-doc", Child12.Id);
        private static readonly AccessControlMenu Child122 = new AccessControlMenu("/pureUtilsLink", "https://pure-admin-utils.netlify.app/", Child12.Id);

        public static List<AccessControlMenu> GetAccessControlMenuSeedData()
        {
            return [Menu1, Menu2, Menu3, Menu4, Menu5];
        }

        public static List<AccessControlMenuMeta> GetAccessControlMenuMetaSeedData()
        {
            return [
                new AccessControlMenuMeta("menus.pureSysManagement",  Menu1.Id, ["SuperAdmin", "admin"])
                {
                    Icon = "ri:settings-3-line",
                    Rank = 13
                },
                new AccessControlMenuMeta("menus.pureSysMonitor",  Menu2.Id, ["SuperAdmin", "admin"])
                {
                    Icon = "ep:monitor",
                    Rank = 14
                },
                new AccessControlMenuMeta("menus.purePermission",  Menu3.Id, ["SuperAdmin", "admin"])
                {
                    Icon = "ep:lollipop",
                    Rank = 12
                },
                new AccessControlMenuMeta("menus.pureExternalPage", Menu4.Id)
                {
                    Icon = "ri:links-fill",
                    Rank = 10
                },
                new AccessControlMenuMeta("menus.pureTabs",  Menu5.Id)
                {
                    Icon = "ri:bookmark-2-line",
                    Rank = 15
                }
                ];
        }

        public static List<AccessControlMenu> GetAccessControlMenuChildrenSeedData()
        {
            return [Child1, Child2, Child3, Child4, Child5, Child6, Child7, Child8, Child9, Child10, Child11, Child12, Child111, Child112, Child113, Child114, Child115, Child116, Child117, Child118, Child121, Child122];
        }

        public static List<AccessControlMenuMeta> GetAccessControlMenuChildMetaSeedData()
        {
            List<AccessControlMenuMeta> metas = [];
            metas.AddRange([
                    new AccessControlMenuMeta("menus.pureUser",  Child1.Id, ["SuperAdmin", "admin"])
                    {
                        Icon = "ri:admin-line"
                    },
                new AccessControlMenuMeta("menus.pureRole",  Child2.Id, ["SuperAdmin", "admin"])
                {
                    Icon = "ri:admin-fill"
                },
                new AccessControlMenuMeta("menus.pureSystemMenu",  Child3.Id, ["SuperAdmin", "admin"])
                {
                    Icon = "ep:menu"
                },
                new AccessControlMenuMeta("menus.pureDept",  Child4.Id, ["SuperAdmin", "admin"])
                {
                    Icon = "ri:git-branch-line"
                }
                    ]);
            metas.AddRange([
                new AccessControlMenuMeta("menus.pureOnlineUser",  Child5.Id, ["SuperAdmin", "admin"])
                {
                    Icon = "ri:user-voice-line"
                },
            new AccessControlMenuMeta("menus.pureLoginLog",   Child6.Id, ["SuperAdmin", "admin"])
            {
                Icon = "ri:window-line"
            },
            new AccessControlMenuMeta("menus.pureOperationLog",  Child7.Id, ["SuperAdmin", "admin"])
            {
                Icon = "ri:history-fill"
            },
            new AccessControlMenuMeta("menus.pureSystemLog", Child8.Id, ["SuperAdmin", "admin"])
            {
                Icon = "ri:file-search-line"
            }
                ]);
            metas.AddRange([
               new AccessControlMenuMeta("menus.purePermissionPage", Child9.Id, ["SuperAdmin", "admin"]),
            new AccessControlMenuMeta("menus.purePermissionButton",   Child10.Id, ["SuperAdmin", "admin"])
            {
                  Auths = ["permission:btn:add", "permission:btn:edit", "permission:btn:delete"]
            }
               ]);
            metas.AddRange([
            new AccessControlMenuMeta("menus.pureColorHuntDoc", Child111.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://colorhunt.co/",
                KeepAlive = true
            },
            new AccessControlMenuMeta("menus.pureUiGradients",  Child112.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://uigradients.com/",
                KeepAlive = true
            },
            new AccessControlMenuMeta("menus.pureEpDoc", Child113.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://element-plus.org/zh-CN/",
                KeepAlive = true
            },
            new AccessControlMenuMeta("menus.pureTailwindcssDoc",  Child114.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://tailwindcss.com/docs/installation",
                KeepAlive = true
            },
            new AccessControlMenuMeta("menus.pureVueDoc", Child115.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://cn.vuejs.org/",
                KeepAlive = true
            },
            new AccessControlMenuMeta("menus.pureViteDoc",  Child116.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://cn.vitejs.dev/",
                KeepAlive = true
            },
            new AccessControlMenuMeta("menus.purePiniaDoc", Child117.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://pinia.vuejs.org/zh/index.html",
                KeepAlive = true
            },
            new AccessControlMenuMeta("menus.pureRouterDoc", Child118.Id, ["SuperAdmin", "admin"])
            {
                FrameSrc = "https://router.vuejs.org/zh/",
                KeepAlive = true
            },
           ]);
            metas.AddRange([
          new AccessControlMenuMeta("menus.pureEmbeddedDoc", Child121.Id, ["SuperAdmin", "admin"]),
            new AccessControlMenuMeta("menus.pureExternalDoc", Child122.Id, ["SuperAdmin", "admin"])
          ]);

            metas.AddRange([
            new AccessControlMenuMeta("menus.pureExternalLink", Child11.Id, ["SuperAdmin", "admin"]),
            new AccessControlMenuMeta("menus.pureUtilsLink", Child12.Id, ["SuperAdmin", "admin"])
            ]);
            return metas;
        }
    }
}
