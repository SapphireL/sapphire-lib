﻿using System.Diagnostics;

namespace SapphireArchitecture.Repository.Database;

public class SapphireArchitectureDbContext : BaseDbContext<SapphireArchitectureDbContext>
{
    public SapphireArchitectureDbContext(EFCoreOptions<SapphireArchitectureDbContext> configOptions, IDomainEventBus domainEventBus)
        : base(configOptions)
    {
        _domainEventBus = domainEventBus;
    }

    public SapphireArchitectureDbContext(EFCoreOptions<SapphireArchitectureDbContext> configOptions)
   : base(configOptions)
    {
    }

    protected SapphireArchitectureDbContext()
        : base()
    {
    }

    public DbSet<AccessControlUser> AccessControlUsers { get; set; }

    public DbSet<AccessControlRole> AccessControlRoles { get; set; }

    public DbSet<AccessControlUserRole> AccessControlUserRoles { get; set; }

    public DbSet<AccessControlRoleClaim> AccessControlRoleClaims { get; set; }

    public DbSet<AccessControlUserLogin> AccessControlUserLogins { get; set; }

    public DbSet<AccessControlMenu> AccessControlMenus { get; set; }

    public DbSet<AccessControlMenuMeta> AccessControlMenuMetas { get; set; }

    public DbSet<AccessControlMenuMetaTransition> AccessControlMenuMetaTransitions { get; set; }

    public DbSet<BasicUser> BasicUsers { get; set; }

    public DbSet<SystemLogUserLogin> SystemLogUserLogins { get; set; }

    public DbSet<SystemLogOperationLog> SystemLogOperationLogs { get; set; }

    public DbSet<BasicDepartment> BasicDepartments { get; set; }

    private readonly IDomainEventBus _domainEventBus = null!;

    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await _domainEventBus.DispatchDomainEventsAsync(GetDomainEvents(this), cancellationToken);
        await SaveChangesAsync(cancellationToken);

        return true;
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyAccessControlEntityTypeConfiguration(BaseDbConfigOptions.Schema);
        builder.ApplyBasicEntityTypeConfiguration(BaseDbConfigOptions.Schema);
        builder.ApplySystemLogsEntityTypeConfiguration(BaseDbConfigOptions.Schema);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        optionsBuilder.EnableSensitiveDataLogging();
        optionsBuilder.LogTo(message => Debug.WriteLine(message));
    }

    private static List<IDomainEvent> GetDomainEvents(SapphireArchitectureDbContext ctx)
    {
        var domainEntities = ctx.ChangeTracker
           .Entries<Entity>()
           .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Count != 0);

        var domainEvents = domainEntities
            .SelectMany(x => x.Entity.DomainEvents)
            .ToList();

        return domainEvents;
    }
}
