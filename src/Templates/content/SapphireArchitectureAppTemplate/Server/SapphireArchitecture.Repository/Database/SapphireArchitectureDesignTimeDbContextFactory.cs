﻿namespace SapphireArchitecture.Modules.Administration.AccessControl.Repository.Database;

public class SapphireArchitectureDesignTimeDbContextFactory : BaseDesignTimeDbContextFactory<SapphireArchitectureDbContext, EFCoreOptions<SapphireArchitectureDbContext>>
{
    public override SapphireArchitectureDbContext CreateDbContext(string[] args)
    {
        var context = base.CreateDbContext(args);

        return context;
    }
}
