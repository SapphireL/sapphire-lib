﻿global using Microsoft.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore.Metadata.Builders;
global using Microsoft.Extensions.Logging;

global using SapphireArchitecture.Domain.AccessControls;
global using SapphireArchitecture.Domain.Basics;
global using SapphireArchitecture.Domain.SystemLogs;
global using SapphireArchitecture.Domain.Users;
global using SapphireArchitecture.Repository.AccessControls.Configurations;
global using SapphireArchitecture.Repository.Basics.Configurations;
global using SapphireArchitecture.Repository.Database;
global using SapphireArchitecture.Repository.SystemLogs.Configurations;

global using SapphireLib.Domain;
global using SapphireLib.Domain.Entities;
global using SapphireLib.Repository;
global using SapphireLib.Repository.EFCore;
global using SapphireLib.Repository.EFCore.Definitions;
global using SapphireLib.Repository.EFCore.Extensions;
global using SapphireLib.Tools;
global using SapphireLib.UnitOfWork.Abstractions;
