﻿namespace SapphireArchitecture.Repository.Interceptors;

public class DomainEventInterceptor(IDomainEventBus domainEventBus) : IUnitOfWorkInterceptor<SapphireArchitectureDbContext>
{
    public async ValueTask InterceptSavingChangesAsync(SapphireArchitectureDbContext context, CancellationToken cancellationToken = default)
    {
        await domainEventBus.DispatchDomainEventsAsync(GetDomainEvents(context));
    }

    private List<IDomainEvent> GetDomainEvents(SapphireArchitectureDbContext ctx)
    {
        // var domainEntities = ctx.ChangeTracker
        //   .Entries<Entity>()
        //   .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());
        var domainEvents = ctx.ChangeTracker
          .Entries<Entity>()
          .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any())
          .Select(x => x.Entity)
          .SelectMany(entity =>
          {
              IReadOnlyCollection<IDomainEvent> domainEvents = entity.DomainEvents;

              entity.ClearDomainEvents();

              return domainEvents;
          }).ToList();

        // var domainEvents = domainEntities
        //    .SelectMany(x => x.Entity.DomainEvents)
        //    .ToList();
        return domainEvents;
    }
}
