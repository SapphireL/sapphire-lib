﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class CreateDatabase : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "SapphireArchitecture");

            migrationBuilder.CreateTable(
                name: "AccessControl_Menus",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Path = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Redirect = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Component = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParentId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_Menus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccessControl_Menus_AccessControl_Menus_ParentId",
                        column: x => x.ParentId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "AccessControl_Menus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AccessControl_Roles",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccessControl_Users",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Salt = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RefreshToken_Expiry = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RefreshToken_RefreshToken = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Basic_Users",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2024, 7, 16, 12, 27, 46, 514, DateTimeKind.Local).AddTicks(5825))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Basic_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SystemLog_UserLogins",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OS = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserAgent = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsSuccess = table.Column<bool>(type: "bit", nullable: false),
                    FailureReason = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LoginAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemLog_UserLogins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccessControl_MenuMetas",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Icon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ExtraIcon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShowLink = table.Column<bool>(type: "bit", nullable: false),
                    ShowParent = table.Column<bool>(type: "bit", nullable: false),
                    Rank = table.Column<int>(type: "int", nullable: true),
                    Auths = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Roles = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    KeepAlive = table.Column<bool>(type: "bit", nullable: false),
                    FrameSrc = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FrameLoading = table.Column<bool>(type: "bit", nullable: false),
                    HiddenTag = table.Column<bool>(type: "bit", nullable: false),
                    DynamicLevel = table.Column<int>(type: "int", nullable: true),
                    ActivePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MenuId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_MenuMetas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccessControl_MenuMetas_AccessControl_Menus_MenuId",
                        column: x => x.MenuId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "AccessControl_Menus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccessControl_RoleClaims",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccessControl_RoleClaims_AccessControl_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "AccessControl_Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccessControl_UserLogins",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AccessControl_UserLogins_AccessControl_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "AccessControl_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccessControl_UserRoles",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_UserRoles", x => new { x.RoleId, x.UserId });
                    table.ForeignKey(
                        name: "FK_AccessControl_UserRoles_AccessControl_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "AccessControl_Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccessControl_UserRoles_AccessControl_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "AccessControl_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccessControl_MenuChildMetaTransitions",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EnterTransition = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LeaveTransition = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MetaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessControl_MenuChildMetaTransitions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccessControl_MenuChildMetaTransitions_AccessControl_MenuMetas_MetaId",
                        column: x => x.MetaId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "AccessControl_MenuMetas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), null, "iframe", null, "/iframe", null },
                    { new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), null, "permission", null, "/permission", null },
                    { new Guid("b026ed8c-7696-4f79-8a13-335fdf840590"), null, "tabs", null, "/tabs", null },
                    { new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), null, "system", null, "/system", null },
                    { new Guid("e9097735-5d48-479f-b221-1b9694df306e"), null, "monitor", null, "/monitor", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("c41750bd-c3f7-4e3b-b752-871ea2e1d66a"), "a6b7c90e21deaa08dfa06950cc594d33", "C2l9G", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0418ba37-7e8d-4076-9219-14dc5f7dbbf7"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("177f4af0-c904-46c8-a682-1c8e434e8854"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("b026ed8c-7696-4f79-8a13-335fdf840590"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("982881b4-dfa0-4d2d-95fa-05202894cfb6"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("9b552297-b5f0-47a9-b005-eed4320eed73"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("e9097735-5d48-479f-b221-1b9694df306e"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("a9c97c9b-da60-4e40-81d4-101517f4c46d"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), null, "Embedded", new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), "/iframe/embedded", null },
                    { new Guid("0c65671e-d096-44cf-a934-84708ab523ba"), "monitor/logs/login/index", "LoginLog", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/login-logs", null },
                    { new Guid("13cfd1e6-66cd-46c5-a21a-837ba9e99f4b"), "monitor/online/index", "OnlineUser", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/online-user", null },
                    { new Guid("2f26b15b-d848-4678-8c21-aec4b22cb036"), null, "PermissionButton", new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), "/permission/button/index", null },
                    { new Guid("50b30828-987b-4fc1-97bc-8151cfd0954f"), null, "SystemMenu", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/menu/index", null },
                    { new Guid("7199fc3f-f519-4e17-9769-b614573f01e2"), "monitor/logs/operation/index", "OperationLog", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/operation-logs", null },
                    { new Guid("841ebfdc-ca68-4d09-afde-b817af70076f"), null, "SystemRole", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/role/index", null },
                    { new Guid("8817e3f4-dcca-4872-915b-44e2ffb5ec3e"), null, "SystemUser", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/user/index", null },
                    { new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), null, "External", new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), "/iframe/external", null },
                    { new Guid("9bde5810-db79-4734-9fc2-4dab087c18a3"), null, "SystemDept", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/dept/index", null },
                    { new Guid("d836b185-34c6-4fff-863e-bb39e275a906"), "monitor/logs/system/index", "SystemLog", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/system-logs", null },
                    { new Guid("f62fba9c-a37d-4a6c-97cb-7c04173ca20a"), null, "PermissionPage", new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), "/permission/page/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                columns: new[] { "Id", "ClaimType", "ClaimValue", "RoleId" },
                values: new object[,]
                {
                    { new Guid("6418b923-55ec-4759-ab68-2ce011ad8906"), "permission", "Admin", new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd") },
                    { new Guid("83881f92-04ef-4b91-bcbe-87d2ab689d30"), "permission", "SuperAdmin", new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd") }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd"), new Guid("c41750bd-c3f7-4e3b-b752-871ea2e1d66a"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0223c9a2-14d2-474a-8ea9-7f4519f2bc08"), null, "[]", null, null, false, null, false, null, false, new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("030d8cea-e58a-47d3-addd-998f047018ef"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("9bde5810-db79-4734-9fc2-4dab087c18a3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("2ba02f2d-1a26-49e5-820b-23b6707cbc09"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("d836b185-34c6-4fff-863e-bb39e275a906"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("2d0cbbe2-aed3-48c5-8cb8-dd16f4538d07"), null, "[]", null, null, false, null, false, null, false, new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("3048b77d-5f1a-4012-b3b5-8e140725f8a1"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("50b30828-987b-4fc1-97bc-8151cfd0954f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("3c969a6a-d9e2-4de3-ace6-8a0db5787ef1"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("7199fc3f-f519-4e17-9769-b614573f01e2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("45a76ae8-abd7-41be-8091-a052592b1d17"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("2f26b15b-d848-4678-8c21-aec4b22cb036"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("50a8ae2b-8baa-4f8f-8563-cef5eec9b901"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("8817e3f4-dcca-4872-915b-44e2ffb5ec3e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("78f95b4b-296f-4dcb-98ec-6552ad2a9854"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("0c65671e-d096-44cf-a934-84708ab523ba"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("c0616a45-0872-4840-be86-d9f1beae3ccd"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("13cfd1e6-66cd-46c5-a21a-837ba9e99f4b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("e0948ac0-e70e-49cb-9af7-789a786bef33"), null, "[]", null, null, false, null, false, null, false, new Guid("f62fba9c-a37d-4a6c-97cb-7c04173ca20a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("f3028574-b76b-457c-8ef8-2f31db4cb486"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("841ebfdc-ca68-4d09-afde-b817af70076f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("14cb259c-4f1b-4912-88b4-ecda7e0a59ef"), null, "FrameVue", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/vue3", null },
                    { new Guid("555963bd-858a-45f5-a157-69a4c0810b82"), null, "FrameVite", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/vite", null },
                    { new Guid("56ba6f25-37d3-4554-b833-8c389f8fcb03"), null, "FramePinia", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/pinia", null },
                    { new Guid("576d7394-e2b9-4875-afeb-0dd00417d503"), null, "FrameRouter", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/vue-router", null },
                    { new Guid("a8a42035-7659-47db-b48f-b8429dfdb304"), null, "https://pure-admin-utils.netlify.app/", new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), "/pureUtilsLink", null },
                    { new Guid("beff7478-2b64-42bd-833b-5ab46c2409c8"), null, "FrameEp", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/ep", null },
                    { new Guid("e3c72b5d-da67-4e26-925e-370f1d690e03"), null, "FrameTailwindcss", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/tailwindcss", null },
                    { new Guid("e9413850-91b0-4d2a-ae76-18ccf59ac51a"), null, "FrameUiGradients", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/uigradients", null },
                    { new Guid("f19353be-aaec-4e99-a5e9-915279e0745a"), null, "FrameColorHunt", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/colorhunt", null },
                    { new Guid("f4b9fe52-9523-4f93-9a3e-1e6211ccd083"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), "/external", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("103eec53-a2de-4a0e-88b7-dc645b2b55fe"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("14cb259c-4f1b-4912-88b4-ecda7e0a59ef"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("12c3ca04-d143-473a-bc1e-9b3fbd378cd3"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("beff7478-2b64-42bd-833b-5ab46c2409c8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("317ad030-ea0f-447c-850e-846c01e35b89"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("555963bd-858a-45f5-a157-69a4c0810b82"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("4f19f3b7-77c8-45ce-b20b-826dc793ffb2"), null, "[]", null, null, false, null, false, null, false, new Guid("f4b9fe52-9523-4f93-9a3e-1e6211ccd083"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("53349f97-b096-4231-8ad6-4f89fe9baa0b"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("f19353be-aaec-4e99-a5e9-915279e0745a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("58230e6f-66b2-44a6-8eb1-77e12a100da1"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("56ba6f25-37d3-4554-b833-8c389f8fcb03"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("8c2a390c-4499-46dc-a98e-72c2af04e348"), null, "[]", null, null, false, null, false, null, false, new Guid("a8a42035-7659-47db-b48f-b8429dfdb304"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("a94c1cee-3c91-43cc-9db5-ae5480488812"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("e9413850-91b0-4d2a-ae76-18ccf59ac51a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("e6d2adce-0d08-45f2-b4ce-93ee57341132"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("e3c72b5d-da67-4e26-925e-370f1d690e03"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("f7fa2a4e-28c7-47e1-b140-e180f13e430d"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("576d7394-e2b9-4875-afeb-0dd00417d503"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccessControl_MenuChildMetaTransitions_MetaId",
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuChildMetaTransitions",
                column: "MetaId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccessControl_MenuMetas_MenuId",
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                column: "MenuId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccessControl_Menus_ParentId",
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_AccessControl_RoleClaims_RoleId",
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AccessControl_Roles_NormalizedName",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AccessControl_UserLogins_UserId",
                schema: "SapphireArchitecture",
                table: "AccessControl_UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccessControl_UserRoles_UserId",
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                column: "UserId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccessControl_MenuChildMetaTransitions",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "AccessControl_RoleClaims",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "AccessControl_UserLogins",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "AccessControl_UserRoles",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "Basic_Users",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "SystemLog_UserLogins",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "AccessControl_MenuMetas",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "AccessControl_Roles",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "AccessControl_Users",
                schema: "SapphireArchitecture");

            migrationBuilder.DropTable(
                name: "AccessControl_Menus",
                schema: "SapphireArchitecture");
        }
    }
}
