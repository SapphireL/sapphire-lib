﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class AddDepartment : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0223c9a2-14d2-474a-8ea9-7f4519f2bc08"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("030d8cea-e58a-47d3-addd-998f047018ef"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0418ba37-7e8d-4076-9219-14dc5f7dbbf7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("103eec53-a2de-4a0e-88b7-dc645b2b55fe"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("12c3ca04-d143-473a-bc1e-9b3fbd378cd3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("177f4af0-c904-46c8-a682-1c8e434e8854"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2ba02f2d-1a26-49e5-820b-23b6707cbc09"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2d0cbbe2-aed3-48c5-8cb8-dd16f4538d07"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("3048b77d-5f1a-4012-b3b5-8e140725f8a1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("317ad030-ea0f-447c-850e-846c01e35b89"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("3c969a6a-d9e2-4de3-ace6-8a0db5787ef1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("45a76ae8-abd7-41be-8091-a052592b1d17"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4f19f3b7-77c8-45ce-b20b-826dc793ffb2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("50a8ae2b-8baa-4f8f-8563-cef5eec9b901"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("53349f97-b096-4231-8ad6-4f89fe9baa0b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("58230e6f-66b2-44a6-8eb1-77e12a100da1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("78f95b4b-296f-4dcb-98ec-6552ad2a9854"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("8c2a390c-4499-46dc-a98e-72c2af04e348"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("982881b4-dfa0-4d2d-95fa-05202894cfb6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9b552297-b5f0-47a9-b005-eed4320eed73"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a94c1cee-3c91-43cc-9db5-ae5480488812"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a9c97c9b-da60-4e40-81d4-101517f4c46d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c0616a45-0872-4840-be86-d9f1beae3ccd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e0948ac0-e70e-49cb-9af7-789a786bef33"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e6d2adce-0d08-45f2-b4ce-93ee57341132"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f3028574-b76b-457c-8ef8-2f31db4cb486"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f7fa2a4e-28c7-47e1-b140-e180f13e430d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("6418b923-55ec-4759-ab68-2ce011ad8906"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("83881f92-04ef-4b91-bcbe-87d2ab689d30"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd"), new Guid("c41750bd-c3f7-4e3b-b752-871ea2e1d66a") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0c65671e-d096-44cf-a934-84708ab523ba"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("13cfd1e6-66cd-46c5-a21a-837ba9e99f4b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("14cb259c-4f1b-4912-88b4-ecda7e0a59ef"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2f26b15b-d848-4678-8c21-aec4b22cb036"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("50b30828-987b-4fc1-97bc-8151cfd0954f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("555963bd-858a-45f5-a157-69a4c0810b82"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("56ba6f25-37d3-4554-b833-8c389f8fcb03"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("576d7394-e2b9-4875-afeb-0dd00417d503"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7199fc3f-f519-4e17-9769-b614573f01e2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("841ebfdc-ca68-4d09-afde-b817af70076f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8817e3f4-dcca-4872-915b-44e2ffb5ec3e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9bde5810-db79-4734-9fc2-4dab087c18a3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a8a42035-7659-47db-b48f-b8429dfdb304"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b026ed8c-7696-4f79-8a13-335fdf840590"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("beff7478-2b64-42bd-833b-5ab46c2409c8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d836b185-34c6-4fff-863e-bb39e275a906"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e3c72b5d-da67-4e26-925e-370f1d690e03"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e9413850-91b0-4d2a-ae76-18ccf59ac51a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f19353be-aaec-4e99-a5e9-915279e0745a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f4b9fe52-9523-4f93-9a3e-1e6211ccd083"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f62fba9c-a37d-4a6c-97cb-7c04173ca20a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("c41750bd-c3f7-4e3b-b752-871ea2e1d66a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e9097735-5d48-479f-b221-1b9694df306e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("04a5752d-14ed-4f24-b5b2-482208211901"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 7, 19, 14, 20, 28, 939, DateTimeKind.Local).AddTicks(3311),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 7, 16, 12, 27, 46, 514, DateTimeKind.Local).AddTicks(5825));

            migrationBuilder.CreateTable(
                name: "Basic_Departments",
                schema: "SapphireArchitecture",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Director = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Enabled = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ParentId = table.Column<int>(type: "int", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValue: new DateTime(2024, 7, 19, 14, 20, 28, 939, DateTimeKind.Local).AddTicks(8079))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Basic_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Basic_Departments_Basic_Departments_ParentId",
                        column: x => x.ParentId,
                        principalSchema: "SapphireArchitecture",
                        principalTable: "Basic_Departments",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), null, "permission", null, "/permission", null },
                    { new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), null, "iframe", null, "/iframe", null },
                    { new Guid("7b7f03e3-c645-480f-8034-8acab1de1183"), null, "tabs", null, "/tabs", null },
                    { new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), null, "system", null, "/system", null },
                    { new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), null, "monitor", null, "/monitor", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70"), "a50d9323dbfe72efcde19d8221bb6f82", "gzZNj", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("6c111121-6978-4284-8b39-05b8b7826be5"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("7dae25ed-52fc-43b8-8cef-6ce01a089ca5"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("994a003d-c4de-487f-a1db-84eaafb363ae"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("9f2f07b7-b142-4292-beda-027c2c53e8ff"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("7b7f03e3-c645-480f-8034-8acab1de1183"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("d56054d4-765f-4fa8-8879-645b3216afe2"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("1444a79b-f019-4f02-8f67-b013624bb6ac"), null, "PermissionButton", new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), "/permission/button/index", null },
                    { new Guid("2bcbf010-a893-4e37-8a51-017c6b907f44"), null, "SystemDept", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/dept/index", null },
                    { new Guid("4553bc12-2af2-4a52-964a-4b80fca3c574"), "monitor/logs/login/index", "LoginLog", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/login-logs", null },
                    { new Guid("57201af4-f115-4e2f-9d06-bbdfaf144a98"), null, "PermissionPage", new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), "/permission/page/index", null },
                    { new Guid("6c3628cb-98a2-4dc3-ae3d-38c1f1bcbe8f"), "monitor/logs/operation/index", "OperationLog", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/operation-logs", null },
                    { new Guid("a8abe87a-7093-4468-90f3-4d42dd80b9f6"), null, "SystemMenu", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/menu/index", null },
                    { new Guid("c973e906-1b05-4799-a366-3bbaca99f636"), null, "SystemUser", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/user/index", null },
                    { new Guid("e0e45243-17ac-4c52-8433-1433521c9b34"), null, "SystemRole", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/role/index", null },
                    { new Guid("e48ef7e1-c4ef-437d-ad4c-b1705e82df4a"), "monitor/online/index", "OnlineUser", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/online-user", null },
                    { new Guid("e639a5ce-db88-4011-9986-623717aa41f6"), "monitor/logs/system/index", "SystemLog", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/system-logs", null },
                    { new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), null, "Embedded", new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), "/iframe/embedded", null },
                    { new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), null, "External", new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), "/iframe/external", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                columns: new[] { "Id", "ClaimType", "ClaimValue", "RoleId" },
                values: new object[,]
                {
                    { new Guid("9a572fdb-f1e1-4eb8-aa2b-ee74ac7b1836"), "permission", "Admin", new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9") },
                    { new Guid("e11c874f-53e7-4a47-9e64-6ef274e89a60"), "permission", "SuperAdmin", new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9") }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"), new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("083022da-d021-4124-b25a-139c49436890"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("2bcbf010-a893-4e37-8a51-017c6b907f44"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("0d73716e-cb46-4d4a-bec1-b1b6d9717963"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("c973e906-1b05-4799-a366-3bbaca99f636"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("15817879-116a-4778-bdc7-c3ea5873a5cc"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("e0e45243-17ac-4c52-8433-1433521c9b34"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("2ce8bc3a-ebcd-4dd8-b6cb-2340d934e8c0"), null, "[]", null, null, false, null, false, null, false, new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("2f36c3ab-b4d8-47b1-897c-54db3ad5933a"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("a8abe87a-7093-4468-90f3-4d42dd80b9f6"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("5305a442-9646-4457-ad28-58adb9dd3542"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("e639a5ce-db88-4011-9986-623717aa41f6"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("5def1939-f8ff-42fe-a28a-93d676fa971f"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("6c3628cb-98a2-4dc3-ae3d-38c1f1bcbe8f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("66ba2216-0646-409c-8933-cb9dfbe00761"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("4553bc12-2af2-4a52-964a-4b80fca3c574"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("9b4ff4aa-5605-4aa5-b56a-72c649c791a5"), null, "[]", null, null, false, null, false, null, false, new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("a4af0823-4125-4362-abbe-27f241bb5a9d"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("1444a79b-f019-4f02-8f67-b013624bb6ac"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("a52d0f60-4adf-406f-a5e9-8a7eb3a7ec8c"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("e48ef7e1-c4ef-437d-ad4c-b1705e82df4a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("da99ea91-daf1-4931-b0d1-9a355a0fb9fc"), null, "[]", null, null, false, null, false, null, false, new Guid("57201af4-f115-4e2f-9d06-bbdfaf144a98"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("13b91358-694a-472a-9145-b211689fdece"), null, "FrameColorHunt", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/colorhunt", null },
                    { new Guid("38115f1a-1bd4-4560-a1b7-18bd85c5834f"), null, "FrameEp", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/ep", null },
                    { new Guid("41df1ca3-d595-410d-b007-6be8e3239a92"), null, "FramePinia", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/pinia", null },
                    { new Guid("6d0b84c1-bea0-4639-9cbb-82f1c5c41713"), null, "FrameRouter", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/vue-router", null },
                    { new Guid("6e4fff2d-b6e4-4ebf-9495-96c2b1247496"), null, "FrameVite", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/vite", null },
                    { new Guid("8f2328b6-0209-4ddf-994b-3ec4fee49688"), null, "https://pure-admin-utils.netlify.app/", new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), "/pureUtilsLink", null },
                    { new Guid("91d95578-886d-4465-85d2-f2491001a752"), null, "FrameTailwindcss", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/tailwindcss", null },
                    { new Guid("a5e9cbcf-d29c-48f0-8711-2c4fbf6a18f7"), null, "FrameUiGradients", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/uigradients", null },
                    { new Guid("bd92c334-3821-44fb-9778-03f3ca2dabb4"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), "/external", null },
                    { new Guid("da2b2dd4-bf3c-46aa-bbc5-12515753d49d"), null, "FrameVue", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/vue3", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("11e8dbb2-b51e-439a-bcce-9d0c94ec941d"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("91d95578-886d-4465-85d2-f2491001a752"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("65f53177-1ab1-4733-8d22-d133b169159f"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("13b91358-694a-472a-9145-b211689fdece"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("686b56fe-69c7-49da-8f62-08b28861d212"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("38115f1a-1bd4-4560-a1b7-18bd85c5834f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("84f7c910-41f4-4035-9165-e0c22d3d0bd7"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("6d0b84c1-bea0-4639-9cbb-82f1c5c41713"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("cf9e109d-1449-4159-b89d-bdac09007d88"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("6e4fff2d-b6e4-4ebf-9495-96c2b1247496"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("d6c98d8b-8e8e-41e2-a37b-5e678cf4bc23"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("41df1ca3-d595-410d-b007-6be8e3239a92"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("d73be64a-c8d5-445c-ad11-479db921533c"), null, "[]", null, null, false, null, false, null, false, new Guid("8f2328b6-0209-4ddf-994b-3ec4fee49688"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("de985c17-856f-4860-b927-ba6b40e146d9"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("da2b2dd4-bf3c-46aa-bbc5-12515753d49d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("ec13992d-ebb0-4cf7-b7f0-304e64beea91"), null, "[]", null, null, false, null, false, null, false, new Guid("bd92c334-3821-44fb-9778-03f3ca2dabb4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("f28f22ea-d7d4-497e-a08f-656d38f2d5e0"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("a5e9cbcf-d29c-48f0-8711-2c4fbf6a18f7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Basic_Departments_ParentId",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                column: "ParentId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Basic_Departments",
                schema: "SapphireArchitecture");

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("083022da-d021-4124-b25a-139c49436890"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0d73716e-cb46-4d4a-bec1-b1b6d9717963"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("11e8dbb2-b51e-439a-bcce-9d0c94ec941d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("15817879-116a-4778-bdc7-c3ea5873a5cc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2ce8bc3a-ebcd-4dd8-b6cb-2340d934e8c0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2f36c3ab-b4d8-47b1-897c-54db3ad5933a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5305a442-9646-4457-ad28-58adb9dd3542"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5def1939-f8ff-42fe-a28a-93d676fa971f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("65f53177-1ab1-4733-8d22-d133b169159f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("66ba2216-0646-409c-8933-cb9dfbe00761"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("686b56fe-69c7-49da-8f62-08b28861d212"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6c111121-6978-4284-8b39-05b8b7826be5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7dae25ed-52fc-43b8-8cef-6ce01a089ca5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("84f7c910-41f4-4035-9165-e0c22d3d0bd7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("994a003d-c4de-487f-a1db-84eaafb363ae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9b4ff4aa-5605-4aa5-b56a-72c649c791a5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9f2f07b7-b142-4292-beda-027c2c53e8ff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a4af0823-4125-4362-abbe-27f241bb5a9d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a52d0f60-4adf-406f-a5e9-8a7eb3a7ec8c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("cf9e109d-1449-4159-b89d-bdac09007d88"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d56054d4-765f-4fa8-8879-645b3216afe2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d6c98d8b-8e8e-41e2-a37b-5e678cf4bc23"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d73be64a-c8d5-445c-ad11-479db921533c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("da99ea91-daf1-4931-b0d1-9a355a0fb9fc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("de985c17-856f-4860-b927-ba6b40e146d9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ec13992d-ebb0-4cf7-b7f0-304e64beea91"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f28f22ea-d7d4-497e-a08f-656d38f2d5e0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("9a572fdb-f1e1-4eb8-aa2b-ee74ac7b1836"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("e11c874f-53e7-4a47-9e64-6ef274e89a60"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"), new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("13b91358-694a-472a-9145-b211689fdece"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1444a79b-f019-4f02-8f67-b013624bb6ac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2bcbf010-a893-4e37-8a51-017c6b907f44"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("38115f1a-1bd4-4560-a1b7-18bd85c5834f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("41df1ca3-d595-410d-b007-6be8e3239a92"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("4553bc12-2af2-4a52-964a-4b80fca3c574"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("57201af4-f115-4e2f-9d06-bbdfaf144a98"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6c3628cb-98a2-4dc3-ae3d-38c1f1bcbe8f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6d0b84c1-bea0-4639-9cbb-82f1c5c41713"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6e4fff2d-b6e4-4ebf-9495-96c2b1247496"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7b7f03e3-c645-480f-8034-8acab1de1183"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8f2328b6-0209-4ddf-994b-3ec4fee49688"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("91d95578-886d-4465-85d2-f2491001a752"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a5e9cbcf-d29c-48f0-8711-2c4fbf6a18f7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a8abe87a-7093-4468-90f3-4d42dd80b9f6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bd92c334-3821-44fb-9778-03f3ca2dabb4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c973e906-1b05-4799-a366-3bbaca99f636"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("da2b2dd4-bf3c-46aa-bbc5-12515753d49d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e0e45243-17ac-4c52-8433-1433521c9b34"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e48ef7e1-c4ef-437d-ad4c-b1705e82df4a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e639a5ce-db88-4011-9986-623717aa41f6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 7, 16, 12, 27, 46, 514, DateTimeKind.Local).AddTicks(5825),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 7, 19, 14, 20, 28, 939, DateTimeKind.Local).AddTicks(3311));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), null, "iframe", null, "/iframe", null },
                    { new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), null, "permission", null, "/permission", null },
                    { new Guid("b026ed8c-7696-4f79-8a13-335fdf840590"), null, "tabs", null, "/tabs", null },
                    { new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), null, "system", null, "/system", null },
                    { new Guid("e9097735-5d48-479f-b221-1b9694df306e"), null, "monitor", null, "/monitor", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("c41750bd-c3f7-4e3b-b752-871ea2e1d66a"), "a6b7c90e21deaa08dfa06950cc594d33", "C2l9G", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0418ba37-7e8d-4076-9219-14dc5f7dbbf7"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("177f4af0-c904-46c8-a682-1c8e434e8854"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("b026ed8c-7696-4f79-8a13-335fdf840590"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("982881b4-dfa0-4d2d-95fa-05202894cfb6"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("9b552297-b5f0-47a9-b005-eed4320eed73"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("e9097735-5d48-479f-b221-1b9694df306e"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("a9c97c9b-da60-4e40-81d4-101517f4c46d"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), null, "Embedded", new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), "/iframe/embedded", null },
                    { new Guid("0c65671e-d096-44cf-a934-84708ab523ba"), "monitor/logs/login/index", "LoginLog", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/login-logs", null },
                    { new Guid("13cfd1e6-66cd-46c5-a21a-837ba9e99f4b"), "monitor/online/index", "OnlineUser", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/online-user", null },
                    { new Guid("2f26b15b-d848-4678-8c21-aec4b22cb036"), null, "PermissionButton", new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), "/permission/button/index", null },
                    { new Guid("50b30828-987b-4fc1-97bc-8151cfd0954f"), null, "SystemMenu", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/menu/index", null },
                    { new Guid("7199fc3f-f519-4e17-9769-b614573f01e2"), "monitor/logs/operation/index", "OperationLog", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/operation-logs", null },
                    { new Guid("841ebfdc-ca68-4d09-afde-b817af70076f"), null, "SystemRole", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/role/index", null },
                    { new Guid("8817e3f4-dcca-4872-915b-44e2ffb5ec3e"), null, "SystemUser", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/user/index", null },
                    { new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), null, "External", new Guid("04a5752d-14ed-4f24-b5b2-482208211901"), "/iframe/external", null },
                    { new Guid("9bde5810-db79-4734-9fc2-4dab087c18a3"), null, "SystemDept", new Guid("bf67f32c-7b81-4a52-a60a-e0e8e7928ac1"), "/system/dept/index", null },
                    { new Guid("d836b185-34c6-4fff-863e-bb39e275a906"), "monitor/logs/system/index", "SystemLog", new Guid("e9097735-5d48-479f-b221-1b9694df306e"), "/monitor/system-logs", null },
                    { new Guid("f62fba9c-a37d-4a6c-97cb-7c04173ca20a"), null, "PermissionPage", new Guid("2c1ce945-212a-4647-8ef1-4bed0dd82e7b"), "/permission/page/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                columns: new[] { "Id", "ClaimType", "ClaimValue", "RoleId" },
                values: new object[,]
                {
                    { new Guid("6418b923-55ec-4759-ab68-2ce011ad8906"), "permission", "Admin", new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd") },
                    { new Guid("83881f92-04ef-4b91-bcbe-87d2ab689d30"), "permission", "SuperAdmin", new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd") }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("8ef983e4-41f3-4929-b19e-251458c65dbd"), new Guid("c41750bd-c3f7-4e3b-b752-871ea2e1d66a"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0223c9a2-14d2-474a-8ea9-7f4519f2bc08"), null, "[]", null, null, false, null, false, null, false, new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("030d8cea-e58a-47d3-addd-998f047018ef"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("9bde5810-db79-4734-9fc2-4dab087c18a3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("2ba02f2d-1a26-49e5-820b-23b6707cbc09"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("d836b185-34c6-4fff-863e-bb39e275a906"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("2d0cbbe2-aed3-48c5-8cb8-dd16f4538d07"), null, "[]", null, null, false, null, false, null, false, new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("3048b77d-5f1a-4012-b3b5-8e140725f8a1"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("50b30828-987b-4fc1-97bc-8151cfd0954f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("3c969a6a-d9e2-4de3-ace6-8a0db5787ef1"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("7199fc3f-f519-4e17-9769-b614573f01e2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("45a76ae8-abd7-41be-8091-a052592b1d17"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("2f26b15b-d848-4678-8c21-aec4b22cb036"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("50a8ae2b-8baa-4f8f-8563-cef5eec9b901"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("8817e3f4-dcca-4872-915b-44e2ffb5ec3e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("78f95b4b-296f-4dcb-98ec-6552ad2a9854"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("0c65671e-d096-44cf-a934-84708ab523ba"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("c0616a45-0872-4840-be86-d9f1beae3ccd"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("13cfd1e6-66cd-46c5-a21a-837ba9e99f4b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("e0948ac0-e70e-49cb-9af7-789a786bef33"), null, "[]", null, null, false, null, false, null, false, new Guid("f62fba9c-a37d-4a6c-97cb-7c04173ca20a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("f3028574-b76b-457c-8ef8-2f31db4cb486"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("841ebfdc-ca68-4d09-afde-b817af70076f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("14cb259c-4f1b-4912-88b4-ecda7e0a59ef"), null, "FrameVue", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/vue3", null },
                    { new Guid("555963bd-858a-45f5-a157-69a4c0810b82"), null, "FrameVite", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/vite", null },
                    { new Guid("56ba6f25-37d3-4554-b833-8c389f8fcb03"), null, "FramePinia", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/pinia", null },
                    { new Guid("576d7394-e2b9-4875-afeb-0dd00417d503"), null, "FrameRouter", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/vue-router", null },
                    { new Guid("a8a42035-7659-47db-b48f-b8429dfdb304"), null, "https://pure-admin-utils.netlify.app/", new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), "/pureUtilsLink", null },
                    { new Guid("beff7478-2b64-42bd-833b-5ab46c2409c8"), null, "FrameEp", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/ep", null },
                    { new Guid("e3c72b5d-da67-4e26-925e-370f1d690e03"), null, "FrameTailwindcss", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/tailwindcss", null },
                    { new Guid("e9413850-91b0-4d2a-ae76-18ccf59ac51a"), null, "FrameUiGradients", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/uigradients", null },
                    { new Guid("f19353be-aaec-4e99-a5e9-915279e0745a"), null, "FrameColorHunt", new Guid("02970f32-dcc2-4e8b-8685-9d15160ef2b3"), "/iframe/colorhunt", null },
                    { new Guid("f4b9fe52-9523-4f93-9a3e-1e6211ccd083"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("9b0122d5-bc85-4e9a-8601-bc7f5d12acc5"), "/external", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("103eec53-a2de-4a0e-88b7-dc645b2b55fe"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("14cb259c-4f1b-4912-88b4-ecda7e0a59ef"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("12c3ca04-d143-473a-bc1e-9b3fbd378cd3"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("beff7478-2b64-42bd-833b-5ab46c2409c8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("317ad030-ea0f-447c-850e-846c01e35b89"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("555963bd-858a-45f5-a157-69a4c0810b82"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("4f19f3b7-77c8-45ce-b20b-826dc793ffb2"), null, "[]", null, null, false, null, false, null, false, new Guid("f4b9fe52-9523-4f93-9a3e-1e6211ccd083"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("53349f97-b096-4231-8ad6-4f89fe9baa0b"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("f19353be-aaec-4e99-a5e9-915279e0745a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("58230e6f-66b2-44a6-8eb1-77e12a100da1"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("56ba6f25-37d3-4554-b833-8c389f8fcb03"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("8c2a390c-4499-46dc-a98e-72c2af04e348"), null, "[]", null, null, false, null, false, null, false, new Guid("a8a42035-7659-47db-b48f-b8429dfdb304"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("a94c1cee-3c91-43cc-9db5-ae5480488812"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("e9413850-91b0-4d2a-ae76-18ccf59ac51a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("e6d2adce-0d08-45f2-b4ce-93ee57341132"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("e3c72b5d-da67-4e26-925e-370f1d690e03"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("f7fa2a4e-28c7-47e1-b140-e180f13e430d"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("576d7394-e2b9-4875-afeb-0dd00417d503"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" }
                });
        }
    }
}
