﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class UpdateRole : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("083022da-d021-4124-b25a-139c49436890"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0d73716e-cb46-4d4a-bec1-b1b6d9717963"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("11e8dbb2-b51e-439a-bcce-9d0c94ec941d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("15817879-116a-4778-bdc7-c3ea5873a5cc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2ce8bc3a-ebcd-4dd8-b6cb-2340d934e8c0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2f36c3ab-b4d8-47b1-897c-54db3ad5933a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5305a442-9646-4457-ad28-58adb9dd3542"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5def1939-f8ff-42fe-a28a-93d676fa971f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("65f53177-1ab1-4733-8d22-d133b169159f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("66ba2216-0646-409c-8933-cb9dfbe00761"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("686b56fe-69c7-49da-8f62-08b28861d212"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6c111121-6978-4284-8b39-05b8b7826be5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7dae25ed-52fc-43b8-8cef-6ce01a089ca5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("84f7c910-41f4-4035-9165-e0c22d3d0bd7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("994a003d-c4de-487f-a1db-84eaafb363ae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9b4ff4aa-5605-4aa5-b56a-72c649c791a5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9f2f07b7-b142-4292-beda-027c2c53e8ff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a4af0823-4125-4362-abbe-27f241bb5a9d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a52d0f60-4adf-406f-a5e9-8a7eb3a7ec8c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("cf9e109d-1449-4159-b89d-bdac09007d88"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d56054d4-765f-4fa8-8879-645b3216afe2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d6c98d8b-8e8e-41e2-a37b-5e678cf4bc23"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d73be64a-c8d5-445c-ad11-479db921533c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("da99ea91-daf1-4931-b0d1-9a355a0fb9fc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("de985c17-856f-4860-b927-ba6b40e146d9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ec13992d-ebb0-4cf7-b7f0-304e64beea91"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f28f22ea-d7d4-497e-a08f-656d38f2d5e0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("9a572fdb-f1e1-4eb8-aa2b-ee74ac7b1836"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("e11c874f-53e7-4a47-9e64-6ef274e89a60"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"), new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("13b91358-694a-472a-9145-b211689fdece"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1444a79b-f019-4f02-8f67-b013624bb6ac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2bcbf010-a893-4e37-8a51-017c6b907f44"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("38115f1a-1bd4-4560-a1b7-18bd85c5834f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("41df1ca3-d595-410d-b007-6be8e3239a92"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("4553bc12-2af2-4a52-964a-4b80fca3c574"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("57201af4-f115-4e2f-9d06-bbdfaf144a98"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6c3628cb-98a2-4dc3-ae3d-38c1f1bcbe8f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6d0b84c1-bea0-4639-9cbb-82f1c5c41713"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6e4fff2d-b6e4-4ebf-9495-96c2b1247496"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7b7f03e3-c645-480f-8034-8acab1de1183"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8f2328b6-0209-4ddf-994b-3ec4fee49688"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("91d95578-886d-4465-85d2-f2491001a752"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a5e9cbcf-d29c-48f0-8711-2c4fbf6a18f7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a8abe87a-7093-4468-90f3-4d42dd80b9f6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bd92c334-3821-44fb-9778-03f3ca2dabb4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c973e906-1b05-4799-a366-3bbaca99f636"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("da2b2dd4-bf3c-46aa-bbc5-12515753d49d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e0e45243-17ac-4c52-8433-1433521c9b34"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e48ef7e1-c4ef-437d-ad4c-b1705e82df4a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e639a5ce-db88-4011-9986-623717aa41f6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(3048),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 7, 19, 14, 20, 28, 939, DateTimeKind.Local).AddTicks(3311));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(8230),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 7, 19, 14, 20, 28, 939, DateTimeKind.Local).AddTicks(8079));

            migrationBuilder.CreateTable(
                name: "SystemLogOperationLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Operator = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OperatorId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    BelongTo = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Summary = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IpAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Os = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserAgent = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsSuccess = table.Column<bool>(type: "bit", nullable: false),
                    OperationAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Remarks = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemLogOperationLogs", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), null, "iframe", null, "/iframe", null },
                    { new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), null, "permission", null, "/permission", null },
                    { new Guid("6a23635f-e7ac-439d-b10c-7cb176e703eb"), null, "tabs", null, "/tabs", null },
                    { new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), null, "monitor", null, "/monitor", null },
                    { new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), null, "system", null, "/system", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db"), "c72cca1092a7e206daada912d19f879b", "OKo3Y", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("466b60ce-eeae-4655-bb4c-d62c5622e163"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("64ade514-6930-4c96-9ea6-62f227b6deb5"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("b0f25851-f613-4c2c-aace-14fd68fabf2c"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("6a23635f-e7ac-439d-b10c-7cb176e703eb"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("d0a8dffd-42ee-42af-b3d0-888042f4471c"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("feb36e00-c0e9-4c10-9a60-480288962951"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0b9521c2-5de9-430f-acc3-d2207b534f0b"), "monitor/online/index", "OnlineUser", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/online-user", null },
                    { new Guid("14c08ee3-19b9-465d-be20-63f559504269"), null, "SystemRole", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/role/index", null },
                    { new Guid("35cc0490-a6c5-42fb-9ccc-4ddbe297d041"), "monitor/logs/operation/index", "OperationLog", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/operation-logs", null },
                    { new Guid("371a3226-1898-4a4c-bd8d-2619102554a1"), "monitor/logs/login/index", "LoginLog", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/login-logs", null },
                    { new Guid("3c12106b-f4f3-41f4-a2a0-8aa1e3f23795"), "monitor/logs/system/index", "SystemLog", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/system-logs", null },
                    { new Guid("8dbbced1-63d3-43a4-b97a-83f7538ff1fd"), null, "SystemMenu", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/menu/index", null },
                    { new Guid("8ee1711d-003b-44c3-be0b-db0648ed25c0"), null, "SystemUser", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/user/index", null },
                    { new Guid("9055bb6f-5d32-4697-95af-f09cbdde288e"), null, "SystemDept", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/dept/index", null },
                    { new Guid("954883e5-4f78-476a-b8c2-549767410959"), null, "Embedded", new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), "/iframe/embedded", null },
                    { new Guid("c1e7136c-648c-4afd-9661-743741cf33fa"), null, "PermissionPage", new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), "/permission/page/index", null },
                    { new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), null, "External", new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), "/iframe/external", null },
                    { new Guid("fff47f28-a91c-404b-b0a2-fd3bf2ece51b"), null, "PermissionButton", new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), "/permission/button/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                columns: new[] { "Id", "ClaimType", "ClaimValue", "RoleId" },
                values: new object[,]
                {
                    { new Guid("08c954a0-adc7-4be4-ad63-7c12a74480ab"), "permission", "SuperAdmin", new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad") },
                    { new Guid("4bcadb50-3356-45e9-83d5-c8f0930cbeec"), "permission", "Admin", new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad") }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"), new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("000e83a3-8745-4855-a0d9-094f749d3525"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("0b9521c2-5de9-430f-acc3-d2207b534f0b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("0534015c-2336-48f7-b5a9-f52ba7039854"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("fff47f28-a91c-404b-b0a2-fd3bf2ece51b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("1304bdef-2ffe-4288-88da-268fe88fd266"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("8dbbced1-63d3-43a4-b97a-83f7538ff1fd"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("1b875fbb-4018-4f4f-a70c-3a92f690862f"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("35cc0490-a6c5-42fb-9ccc-4ddbe297d041"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("49643af0-aef1-4726-a1f5-a5cd3bc2b3fc"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("8ee1711d-003b-44c3-be0b-db0648ed25c0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("4ea571d9-98af-46e1-a5d8-c99bc1888020"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("3c12106b-f4f3-41f4-a2a0-8aa1e3f23795"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("667060f3-17db-46ad-9a01-5b6438d4ad73"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("371a3226-1898-4a4c-bd8d-2619102554a1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("9ff05d71-c544-4df2-8f63-1494196a8067"), null, "[]", null, null, false, null, false, null, false, new Guid("c1e7136c-648c-4afd-9661-743741cf33fa"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("e481cdda-5453-4ca2-b120-8ef41e8dcd13"), null, "[]", null, null, false, null, false, null, false, new Guid("954883e5-4f78-476a-b8c2-549767410959"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("e6ee060a-ee56-48da-b69c-c458a9d210b9"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("9055bb6f-5d32-4697-95af-f09cbdde288e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("e73854b7-9e66-4d74-933c-25774840639a"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("14c08ee3-19b9-465d-be20-63f559504269"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("f1148fbd-6358-44e2-b0e3-10b871b98e34"), null, "[]", null, null, false, null, false, null, false, new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("06afac74-13c0-4956-b09f-f68d12ff8e53"), null, "FrameUiGradients", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/uigradients", null },
                    { new Guid("06fbf8b3-d4a6-40bb-9c8a-88f8145ca61f"), null, "FrameRouter", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/vue-router", null },
                    { new Guid("10ed5888-ffce-411e-9b1e-8668d6014ded"), null, "FrameVue", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/vue3", null },
                    { new Guid("33beda04-ea0c-4e2b-9117-6cca201d0741"), null, "FrameTailwindcss", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/tailwindcss", null },
                    { new Guid("562f7150-1d17-4703-b1e3-d1a546307b63"), null, "FrameVite", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/vite", null },
                    { new Guid("577841a3-0f3f-4484-b263-3b4765629854"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), "/external", null },
                    { new Guid("5e62a354-cdcb-4b7f-a83b-75dbc69042b2"), null, "FrameColorHunt", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/colorhunt", null },
                    { new Guid("97fc592a-9ec9-4824-bbce-46bdaf7c6f7c"), null, "FramePinia", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/pinia", null },
                    { new Guid("d2e1706b-7ca8-461a-b1b6-6d6aa3fdbb0b"), null, "FrameEp", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/ep", null },
                    { new Guid("e8a0e573-7416-41d7-ba2d-90e394a96c0e"), null, "https://pure-admin-utils.netlify.app/", new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), "/pureUtilsLink", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("24f833b5-e52c-4863-9142-67f8ac647262"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("06fbf8b3-d4a6-40bb-9c8a-88f8145ca61f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("26c7ce6c-b6b4-4353-94a3-95c7d4ae670a"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("562f7150-1d17-4703-b1e3-d1a546307b63"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("26daf244-0125-47be-ad69-129e2eb38b77"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("06afac74-13c0-4956-b09f-f68d12ff8e53"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("32c99388-3df7-4620-85d7-849043625a5c"), null, "[]", null, null, false, null, false, null, false, new Guid("577841a3-0f3f-4484-b263-3b4765629854"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("87e577a1-f271-447c-b477-945ecb476fa3"), null, "[]", null, null, false, null, false, null, false, new Guid("e8a0e573-7416-41d7-ba2d-90e394a96c0e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("9c5530e4-2a4d-41bd-bc3b-c62107596126"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("5e62a354-cdcb-4b7f-a83b-75dbc69042b2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("e0eb7638-9ccb-49e7-ada1-dec50c755fc6"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("97fc592a-9ec9-4824-bbce-46bdaf7c6f7c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("e8a88f1b-b71e-41ba-bf08-bc07d8db663f"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("d2e1706b-7ca8-461a-b1b6-6d6aa3fdbb0b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("fd9c7397-6766-413e-9abd-8a09a2e59cb7"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("10ed5888-ffce-411e-9b1e-8668d6014ded"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("fe89be55-f54c-4456-ac2c-b53808518208"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("33beda04-ea0c-4e2b-9117-6cca201d0741"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SystemLogOperationLogs");

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("000e83a3-8745-4855-a0d9-094f749d3525"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0534015c-2336-48f7-b5a9-f52ba7039854"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1304bdef-2ffe-4288-88da-268fe88fd266"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1b875fbb-4018-4f4f-a70c-3a92f690862f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("24f833b5-e52c-4863-9142-67f8ac647262"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("26c7ce6c-b6b4-4353-94a3-95c7d4ae670a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("26daf244-0125-47be-ad69-129e2eb38b77"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("32c99388-3df7-4620-85d7-849043625a5c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("466b60ce-eeae-4655-bb4c-d62c5622e163"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("49643af0-aef1-4726-a1f5-a5cd3bc2b3fc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4ea571d9-98af-46e1-a5d8-c99bc1888020"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("64ade514-6930-4c96-9ea6-62f227b6deb5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("667060f3-17db-46ad-9a01-5b6438d4ad73"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("87e577a1-f271-447c-b477-945ecb476fa3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9c5530e4-2a4d-41bd-bc3b-c62107596126"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9ff05d71-c544-4df2-8f63-1494196a8067"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b0f25851-f613-4c2c-aace-14fd68fabf2c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d0a8dffd-42ee-42af-b3d0-888042f4471c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e0eb7638-9ccb-49e7-ada1-dec50c755fc6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e481cdda-5453-4ca2-b120-8ef41e8dcd13"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e6ee060a-ee56-48da-b69c-c458a9d210b9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e73854b7-9e66-4d74-933c-25774840639a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e8a88f1b-b71e-41ba-bf08-bc07d8db663f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f1148fbd-6358-44e2-b0e3-10b871b98e34"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("fd9c7397-6766-413e-9abd-8a09a2e59cb7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("fe89be55-f54c-4456-ac2c-b53808518208"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("feb36e00-c0e9-4c10-9a60-480288962951"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("08c954a0-adc7-4be4-ad63-7c12a74480ab"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("4bcadb50-3356-45e9-83d5-c8f0930cbeec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"), new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("06afac74-13c0-4956-b09f-f68d12ff8e53"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("06fbf8b3-d4a6-40bb-9c8a-88f8145ca61f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0b9521c2-5de9-430f-acc3-d2207b534f0b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("10ed5888-ffce-411e-9b1e-8668d6014ded"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("14c08ee3-19b9-465d-be20-63f559504269"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("33beda04-ea0c-4e2b-9117-6cca201d0741"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("35cc0490-a6c5-42fb-9ccc-4ddbe297d041"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("371a3226-1898-4a4c-bd8d-2619102554a1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3c12106b-f4f3-41f4-a2a0-8aa1e3f23795"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("562f7150-1d17-4703-b1e3-d1a546307b63"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("577841a3-0f3f-4484-b263-3b4765629854"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5e62a354-cdcb-4b7f-a83b-75dbc69042b2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6a23635f-e7ac-439d-b10c-7cb176e703eb"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8dbbced1-63d3-43a4-b97a-83f7538ff1fd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8ee1711d-003b-44c3-be0b-db0648ed25c0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9055bb6f-5d32-4697-95af-f09cbdde288e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("97fc592a-9ec9-4824-bbce-46bdaf7c6f7c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c1e7136c-648c-4afd-9661-743741cf33fa"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d2e1706b-7ca8-461a-b1b6-6d6aa3fdbb0b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e8a0e573-7416-41d7-ba2d-90e394a96c0e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("fff47f28-a91c-404b-b0a2-fd3bf2ece51b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("954883e5-4f78-476a-b8c2-549767410959"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 7, 19, 14, 20, 28, 939, DateTimeKind.Local).AddTicks(3311),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(3048));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 7, 19, 14, 20, 28, 939, DateTimeKind.Local).AddTicks(8079),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(8230));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), null, "permission", null, "/permission", null },
                    { new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), null, "iframe", null, "/iframe", null },
                    { new Guid("7b7f03e3-c645-480f-8034-8acab1de1183"), null, "tabs", null, "/tabs", null },
                    { new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), null, "system", null, "/system", null },
                    { new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), null, "monitor", null, "/monitor", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70"), "a50d9323dbfe72efcde19d8221bb6f82", "gzZNj", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("6c111121-6978-4284-8b39-05b8b7826be5"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("7dae25ed-52fc-43b8-8cef-6ce01a089ca5"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("994a003d-c4de-487f-a1db-84eaafb363ae"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("9f2f07b7-b142-4292-beda-027c2c53e8ff"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("7b7f03e3-c645-480f-8034-8acab1de1183"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("d56054d4-765f-4fa8-8879-645b3216afe2"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("1444a79b-f019-4f02-8f67-b013624bb6ac"), null, "PermissionButton", new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), "/permission/button/index", null },
                    { new Guid("2bcbf010-a893-4e37-8a51-017c6b907f44"), null, "SystemDept", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/dept/index", null },
                    { new Guid("4553bc12-2af2-4a52-964a-4b80fca3c574"), "monitor/logs/login/index", "LoginLog", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/login-logs", null },
                    { new Guid("57201af4-f115-4e2f-9d06-bbdfaf144a98"), null, "PermissionPage", new Guid("38e7032e-53a7-4ba5-81ad-1ecb33e25594"), "/permission/page/index", null },
                    { new Guid("6c3628cb-98a2-4dc3-ae3d-38c1f1bcbe8f"), "monitor/logs/operation/index", "OperationLog", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/operation-logs", null },
                    { new Guid("a8abe87a-7093-4468-90f3-4d42dd80b9f6"), null, "SystemMenu", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/menu/index", null },
                    { new Guid("c973e906-1b05-4799-a366-3bbaca99f636"), null, "SystemUser", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/user/index", null },
                    { new Guid("e0e45243-17ac-4c52-8433-1433521c9b34"), null, "SystemRole", new Guid("b9ac0f51-c90b-466f-9a34-c80028248dc4"), "/system/role/index", null },
                    { new Guid("e48ef7e1-c4ef-437d-ad4c-b1705e82df4a"), "monitor/online/index", "OnlineUser", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/online-user", null },
                    { new Guid("e639a5ce-db88-4011-9986-623717aa41f6"), "monitor/logs/system/index", "SystemLog", new Guid("d59dcaf7-980a-423a-a16a-52ff85f17fd1"), "/monitor/system-logs", null },
                    { new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), null, "Embedded", new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), "/iframe/embedded", null },
                    { new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), null, "External", new Guid("6610f5f5-dd7d-41cc-8d31-97f0a7d857c7"), "/iframe/external", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                columns: new[] { "Id", "ClaimType", "ClaimValue", "RoleId" },
                values: new object[,]
                {
                    { new Guid("9a572fdb-f1e1-4eb8-aa2b-ee74ac7b1836"), "permission", "Admin", new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9") },
                    { new Guid("e11c874f-53e7-4a47-9e64-6ef274e89a60"), "permission", "SuperAdmin", new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9") }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("3be8c15c-b84f-4c80-8eb3-fa12138508b9"), new Guid("5fa8d198-3a6e-45c8-a70b-156eee8c5c70"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("083022da-d021-4124-b25a-139c49436890"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("2bcbf010-a893-4e37-8a51-017c6b907f44"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("0d73716e-cb46-4d4a-bec1-b1b6d9717963"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("c973e906-1b05-4799-a366-3bbaca99f636"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("15817879-116a-4778-bdc7-c3ea5873a5cc"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("e0e45243-17ac-4c52-8433-1433521c9b34"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("2ce8bc3a-ebcd-4dd8-b6cb-2340d934e8c0"), null, "[]", null, null, false, null, false, null, false, new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("2f36c3ab-b4d8-47b1-897c-54db3ad5933a"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("a8abe87a-7093-4468-90f3-4d42dd80b9f6"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("5305a442-9646-4457-ad28-58adb9dd3542"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("e639a5ce-db88-4011-9986-623717aa41f6"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("5def1939-f8ff-42fe-a28a-93d676fa971f"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("6c3628cb-98a2-4dc3-ae3d-38c1f1bcbe8f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("66ba2216-0646-409c-8933-cb9dfbe00761"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("4553bc12-2af2-4a52-964a-4b80fca3c574"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("9b4ff4aa-5605-4aa5-b56a-72c649c791a5"), null, "[]", null, null, false, null, false, null, false, new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("a4af0823-4125-4362-abbe-27f241bb5a9d"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("1444a79b-f019-4f02-8f67-b013624bb6ac"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("a52d0f60-4adf-406f-a5e9-8a7eb3a7ec8c"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("e48ef7e1-c4ef-437d-ad4c-b1705e82df4a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("da99ea91-daf1-4931-b0d1-9a355a0fb9fc"), null, "[]", null, null, false, null, false, null, false, new Guid("57201af4-f115-4e2f-9d06-bbdfaf144a98"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("13b91358-694a-472a-9145-b211689fdece"), null, "FrameColorHunt", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/colorhunt", null },
                    { new Guid("38115f1a-1bd4-4560-a1b7-18bd85c5834f"), null, "FrameEp", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/ep", null },
                    { new Guid("41df1ca3-d595-410d-b007-6be8e3239a92"), null, "FramePinia", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/pinia", null },
                    { new Guid("6d0b84c1-bea0-4639-9cbb-82f1c5c41713"), null, "FrameRouter", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/vue-router", null },
                    { new Guid("6e4fff2d-b6e4-4ebf-9495-96c2b1247496"), null, "FrameVite", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/vite", null },
                    { new Guid("8f2328b6-0209-4ddf-994b-3ec4fee49688"), null, "https://pure-admin-utils.netlify.app/", new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), "/pureUtilsLink", null },
                    { new Guid("91d95578-886d-4465-85d2-f2491001a752"), null, "FrameTailwindcss", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/tailwindcss", null },
                    { new Guid("a5e9cbcf-d29c-48f0-8711-2c4fbf6a18f7"), null, "FrameUiGradients", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/uigradients", null },
                    { new Guid("bd92c334-3821-44fb-9778-03f3ca2dabb4"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("eda18245-17b2-4d5f-a50e-3637a81a000c"), "/external", null },
                    { new Guid("da2b2dd4-bf3c-46aa-bbc5-12515753d49d"), null, "FrameVue", new Guid("e79c4f69-78f0-4d3d-b6ff-d6883d3bf3ad"), "/iframe/vue3", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("11e8dbb2-b51e-439a-bcce-9d0c94ec941d"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("91d95578-886d-4465-85d2-f2491001a752"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("65f53177-1ab1-4733-8d22-d133b169159f"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("13b91358-694a-472a-9145-b211689fdece"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("686b56fe-69c7-49da-8f62-08b28861d212"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("38115f1a-1bd4-4560-a1b7-18bd85c5834f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("84f7c910-41f4-4035-9165-e0c22d3d0bd7"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("6d0b84c1-bea0-4639-9cbb-82f1c5c41713"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("cf9e109d-1449-4159-b89d-bdac09007d88"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("6e4fff2d-b6e4-4ebf-9495-96c2b1247496"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("d6c98d8b-8e8e-41e2-a37b-5e678cf4bc23"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("41df1ca3-d595-410d-b007-6be8e3239a92"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("d73be64a-c8d5-445c-ad11-479db921533c"), null, "[]", null, null, false, null, false, null, false, new Guid("8f2328b6-0209-4ddf-994b-3ec4fee49688"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("de985c17-856f-4860-b927-ba6b40e146d9"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("da2b2dd4-bf3c-46aa-bbc5-12515753d49d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("ec13992d-ebb0-4cf7-b7f0-304e64beea91"), null, "[]", null, null, false, null, false, null, false, new Guid("bd92c334-3821-44fb-9778-03f3ca2dabb4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("f28f22ea-d7d4-497e-a08f-656d38f2d5e0"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("a5e9cbcf-d29c-48f0-8711-2c4fbf6a18f7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" }
                });
        }
    }
}
