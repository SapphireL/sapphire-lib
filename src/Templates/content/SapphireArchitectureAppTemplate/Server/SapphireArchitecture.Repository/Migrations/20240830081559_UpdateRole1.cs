﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class UpdateRole1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("000e83a3-8745-4855-a0d9-094f749d3525"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0534015c-2336-48f7-b5a9-f52ba7039854"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1304bdef-2ffe-4288-88da-268fe88fd266"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1b875fbb-4018-4f4f-a70c-3a92f690862f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("24f833b5-e52c-4863-9142-67f8ac647262"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("26c7ce6c-b6b4-4353-94a3-95c7d4ae670a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("26daf244-0125-47be-ad69-129e2eb38b77"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("32c99388-3df7-4620-85d7-849043625a5c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("466b60ce-eeae-4655-bb4c-d62c5622e163"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("49643af0-aef1-4726-a1f5-a5cd3bc2b3fc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4ea571d9-98af-46e1-a5d8-c99bc1888020"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("64ade514-6930-4c96-9ea6-62f227b6deb5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("667060f3-17db-46ad-9a01-5b6438d4ad73"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("87e577a1-f271-447c-b477-945ecb476fa3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9c5530e4-2a4d-41bd-bc3b-c62107596126"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9ff05d71-c544-4df2-8f63-1494196a8067"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b0f25851-f613-4c2c-aace-14fd68fabf2c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d0a8dffd-42ee-42af-b3d0-888042f4471c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e0eb7638-9ccb-49e7-ada1-dec50c755fc6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e481cdda-5453-4ca2-b120-8ef41e8dcd13"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e6ee060a-ee56-48da-b69c-c458a9d210b9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e73854b7-9e66-4d74-933c-25774840639a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e8a88f1b-b71e-41ba-bf08-bc07d8db663f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f1148fbd-6358-44e2-b0e3-10b871b98e34"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("fd9c7397-6766-413e-9abd-8a09a2e59cb7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("fe89be55-f54c-4456-ac2c-b53808518208"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("feb36e00-c0e9-4c10-9a60-480288962951"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("08c954a0-adc7-4be4-ad63-7c12a74480ab"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                keyColumn: "Id",
                keyValue: new Guid("4bcadb50-3356-45e9-83d5-c8f0930cbeec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"), new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("06afac74-13c0-4956-b09f-f68d12ff8e53"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("06fbf8b3-d4a6-40bb-9c8a-88f8145ca61f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0b9521c2-5de9-430f-acc3-d2207b534f0b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("10ed5888-ffce-411e-9b1e-8668d6014ded"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("14c08ee3-19b9-465d-be20-63f559504269"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("33beda04-ea0c-4e2b-9117-6cca201d0741"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("35cc0490-a6c5-42fb-9ccc-4ddbe297d041"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("371a3226-1898-4a4c-bd8d-2619102554a1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3c12106b-f4f3-41f4-a2a0-8aa1e3f23795"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("562f7150-1d17-4703-b1e3-d1a546307b63"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("577841a3-0f3f-4484-b263-3b4765629854"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5e62a354-cdcb-4b7f-a83b-75dbc69042b2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6a23635f-e7ac-439d-b10c-7cb176e703eb"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8dbbced1-63d3-43a4-b97a-83f7538ff1fd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8ee1711d-003b-44c3-be0b-db0648ed25c0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9055bb6f-5d32-4697-95af-f09cbdde288e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("97fc592a-9ec9-4824-bbce-46bdaf7c6f7c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c1e7136c-648c-4afd-9661-743741cf33fa"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d2e1706b-7ca8-461a-b1b6-6d6aa3fdbb0b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e8a0e573-7416-41d7-ba2d-90e394a96c0e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("fff47f28-a91c-404b-b0a2-fd3bf2ece51b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("954883e5-4f78-476a-b8c2-549767410959"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(1168),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(3048));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(6343),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(8230));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), null, "monitor", null, "/monitor", null },
                    { new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), null, "permission", null, "/permission", null },
                    { new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), null, "system", null, "/system", null },
                    { new Guid("7d117a33-f753-473b-865d-2c572c0d8882"), null, "tabs", null, "/tabs", null },
                    { new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), null, "iframe", null, "/iframe", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243"), "c5401d4177a4a398ffe404b34e01b57c", "WRZbZ", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("22c87f65-812f-42b4-9247-b25c5f06386e"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("62d57b7e-4a20-46a7-99d7-40263e0d91e3"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("86b73f0b-f114-4576-a40d-34cc90d817e6"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("b41e96d9-2c69-4808-921a-8e90eb4d6235"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("7d117a33-f753-473b-865d-2c572c0d8882"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("ffee6af9-8c84-417f-984d-c256cd7cd1e3"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("3773e9d3-ce63-4df6-ae50-0443bf8bdc70"), "monitor/logs/operation/index", "OperationLog", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/operation-logs", null },
                    { new Guid("446f9810-a237-426c-8966-6a5fc8a52412"), "monitor/online/index", "OnlineUser", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/online-user", null },
                    { new Guid("78ca57b6-1b86-43d3-9d25-6a7d87a36d3e"), "monitor/logs/login/index", "LoginLog", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/login-logs", null },
                    { new Guid("792802d4-f54a-407f-93f1-9897d87de861"), null, "SystemRole", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/role/index", null },
                    { new Guid("7972365b-a1d0-4832-805c-98ee95d7f646"), null, "PermissionButton", new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), "/permission/button/index", null },
                    { new Guid("83583a68-b52c-4a5c-9d9e-00d781f49a25"), "monitor/logs/system/index", "SystemLog", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/system-logs", null },
                    { new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), null, "External", new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), "/iframe/external", null },
                    { new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), null, "Embedded", new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), "/iframe/embedded", null },
                    { new Guid("c3c86329-7a36-4828-a34f-6edcabd8d8ec"), null, "SystemMenu", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/menu/index", null },
                    { new Guid("d9257d6f-b8f6-442e-8ccf-16974fa7c063"), null, "SystemUser", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/user/index", null },
                    { new Guid("db1b8189-45b5-42f4-a0d6-0d4f28d4c8b4"), null, "PermissionPage", new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), "/permission/page/index", null },
                    { new Guid("e9d8f1c7-119b-417e-aaf8-03cf480a5e26"), null, "SystemDept", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/dept/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"), new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("04c09211-7794-4b5d-8022-2a0df0d69e79"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("83583a68-b52c-4a5c-9d9e-00d781f49a25"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("09dfdf66-386c-49fc-96b3-c995298fcdcc"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("7972365b-a1d0-4832-805c-98ee95d7f646"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("587556c7-9fe5-4046-812d-7f65e2ce87d7"), null, "[]", null, null, false, null, false, null, false, new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("5ead74e7-fe6b-4602-9dc2-a7df8bbd0bfe"), null, "[]", null, null, false, null, false, null, false, new Guid("db1b8189-45b5-42f4-a0d6-0d4f28d4c8b4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("6d345d12-0d73-44d9-b435-a6d02d87a7c9"), null, "[]", null, null, false, null, false, null, false, new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("7bc41d58-c0e3-40e7-aa88-c4ce1cfccfae"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("78ca57b6-1b86-43d3-9d25-6a7d87a36d3e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("8155c423-c232-40e7-869b-137ff5677cb7"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("e9d8f1c7-119b-417e-aaf8-03cf480a5e26"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("908d64c4-fc6a-4b0d-84cb-60c176f37679"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("d9257d6f-b8f6-442e-8ccf-16974fa7c063"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("a63ea311-ad43-439d-8515-c91459cf72b1"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("c3c86329-7a36-4828-a34f-6edcabd8d8ec"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("c4b8cf6b-9ea7-4cb6-a98d-a2503eac1354"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("446f9810-a237-426c-8966-6a5fc8a52412"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("e5db1205-6aca-4a66-a6b2-59c1b0e0e697"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("3773e9d3-ce63-4df6-ae50-0443bf8bdc70"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("f56a871b-3a69-4f0d-a1e4-e5903074a2ca"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("792802d4-f54a-407f-93f1-9897d87de861"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0b548967-8b84-4e30-87a2-076deda47477"), null, "FrameUiGradients", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/uigradients", null },
                    { new Guid("1970deca-cf70-4464-b93e-16b52f3f8379"), null, "FrameColorHunt", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/colorhunt", null },
                    { new Guid("45d55af0-4f1e-459c-8106-f4ac8f98fb69"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), "/external", null },
                    { new Guid("5250f805-0eaa-4da5-ad15-6ec3d48ac56f"), null, "FrameVue", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/vue3", null },
                    { new Guid("6089c186-8ad8-4675-ad0a-99988a08e36d"), null, "https://pure-admin-utils.netlify.app/", new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), "/pureUtilsLink", null },
                    { new Guid("611b5c9c-f36b-4c60-a5c6-3286f0f4ace8"), null, "FrameVite", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/vite", null },
                    { new Guid("6154d0b6-9ce2-4d93-982d-c09f8dee1a67"), null, "FramePinia", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/pinia", null },
                    { new Guid("bc798349-a99b-42f8-aff5-aa3657d68644"), null, "FrameRouter", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/vue-router", null },
                    { new Guid("e98ed208-a665-463a-9ee4-320b72e4401a"), null, "FrameEp", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/ep", null },
                    { new Guid("f2f20528-4ed7-4042-91ea-0c91edd7e8ae"), null, "FrameTailwindcss", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/tailwindcss", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0cef2233-4214-4ff4-8e7c-de21e7d7c3d8"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("bc798349-a99b-42f8-aff5-aa3657d68644"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("11446bd1-d72f-4241-8957-d5ac421e640f"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("f2f20528-4ed7-4042-91ea-0c91edd7e8ae"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("24d54f19-23ac-4f90-a208-edc5014ce382"), null, "[]", null, null, false, null, false, null, false, new Guid("6089c186-8ad8-4675-ad0a-99988a08e36d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("2dd7bcb1-2f78-4915-bac8-17829bb7f47c"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("0b548967-8b84-4e30-87a2-076deda47477"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("44d17302-c9c6-4482-97f1-b3d1675fec32"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("6154d0b6-9ce2-4d93-982d-c09f8dee1a67"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("592026e8-24f0-462e-8f65-b5f9699d6b02"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("611b5c9c-f36b-4c60-a5c6-3286f0f4ace8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("60f534fe-d677-463d-a859-2846d4846905"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("e98ed208-a665-463a-9ee4-320b72e4401a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("7540c131-9661-4712-94b5-f4f7b461a625"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("5250f805-0eaa-4da5-ad15-6ec3d48ac56f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("de05b050-a5cc-45e6-8552-12eb0a68dac5"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("1970deca-cf70-4464-b93e-16b52f3f8379"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("f90949e3-a1df-46d2-be0b-2987cdc368ef"), null, "[]", null, null, false, null, false, null, false, new Guid("45d55af0-4f1e-459c-8106-f4ac8f98fb69"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("04c09211-7794-4b5d-8022-2a0df0d69e79"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("09dfdf66-386c-49fc-96b3-c995298fcdcc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0cef2233-4214-4ff4-8e7c-de21e7d7c3d8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("11446bd1-d72f-4241-8957-d5ac421e640f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("22c87f65-812f-42b4-9247-b25c5f06386e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("24d54f19-23ac-4f90-a208-edc5014ce382"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2dd7bcb1-2f78-4915-bac8-17829bb7f47c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("44d17302-c9c6-4482-97f1-b3d1675fec32"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("587556c7-9fe5-4046-812d-7f65e2ce87d7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("592026e8-24f0-462e-8f65-b5f9699d6b02"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5ead74e7-fe6b-4602-9dc2-a7df8bbd0bfe"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("60f534fe-d677-463d-a859-2846d4846905"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("62d57b7e-4a20-46a7-99d7-40263e0d91e3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6d345d12-0d73-44d9-b435-a6d02d87a7c9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7540c131-9661-4712-94b5-f4f7b461a625"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7bc41d58-c0e3-40e7-aa88-c4ce1cfccfae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("8155c423-c232-40e7-869b-137ff5677cb7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("86b73f0b-f114-4576-a40d-34cc90d817e6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("908d64c4-fc6a-4b0d-84cb-60c176f37679"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a63ea311-ad43-439d-8515-c91459cf72b1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b41e96d9-2c69-4808-921a-8e90eb4d6235"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c4b8cf6b-9ea7-4cb6-a98d-a2503eac1354"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("de05b050-a5cc-45e6-8552-12eb0a68dac5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e5db1205-6aca-4a66-a6b2-59c1b0e0e697"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f56a871b-3a69-4f0d-a1e4-e5903074a2ca"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f90949e3-a1df-46d2-be0b-2987cdc368ef"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ffee6af9-8c84-417f-984d-c256cd7cd1e3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"), new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0b548967-8b84-4e30-87a2-076deda47477"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1970deca-cf70-4464-b93e-16b52f3f8379"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3773e9d3-ce63-4df6-ae50-0443bf8bdc70"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("446f9810-a237-426c-8966-6a5fc8a52412"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("45d55af0-4f1e-459c-8106-f4ac8f98fb69"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5250f805-0eaa-4da5-ad15-6ec3d48ac56f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6089c186-8ad8-4675-ad0a-99988a08e36d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("611b5c9c-f36b-4c60-a5c6-3286f0f4ace8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6154d0b6-9ce2-4d93-982d-c09f8dee1a67"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("78ca57b6-1b86-43d3-9d25-6a7d87a36d3e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("792802d4-f54a-407f-93f1-9897d87de861"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7972365b-a1d0-4832-805c-98ee95d7f646"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7d117a33-f753-473b-865d-2c572c0d8882"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("83583a68-b52c-4a5c-9d9e-00d781f49a25"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bc798349-a99b-42f8-aff5-aa3657d68644"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c3c86329-7a36-4828-a34f-6edcabd8d8ec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d9257d6f-b8f6-442e-8ccf-16974fa7c063"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("db1b8189-45b5-42f4-a0d6-0d4f28d4c8b4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e98ed208-a665-463a-9ee4-320b72e4401a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e9d8f1c7-119b-417e-aaf8-03cf480a5e26"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f2f20528-4ed7-4042-91ea-0c91edd7e8ae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(3048),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(1168));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 14, 40, 1, 68, DateTimeKind.Local).AddTicks(8230),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(6343));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), null, "iframe", null, "/iframe", null },
                    { new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), null, "permission", null, "/permission", null },
                    { new Guid("6a23635f-e7ac-439d-b10c-7cb176e703eb"), null, "tabs", null, "/tabs", null },
                    { new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), null, "monitor", null, "/monitor", null },
                    { new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), null, "system", null, "/system", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db"), "c72cca1092a7e206daada912d19f879b", "OKo3Y", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("466b60ce-eeae-4655-bb4c-d62c5622e163"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("64ade514-6930-4c96-9ea6-62f227b6deb5"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("b0f25851-f613-4c2c-aace-14fd68fabf2c"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("6a23635f-e7ac-439d-b10c-7cb176e703eb"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("d0a8dffd-42ee-42af-b3d0-888042f4471c"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("feb36e00-c0e9-4c10-9a60-480288962951"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0b9521c2-5de9-430f-acc3-d2207b534f0b"), "monitor/online/index", "OnlineUser", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/online-user", null },
                    { new Guid("14c08ee3-19b9-465d-be20-63f559504269"), null, "SystemRole", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/role/index", null },
                    { new Guid("35cc0490-a6c5-42fb-9ccc-4ddbe297d041"), "monitor/logs/operation/index", "OperationLog", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/operation-logs", null },
                    { new Guid("371a3226-1898-4a4c-bd8d-2619102554a1"), "monitor/logs/login/index", "LoginLog", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/login-logs", null },
                    { new Guid("3c12106b-f4f3-41f4-a2a0-8aa1e3f23795"), "monitor/logs/system/index", "SystemLog", new Guid("b1c8e0e6-89f4-4610-8a27-bb94002252e7"), "/monitor/system-logs", null },
                    { new Guid("8dbbced1-63d3-43a4-b97a-83f7538ff1fd"), null, "SystemMenu", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/menu/index", null },
                    { new Guid("8ee1711d-003b-44c3-be0b-db0648ed25c0"), null, "SystemUser", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/user/index", null },
                    { new Guid("9055bb6f-5d32-4697-95af-f09cbdde288e"), null, "SystemDept", new Guid("bb7cfb5a-65e5-48aa-a2cd-a999f838da98"), "/system/dept/index", null },
                    { new Guid("954883e5-4f78-476a-b8c2-549767410959"), null, "Embedded", new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), "/iframe/embedded", null },
                    { new Guid("c1e7136c-648c-4afd-9661-743741cf33fa"), null, "PermissionPage", new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), "/permission/page/index", null },
                    { new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), null, "External", new Guid("03623766-1b6a-4d15-a9e6-950ad64b8344"), "/iframe/external", null },
                    { new Guid("fff47f28-a91c-404b-b0a2-fd3bf2ece51b"), null, "PermissionButton", new Guid("34940ab0-dc30-4e93-a633-d4ee8ad4035b"), "/permission/button/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_RoleClaims",
                columns: new[] { "Id", "ClaimType", "ClaimValue", "RoleId" },
                values: new object[,]
                {
                    { new Guid("08c954a0-adc7-4be4-ad63-7c12a74480ab"), "permission", "SuperAdmin", new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad") },
                    { new Guid("4bcadb50-3356-45e9-83d5-c8f0930cbeec"), "permission", "Admin", new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad") }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("bdaf2451-ac18-4aca-9717-bfc9faf942ad"), new Guid("2e9276b2-5489-489c-8d61-ac59a62ab7db"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("000e83a3-8745-4855-a0d9-094f749d3525"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("0b9521c2-5de9-430f-acc3-d2207b534f0b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("0534015c-2336-48f7-b5a9-f52ba7039854"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("fff47f28-a91c-404b-b0a2-fd3bf2ece51b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("1304bdef-2ffe-4288-88da-268fe88fd266"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("8dbbced1-63d3-43a4-b97a-83f7538ff1fd"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("1b875fbb-4018-4f4f-a70c-3a92f690862f"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("35cc0490-a6c5-42fb-9ccc-4ddbe297d041"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("49643af0-aef1-4726-a1f5-a5cd3bc2b3fc"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("8ee1711d-003b-44c3-be0b-db0648ed25c0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("4ea571d9-98af-46e1-a5d8-c99bc1888020"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("3c12106b-f4f3-41f4-a2a0-8aa1e3f23795"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("667060f3-17db-46ad-9a01-5b6438d4ad73"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("371a3226-1898-4a4c-bd8d-2619102554a1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("9ff05d71-c544-4df2-8f63-1494196a8067"), null, "[]", null, null, false, null, false, null, false, new Guid("c1e7136c-648c-4afd-9661-743741cf33fa"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("e481cdda-5453-4ca2-b120-8ef41e8dcd13"), null, "[]", null, null, false, null, false, null, false, new Guid("954883e5-4f78-476a-b8c2-549767410959"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("e6ee060a-ee56-48da-b69c-c458a9d210b9"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("9055bb6f-5d32-4697-95af-f09cbdde288e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("e73854b7-9e66-4d74-933c-25774840639a"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("14c08ee3-19b9-465d-be20-63f559504269"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("f1148fbd-6358-44e2-b0e3-10b871b98e34"), null, "[]", null, null, false, null, false, null, false, new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("06afac74-13c0-4956-b09f-f68d12ff8e53"), null, "FrameUiGradients", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/uigradients", null },
                    { new Guid("06fbf8b3-d4a6-40bb-9c8a-88f8145ca61f"), null, "FrameRouter", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/vue-router", null },
                    { new Guid("10ed5888-ffce-411e-9b1e-8668d6014ded"), null, "FrameVue", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/vue3", null },
                    { new Guid("33beda04-ea0c-4e2b-9117-6cca201d0741"), null, "FrameTailwindcss", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/tailwindcss", null },
                    { new Guid("562f7150-1d17-4703-b1e3-d1a546307b63"), null, "FrameVite", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/vite", null },
                    { new Guid("577841a3-0f3f-4484-b263-3b4765629854"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), "/external", null },
                    { new Guid("5e62a354-cdcb-4b7f-a83b-75dbc69042b2"), null, "FrameColorHunt", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/colorhunt", null },
                    { new Guid("97fc592a-9ec9-4824-bbce-46bdaf7c6f7c"), null, "FramePinia", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/pinia", null },
                    { new Guid("d2e1706b-7ca8-461a-b1b6-6d6aa3fdbb0b"), null, "FrameEp", new Guid("954883e5-4f78-476a-b8c2-549767410959"), "/iframe/ep", null },
                    { new Guid("e8a0e573-7416-41d7-ba2d-90e394a96c0e"), null, "https://pure-admin-utils.netlify.app/", new Guid("d0bbd434-cd70-461d-84a9-a8d88a75e07f"), "/pureUtilsLink", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("24f833b5-e52c-4863-9142-67f8ac647262"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("06fbf8b3-d4a6-40bb-9c8a-88f8145ca61f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("26c7ce6c-b6b4-4353-94a3-95c7d4ae670a"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("562f7150-1d17-4703-b1e3-d1a546307b63"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("26daf244-0125-47be-ad69-129e2eb38b77"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("06afac74-13c0-4956-b09f-f68d12ff8e53"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("32c99388-3df7-4620-85d7-849043625a5c"), null, "[]", null, null, false, null, false, null, false, new Guid("577841a3-0f3f-4484-b263-3b4765629854"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("87e577a1-f271-447c-b477-945ecb476fa3"), null, "[]", null, null, false, null, false, null, false, new Guid("e8a0e573-7416-41d7-ba2d-90e394a96c0e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("9c5530e4-2a4d-41bd-bc3b-c62107596126"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("5e62a354-cdcb-4b7f-a83b-75dbc69042b2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("e0eb7638-9ccb-49e7-ada1-dec50c755fc6"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("97fc592a-9ec9-4824-bbce-46bdaf7c6f7c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("e8a88f1b-b71e-41ba-bf08-bc07d8db663f"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("d2e1706b-7ca8-461a-b1b6-6d6aa3fdbb0b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("fd9c7397-6766-413e-9abd-8a09a2e59cb7"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("10ed5888-ffce-411e-9b1e-8668d6014ded"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("fe89be55-f54c-4456-ac2c-b53808518208"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("33beda04-ea0c-4e2b-9117-6cca201d0741"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" }
                });
        }
    }
}
