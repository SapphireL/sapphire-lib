﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class UpdateRole2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("04c09211-7794-4b5d-8022-2a0df0d69e79"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("09dfdf66-386c-49fc-96b3-c995298fcdcc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0cef2233-4214-4ff4-8e7c-de21e7d7c3d8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("11446bd1-d72f-4241-8957-d5ac421e640f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("22c87f65-812f-42b4-9247-b25c5f06386e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("24d54f19-23ac-4f90-a208-edc5014ce382"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2dd7bcb1-2f78-4915-bac8-17829bb7f47c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("44d17302-c9c6-4482-97f1-b3d1675fec32"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("587556c7-9fe5-4046-812d-7f65e2ce87d7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("592026e8-24f0-462e-8f65-b5f9699d6b02"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5ead74e7-fe6b-4602-9dc2-a7df8bbd0bfe"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("60f534fe-d677-463d-a859-2846d4846905"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("62d57b7e-4a20-46a7-99d7-40263e0d91e3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6d345d12-0d73-44d9-b435-a6d02d87a7c9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7540c131-9661-4712-94b5-f4f7b461a625"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7bc41d58-c0e3-40e7-aa88-c4ce1cfccfae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("8155c423-c232-40e7-869b-137ff5677cb7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("86b73f0b-f114-4576-a40d-34cc90d817e6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("908d64c4-fc6a-4b0d-84cb-60c176f37679"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a63ea311-ad43-439d-8515-c91459cf72b1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b41e96d9-2c69-4808-921a-8e90eb4d6235"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c4b8cf6b-9ea7-4cb6-a98d-a2503eac1354"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("de05b050-a5cc-45e6-8552-12eb0a68dac5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e5db1205-6aca-4a66-a6b2-59c1b0e0e697"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f56a871b-3a69-4f0d-a1e4-e5903074a2ca"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f90949e3-a1df-46d2-be0b-2987cdc368ef"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ffee6af9-8c84-417f-984d-c256cd7cd1e3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"), new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0b548967-8b84-4e30-87a2-076deda47477"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1970deca-cf70-4464-b93e-16b52f3f8379"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3773e9d3-ce63-4df6-ae50-0443bf8bdc70"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("446f9810-a237-426c-8966-6a5fc8a52412"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("45d55af0-4f1e-459c-8106-f4ac8f98fb69"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5250f805-0eaa-4da5-ad15-6ec3d48ac56f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6089c186-8ad8-4675-ad0a-99988a08e36d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("611b5c9c-f36b-4c60-a5c6-3286f0f4ace8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6154d0b6-9ce2-4d93-982d-c09f8dee1a67"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("78ca57b6-1b86-43d3-9d25-6a7d87a36d3e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("792802d4-f54a-407f-93f1-9897d87de861"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7972365b-a1d0-4832-805c-98ee95d7f646"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7d117a33-f753-473b-865d-2c572c0d8882"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("83583a68-b52c-4a5c-9d9e-00d781f49a25"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bc798349-a99b-42f8-aff5-aa3657d68644"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c3c86329-7a36-4828-a34f-6edcabd8d8ec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d9257d6f-b8f6-442e-8ccf-16974fa7c063"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("db1b8189-45b5-42f4-a0d6-0d4f28d4c8b4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e98ed208-a665-463a-9ee4-320b72e4401a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e9d8f1c7-119b-417e-aaf8-03cf480a5e26"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f2f20528-4ed7-4042-91ea-0c91edd7e8ae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 261, DateTimeKind.Local).AddTicks(4968),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(1168));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 262, DateTimeKind.Local).AddTicks(533),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(6343));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), null, "iframe", null, "/iframe", null },
                    { new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), null, "system", null, "/system", null },
                    { new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), null, "monitor", null, "/monitor", null },
                    { new Guid("bed411fb-3e16-4a13-bf32-80ecd87bd4b0"), null, "tabs", null, "/tabs", null },
                    { new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), null, "permission", null, "/permission", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335"), "b9bad996bbb9400ad48514e91a7de341", "T9qHs", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("10f31450-b299-4381-86cd-df76fa212162"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("10f4cada-351d-4a5e-89d7-f42b93c6430e"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("6a9b3d3c-c9a0-4cae-8e8d-1c1d99a95337"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("7133fab3-6ea2-433c-944d-055efc859256"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("bed411fb-3e16-4a13-bf32-80ecd87bd4b0"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("c99573d7-e6ff-4a17-bb2c-bf34fba178c1"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("1acd7614-c2e0-4928-a266-359d342d98d3"), "monitor/logs/operation/index", "OperationLog", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/operation-logs", null },
                    { new Guid("23d91ad3-e3f7-4401-85cf-4584b6f46895"), null, "PermissionPage", new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), "/permission/page/index", null },
                    { new Guid("47f5c87e-ade3-475f-a53e-1ceaa888a0c1"), null, "SystemMenu", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/menu/index", null },
                    { new Guid("51ce0c8f-4d09-4121-ae58-87572d5a3f7d"), null, "PermissionButton", new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), "/permission/button/index", null },
                    { new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), null, "External", new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), "/iframe/external", null },
                    { new Guid("70826b3b-3701-420e-b46c-ae3811dbd324"), "monitor/logs/system/index", "SystemLog", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/system-logs", null },
                    { new Guid("a7e012df-f47b-47a2-8da6-c2de3877f4a4"), null, "SystemDept", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/dept/index", null },
                    { new Guid("c1dbf48a-bc43-423b-9063-f1fd05315042"), "monitor/online/index", "OnlineUser", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/online-user", null },
                    { new Guid("cbda6d44-3c9d-484c-bff5-f7e3184ac888"), "monitor/logs/login/index", "LoginLog", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/login-logs", null },
                    { new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), null, "Embedded", new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), "/iframe/embedded", null },
                    { new Guid("e2000791-2454-423e-9f99-31f02ea06038"), null, "SystemUser", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/user/index", null },
                    { new Guid("e283dd22-220c-4100-a190-0316562661c2"), null, "SystemRole", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/role/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"), new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0e894014-9cfb-4b11-bfcd-64d6519d4c5f"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("e2000791-2454-423e-9f99-31f02ea06038"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("135d27a4-199a-472a-87d2-fd60561fbeba"), null, "[]", null, null, false, null, false, null, false, new Guid("23d91ad3-e3f7-4401-85cf-4584b6f46895"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("2d13c6a6-9399-4cb0-8808-7f285792ccd9"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("1acd7614-c2e0-4928-a266-359d342d98d3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("4e04f5d2-b523-4991-bd98-95f4d040d14b"), null, "[]", null, null, false, null, false, null, false, new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("75f85f30-0ee9-4ae1-86b1-1bf00a373202"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("c1dbf48a-bc43-423b-9063-f1fd05315042"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("7c7ea5ca-2b10-41f6-ac0c-25546d046308"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("51ce0c8f-4d09-4121-ae58-87572d5a3f7d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("b1c5c604-1572-44bc-a065-7976b56f2ba1"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("e283dd22-220c-4100-a190-0316562661c2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("b673112a-1de4-4ab8-99bf-727e940edf1f"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("cbda6d44-3c9d-484c-bff5-f7e3184ac888"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("c66f41fb-b426-4d0d-8f1b-8f26a9be8385"), null, "[]", null, null, false, null, false, null, false, new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("c8650940-d2ef-4ca2-9d19-7b48af92e501"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("70826b3b-3701-420e-b46c-ae3811dbd324"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("e9b57140-5a08-4042-908d-3f89933e5133"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("a7e012df-f47b-47a2-8da6-c2de3877f4a4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("f88a6bf6-66dd-4e90-8880-c6cd70aa51be"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("47f5c87e-ade3-475f-a53e-1ceaa888a0c1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("13890b02-3405-440c-a5ab-567832fd764b"), null, "FrameEp", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/ep", null },
                    { new Guid("19676894-bcc9-42dd-a6f3-915fab85a6ff"), null, "FrameVite", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/vite", null },
                    { new Guid("3c26dc0e-68b2-4328-84b8-5360b75c3632"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), "/external", null },
                    { new Guid("463f95ae-d2d6-4623-b18a-728b19d102b0"), null, "FrameVue", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/vue3", null },
                    { new Guid("55d9d8c2-c034-4608-ac84-211644942ca6"), null, "FrameColorHunt", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/colorhunt", null },
                    { new Guid("5d4e0050-e3b9-4b12-bfa9-9663c2b1c930"), null, "FrameRouter", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/vue-router", null },
                    { new Guid("646c9f95-fcc4-49d7-b3a9-9b5ef893454c"), null, "https://pure-admin-utils.netlify.app/", new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), "/pureUtilsLink", null },
                    { new Guid("872350b6-a5ea-42ac-beff-407c9fc1af32"), null, "FrameTailwindcss", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/tailwindcss", null },
                    { new Guid("9fd4ffe9-ed72-4d82-938d-f2678a012394"), null, "FramePinia", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/pinia", null },
                    { new Guid("c302b5f4-3f34-4f87-a01f-1ef20429a7a1"), null, "FrameUiGradients", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/uigradients", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0423bb7e-7eac-4c77-ba68-ccff1030d6c8"), null, "[]", null, null, false, null, false, null, false, new Guid("646c9f95-fcc4-49d7-b3a9-9b5ef893454c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("75b09ae6-3847-4c7b-8916-25cf123ac4b5"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("5d4e0050-e3b9-4b12-bfa9-9663c2b1c930"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("b3d676ae-925d-48b0-ad26-a99099cb6a10"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("872350b6-a5ea-42ac-beff-407c9fc1af32"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("be9933cc-f1f1-4518-9d67-a66b49b380c9"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("c302b5f4-3f34-4f87-a01f-1ef20429a7a1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("bfe0aa8a-837b-4280-bb0d-f8fd284bf3a7"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("55d9d8c2-c034-4608-ac84-211644942ca6"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("c9e0310f-ee0e-4e09-a130-e93a0445cf67"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("463f95ae-d2d6-4623-b18a-728b19d102b0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("d3e1e32a-74ce-4412-bc5f-38938573e487"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("9fd4ffe9-ed72-4d82-938d-f2678a012394"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("d96dae6d-e65b-46b3-b7b2-0319cf24781c"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("19676894-bcc9-42dd-a6f3-915fab85a6ff"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("f1354533-537b-42ce-ae02-eeca9c251f62"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("13890b02-3405-440c-a5ab-567832fd764b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("f7457afc-fe05-4aee-aa06-ffd155aff5be"), null, "[]", null, null, false, null, false, null, false, new Guid("3c26dc0e-68b2-4328-84b8-5360b75c3632"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0423bb7e-7eac-4c77-ba68-ccff1030d6c8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0e894014-9cfb-4b11-bfcd-64d6519d4c5f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("10f31450-b299-4381-86cd-df76fa212162"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("10f4cada-351d-4a5e-89d7-f42b93c6430e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("135d27a4-199a-472a-87d2-fd60561fbeba"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2d13c6a6-9399-4cb0-8808-7f285792ccd9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4e04f5d2-b523-4991-bd98-95f4d040d14b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6a9b3d3c-c9a0-4cae-8e8d-1c1d99a95337"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7133fab3-6ea2-433c-944d-055efc859256"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("75b09ae6-3847-4c7b-8916-25cf123ac4b5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("75f85f30-0ee9-4ae1-86b1-1bf00a373202"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7c7ea5ca-2b10-41f6-ac0c-25546d046308"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b1c5c604-1572-44bc-a065-7976b56f2ba1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b3d676ae-925d-48b0-ad26-a99099cb6a10"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b673112a-1de4-4ab8-99bf-727e940edf1f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("be9933cc-f1f1-4518-9d67-a66b49b380c9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("bfe0aa8a-837b-4280-bb0d-f8fd284bf3a7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c66f41fb-b426-4d0d-8f1b-8f26a9be8385"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c8650940-d2ef-4ca2-9d19-7b48af92e501"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c99573d7-e6ff-4a17-bb2c-bf34fba178c1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c9e0310f-ee0e-4e09-a130-e93a0445cf67"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d3e1e32a-74ce-4412-bc5f-38938573e487"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d96dae6d-e65b-46b3-b7b2-0319cf24781c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e9b57140-5a08-4042-908d-3f89933e5133"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f1354533-537b-42ce-ae02-eeca9c251f62"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f7457afc-fe05-4aee-aa06-ffd155aff5be"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f88a6bf6-66dd-4e90-8880-c6cd70aa51be"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"), new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("13890b02-3405-440c-a5ab-567832fd764b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("19676894-bcc9-42dd-a6f3-915fab85a6ff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1acd7614-c2e0-4928-a266-359d342d98d3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("23d91ad3-e3f7-4401-85cf-4584b6f46895"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3c26dc0e-68b2-4328-84b8-5360b75c3632"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("463f95ae-d2d6-4623-b18a-728b19d102b0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("47f5c87e-ade3-475f-a53e-1ceaa888a0c1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("51ce0c8f-4d09-4121-ae58-87572d5a3f7d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("55d9d8c2-c034-4608-ac84-211644942ca6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5d4e0050-e3b9-4b12-bfa9-9663c2b1c930"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("646c9f95-fcc4-49d7-b3a9-9b5ef893454c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("70826b3b-3701-420e-b46c-ae3811dbd324"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("872350b6-a5ea-42ac-beff-407c9fc1af32"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9fd4ffe9-ed72-4d82-938d-f2678a012394"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a7e012df-f47b-47a2-8da6-c2de3877f4a4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bed411fb-3e16-4a13-bf32-80ecd87bd4b0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c1dbf48a-bc43-423b-9063-f1fd05315042"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c302b5f4-3f34-4f87-a01f-1ef20429a7a1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("cbda6d44-3c9d-484c-bff5-f7e3184ac888"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e2000791-2454-423e-9f99-31f02ea06038"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e283dd22-220c-4100-a190-0316562661c2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("25d67109-6edd-44a6-893c-a27c18c82072"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(1168),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 261, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 15, 58, 767, DateTimeKind.Local).AddTicks(6343),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 262, DateTimeKind.Local).AddTicks(533));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), null, "monitor", null, "/monitor", null },
                    { new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), null, "permission", null, "/permission", null },
                    { new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), null, "system", null, "/system", null },
                    { new Guid("7d117a33-f753-473b-865d-2c572c0d8882"), null, "tabs", null, "/tabs", null },
                    { new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), null, "iframe", null, "/iframe", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243"), "c5401d4177a4a398ffe404b34e01b57c", "WRZbZ", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("22c87f65-812f-42b4-9247-b25c5f06386e"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("62d57b7e-4a20-46a7-99d7-40263e0d91e3"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("86b73f0b-f114-4576-a40d-34cc90d817e6"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("b41e96d9-2c69-4808-921a-8e90eb4d6235"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("7d117a33-f753-473b-865d-2c572c0d8882"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("ffee6af9-8c84-417f-984d-c256cd7cd1e3"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("3773e9d3-ce63-4df6-ae50-0443bf8bdc70"), "monitor/logs/operation/index", "OperationLog", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/operation-logs", null },
                    { new Guid("446f9810-a237-426c-8966-6a5fc8a52412"), "monitor/online/index", "OnlineUser", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/online-user", null },
                    { new Guid("78ca57b6-1b86-43d3-9d25-6a7d87a36d3e"), "monitor/logs/login/index", "LoginLog", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/login-logs", null },
                    { new Guid("792802d4-f54a-407f-93f1-9897d87de861"), null, "SystemRole", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/role/index", null },
                    { new Guid("7972365b-a1d0-4832-805c-98ee95d7f646"), null, "PermissionButton", new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), "/permission/button/index", null },
                    { new Guid("83583a68-b52c-4a5c-9d9e-00d781f49a25"), "monitor/logs/system/index", "SystemLog", new Guid("21d49d15-ee94-4e87-9622-761ab7ecaab4"), "/monitor/system-logs", null },
                    { new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), null, "External", new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), "/iframe/external", null },
                    { new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), null, "Embedded", new Guid("f4fcfd9a-2fe5-4923-bf90-86605bafcd51"), "/iframe/embedded", null },
                    { new Guid("c3c86329-7a36-4828-a34f-6edcabd8d8ec"), null, "SystemMenu", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/menu/index", null },
                    { new Guid("d9257d6f-b8f6-442e-8ccf-16974fa7c063"), null, "SystemUser", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/user/index", null },
                    { new Guid("db1b8189-45b5-42f4-a0d6-0d4f28d4c8b4"), null, "PermissionPage", new Guid("29249f58-7568-40ac-9986-5c49f92ad54b"), "/permission/page/index", null },
                    { new Guid("e9d8f1c7-119b-417e-aaf8-03cf480a5e26"), null, "SystemDept", new Guid("602412cc-2fab-4cba-90f5-fedaebc02dac"), "/system/dept/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("78d28299-b03c-4ff8-8a5f-980cb8e82aee"), new Guid("63ed9ebb-fec3-4360-82a0-d1b3e2c05243"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("04c09211-7794-4b5d-8022-2a0df0d69e79"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("83583a68-b52c-4a5c-9d9e-00d781f49a25"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("09dfdf66-386c-49fc-96b3-c995298fcdcc"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("7972365b-a1d0-4832-805c-98ee95d7f646"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("587556c7-9fe5-4046-812d-7f65e2ce87d7"), null, "[]", null, null, false, null, false, null, false, new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("5ead74e7-fe6b-4602-9dc2-a7df8bbd0bfe"), null, "[]", null, null, false, null, false, null, false, new Guid("db1b8189-45b5-42f4-a0d6-0d4f28d4c8b4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("6d345d12-0d73-44d9-b435-a6d02d87a7c9"), null, "[]", null, null, false, null, false, null, false, new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("7bc41d58-c0e3-40e7-aa88-c4ce1cfccfae"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("78ca57b6-1b86-43d3-9d25-6a7d87a36d3e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("8155c423-c232-40e7-869b-137ff5677cb7"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("e9d8f1c7-119b-417e-aaf8-03cf480a5e26"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("908d64c4-fc6a-4b0d-84cb-60c176f37679"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("d9257d6f-b8f6-442e-8ccf-16974fa7c063"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("a63ea311-ad43-439d-8515-c91459cf72b1"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("c3c86329-7a36-4828-a34f-6edcabd8d8ec"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("c4b8cf6b-9ea7-4cb6-a98d-a2503eac1354"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("446f9810-a237-426c-8966-6a5fc8a52412"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("e5db1205-6aca-4a66-a6b2-59c1b0e0e697"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("3773e9d3-ce63-4df6-ae50-0443bf8bdc70"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("f56a871b-3a69-4f0d-a1e4-e5903074a2ca"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("792802d4-f54a-407f-93f1-9897d87de861"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0b548967-8b84-4e30-87a2-076deda47477"), null, "FrameUiGradients", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/uigradients", null },
                    { new Guid("1970deca-cf70-4464-b93e-16b52f3f8379"), null, "FrameColorHunt", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/colorhunt", null },
                    { new Guid("45d55af0-4f1e-459c-8106-f4ac8f98fb69"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), "/external", null },
                    { new Guid("5250f805-0eaa-4da5-ad15-6ec3d48ac56f"), null, "FrameVue", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/vue3", null },
                    { new Guid("6089c186-8ad8-4675-ad0a-99988a08e36d"), null, "https://pure-admin-utils.netlify.app/", new Guid("8806610c-4bf2-4e2e-adfc-cf3a5ecc4303"), "/pureUtilsLink", null },
                    { new Guid("611b5c9c-f36b-4c60-a5c6-3286f0f4ace8"), null, "FrameVite", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/vite", null },
                    { new Guid("6154d0b6-9ce2-4d93-982d-c09f8dee1a67"), null, "FramePinia", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/pinia", null },
                    { new Guid("bc798349-a99b-42f8-aff5-aa3657d68644"), null, "FrameRouter", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/vue-router", null },
                    { new Guid("e98ed208-a665-463a-9ee4-320b72e4401a"), null, "FrameEp", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/ep", null },
                    { new Guid("f2f20528-4ed7-4042-91ea-0c91edd7e8ae"), null, "FrameTailwindcss", new Guid("b82b4c29-64b7-4d0d-9cae-3d14c660c271"), "/iframe/tailwindcss", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0cef2233-4214-4ff4-8e7c-de21e7d7c3d8"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("bc798349-a99b-42f8-aff5-aa3657d68644"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("11446bd1-d72f-4241-8957-d5ac421e640f"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("f2f20528-4ed7-4042-91ea-0c91edd7e8ae"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("24d54f19-23ac-4f90-a208-edc5014ce382"), null, "[]", null, null, false, null, false, null, false, new Guid("6089c186-8ad8-4675-ad0a-99988a08e36d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("2dd7bcb1-2f78-4915-bac8-17829bb7f47c"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("0b548967-8b84-4e30-87a2-076deda47477"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("44d17302-c9c6-4482-97f1-b3d1675fec32"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("6154d0b6-9ce2-4d93-982d-c09f8dee1a67"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("592026e8-24f0-462e-8f65-b5f9699d6b02"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("611b5c9c-f36b-4c60-a5c6-3286f0f4ace8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("60f534fe-d677-463d-a859-2846d4846905"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("e98ed208-a665-463a-9ee4-320b72e4401a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("7540c131-9661-4712-94b5-f4f7b461a625"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("5250f805-0eaa-4da5-ad15-6ec3d48ac56f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("de05b050-a5cc-45e6-8552-12eb0a68dac5"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("1970deca-cf70-4464-b93e-16b52f3f8379"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("f90949e3-a1df-46d2-be0b-2987cdc368ef"), null, "[]", null, null, false, null, false, null, false, new Guid("45d55af0-4f1e-459c-8106-f4ac8f98fb69"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" }
                });
        }
    }
}
