﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class AddRoleHasAllPermissions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0423bb7e-7eac-4c77-ba68-ccff1030d6c8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0e894014-9cfb-4b11-bfcd-64d6519d4c5f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("10f31450-b299-4381-86cd-df76fa212162"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("10f4cada-351d-4a5e-89d7-f42b93c6430e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("135d27a4-199a-472a-87d2-fd60561fbeba"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2d13c6a6-9399-4cb0-8808-7f285792ccd9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4e04f5d2-b523-4991-bd98-95f4d040d14b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6a9b3d3c-c9a0-4cae-8e8d-1c1d99a95337"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7133fab3-6ea2-433c-944d-055efc859256"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("75b09ae6-3847-4c7b-8916-25cf123ac4b5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("75f85f30-0ee9-4ae1-86b1-1bf00a373202"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7c7ea5ca-2b10-41f6-ac0c-25546d046308"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b1c5c604-1572-44bc-a065-7976b56f2ba1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b3d676ae-925d-48b0-ad26-a99099cb6a10"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b673112a-1de4-4ab8-99bf-727e940edf1f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("be9933cc-f1f1-4518-9d67-a66b49b380c9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("bfe0aa8a-837b-4280-bb0d-f8fd284bf3a7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c66f41fb-b426-4d0d-8f1b-8f26a9be8385"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c8650940-d2ef-4ca2-9d19-7b48af92e501"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c99573d7-e6ff-4a17-bb2c-bf34fba178c1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c9e0310f-ee0e-4e09-a130-e93a0445cf67"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d3e1e32a-74ce-4412-bc5f-38938573e487"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d96dae6d-e65b-46b3-b7b2-0319cf24781c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e9b57140-5a08-4042-908d-3f89933e5133"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f1354533-537b-42ce-ae02-eeca9c251f62"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f7457afc-fe05-4aee-aa06-ffd155aff5be"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f88a6bf6-66dd-4e90-8880-c6cd70aa51be"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"), new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("13890b02-3405-440c-a5ab-567832fd764b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("19676894-bcc9-42dd-a6f3-915fab85a6ff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1acd7614-c2e0-4928-a266-359d342d98d3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("23d91ad3-e3f7-4401-85cf-4584b6f46895"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3c26dc0e-68b2-4328-84b8-5360b75c3632"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("463f95ae-d2d6-4623-b18a-728b19d102b0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("47f5c87e-ade3-475f-a53e-1ceaa888a0c1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("51ce0c8f-4d09-4121-ae58-87572d5a3f7d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("55d9d8c2-c034-4608-ac84-211644942ca6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5d4e0050-e3b9-4b12-bfa9-9663c2b1c930"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("646c9f95-fcc4-49d7-b3a9-9b5ef893454c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("70826b3b-3701-420e-b46c-ae3811dbd324"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("872350b6-a5ea-42ac-beff-407c9fc1af32"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9fd4ffe9-ed72-4d82-938d-f2678a012394"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a7e012df-f47b-47a2-8da6-c2de3877f4a4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bed411fb-3e16-4a13-bf32-80ecd87bd4b0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c1dbf48a-bc43-423b-9063-f1fd05315042"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c302b5f4-3f34-4f87-a01f-1ef20429a7a1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("cbda6d44-3c9d-484c-bff5-f7e3184ac888"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e2000791-2454-423e-9f99-31f02ea06038"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e283dd22-220c-4100-a190-0316562661c2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("25d67109-6edd-44a6-893c-a27c18c82072"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(3851),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 261, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(9207),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 262, DateTimeKind.Local).AddTicks(533));

            migrationBuilder.AddColumn<bool>(
                name: "HasAllPermissions",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                type: "bit",
                nullable: true);

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), null, "iframe", null, "/iframe", null },
                    { new Guid("ae2ef13a-dcb7-4ea9-bbfd-ae95f709f588"), null, "tabs", null, "/tabs", null },
                    { new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), null, "system", null, "/system", null },
                    { new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), null, "permission", null, "/permission", null },
                    { new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), null, "monitor", null, "/monitor", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "HasAllPermissions", "Name", "NormalizedName" },
                values: new object[] { new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"), null, false, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a"), "0de48e1af0cf5b629369a8240a881968", "tjrvy", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0c64ba14-8fcd-4a48-a14c-ae4ad63ac52b"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("2182c665-be5b-4cab-bca2-de2f30e26e95"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("9a785f25-426e-44e7-9dc4-0871262c2af5"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("ae2ef13a-dcb7-4ea9-bbfd-ae95f709f588"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("f50c49f6-12af-4bd5-8020-dd8a2ea18a65"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("f5b4e80f-3938-4da7-ae40-7f310d7c6f4c"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("1ba74fbf-8dcf-4571-a3d9-e37c474e6db8"), "monitor/online/index", "OnlineUser", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/online-user", null },
                    { new Guid("3119c836-77ef-4bfa-93fb-de30310e88c2"), null, "SystemRole", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/role/index", null },
                    { new Guid("59beb0af-4cef-463d-93f7-a1cfccb3d0b5"), "monitor/logs/system/index", "SystemLog", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/system-logs", null },
                    { new Guid("6930bd11-784e-456d-98e8-158ae1c61aa0"), null, "SystemUser", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/user/index", null },
                    { new Guid("71f33269-c366-42a7-beb7-4ab429a9e72f"), null, "SystemMenu", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/menu/index", null },
                    { new Guid("74957df9-27cd-43df-af29-b1e532d33747"), "monitor/logs/login/index", "LoginLog", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/login-logs", null },
                    { new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), null, "Embedded", new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), "/iframe/embedded", null },
                    { new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), null, "External", new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), "/iframe/external", null },
                    { new Guid("ad509937-0db4-478f-abe1-dcf0eae85ff3"), null, "SystemDept", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/dept/index", null },
                    { new Guid("cca2989b-af92-47fe-bceb-1a3a44a0dc9e"), "monitor/logs/operation/index", "OperationLog", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/operation-logs", null },
                    { new Guid("e095045e-fb88-4d96-a9ad-a0fee5b74647"), null, "PermissionButton", new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), "/permission/button/index", null },
                    { new Guid("ed912f48-cc56-470a-8be9-0de495642371"), null, "PermissionPage", new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), "/permission/page/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"), new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("02470735-7874-46c8-89c5-6ab097c92d16"), null, "[]", null, null, false, null, false, null, false, new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("2d2d1003-a0bf-4819-bacc-8100c3d85469"), null, "[]", null, null, false, null, false, null, false, new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("38981d48-1b45-4cdd-9226-7b3ecd468546"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("ad509937-0db4-478f-abe1-dcf0eae85ff3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("6480ce04-b6b0-4bc4-b9a7-c646b6931003"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("3119c836-77ef-4bfa-93fb-de30310e88c2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("90a2b897-e297-4a7e-b2d1-e8121621c4e1"), null, "[]", null, null, false, null, false, null, false, new Guid("ed912f48-cc56-470a-8be9-0de495642371"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("bc1d2173-9aa7-4d54-893c-c7ff56e3ad1a"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("1ba74fbf-8dcf-4571-a3d9-e37c474e6db8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("c02c4d97-6681-4041-886c-f6c87378c915"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("cca2989b-af92-47fe-bceb-1a3a44a0dc9e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("cdca6fe2-4d7e-4c4d-8a86-db4fef9c98a2"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("e095045e-fb88-4d96-a9ad-a0fee5b74647"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("e27e0ad2-e91c-446b-afe3-34d8108abfb3"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("74957df9-27cd-43df-af29-b1e532d33747"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("f34c6de9-0adb-4a53-a43b-db8787d0a877"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("6930bd11-784e-456d-98e8-158ae1c61aa0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("f51e5fac-3d41-4e60-9cc0-a1bddbd2a669"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("71f33269-c366-42a7-beb7-4ab429a9e72f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("f9d06440-c1c2-4c89-b22c-1960157b3f60"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("59beb0af-4cef-463d-93f7-a1cfccb3d0b5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("03e9972f-b86c-43ca-835b-b2c8ddd46daa"), null, "FrameUiGradients", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/uigradients", null },
                    { new Guid("1882375b-7f1f-4427-8e08-ebd91f4c7bcf"), null, "FrameVite", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/vite", null },
                    { new Guid("3438662b-8176-4769-a981-6fe713382c67"), null, "FramePinia", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/pinia", null },
                    { new Guid("40eac384-788d-4cd3-a47b-bb6b29504fb4"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), "/external", null },
                    { new Guid("77094f92-f0d9-43e8-a33b-614fd476c5f0"), null, "FrameEp", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/ep", null },
                    { new Guid("86e3080a-732d-479b-8c66-bc796311cb82"), null, "FrameRouter", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/vue-router", null },
                    { new Guid("968d1cca-73d1-441b-96da-0706b31366ff"), null, "FrameVue", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/vue3", null },
                    { new Guid("d484521c-3116-466a-9581-ce873bb66f3b"), null, "FrameTailwindcss", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/tailwindcss", null },
                    { new Guid("de1e5db2-35b0-4c65-b373-fef9a2e20867"), null, "https://pure-admin-utils.netlify.app/", new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), "/pureUtilsLink", null },
                    { new Guid("e88fdbf7-1c66-4652-9031-8d76a99ab735"), null, "FrameColorHunt", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/colorhunt", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("09406cce-6e21-48f7-8b78-da08c785a418"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("1882375b-7f1f-4427-8e08-ebd91f4c7bcf"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("1166ec01-ed40-4d84-b9c4-75b2b56738fa"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("03e9972f-b86c-43ca-835b-b2c8ddd46daa"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("1b36fc11-a44e-4bf1-97f4-a23c3f4004c9"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("77094f92-f0d9-43e8-a33b-614fd476c5f0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("34d3363b-d796-44e3-8064-0307335e076e"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("e88fdbf7-1c66-4652-9031-8d76a99ab735"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("50285a65-2138-465c-9306-fce6a8678bac"), null, "[]", null, null, false, null, false, null, false, new Guid("de1e5db2-35b0-4c65-b373-fef9a2e20867"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("625ef9b4-6e2f-41e5-a05f-b3db895c14fe"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("3438662b-8176-4769-a981-6fe713382c67"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("a1f881bb-1a4a-4650-9dc2-890570241e39"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("86e3080a-732d-479b-8c66-bc796311cb82"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("d311f4e9-7845-49d7-b997-3d9e2f719e4b"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("d484521c-3116-466a-9581-ce873bb66f3b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("e8705417-4f2f-404b-8401-bc71de2ba3ae"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("968d1cca-73d1-441b-96da-0706b31366ff"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("ff2a1264-72e3-4871-ab63-98e499b57455"), null, "[]", null, null, false, null, false, null, false, new Guid("40eac384-788d-4cd3-a47b-bb6b29504fb4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("02470735-7874-46c8-89c5-6ab097c92d16"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("09406cce-6e21-48f7-8b78-da08c785a418"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0c64ba14-8fcd-4a48-a14c-ae4ad63ac52b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1166ec01-ed40-4d84-b9c4-75b2b56738fa"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1b36fc11-a44e-4bf1-97f4-a23c3f4004c9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2182c665-be5b-4cab-bca2-de2f30e26e95"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2d2d1003-a0bf-4819-bacc-8100c3d85469"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("34d3363b-d796-44e3-8064-0307335e076e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("38981d48-1b45-4cdd-9226-7b3ecd468546"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("50285a65-2138-465c-9306-fce6a8678bac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("625ef9b4-6e2f-41e5-a05f-b3db895c14fe"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6480ce04-b6b0-4bc4-b9a7-c646b6931003"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("90a2b897-e297-4a7e-b2d1-e8121621c4e1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9a785f25-426e-44e7-9dc4-0871262c2af5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a1f881bb-1a4a-4650-9dc2-890570241e39"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("bc1d2173-9aa7-4d54-893c-c7ff56e3ad1a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c02c4d97-6681-4041-886c-f6c87378c915"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("cdca6fe2-4d7e-4c4d-8a86-db4fef9c98a2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d311f4e9-7845-49d7-b997-3d9e2f719e4b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e27e0ad2-e91c-446b-afe3-34d8108abfb3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e8705417-4f2f-404b-8401-bc71de2ba3ae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f34c6de9-0adb-4a53-a43b-db8787d0a877"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f50c49f6-12af-4bd5-8020-dd8a2ea18a65"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f51e5fac-3d41-4e60-9cc0-a1bddbd2a669"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f5b4e80f-3938-4da7-ae40-7f310d7c6f4c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f9d06440-c1c2-4c89-b22c-1960157b3f60"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ff2a1264-72e3-4871-ab63-98e499b57455"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"), new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("03e9972f-b86c-43ca-835b-b2c8ddd46daa"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1882375b-7f1f-4427-8e08-ebd91f4c7bcf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1ba74fbf-8dcf-4571-a3d9-e37c474e6db8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3119c836-77ef-4bfa-93fb-de30310e88c2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3438662b-8176-4769-a981-6fe713382c67"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("40eac384-788d-4cd3-a47b-bb6b29504fb4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("59beb0af-4cef-463d-93f7-a1cfccb3d0b5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6930bd11-784e-456d-98e8-158ae1c61aa0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("71f33269-c366-42a7-beb7-4ab429a9e72f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("74957df9-27cd-43df-af29-b1e532d33747"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("77094f92-f0d9-43e8-a33b-614fd476c5f0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("86e3080a-732d-479b-8c66-bc796311cb82"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("968d1cca-73d1-441b-96da-0706b31366ff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ad509937-0db4-478f-abe1-dcf0eae85ff3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ae2ef13a-dcb7-4ea9-bbfd-ae95f709f588"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("cca2989b-af92-47fe-bceb-1a3a44a0dc9e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d484521c-3116-466a-9581-ce873bb66f3b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("de1e5db2-35b0-4c65-b373-fef9a2e20867"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e095045e-fb88-4d96-a9ad-a0fee5b74647"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e88fdbf7-1c66-4652-9031-8d76a99ab735"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ed912f48-cc56-470a-8be9-0de495642371"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"));

            migrationBuilder.DropColumn(
                name: "HasAllPermissions",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 261, DateTimeKind.Local).AddTicks(4968),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(3851));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 8, 30, 16, 20, 57, 262, DateTimeKind.Local).AddTicks(533),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(9207));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), null, "iframe", null, "/iframe", null },
                    { new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), null, "system", null, "/system", null },
                    { new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), null, "monitor", null, "/monitor", null },
                    { new Guid("bed411fb-3e16-4a13-bf32-80ecd87bd4b0"), null, "tabs", null, "/tabs", null },
                    { new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), null, "permission", null, "/permission", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"), null, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335"), "b9bad996bbb9400ad48514e91a7de341", "T9qHs", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("10f31450-b299-4381-86cd-df76fa212162"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("10f4cada-351d-4a5e-89d7-f42b93c6430e"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("6a9b3d3c-c9a0-4cae-8e8d-1c1d99a95337"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("7133fab3-6ea2-433c-944d-055efc859256"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("bed411fb-3e16-4a13-bf32-80ecd87bd4b0"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("c99573d7-e6ff-4a17-bb2c-bf34fba178c1"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("1acd7614-c2e0-4928-a266-359d342d98d3"), "monitor/logs/operation/index", "OperationLog", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/operation-logs", null },
                    { new Guid("23d91ad3-e3f7-4401-85cf-4584b6f46895"), null, "PermissionPage", new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), "/permission/page/index", null },
                    { new Guid("47f5c87e-ade3-475f-a53e-1ceaa888a0c1"), null, "SystemMenu", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/menu/index", null },
                    { new Guid("51ce0c8f-4d09-4121-ae58-87572d5a3f7d"), null, "PermissionButton", new Guid("ed44e1c7-b62f-4318-a79c-510e289517de"), "/permission/button/index", null },
                    { new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), null, "External", new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), "/iframe/external", null },
                    { new Guid("70826b3b-3701-420e-b46c-ae3811dbd324"), "monitor/logs/system/index", "SystemLog", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/system-logs", null },
                    { new Guid("a7e012df-f47b-47a2-8da6-c2de3877f4a4"), null, "SystemDept", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/dept/index", null },
                    { new Guid("c1dbf48a-bc43-423b-9063-f1fd05315042"), "monitor/online/index", "OnlineUser", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/online-user", null },
                    { new Guid("cbda6d44-3c9d-484c-bff5-f7e3184ac888"), "monitor/logs/login/index", "LoginLog", new Guid("3f27bce1-2055-4027-acf8-ca59ccf003be"), "/monitor/login-logs", null },
                    { new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), null, "Embedded", new Guid("25d67109-6edd-44a6-893c-a27c18c82072"), "/iframe/embedded", null },
                    { new Guid("e2000791-2454-423e-9f99-31f02ea06038"), null, "SystemUser", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/user/index", null },
                    { new Guid("e283dd22-220c-4100-a190-0316562661c2"), null, "SystemRole", new Guid("36c02ef1-2f07-4ba3-8e05-fa438f83d29f"), "/system/role/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("d0edbb2f-d35a-4a1d-b14b-1410ef3f9890"), new Guid("c958b20c-325c-488b-a29c-f6b8e3fa5335"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0e894014-9cfb-4b11-bfcd-64d6519d4c5f"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("e2000791-2454-423e-9f99-31f02ea06038"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("135d27a4-199a-472a-87d2-fd60561fbeba"), null, "[]", null, null, false, null, false, null, false, new Guid("23d91ad3-e3f7-4401-85cf-4584b6f46895"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("2d13c6a6-9399-4cb0-8808-7f285792ccd9"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("1acd7614-c2e0-4928-a266-359d342d98d3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("4e04f5d2-b523-4991-bd98-95f4d040d14b"), null, "[]", null, null, false, null, false, null, false, new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("75f85f30-0ee9-4ae1-86b1-1bf00a373202"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("c1dbf48a-bc43-423b-9063-f1fd05315042"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("7c7ea5ca-2b10-41f6-ac0c-25546d046308"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("51ce0c8f-4d09-4121-ae58-87572d5a3f7d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("b1c5c604-1572-44bc-a065-7976b56f2ba1"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("e283dd22-220c-4100-a190-0316562661c2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("b673112a-1de4-4ab8-99bf-727e940edf1f"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("cbda6d44-3c9d-484c-bff5-f7e3184ac888"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("c66f41fb-b426-4d0d-8f1b-8f26a9be8385"), null, "[]", null, null, false, null, false, null, false, new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("c8650940-d2ef-4ca2-9d19-7b48af92e501"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("70826b3b-3701-420e-b46c-ae3811dbd324"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("e9b57140-5a08-4042-908d-3f89933e5133"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("a7e012df-f47b-47a2-8da6-c2de3877f4a4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("f88a6bf6-66dd-4e90-8880-c6cd70aa51be"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("47f5c87e-ade3-475f-a53e-1ceaa888a0c1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("13890b02-3405-440c-a5ab-567832fd764b"), null, "FrameEp", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/ep", null },
                    { new Guid("19676894-bcc9-42dd-a6f3-915fab85a6ff"), null, "FrameVite", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/vite", null },
                    { new Guid("3c26dc0e-68b2-4328-84b8-5360b75c3632"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), "/external", null },
                    { new Guid("463f95ae-d2d6-4623-b18a-728b19d102b0"), null, "FrameVue", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/vue3", null },
                    { new Guid("55d9d8c2-c034-4608-ac84-211644942ca6"), null, "FrameColorHunt", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/colorhunt", null },
                    { new Guid("5d4e0050-e3b9-4b12-bfa9-9663c2b1c930"), null, "FrameRouter", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/vue-router", null },
                    { new Guid("646c9f95-fcc4-49d7-b3a9-9b5ef893454c"), null, "https://pure-admin-utils.netlify.app/", new Guid("64e9a2b2-85e1-41b1-bb6c-07c7774b86f7"), "/pureUtilsLink", null },
                    { new Guid("872350b6-a5ea-42ac-beff-407c9fc1af32"), null, "FrameTailwindcss", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/tailwindcss", null },
                    { new Guid("9fd4ffe9-ed72-4d82-938d-f2678a012394"), null, "FramePinia", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/pinia", null },
                    { new Guid("c302b5f4-3f34-4f87-a01f-1ef20429a7a1"), null, "FrameUiGradients", new Guid("d78c73bd-9125-4ee3-95f3-2a52414a8696"), "/iframe/uigradients", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0423bb7e-7eac-4c77-ba68-ccff1030d6c8"), null, "[]", null, null, false, null, false, null, false, new Guid("646c9f95-fcc4-49d7-b3a9-9b5ef893454c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("75b09ae6-3847-4c7b-8916-25cf123ac4b5"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("5d4e0050-e3b9-4b12-bfa9-9663c2b1c930"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("b3d676ae-925d-48b0-ad26-a99099cb6a10"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("872350b6-a5ea-42ac-beff-407c9fc1af32"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("be9933cc-f1f1-4518-9d67-a66b49b380c9"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("c302b5f4-3f34-4f87-a01f-1ef20429a7a1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("bfe0aa8a-837b-4280-bb0d-f8fd284bf3a7"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("55d9d8c2-c034-4608-ac84-211644942ca6"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("c9e0310f-ee0e-4e09-a130-e93a0445cf67"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("463f95ae-d2d6-4623-b18a-728b19d102b0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("d3e1e32a-74ce-4412-bc5f-38938573e487"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("9fd4ffe9-ed72-4d82-938d-f2678a012394"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("d96dae6d-e65b-46b3-b7b2-0319cf24781c"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("19676894-bcc9-42dd-a6f3-915fab85a6ff"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("f1354533-537b-42ce-ae02-eeca9c251f62"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("13890b02-3405-440c-a5ab-567832fd764b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("f7457afc-fe05-4aee-aa06-ffd155aff5be"), null, "[]", null, null, false, null, false, null, false, new Guid("3c26dc0e-68b2-4328-84b8-5360b75c3632"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" }
                });
        }
    }
}
