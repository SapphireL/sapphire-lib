﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class UpdateBasicUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("02470735-7874-46c8-89c5-6ab097c92d16"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("09406cce-6e21-48f7-8b78-da08c785a418"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0c64ba14-8fcd-4a48-a14c-ae4ad63ac52b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1166ec01-ed40-4d84-b9c4-75b2b56738fa"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("1b36fc11-a44e-4bf1-97f4-a23c3f4004c9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2182c665-be5b-4cab-bca2-de2f30e26e95"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2d2d1003-a0bf-4819-bacc-8100c3d85469"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("34d3363b-d796-44e3-8064-0307335e076e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("38981d48-1b45-4cdd-9226-7b3ecd468546"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("50285a65-2138-465c-9306-fce6a8678bac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("625ef9b4-6e2f-41e5-a05f-b3db895c14fe"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6480ce04-b6b0-4bc4-b9a7-c646b6931003"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("90a2b897-e297-4a7e-b2d1-e8121621c4e1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("9a785f25-426e-44e7-9dc4-0871262c2af5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a1f881bb-1a4a-4650-9dc2-890570241e39"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("bc1d2173-9aa7-4d54-893c-c7ff56e3ad1a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c02c4d97-6681-4041-886c-f6c87378c915"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("cdca6fe2-4d7e-4c4d-8a86-db4fef9c98a2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d311f4e9-7845-49d7-b997-3d9e2f719e4b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e27e0ad2-e91c-446b-afe3-34d8108abfb3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e8705417-4f2f-404b-8401-bc71de2ba3ae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f34c6de9-0adb-4a53-a43b-db8787d0a877"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f50c49f6-12af-4bd5-8020-dd8a2ea18a65"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f51e5fac-3d41-4e60-9cc0-a1bddbd2a669"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f5b4e80f-3938-4da7-ae40-7f310d7c6f4c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f9d06440-c1c2-4c89-b22c-1960157b3f60"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ff2a1264-72e3-4871-ab63-98e499b57455"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"), new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("03e9972f-b86c-43ca-835b-b2c8ddd46daa"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1882375b-7f1f-4427-8e08-ebd91f4c7bcf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1ba74fbf-8dcf-4571-a3d9-e37c474e6db8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3119c836-77ef-4bfa-93fb-de30310e88c2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3438662b-8176-4769-a981-6fe713382c67"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("40eac384-788d-4cd3-a47b-bb6b29504fb4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("59beb0af-4cef-463d-93f7-a1cfccb3d0b5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6930bd11-784e-456d-98e8-158ae1c61aa0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("71f33269-c366-42a7-beb7-4ab429a9e72f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("74957df9-27cd-43df-af29-b1e532d33747"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("77094f92-f0d9-43e8-a33b-614fd476c5f0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("86e3080a-732d-479b-8c66-bc796311cb82"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("968d1cca-73d1-441b-96da-0706b31366ff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ad509937-0db4-478f-abe1-dcf0eae85ff3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ae2ef13a-dcb7-4ea9-bbfd-ae95f709f588"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("cca2989b-af92-47fe-bceb-1a3a44a0dc9e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d484521c-3116-466a-9581-ce873bb66f3b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("de1e5db2-35b0-4c65-b373-fef9a2e20867"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e095045e-fb88-4d96-a9ad-a0fee5b74647"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e88fdbf7-1c66-4652-9031-8d76a99ab735"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ed912f48-cc56-470a-8be9-0de495642371"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(276),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(3851));

            migrationBuilder.AddColumn<string>(
                name: "AvatarUrl",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(5637),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(9207));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), null, "system", null, "/system", null },
                    { new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), null, "monitor", null, "/monitor", null },
                    { new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), null, "permission", null, "/permission", null },
                    { new Guid("c2cb1de1-2f58-4cfd-992c-a3c663ad1673"), null, "tabs", null, "/tabs", null },
                    { new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), null, "iframe", null, "/iframe", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "HasAllPermissions", "Name", "NormalizedName" },
                values: new object[] { new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"), null, false, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("08924ffe-403b-441b-85d1-b8a2925fef62"), "2daccd3a1e5f4d9368ca29ccee834d29", "NAjKo", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("48c6a731-8bfe-42ad-977e-ca6698568264"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("56504323-a1a9-4095-a32a-a7f03a99ad55"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("709ccf9e-8ef1-439b-8b16-94487d1d8afc"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("c2cb1de1-2f58-4cfd-992c-a3c663ad1673"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("7eef1097-22db-4137-aa27-70e6c9bfca4a"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("d9c838c7-318a-4fe3-8aa1-6f4c63f20dea"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("18e8833e-f02e-4f01-998c-c7c50b2c8705"), "monitor/logs/login/index", "LoginLog", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/login-logs", null },
                    { new Guid("36697735-e280-4cda-ba38-6be6fb8ca760"), null, "SystemDept", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/dept/index", null },
                    { new Guid("8901e14b-dc1d-4c6d-a152-972385ef953a"), "monitor/logs/operation/index", "OperationLog", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/operation-logs", null },
                    { new Guid("8ccd85c1-adbc-418e-b21e-a16acc24d2c8"), null, "PermissionButton", new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), "/permission/button/index", null },
                    { new Guid("903552d6-3419-4428-9718-54a74179dd83"), null, "SystemUser", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/user/index", null },
                    { new Guid("99fdeb7a-6266-4061-9268-558ad17f4b77"), null, "PermissionPage", new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), "/permission/page/index", null },
                    { new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), null, "External", new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), "/iframe/external", null },
                    { new Guid("a030e15a-70dc-44a1-92ac-53d6b7efc4e8"), "monitor/online/index", "OnlineUser", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/online-user", null },
                    { new Guid("b75130bc-9eb6-4118-90fb-1c9a4816067f"), null, "SystemRole", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/role/index", null },
                    { new Guid("ce419519-37b7-45fa-ab6e-dc7851d5bf75"), null, "SystemMenu", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/menu/index", null },
                    { new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), null, "Embedded", new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), "/iframe/embedded", null },
                    { new Guid("f6892f16-7f18-4a51-a52e-a14b52d073e9"), "monitor/logs/system/index", "SystemLog", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/system-logs", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"), new Guid("08924ffe-403b-441b-85d1-b8a2925fef62"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("12770614-c50c-4c18-9bb6-802f3708b0bf"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("8ccd85c1-adbc-418e-b21e-a16acc24d2c8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("4ecb0083-de17-4979-9e9b-882beb17e129"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("f6892f16-7f18-4a51-a52e-a14b52d073e9"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("5507859b-b427-47c1-af55-26e5b6a085f2"), null, "[]", null, null, false, null, false, null, false, new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("686ec7d9-96aa-45f5-8ef5-3f43de6ac740"), null, "[]", null, null, false, null, false, null, false, new Guid("99fdeb7a-6266-4061-9268-558ad17f4b77"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("6deb98ef-d274-4ef5-9d21-5a1f5b645716"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("8901e14b-dc1d-4c6d-a152-972385ef953a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("7815bf0e-6c24-40ad-91df-8ae9e8916360"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("b75130bc-9eb6-4118-90fb-1c9a4816067f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("808ffe27-c19b-4fea-b7fb-34adb652dee2"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("36697735-e280-4cda-ba38-6be6fb8ca760"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("a519b4cc-9591-48f0-b7a4-4ec0bfb1d4fb"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("18e8833e-f02e-4f01-998c-c7c50b2c8705"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("d8a29e86-fb1a-42ef-8b5d-043c936ceb73"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("ce419519-37b7-45fa-ab6e-dc7851d5bf75"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("dddd521d-f9bc-4c47-8bee-23fea5d12956"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("903552d6-3419-4428-9718-54a74179dd83"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("e7d4b3fc-a70c-446f-9572-61381c022d87"), null, "[]", null, null, false, null, false, null, false, new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("ef799d36-5b2e-43bf-91bd-9c9e2ac167fe"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("a030e15a-70dc-44a1-92ac-53d6b7efc4e8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("114693dd-e026-407c-b09b-8a6e64cef8ec"), null, "FrameColorHunt", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/colorhunt", null },
                    { new Guid("2cfb1cf2-9c92-4100-93af-c3af5f9e9582"), null, "FrameEp", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/ep", null },
                    { new Guid("3a3f2314-b191-4f7c-befe-da602dd70a4d"), null, "FrameUiGradients", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/uigradients", null },
                    { new Guid("5d82036c-15c5-4e6d-a4f4-e769720fc4f1"), null, "FrameTailwindcss", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/tailwindcss", null },
                    { new Guid("774da069-2dde-4b5a-9506-2f99c5b86ff0"), null, "FrameRouter", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/vue-router", null },
                    { new Guid("8875fa7b-4e42-48f6-9567-7b814b102d38"), null, "FramePinia", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/pinia", null },
                    { new Guid("8c02cfbe-806e-4f55-a447-0843dfb40b6d"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), "/external", null },
                    { new Guid("a0b4b2c4-a3bc-40ef-8605-bad5b64de5d5"), null, "FrameVue", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/vue3", null },
                    { new Guid("a334d31f-4536-4b79-8aef-bfddf8ff46db"), null, "https://pure-admin-utils.netlify.app/", new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), "/pureUtilsLink", null },
                    { new Guid("f8d35ff6-c5fd-4fb1-964b-ed9d37ad9ac5"), null, "FrameVite", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/vite", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("04ee4d9f-97aa-4bc1-a0d8-0fa472e10689"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("774da069-2dde-4b5a-9506-2f99c5b86ff0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("29670053-afe4-4137-841f-7657bff22b71"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("114693dd-e026-407c-b09b-8a6e64cef8ec"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("54e93b4e-5613-455c-a10b-afd10fa24154"), null, "[]", null, null, false, null, false, null, false, new Guid("8c02cfbe-806e-4f55-a447-0843dfb40b6d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("5750531c-289a-4096-bdd5-9ea75bacefe3"), null, "[]", null, null, false, null, false, null, false, new Guid("a334d31f-4536-4b79-8aef-bfddf8ff46db"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("84fa6157-9448-4d2d-ba72-f2603e7f8b0d"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("5d82036c-15c5-4e6d-a4f4-e769720fc4f1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("87779379-e50b-4b23-b023-bc3aa63969c2"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("a0b4b2c4-a3bc-40ef-8605-bad5b64de5d5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("99129a5e-f23a-47fa-a17e-2a176f580cad"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("2cfb1cf2-9c92-4100-93af-c3af5f9e9582"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("a2d03626-14d0-4065-9310-f71efc878e47"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("3a3f2314-b191-4f7c-befe-da602dd70a4d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("d67adc57-44f2-4f81-935d-31972bb4ff05"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("f8d35ff6-c5fd-4fb1-964b-ed9d37ad9ac5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("d9cb8974-bcc5-4903-a415-eb183a2695ba"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("8875fa7b-4e42-48f6-9567-7b814b102d38"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("04ee4d9f-97aa-4bc1-a0d8-0fa472e10689"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("12770614-c50c-4c18-9bb6-802f3708b0bf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("29670053-afe4-4137-841f-7657bff22b71"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("48c6a731-8bfe-42ad-977e-ca6698568264"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4ecb0083-de17-4979-9e9b-882beb17e129"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("54e93b4e-5613-455c-a10b-afd10fa24154"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5507859b-b427-47c1-af55-26e5b6a085f2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("56504323-a1a9-4095-a32a-a7f03a99ad55"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5750531c-289a-4096-bdd5-9ea75bacefe3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("686ec7d9-96aa-45f5-8ef5-3f43de6ac740"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6deb98ef-d274-4ef5-9d21-5a1f5b645716"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("709ccf9e-8ef1-439b-8b16-94487d1d8afc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7815bf0e-6c24-40ad-91df-8ae9e8916360"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7eef1097-22db-4137-aa27-70e6c9bfca4a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("808ffe27-c19b-4fea-b7fb-34adb652dee2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("84fa6157-9448-4d2d-ba72-f2603e7f8b0d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("87779379-e50b-4b23-b023-bc3aa63969c2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("99129a5e-f23a-47fa-a17e-2a176f580cad"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a2d03626-14d0-4065-9310-f71efc878e47"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a519b4cc-9591-48f0-b7a4-4ec0bfb1d4fb"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d67adc57-44f2-4f81-935d-31972bb4ff05"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d8a29e86-fb1a-42ef-8b5d-043c936ceb73"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d9c838c7-318a-4fe3-8aa1-6f4c63f20dea"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d9cb8974-bcc5-4903-a415-eb183a2695ba"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("dddd521d-f9bc-4c47-8bee-23fea5d12956"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e7d4b3fc-a70c-446f-9572-61381c022d87"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ef799d36-5b2e-43bf-91bd-9c9e2ac167fe"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"), new Guid("08924ffe-403b-441b-85d1-b8a2925fef62") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("114693dd-e026-407c-b09b-8a6e64cef8ec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("18e8833e-f02e-4f01-998c-c7c50b2c8705"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2cfb1cf2-9c92-4100-93af-c3af5f9e9582"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("36697735-e280-4cda-ba38-6be6fb8ca760"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3a3f2314-b191-4f7c-befe-da602dd70a4d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5d82036c-15c5-4e6d-a4f4-e769720fc4f1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("774da069-2dde-4b5a-9506-2f99c5b86ff0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8875fa7b-4e42-48f6-9567-7b814b102d38"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8901e14b-dc1d-4c6d-a152-972385ef953a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8c02cfbe-806e-4f55-a447-0843dfb40b6d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8ccd85c1-adbc-418e-b21e-a16acc24d2c8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("903552d6-3419-4428-9718-54a74179dd83"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("99fdeb7a-6266-4061-9268-558ad17f4b77"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a030e15a-70dc-44a1-92ac-53d6b7efc4e8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a0b4b2c4-a3bc-40ef-8605-bad5b64de5d5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a334d31f-4536-4b79-8aef-bfddf8ff46db"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b75130bc-9eb6-4118-90fb-1c9a4816067f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c2cb1de1-2f58-4cfd-992c-a3c663ad1673"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ce419519-37b7-45fa-ab6e-dc7851d5bf75"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f6892f16-7f18-4a51-a52e-a14b52d073e9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f8d35ff6-c5fd-4fb1-964b-ed9d37ad9ac5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("08924ffe-403b-441b-85d1-b8a2925fef62"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"));

            migrationBuilder.DropColumn(
                name: "AvatarUrl",
                schema: "SapphireArchitecture",
                table: "Basic_Users");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                schema: "SapphireArchitecture",
                table: "Basic_Users");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(3851),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(276));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 4, 17, 56, 11, 558, DateTimeKind.Local).AddTicks(9207),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(5637));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), null, "iframe", null, "/iframe", null },
                    { new Guid("ae2ef13a-dcb7-4ea9-bbfd-ae95f709f588"), null, "tabs", null, "/tabs", null },
                    { new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), null, "system", null, "/system", null },
                    { new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), null, "permission", null, "/permission", null },
                    { new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), null, "monitor", null, "/monitor", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "HasAllPermissions", "Name", "NormalizedName" },
                values: new object[] { new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"), null, false, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a"), "0de48e1af0cf5b629369a8240a881968", "tjrvy", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0c64ba14-8fcd-4a48-a14c-ae4ad63ac52b"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("2182c665-be5b-4cab-bca2-de2f30e26e95"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("9a785f25-426e-44e7-9dc4-0871262c2af5"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("ae2ef13a-dcb7-4ea9-bbfd-ae95f709f588"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("f50c49f6-12af-4bd5-8020-dd8a2ea18a65"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("f5b4e80f-3938-4da7-ae40-7f310d7c6f4c"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("1ba74fbf-8dcf-4571-a3d9-e37c474e6db8"), "monitor/online/index", "OnlineUser", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/online-user", null },
                    { new Guid("3119c836-77ef-4bfa-93fb-de30310e88c2"), null, "SystemRole", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/role/index", null },
                    { new Guid("59beb0af-4cef-463d-93f7-a1cfccb3d0b5"), "monitor/logs/system/index", "SystemLog", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/system-logs", null },
                    { new Guid("6930bd11-784e-456d-98e8-158ae1c61aa0"), null, "SystemUser", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/user/index", null },
                    { new Guid("71f33269-c366-42a7-beb7-4ab429a9e72f"), null, "SystemMenu", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/menu/index", null },
                    { new Guid("74957df9-27cd-43df-af29-b1e532d33747"), "monitor/logs/login/index", "LoginLog", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/login-logs", null },
                    { new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), null, "Embedded", new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), "/iframe/embedded", null },
                    { new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), null, "External", new Guid("7275f1f7-c6b5-4e6c-9955-4dbda95494d2"), "/iframe/external", null },
                    { new Guid("ad509937-0db4-478f-abe1-dcf0eae85ff3"), null, "SystemDept", new Guid("b4800bfe-5177-44f3-9152-341dc70b242d"), "/system/dept/index", null },
                    { new Guid("cca2989b-af92-47fe-bceb-1a3a44a0dc9e"), "monitor/logs/operation/index", "OperationLog", new Guid("ff36c04d-517e-48de-a333-7503dbcb0c86"), "/monitor/operation-logs", null },
                    { new Guid("e095045e-fb88-4d96-a9ad-a0fee5b74647"), null, "PermissionButton", new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), "/permission/button/index", null },
                    { new Guid("ed912f48-cc56-470a-8be9-0de495642371"), null, "PermissionPage", new Guid("f01ffeb8-7412-4c4b-847f-8ef479736360"), "/permission/page/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("9262d727-8a25-421e-a09b-c8f1d2026f64"), new Guid("ba89bcec-d1cc-49c7-afb5-4fe832b6099a"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("02470735-7874-46c8-89c5-6ab097c92d16"), null, "[]", null, null, false, null, false, null, false, new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("2d2d1003-a0bf-4819-bacc-8100c3d85469"), null, "[]", null, null, false, null, false, null, false, new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("38981d48-1b45-4cdd-9226-7b3ecd468546"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("ad509937-0db4-478f-abe1-dcf0eae85ff3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("6480ce04-b6b0-4bc4-b9a7-c646b6931003"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("3119c836-77ef-4bfa-93fb-de30310e88c2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("90a2b897-e297-4a7e-b2d1-e8121621c4e1"), null, "[]", null, null, false, null, false, null, false, new Guid("ed912f48-cc56-470a-8be9-0de495642371"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("bc1d2173-9aa7-4d54-893c-c7ff56e3ad1a"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("1ba74fbf-8dcf-4571-a3d9-e37c474e6db8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("c02c4d97-6681-4041-886c-f6c87378c915"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("cca2989b-af92-47fe-bceb-1a3a44a0dc9e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("cdca6fe2-4d7e-4c4d-8a86-db4fef9c98a2"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("e095045e-fb88-4d96-a9ad-a0fee5b74647"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("e27e0ad2-e91c-446b-afe3-34d8108abfb3"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("74957df9-27cd-43df-af29-b1e532d33747"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("f34c6de9-0adb-4a53-a43b-db8787d0a877"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("6930bd11-784e-456d-98e8-158ae1c61aa0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("f51e5fac-3d41-4e60-9cc0-a1bddbd2a669"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("71f33269-c366-42a7-beb7-4ab429a9e72f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("f9d06440-c1c2-4c89-b22c-1960157b3f60"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("59beb0af-4cef-463d-93f7-a1cfccb3d0b5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("03e9972f-b86c-43ca-835b-b2c8ddd46daa"), null, "FrameUiGradients", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/uigradients", null },
                    { new Guid("1882375b-7f1f-4427-8e08-ebd91f4c7bcf"), null, "FrameVite", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/vite", null },
                    { new Guid("3438662b-8176-4769-a981-6fe713382c67"), null, "FramePinia", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/pinia", null },
                    { new Guid("40eac384-788d-4cd3-a47b-bb6b29504fb4"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), "/external", null },
                    { new Guid("77094f92-f0d9-43e8-a33b-614fd476c5f0"), null, "FrameEp", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/ep", null },
                    { new Guid("86e3080a-732d-479b-8c66-bc796311cb82"), null, "FrameRouter", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/vue-router", null },
                    { new Guid("968d1cca-73d1-441b-96da-0706b31366ff"), null, "FrameVue", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/vue3", null },
                    { new Guid("d484521c-3116-466a-9581-ce873bb66f3b"), null, "FrameTailwindcss", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/tailwindcss", null },
                    { new Guid("de1e5db2-35b0-4c65-b373-fef9a2e20867"), null, "https://pure-admin-utils.netlify.app/", new Guid("887e5342-81c5-44ad-976a-0ac100c2bdfd"), "/pureUtilsLink", null },
                    { new Guid("e88fdbf7-1c66-4652-9031-8d76a99ab735"), null, "FrameColorHunt", new Guid("84393844-6d78-4e9f-9b71-01d917aa1f40"), "/iframe/colorhunt", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("09406cce-6e21-48f7-8b78-da08c785a418"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("1882375b-7f1f-4427-8e08-ebd91f4c7bcf"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("1166ec01-ed40-4d84-b9c4-75b2b56738fa"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("03e9972f-b86c-43ca-835b-b2c8ddd46daa"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("1b36fc11-a44e-4bf1-97f4-a23c3f4004c9"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("77094f92-f0d9-43e8-a33b-614fd476c5f0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("34d3363b-d796-44e3-8064-0307335e076e"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("e88fdbf7-1c66-4652-9031-8d76a99ab735"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("50285a65-2138-465c-9306-fce6a8678bac"), null, "[]", null, null, false, null, false, null, false, new Guid("de1e5db2-35b0-4c65-b373-fef9a2e20867"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("625ef9b4-6e2f-41e5-a05f-b3db895c14fe"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("3438662b-8176-4769-a981-6fe713382c67"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("a1f881bb-1a4a-4650-9dc2-890570241e39"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("86e3080a-732d-479b-8c66-bc796311cb82"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("d311f4e9-7845-49d7-b997-3d9e2f719e4b"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("d484521c-3116-466a-9581-ce873bb66f3b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("e8705417-4f2f-404b-8401-bc71de2ba3ae"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("968d1cca-73d1-441b-96da-0706b31366ff"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("ff2a1264-72e3-4871-ab63-98e499b57455"), null, "[]", null, null, false, null, false, null, false, new Guid("40eac384-788d-4cd3-a47b-bb6b29504fb4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" }
                });
        }
    }
}
