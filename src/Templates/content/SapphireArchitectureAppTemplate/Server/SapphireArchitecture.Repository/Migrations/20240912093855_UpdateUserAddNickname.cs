﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class UpdateUserAddNickname : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("04ee4d9f-97aa-4bc1-a0d8-0fa472e10689"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("12770614-c50c-4c18-9bb6-802f3708b0bf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("29670053-afe4-4137-841f-7657bff22b71"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("48c6a731-8bfe-42ad-977e-ca6698568264"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4ecb0083-de17-4979-9e9b-882beb17e129"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("54e93b4e-5613-455c-a10b-afd10fa24154"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5507859b-b427-47c1-af55-26e5b6a085f2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("56504323-a1a9-4095-a32a-a7f03a99ad55"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5750531c-289a-4096-bdd5-9ea75bacefe3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("686ec7d9-96aa-45f5-8ef5-3f43de6ac740"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6deb98ef-d274-4ef5-9d21-5a1f5b645716"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("709ccf9e-8ef1-439b-8b16-94487d1d8afc"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7815bf0e-6c24-40ad-91df-8ae9e8916360"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7eef1097-22db-4137-aa27-70e6c9bfca4a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("808ffe27-c19b-4fea-b7fb-34adb652dee2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("84fa6157-9448-4d2d-ba72-f2603e7f8b0d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("87779379-e50b-4b23-b023-bc3aa63969c2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("99129a5e-f23a-47fa-a17e-2a176f580cad"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a2d03626-14d0-4065-9310-f71efc878e47"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("a519b4cc-9591-48f0-b7a4-4ec0bfb1d4fb"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d67adc57-44f2-4f81-935d-31972bb4ff05"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d8a29e86-fb1a-42ef-8b5d-043c936ceb73"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d9c838c7-318a-4fe3-8aa1-6f4c63f20dea"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d9cb8974-bcc5-4903-a415-eb183a2695ba"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("dddd521d-f9bc-4c47-8bee-23fea5d12956"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e7d4b3fc-a70c-446f-9572-61381c022d87"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ef799d36-5b2e-43bf-91bd-9c9e2ac167fe"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"), new Guid("08924ffe-403b-441b-85d1-b8a2925fef62") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("114693dd-e026-407c-b09b-8a6e64cef8ec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("18e8833e-f02e-4f01-998c-c7c50b2c8705"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2cfb1cf2-9c92-4100-93af-c3af5f9e9582"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("36697735-e280-4cda-ba38-6be6fb8ca760"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3a3f2314-b191-4f7c-befe-da602dd70a4d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5d82036c-15c5-4e6d-a4f4-e769720fc4f1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("774da069-2dde-4b5a-9506-2f99c5b86ff0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8875fa7b-4e42-48f6-9567-7b814b102d38"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8901e14b-dc1d-4c6d-a152-972385ef953a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8c02cfbe-806e-4f55-a447-0843dfb40b6d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8ccd85c1-adbc-418e-b21e-a16acc24d2c8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("903552d6-3419-4428-9718-54a74179dd83"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("99fdeb7a-6266-4061-9268-558ad17f4b77"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a030e15a-70dc-44a1-92ac-53d6b7efc4e8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a0b4b2c4-a3bc-40ef-8605-bad5b64de5d5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a334d31f-4536-4b79-8aef-bfddf8ff46db"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b75130bc-9eb6-4118-90fb-1c9a4816067f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c2cb1de1-2f58-4cfd-992c-a3c663ad1673"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ce419519-37b7-45fa-ab6e-dc7851d5bf75"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f6892f16-7f18-4a51-a52e-a14b52d073e9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f8d35ff6-c5fd-4fb1-964b-ed9d37ad9ac5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("08924ffe-403b-441b-85d1-b8a2925fef62"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 921, DateTimeKind.Local).AddTicks(6191),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(276));

            migrationBuilder.AddColumn<string>(
                name: "Nickname",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 922, DateTimeKind.Local).AddTicks(2499),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(5637));

            migrationBuilder.AlterColumn<bool>(
                name: "HasAllPermissions",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                type: "bit",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldNullable: true);

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), null, "iframe", null, "/iframe", null },
                    { new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), null, "system", null, "/system", null },
                    { new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), null, "monitor", null, "/monitor", null },
                    { new Guid("7cdc2bb3-5e51-4b9e-ac0f-aa7ca755d4c3"), null, "tabs", null, "/tabs", null },
                    { new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), null, "permission", null, "/permission", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "HasAllPermissions", "Name", "NormalizedName" },
                values: new object[] { new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"), null, false, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("1ac930c0-bd17-4084-ae36-273046778aec"), "378e4d58f7f956850403c37e16103242", "95nDM", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("54bcbd62-b791-42be-b9dc-4f007e1dbd15"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("64baff73-0cf3-48c6-964e-5fc04e3f8cff"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("b06bb080-e95e-4c3e-91d0-7e286cab85d8"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("db82559e-b2cc-4baa-9a18-934040145b73"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("7cdc2bb3-5e51-4b9e-ac0f-aa7ca755d4c3"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("e82b84a0-5c6b-4996-b0ed-bb4090b3cedf"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), 10, "[]", true, false, "menus.pureExternalPage" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("2f60276c-2baf-4586-8906-254db071ead5"), null, "PermissionPage", new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), "/permission/page/index", null },
                    { new Guid("6ed47df0-eca9-40f5-84a0-c7c8e24b914a"), "monitor/logs/login/index", "LoginLog", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/login-logs", null },
                    { new Guid("88e8ccfc-1ef3-479f-a2bc-e1cceda52931"), null, "PermissionButton", new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), "/permission/button/index", null },
                    { new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), null, "External", new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), "/iframe/external", null },
                    { new Guid("94a1449a-b1fc-4d71-8cbe-04fd2e38b1d0"), null, "SystemUser", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/user/index", null },
                    { new Guid("9905d7f8-61ca-45e9-8e84-141b52b36f36"), "monitor/logs/operation/index", "OperationLog", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/operation-logs", null },
                    { new Guid("9e9f5645-7223-4e07-8332-cc2a212569a3"), null, "SystemDept", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/dept/index", null },
                    { new Guid("b18858c4-78d7-469c-b412-6162ff1971d1"), null, "SystemRole", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/role/index", null },
                    { new Guid("cc132e43-2105-41af-be87-39c8c6658120"), "monitor/logs/system/index", "SystemLog", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/system-logs", null },
                    { new Guid("ce14485c-3d07-442a-81b3-09208d0ae41a"), null, "SystemMenu", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/menu/index", null },
                    { new Guid("e4ea0dd3-5aa2-4dc3-8699-fda64f03e17d"), "monitor/online/index", "OnlineUser", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/online-user", null },
                    { new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), null, "Embedded", new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), "/iframe/embedded", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"), new Guid("1ac930c0-bd17-4084-ae36-273046778aec"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0082d8f4-19cd-46fd-80be-072b5fc903f6"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("cc132e43-2105-41af-be87-39c8c6658120"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("12a2709d-08f5-469f-96fe-8de7ae585d4d"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("b18858c4-78d7-469c-b412-6162ff1971d1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("171fe400-2d54-43da-9956-c661b6cf0d46"), null, "[]", null, null, false, null, false, null, false, new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("22fc3a59-79e4-4a66-a199-aee7431a229a"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("9e9f5645-7223-4e07-8332-cc2a212569a3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("4972f434-33ea-4ac5-96e5-7040cb2acfb4"), null, "[]", null, null, false, null, false, null, false, new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("51bcc4ad-a378-4571-be26-da652abaac55"), null, "[]", null, null, false, null, false, null, false, new Guid("2f60276c-2baf-4586-8906-254db071ead5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("5547d86d-1912-412f-b96c-e6bc0ffb6ca5"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("6ed47df0-eca9-40f5-84a0-c7c8e24b914a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("697614b8-d9e3-418d-9f95-306f70b0ff87"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("e4ea0dd3-5aa2-4dc3-8699-fda64f03e17d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("b16d8a7e-a255-4337-9289-3bf0bad1076a"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("9905d7f8-61ca-45e9-8e84-141b52b36f36"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("d746eae2-d07c-4c51-a2ca-6310caf51570"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("ce14485c-3d07-442a-81b3-09208d0ae41a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("db6830b8-0923-4d51-960d-744bc17550d5"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("88e8ccfc-1ef3-479f-a2bc-e1cceda52931"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("e4750e3e-015f-4bd0-ac22-e25906de14e9"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("94a1449a-b1fc-4d71-8cbe-04fd2e38b1d0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("11f7ba7a-8ac3-472a-9aca-026f8c3f19b9"), null, "FrameColorHunt", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/colorhunt", null },
                    { new Guid("3441e578-b483-4e6e-becc-5d0575cbcf12"), null, "https://pure-admin-utils.netlify.app/", new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), "/pureUtilsLink", null },
                    { new Guid("3abae703-8e55-4757-9b42-8518ac219ce7"), null, "FrameVite", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/vite", null },
                    { new Guid("48ce4194-4a1c-464d-9cba-382fa97eaad4"), null, "FrameTailwindcss", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/tailwindcss", null },
                    { new Guid("585ffc34-c35c-455a-a553-a1119077a02c"), null, "FrameVue", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/vue3", null },
                    { new Guid("a860382a-7a83-4a1d-9cd0-5b6eecd1664e"), null, "FrameUiGradients", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/uigradients", null },
                    { new Guid("c3ef4634-29d1-4365-af42-72c3749d904e"), null, "FrameEp", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/ep", null },
                    { new Guid("c50b8c00-8b95-49fc-8f35-7f85c2e59bf7"), null, "FrameRouter", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/vue-router", null },
                    { new Guid("c64d3b0c-e835-46c2-bb48-3e9b24216dab"), null, "FramePinia", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/pinia", null },
                    { new Guid("e37926f0-9283-4658-b628-f84386dc08ac"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), "/external", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("406f1ad8-8495-45c5-8d7e-055dff6f67cf"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("48ce4194-4a1c-464d-9cba-382fa97eaad4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("499ebe7c-3584-4214-b9b1-b69582f67e4a"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("c3ef4634-29d1-4365-af42-72c3749d904e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("4ac850c4-6abc-48d9-a63d-d4a1b1bdf1ab"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("c50b8c00-8b95-49fc-8f35-7f85c2e59bf7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("4bb914c5-66fe-4fb1-b2c8-f710c131a449"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("3abae703-8e55-4757-9b42-8518ac219ce7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("60d8a395-1e88-43b5-99be-916ed0166ab4"), null, "[]", null, null, false, null, false, null, false, new Guid("3441e578-b483-4e6e-becc-5d0575cbcf12"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("7a517050-4bf2-4c41-9735-2784df43a26b"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("585ffc34-c35c-455a-a553-a1119077a02c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("b4a162df-3195-4b54-9d81-b2d5ba467173"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("c64d3b0c-e835-46c2-bb48-3e9b24216dab"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("b63a07be-2351-41b4-a3c7-be98295d2250"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("a860382a-7a83-4a1d-9cd0-5b6eecd1664e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("d5ce21ea-8e4a-4c05-9b79-2f6e98e9b7b7"), null, "[]", null, null, false, null, false, null, false, new Guid("e37926f0-9283-4658-b628-f84386dc08ac"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("de148382-e89a-47c9-994e-71faa04e84d5"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("11f7ba7a-8ac3-472a-9aca-026f8c3f19b9"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0082d8f4-19cd-46fd-80be-072b5fc903f6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("12a2709d-08f5-469f-96fe-8de7ae585d4d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("171fe400-2d54-43da-9956-c661b6cf0d46"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("22fc3a59-79e4-4a66-a199-aee7431a229a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("406f1ad8-8495-45c5-8d7e-055dff6f67cf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4972f434-33ea-4ac5-96e5-7040cb2acfb4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("499ebe7c-3584-4214-b9b1-b69582f67e4a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4ac850c4-6abc-48d9-a63d-d4a1b1bdf1ab"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4bb914c5-66fe-4fb1-b2c8-f710c131a449"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("51bcc4ad-a378-4571-be26-da652abaac55"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("54bcbd62-b791-42be-b9dc-4f007e1dbd15"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5547d86d-1912-412f-b96c-e6bc0ffb6ca5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("60d8a395-1e88-43b5-99be-916ed0166ab4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("64baff73-0cf3-48c6-964e-5fc04e3f8cff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("697614b8-d9e3-418d-9f95-306f70b0ff87"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7a517050-4bf2-4c41-9735-2784df43a26b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b06bb080-e95e-4c3e-91d0-7e286cab85d8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b16d8a7e-a255-4337-9289-3bf0bad1076a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b4a162df-3195-4b54-9d81-b2d5ba467173"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b63a07be-2351-41b4-a3c7-be98295d2250"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d5ce21ea-8e4a-4c05-9b79-2f6e98e9b7b7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d746eae2-d07c-4c51-a2ca-6310caf51570"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("db6830b8-0923-4d51-960d-744bc17550d5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("db82559e-b2cc-4baa-9a18-934040145b73"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("de148382-e89a-47c9-994e-71faa04e84d5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e4750e3e-015f-4bd0-ac22-e25906de14e9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e82b84a0-5c6b-4996-b0ed-bb4090b3cedf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"), new Guid("1ac930c0-bd17-4084-ae36-273046778aec") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("11f7ba7a-8ac3-472a-9aca-026f8c3f19b9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2f60276c-2baf-4586-8906-254db071ead5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3441e578-b483-4e6e-becc-5d0575cbcf12"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3abae703-8e55-4757-9b42-8518ac219ce7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("48ce4194-4a1c-464d-9cba-382fa97eaad4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("585ffc34-c35c-455a-a553-a1119077a02c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6ed47df0-eca9-40f5-84a0-c7c8e24b914a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7cdc2bb3-5e51-4b9e-ac0f-aa7ca755d4c3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("88e8ccfc-1ef3-479f-a2bc-e1cceda52931"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("94a1449a-b1fc-4d71-8cbe-04fd2e38b1d0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9905d7f8-61ca-45e9-8e84-141b52b36f36"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9e9f5645-7223-4e07-8332-cc2a212569a3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a860382a-7a83-4a1d-9cd0-5b6eecd1664e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b18858c4-78d7-469c-b412-6162ff1971d1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c3ef4634-29d1-4365-af42-72c3749d904e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c50b8c00-8b95-49fc-8f35-7f85c2e59bf7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c64d3b0c-e835-46c2-bb48-3e9b24216dab"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("cc132e43-2105-41af-be87-39c8c6658120"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ce14485c-3d07-442a-81b3-09208d0ae41a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e37926f0-9283-4658-b628-f84386dc08ac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e4ea0dd3-5aa2-4dc3-8699-fda64f03e17d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("1ac930c0-bd17-4084-ae36-273046778aec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"));

            migrationBuilder.DropColumn(
                name: "Nickname",
                schema: "SapphireArchitecture",
                table: "Basic_Users");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(276),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 921, DateTimeKind.Local).AddTicks(6191));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 6, 17, 4, 35, 265, DateTimeKind.Local).AddTicks(5637),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 922, DateTimeKind.Local).AddTicks(2499));

            migrationBuilder.AlterColumn<bool>(
                name: "HasAllPermissions",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                type: "bit",
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "bit");

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), null, "system", null, "/system", null },
                    { new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), null, "monitor", null, "/monitor", null },
                    { new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), null, "permission", null, "/permission", null },
                    { new Guid("c2cb1de1-2f58-4cfd-992c-a3c663ad1673"), null, "tabs", null, "/tabs", null },
                    { new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), null, "iframe", null, "/iframe", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "HasAllPermissions", "Name", "NormalizedName" },
                values: new object[] { new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"), null, false, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("08924ffe-403b-441b-85d1-b8a2925fef62"), "2daccd3a1e5f4d9368ca29ccee834d29", "NAjKo", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("48c6a731-8bfe-42ad-977e-ca6698568264"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("56504323-a1a9-4095-a32a-a7f03a99ad55"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("709ccf9e-8ef1-439b-8b16-94487d1d8afc"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("c2cb1de1-2f58-4cfd-992c-a3c663ad1673"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("7eef1097-22db-4137-aa27-70e6c9bfca4a"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("d9c838c7-318a-4fe3-8aa1-6f4c63f20dea"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("18e8833e-f02e-4f01-998c-c7c50b2c8705"), "monitor/logs/login/index", "LoginLog", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/login-logs", null },
                    { new Guid("36697735-e280-4cda-ba38-6be6fb8ca760"), null, "SystemDept", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/dept/index", null },
                    { new Guid("8901e14b-dc1d-4c6d-a152-972385ef953a"), "monitor/logs/operation/index", "OperationLog", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/operation-logs", null },
                    { new Guid("8ccd85c1-adbc-418e-b21e-a16acc24d2c8"), null, "PermissionButton", new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), "/permission/button/index", null },
                    { new Guid("903552d6-3419-4428-9718-54a74179dd83"), null, "SystemUser", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/user/index", null },
                    { new Guid("99fdeb7a-6266-4061-9268-558ad17f4b77"), null, "PermissionPage", new Guid("47ffaf03-7c63-4bd5-ac40-75d503c010f5"), "/permission/page/index", null },
                    { new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), null, "External", new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), "/iframe/external", null },
                    { new Guid("a030e15a-70dc-44a1-92ac-53d6b7efc4e8"), "monitor/online/index", "OnlineUser", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/online-user", null },
                    { new Guid("b75130bc-9eb6-4118-90fb-1c9a4816067f"), null, "SystemRole", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/role/index", null },
                    { new Guid("ce419519-37b7-45fa-ab6e-dc7851d5bf75"), null, "SystemMenu", new Guid("0a290826-2dfa-4d10-956f-46b678296e0b"), "/system/menu/index", null },
                    { new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), null, "Embedded", new Guid("f9e9ccb0-c49d-4849-8418-d2ac01a151e6"), "/iframe/embedded", null },
                    { new Guid("f6892f16-7f18-4a51-a52e-a14b52d073e9"), "monitor/logs/system/index", "SystemLog", new Guid("27c2943c-291a-4b36-af92-e92e6dc0a885"), "/monitor/system-logs", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("5de14f6e-42eb-49c9-8606-22ced8faf71b"), new Guid("08924ffe-403b-441b-85d1-b8a2925fef62"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("12770614-c50c-4c18-9bb6-802f3708b0bf"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("8ccd85c1-adbc-418e-b21e-a16acc24d2c8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("4ecb0083-de17-4979-9e9b-882beb17e129"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("f6892f16-7f18-4a51-a52e-a14b52d073e9"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("5507859b-b427-47c1-af55-26e5b6a085f2"), null, "[]", null, null, false, null, false, null, false, new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("686ec7d9-96aa-45f5-8ef5-3f43de6ac740"), null, "[]", null, null, false, null, false, null, false, new Guid("99fdeb7a-6266-4061-9268-558ad17f4b77"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("6deb98ef-d274-4ef5-9d21-5a1f5b645716"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("8901e14b-dc1d-4c6d-a152-972385ef953a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("7815bf0e-6c24-40ad-91df-8ae9e8916360"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("b75130bc-9eb6-4118-90fb-1c9a4816067f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("808ffe27-c19b-4fea-b7fb-34adb652dee2"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("36697735-e280-4cda-ba38-6be6fb8ca760"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("a519b4cc-9591-48f0-b7a4-4ec0bfb1d4fb"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("18e8833e-f02e-4f01-998c-c7c50b2c8705"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("d8a29e86-fb1a-42ef-8b5d-043c936ceb73"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("ce419519-37b7-45fa-ab6e-dc7851d5bf75"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("dddd521d-f9bc-4c47-8bee-23fea5d12956"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("903552d6-3419-4428-9718-54a74179dd83"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("e7d4b3fc-a70c-446f-9572-61381c022d87"), null, "[]", null, null, false, null, false, null, false, new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("ef799d36-5b2e-43bf-91bd-9c9e2ac167fe"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("a030e15a-70dc-44a1-92ac-53d6b7efc4e8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("114693dd-e026-407c-b09b-8a6e64cef8ec"), null, "FrameColorHunt", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/colorhunt", null },
                    { new Guid("2cfb1cf2-9c92-4100-93af-c3af5f9e9582"), null, "FrameEp", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/ep", null },
                    { new Guid("3a3f2314-b191-4f7c-befe-da602dd70a4d"), null, "FrameUiGradients", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/uigradients", null },
                    { new Guid("5d82036c-15c5-4e6d-a4f4-e769720fc4f1"), null, "FrameTailwindcss", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/tailwindcss", null },
                    { new Guid("774da069-2dde-4b5a-9506-2f99c5b86ff0"), null, "FrameRouter", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/vue-router", null },
                    { new Guid("8875fa7b-4e42-48f6-9567-7b814b102d38"), null, "FramePinia", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/pinia", null },
                    { new Guid("8c02cfbe-806e-4f55-a447-0843dfb40b6d"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), "/external", null },
                    { new Guid("a0b4b2c4-a3bc-40ef-8605-bad5b64de5d5"), null, "FrameVue", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/vue3", null },
                    { new Guid("a334d31f-4536-4b79-8aef-bfddf8ff46db"), null, "https://pure-admin-utils.netlify.app/", new Guid("9d8a83a7-c702-46da-8098-5afacc657c57"), "/pureUtilsLink", null },
                    { new Guid("f8d35ff6-c5fd-4fb1-964b-ed9d37ad9ac5"), null, "FrameVite", new Guid("f58e0baa-1d5a-49c8-8612-efe670b70e5a"), "/iframe/vite", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("04ee4d9f-97aa-4bc1-a0d8-0fa472e10689"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("774da069-2dde-4b5a-9506-2f99c5b86ff0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("29670053-afe4-4137-841f-7657bff22b71"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("114693dd-e026-407c-b09b-8a6e64cef8ec"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("54e93b4e-5613-455c-a10b-afd10fa24154"), null, "[]", null, null, false, null, false, null, false, new Guid("8c02cfbe-806e-4f55-a447-0843dfb40b6d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("5750531c-289a-4096-bdd5-9ea75bacefe3"), null, "[]", null, null, false, null, false, null, false, new Guid("a334d31f-4536-4b79-8aef-bfddf8ff46db"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("84fa6157-9448-4d2d-ba72-f2603e7f8b0d"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("5d82036c-15c5-4e6d-a4f4-e769720fc4f1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("87779379-e50b-4b23-b023-bc3aa63969c2"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("a0b4b2c4-a3bc-40ef-8605-bad5b64de5d5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("99129a5e-f23a-47fa-a17e-2a176f580cad"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("2cfb1cf2-9c92-4100-93af-c3af5f9e9582"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("a2d03626-14d0-4065-9310-f71efc878e47"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("3a3f2314-b191-4f7c-befe-da602dd70a4d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("d67adc57-44f2-4f81-935d-31972bb4ff05"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("f8d35ff6-c5fd-4fb1-964b-ed9d37ad9ac5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("d9cb8974-bcc5-4903-a415-eb183a2695ba"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("8875fa7b-4e42-48f6-9567-7b814b102d38"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" }
                });
        }
    }
}
