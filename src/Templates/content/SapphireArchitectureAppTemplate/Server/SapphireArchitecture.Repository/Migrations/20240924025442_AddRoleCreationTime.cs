﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace SapphireArchitecture.Repository.Migrations
{
    /// <inheritdoc />
    public partial class AddRoleCreationTime : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("0082d8f4-19cd-46fd-80be-072b5fc903f6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("12a2709d-08f5-469f-96fe-8de7ae585d4d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("171fe400-2d54-43da-9956-c661b6cf0d46"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("22fc3a59-79e4-4a66-a199-aee7431a229a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("406f1ad8-8495-45c5-8d7e-055dff6f67cf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4972f434-33ea-4ac5-96e5-7040cb2acfb4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("499ebe7c-3584-4214-b9b1-b69582f67e4a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4ac850c4-6abc-48d9-a63d-d4a1b1bdf1ab"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("4bb914c5-66fe-4fb1-b2c8-f710c131a449"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("51bcc4ad-a378-4571-be26-da652abaac55"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("54bcbd62-b791-42be-b9dc-4f007e1dbd15"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5547d86d-1912-412f-b96c-e6bc0ffb6ca5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("60d8a395-1e88-43b5-99be-916ed0166ab4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("64baff73-0cf3-48c6-964e-5fc04e3f8cff"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("697614b8-d9e3-418d-9f95-306f70b0ff87"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7a517050-4bf2-4c41-9735-2784df43a26b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b06bb080-e95e-4c3e-91d0-7e286cab85d8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b16d8a7e-a255-4337-9289-3bf0bad1076a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b4a162df-3195-4b54-9d81-b2d5ba467173"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b63a07be-2351-41b4-a3c7-be98295d2250"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d5ce21ea-8e4a-4c05-9b79-2f6e98e9b7b7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("d746eae2-d07c-4c51-a2ca-6310caf51570"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("db6830b8-0923-4d51-960d-744bc17550d5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("db82559e-b2cc-4baa-9a18-934040145b73"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("de148382-e89a-47c9-994e-71faa04e84d5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e4750e3e-015f-4bd0-ac22-e25906de14e9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("e82b84a0-5c6b-4996-b0ed-bb4090b3cedf"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"), new Guid("1ac930c0-bd17-4084-ae36-273046778aec") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("11f7ba7a-8ac3-472a-9aca-026f8c3f19b9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2f60276c-2baf-4586-8906-254db071ead5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3441e578-b483-4e6e-becc-5d0575cbcf12"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3abae703-8e55-4757-9b42-8518ac219ce7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("48ce4194-4a1c-464d-9cba-382fa97eaad4"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("585ffc34-c35c-455a-a553-a1119077a02c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6ed47df0-eca9-40f5-84a0-c7c8e24b914a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("7cdc2bb3-5e51-4b9e-ac0f-aa7ca755d4c3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("88e8ccfc-1ef3-479f-a2bc-e1cceda52931"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("94a1449a-b1fc-4d71-8cbe-04fd2e38b1d0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9905d7f8-61ca-45e9-8e84-141b52b36f36"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("9e9f5645-7223-4e07-8332-cc2a212569a3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a860382a-7a83-4a1d-9cd0-5b6eecd1664e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("b18858c4-78d7-469c-b412-6162ff1971d1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c3ef4634-29d1-4365-af42-72c3749d904e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c50b8c00-8b95-49fc-8f35-7f85c2e59bf7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c64d3b0c-e835-46c2-bb48-3e9b24216dab"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("cc132e43-2105-41af-be87-39c8c6658120"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ce14485c-3d07-442a-81b3-09208d0ae41a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e37926f0-9283-4658-b628-f84386dc08ac"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e4ea0dd3-5aa2-4dc3-8699-fda64f03e17d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("1ac930c0-bd17-4084-ae36-273046778aec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 24, 10, 54, 42, 307, DateTimeKind.Local).AddTicks(8524),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 921, DateTimeKind.Local).AddTicks(6191));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 24, 10, 54, 42, 308, DateTimeKind.Local).AddTicks(4757),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 922, DateTimeKind.Local).AddTicks(2499));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("42795c07-968c-4d58-b238-9a36f7d351e3"), null, "system", null, "/system", null },
                    { new Guid("5340b095-ce92-4e3d-b706-20674c090764"), null, "monitor", null, "/monitor", null },
                    { new Guid("cc67c918-b0da-45d4-bbf3-8a421ca4deec"), null, "permission", null, "/permission", null },
                    { new Guid("e0a476d8-cd5e-4df0-990b-2ef65be79818"), null, "iframe", null, "/iframe", null },
                    { new Guid("e4d34477-a6d7-4ecc-b371-8916b346c374"), null, "tabs", null, "/tabs", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "CreationTime", "Description", "HasAllPermissions", "IsDeleted", "Name", "NormalizedName" },
                values: new object[] { new Guid("2ef98c70-ce69-4ad0-b3c6-a2a37e90a6e9"), new DateTime(2024, 9, 24, 10, 54, 42, 304, DateTimeKind.Local).AddTicks(2782), null, false, false, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("5372cbde-3a85-4561-9210-0927fa790f8c"), "495f39e9920585c85f9fd836df49fc46", "DLvTY", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("11b9a3c5-f363-4245-863c-6b2fba198bc5"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("42795c07-968c-4d58-b238-9a36f7d351e3"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("627cef77-7629-4ab8-bd3a-31cd5c1fdc46"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("cc67c918-b0da-45d4-bbf3-8a421ca4deec"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("67b00e0b-2839-4ed0-8d16-ebde6decea3b"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("e4d34477-a6d7-4ecc-b371-8916b346c374"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("c1fb7268-036a-429a-9a00-d19835b9573e"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("e0a476d8-cd5e-4df0-990b-2ef65be79818"), 10, "[]", true, false, "menus.pureExternalPage" },
                    { new Guid("ed4b78d7-c302-46f7-b1ba-e9b03c8da413"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("5340b095-ce92-4e3d-b706-20674c090764"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("197d1029-b854-43f6-a697-e9dac503153d"), "monitor/online/index", "OnlineUser", new Guid("5340b095-ce92-4e3d-b706-20674c090764"), "/monitor/online-user", null },
                    { new Guid("1c9357e9-9771-4cdc-8e50-c682c2853480"), null, "External", new Guid("e0a476d8-cd5e-4df0-990b-2ef65be79818"), "/iframe/external", null },
                    { new Guid("2cfd8c84-be53-44ef-8dcf-0bfbac3e87e2"), null, "SystemRole", new Guid("42795c07-968c-4d58-b238-9a36f7d351e3"), "/system/role/index", null },
                    { new Guid("419a126b-0ca5-48c5-84ab-903e34d80777"), "monitor/logs/login/index", "LoginLog", new Guid("5340b095-ce92-4e3d-b706-20674c090764"), "/monitor/login-logs", null },
                    { new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), null, "Embedded", new Guid("e0a476d8-cd5e-4df0-990b-2ef65be79818"), "/iframe/embedded", null },
                    { new Guid("725bec13-c227-4d3e-86eb-aecf6bb50074"), null, "PermissionPage", new Guid("cc67c918-b0da-45d4-bbf3-8a421ca4deec"), "/permission/page/index", null },
                    { new Guid("848cae0f-208e-494c-ae24-b8cee9a8cfb8"), "monitor/logs/operation/index", "OperationLog", new Guid("5340b095-ce92-4e3d-b706-20674c090764"), "/monitor/operation-logs", null },
                    { new Guid("8e96d785-5ae0-484f-8126-78ffaa3256ab"), null, "SystemMenu", new Guid("42795c07-968c-4d58-b238-9a36f7d351e3"), "/system/menu/index", null },
                    { new Guid("a1348692-8f0c-46fa-a1b6-9566d9aa165c"), null, "PermissionButton", new Guid("cc67c918-b0da-45d4-bbf3-8a421ca4deec"), "/permission/button/index", null },
                    { new Guid("bd7d6e19-ddc1-406f-9330-0916a41671bd"), null, "SystemUser", new Guid("42795c07-968c-4d58-b238-9a36f7d351e3"), "/system/user/index", null },
                    { new Guid("c22699fc-a083-4e1a-b8fb-e248951dbea1"), "monitor/logs/system/index", "SystemLog", new Guid("5340b095-ce92-4e3d-b706-20674c090764"), "/monitor/system-logs", null },
                    { new Guid("e5516fd6-eb73-452e-8d49-ac3c5a4d3ac7"), null, "SystemDept", new Guid("42795c07-968c-4d58-b238-9a36f7d351e3"), "/system/dept/index", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("2ef98c70-ce69-4ad0-b3c6-a2a37e90a6e9"), new Guid("5372cbde-3a85-4561-9210-0927fa790f8c"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("2eac5801-86ce-4a90-b91a-9d9e8603d77d"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("a1348692-8f0c-46fa-a1b6-9566d9aa165c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("3c8abd26-3bf9-453b-8259-95f4ad5650c0"), null, "[]", null, null, false, null, false, null, false, new Guid("1c9357e9-9771-4cdc-8e50-c682c2853480"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("50ea955b-0230-4c13-aa7a-0ff5fde97c4b"), null, "[]", null, null, false, null, false, null, false, new Guid("725bec13-c227-4d3e-86eb-aecf6bb50074"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("5b538889-c873-4287-9095-a19d7a381705"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("bd7d6e19-ddc1-406f-9330-0916a41671bd"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" },
                    { new Guid("6a1303d4-d503-4fd5-ae54-f12b3e2f5470"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("2cfd8c84-be53-44ef-8dcf-0bfbac3e87e2"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("6f776ef9-50c4-4a8e-beb1-bf81971f8e85"), null, "[]", null, null, false, null, false, null, false, new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("74b8f4d2-e347-4586-8122-fca78744ad39"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("197d1029-b854-43f6-a697-e9dac503153d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("7b32df6f-967e-4f4b-8ca9-cddf1e9f1038"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("e5516fd6-eb73-452e-8d49-ac3c5a4d3ac7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("7e419779-913d-4164-bc71-6348552c4b61"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("c22699fc-a083-4e1a-b8fb-e248951dbea1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("b0456d4d-f193-47f9-b42a-4438d6cd2f60"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("8e96d785-5ae0-484f-8126-78ffaa3256ab"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("f37e1d10-e663-478a-b059-f863c48c8654"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("848cae0f-208e-494c-ae24-b8cee9a8cfb8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("fdcf36ac-b08f-4f2a-b8c8-e3b6da570778"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("419a126b-0ca5-48c5-84ab-903e34d80777"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0735a872-e098-480f-937b-c46921dc2c67"), null, "FramePinia", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/pinia", null },
                    { new Guid("3d666f1b-3138-43f9-a239-af0e7234cd9b"), null, "https://pure-admin-utils.netlify.app/", new Guid("1c9357e9-9771-4cdc-8e50-c682c2853480"), "/pureUtilsLink", null },
                    { new Guid("5224fa08-3da5-4bfb-a12c-46a3a2dce591"), null, "FrameTailwindcss", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/tailwindcss", null },
                    { new Guid("6fe6a8b5-3bc4-4152-88fd-7f14f7e75bc8"), null, "FrameVite", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/vite", null },
                    { new Guid("98c82187-24d0-412b-9b1b-c3481a5539cd"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("1c9357e9-9771-4cdc-8e50-c682c2853480"), "/external", null },
                    { new Guid("a1fa2b37-43f0-4c44-8dc4-5318c77ae235"), null, "FrameVue", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/vue3", null },
                    { new Guid("be69c8f6-7228-4b20-a392-3204f568115f"), null, "FrameRouter", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/vue-router", null },
                    { new Guid("d739cbbf-e8ea-4247-983d-028e9a6b7e94"), null, "FrameColorHunt", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/colorhunt", null },
                    { new Guid("ef68f1dd-dca1-41b4-9265-7365ea5a85c6"), null, "FrameEp", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/ep", null },
                    { new Guid("f788f2ee-771a-4713-87a2-e3400f484380"), null, "FrameUiGradients", new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"), "/iframe/uigradients", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("167fb7fb-14e9-4cef-b513-2a0933d7a1d5"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("6fe6a8b5-3bc4-4152-88fd-7f14f7e75bc8"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("214b1995-dae8-4a55-bf74-8c5fd2949952"), null, "[]", null, null, false, null, false, null, false, new Guid("98c82187-24d0-412b-9b1b-c3481a5539cd"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("5c1382c9-c89d-4584-a0f3-4aada3443aae"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("5224fa08-3da5-4bfb-a12c-46a3a2dce591"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("60319d1f-4107-402e-b497-95fd3a9c1a8c"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("0735a872-e098-480f-937b-c46921dc2c67"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("6aa7726a-ea4d-4742-99c3-0564c10c72d7"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("d739cbbf-e8ea-4247-983d-028e9a6b7e94"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" },
                    { new Guid("84c71c2d-eafb-45f8-a7b9-68480e43b52b"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("f788f2ee-771a-4713-87a2-e3400f484380"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("ba0c729b-ba78-4205-87d3-0511130b5b4f"), null, "[]", null, null, false, null, false, null, false, new Guid("3d666f1b-3138-43f9-a239-af0e7234cd9b"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("bf29a2db-4451-44fc-9e62-7bb2f3af7860"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("ef68f1dd-dca1-41b4-9265-7365ea5a85c6"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("ce762c0b-0197-4bcb-942f-5e26530030ae"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("be69c8f6-7228-4b20-a392-3204f568115f"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("ea41094a-4f4d-40b8-86ec-e60b4e02e70b"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("a1fa2b37-43f0-4c44-8dc4-5318c77ae235"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("11b9a3c5-f363-4245-863c-6b2fba198bc5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("167fb7fb-14e9-4cef-b513-2a0933d7a1d5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("214b1995-dae8-4a55-bf74-8c5fd2949952"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("2eac5801-86ce-4a90-b91a-9d9e8603d77d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("3c8abd26-3bf9-453b-8259-95f4ad5650c0"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("50ea955b-0230-4c13-aa7a-0ff5fde97c4b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5b538889-c873-4287-9095-a19d7a381705"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("5c1382c9-c89d-4584-a0f3-4aada3443aae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("60319d1f-4107-402e-b497-95fd3a9c1a8c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("627cef77-7629-4ab8-bd3a-31cd5c1fdc46"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("67b00e0b-2839-4ed0-8d16-ebde6decea3b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6a1303d4-d503-4fd5-ae54-f12b3e2f5470"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6aa7726a-ea4d-4742-99c3-0564c10c72d7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("6f776ef9-50c4-4a8e-beb1-bf81971f8e85"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("74b8f4d2-e347-4586-8122-fca78744ad39"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7b32df6f-967e-4f4b-8ca9-cddf1e9f1038"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("7e419779-913d-4164-bc71-6348552c4b61"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("84c71c2d-eafb-45f8-a7b9-68480e43b52b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("b0456d4d-f193-47f9-b42a-4438d6cd2f60"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ba0c729b-ba78-4205-87d3-0511130b5b4f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("bf29a2db-4451-44fc-9e62-7bb2f3af7860"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("c1fb7268-036a-429a-9a00-d19835b9573e"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ce762c0b-0197-4bcb-942f-5e26530030ae"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ea41094a-4f4d-40b8-86ec-e60b4e02e70b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("ed4b78d7-c302-46f7-b1ba-e9b03c8da413"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("f37e1d10-e663-478a-b059-f863c48c8654"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                keyColumn: "Id",
                keyValue: new Guid("fdcf36ac-b08f-4f2a-b8c8-e3b6da570778"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("2ef98c70-ce69-4ad0-b3c6-a2a37e90a6e9"), new Guid("5372cbde-3a85-4561-9210-0927fa790f8c") });

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("0735a872-e098-480f-937b-c46921dc2c67"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("197d1029-b854-43f6-a697-e9dac503153d"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("2cfd8c84-be53-44ef-8dcf-0bfbac3e87e2"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("3d666f1b-3138-43f9-a239-af0e7234cd9b"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("419a126b-0ca5-48c5-84ab-903e34d80777"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5224fa08-3da5-4bfb-a12c-46a3a2dce591"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("6fe6a8b5-3bc4-4152-88fd-7f14f7e75bc8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("725bec13-c227-4d3e-86eb-aecf6bb50074"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("848cae0f-208e-494c-ae24-b8cee9a8cfb8"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("8e96d785-5ae0-484f-8126-78ffaa3256ab"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("98c82187-24d0-412b-9b1b-c3481a5539cd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a1348692-8f0c-46fa-a1b6-9566d9aa165c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("a1fa2b37-43f0-4c44-8dc4-5318c77ae235"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("bd7d6e19-ddc1-406f-9330-0916a41671bd"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("be69c8f6-7228-4b20-a392-3204f568115f"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("c22699fc-a083-4e1a-b8fb-e248951dbea1"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("d739cbbf-e8ea-4247-983d-028e9a6b7e94"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e4d34477-a6d7-4ecc-b371-8916b346c374"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e5516fd6-eb73-452e-8d49-ac3c5a4d3ac7"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("ef68f1dd-dca1-41b4-9265-7365ea5a85c6"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("f788f2ee-771a-4713-87a2-e3400f484380"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                keyColumn: "Id",
                keyValue: new Guid("2ef98c70-ce69-4ad0-b3c6-a2a37e90a6e9"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                keyColumn: "Id",
                keyValue: new Guid("5372cbde-3a85-4561-9210-0927fa790f8c"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("1c9357e9-9771-4cdc-8e50-c682c2853480"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("42795c07-968c-4d58-b238-9a36f7d351e3"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("4f85b24b-f70d-4b58-9d0b-f50ad2ad8bc5"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("5340b095-ce92-4e3d-b706-20674c090764"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("cc67c918-b0da-45d4-bbf3-8a421ca4deec"));

            migrationBuilder.DeleteData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                keyColumn: "Id",
                keyValue: new Guid("e0a476d8-cd5e-4df0-990b-2ef65be79818"));

            migrationBuilder.DropColumn(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 921, DateTimeKind.Local).AddTicks(6191),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 24, 10, 54, 42, 307, DateTimeKind.Local).AddTicks(8524));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationTime",
                schema: "SapphireArchitecture",
                table: "Basic_Departments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2024, 9, 12, 17, 38, 54, 922, DateTimeKind.Local).AddTicks(2499),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2024, 9, 24, 10, 54, 42, 308, DateTimeKind.Local).AddTicks(4757));

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), null, "iframe", null, "/iframe", null },
                    { new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), null, "system", null, "/system", null },
                    { new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), null, "monitor", null, "/monitor", null },
                    { new Guid("7cdc2bb3-5e51-4b9e-ac0f-aa7ca755d4c3"), null, "tabs", null, "/tabs", null },
                    { new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), null, "permission", null, "/permission", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Roles",
                columns: new[] { "Id", "Description", "HasAllPermissions", "Name", "NormalizedName" },
                values: new object[] { new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"), null, false, "SuperAdmin", "SUPERADMIN" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "UserName" },
                values: new object[] { new Guid("1ac930c0-bd17-4084-ae36-273046778aec"), "378e4d58f7f956850403c37e16103242", "95nDM", "admin" });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("54bcbd62-b791-42be-b9dc-4f007e1dbd15"), null, "[]", null, null, false, null, false, "ep:lollipop", false, new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), 12, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermission" },
                    { new Guid("64baff73-0cf3-48c6-964e-5fc04e3f8cff"), null, "[]", null, null, false, null, false, "ep:monitor", false, new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), 14, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysMonitor" },
                    { new Guid("b06bb080-e95e-4c3e-91d0-7e286cab85d8"), null, "[]", null, null, false, null, false, "ri:settings-3-line", false, new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), 13, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSysManagement" },
                    { new Guid("db82559e-b2cc-4baa-9a18-934040145b73"), null, "[]", null, null, false, null, false, "ri:bookmark-2-line", false, new Guid("7cdc2bb3-5e51-4b9e-ac0f-aa7ca755d4c3"), 15, "[]", true, false, "menus.pureTabs" },
                    { new Guid("e82b84a0-5c6b-4996-b0ed-bb4090b3cedf"), null, "[]", null, null, false, null, false, "ri:links-fill", false, new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), 10, "[]", true, false, "menus.pureExternalPage" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("2f60276c-2baf-4586-8906-254db071ead5"), null, "PermissionPage", new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), "/permission/page/index", null },
                    { new Guid("6ed47df0-eca9-40f5-84a0-c7c8e24b914a"), "monitor/logs/login/index", "LoginLog", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/login-logs", null },
                    { new Guid("88e8ccfc-1ef3-479f-a2bc-e1cceda52931"), null, "PermissionButton", new Guid("894c2308-1c0b-4a98-9f0c-79f2a89f156a"), "/permission/button/index", null },
                    { new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), null, "External", new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), "/iframe/external", null },
                    { new Guid("94a1449a-b1fc-4d71-8cbe-04fd2e38b1d0"), null, "SystemUser", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/user/index", null },
                    { new Guid("9905d7f8-61ca-45e9-8e84-141b52b36f36"), "monitor/logs/operation/index", "OperationLog", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/operation-logs", null },
                    { new Guid("9e9f5645-7223-4e07-8332-cc2a212569a3"), null, "SystemDept", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/dept/index", null },
                    { new Guid("b18858c4-78d7-469c-b412-6162ff1971d1"), null, "SystemRole", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/role/index", null },
                    { new Guid("cc132e43-2105-41af-be87-39c8c6658120"), "monitor/logs/system/index", "SystemLog", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/system-logs", null },
                    { new Guid("ce14485c-3d07-442a-81b3-09208d0ae41a"), null, "SystemMenu", new Guid("2b342f3c-45d4-4e0b-a546-10e1a34a1272"), "/system/menu/index", null },
                    { new Guid("e4ea0dd3-5aa2-4dc3-8699-fda64f03e17d"), "monitor/online/index", "OnlineUser", new Guid("4dbf4d94-181b-4d38-9eeb-e29c90c67d0c"), "/monitor/online-user", null },
                    { new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), null, "Embedded", new Guid("0777acbd-e5dc-477d-9bfd-08aa00765db9"), "/iframe/embedded", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_UserRoles",
                columns: new[] { "RoleId", "UserId", "IsDefault" },
                values: new object[] { new Guid("7c38e197-ec50-4d23-934a-34178fc212a3"), new Guid("1ac930c0-bd17-4084-ae36-273046778aec"), true });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("0082d8f4-19cd-46fd-80be-072b5fc903f6"), null, "[]", null, null, false, null, false, "ri:file-search-line", false, new Guid("cc132e43-2105-41af-be87-39c8c6658120"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemLog" },
                    { new Guid("12a2709d-08f5-469f-96fe-8de7ae585d4d"), null, "[]", null, null, false, null, false, "ri:admin-fill", false, new Guid("b18858c4-78d7-469c-b412-6162ff1971d1"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRole" },
                    { new Guid("171fe400-2d54-43da-9956-c661b6cf0d46"), null, "[]", null, null, false, null, false, null, false, new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalLink" },
                    { new Guid("22fc3a59-79e4-4a66-a199-aee7431a229a"), null, "[]", null, null, false, null, false, "ri:git-branch-line", false, new Guid("9e9f5645-7223-4e07-8332-cc2a212569a3"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureDept" },
                    { new Guid("4972f434-33ea-4ac5-96e5-7040cb2acfb4"), null, "[]", null, null, false, null, false, null, false, new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUtilsLink" },
                    { new Guid("51bcc4ad-a378-4571-be26-da652abaac55"), null, "[]", null, null, false, null, false, null, false, new Guid("2f60276c-2baf-4586-8906-254db071ead5"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionPage" },
                    { new Guid("5547d86d-1912-412f-b96c-e6bc0ffb6ca5"), null, "[]", null, null, false, null, false, "ri:window-line", false, new Guid("6ed47df0-eca9-40f5-84a0-c7c8e24b914a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureLoginLog" },
                    { new Guid("697614b8-d9e3-418d-9f95-306f70b0ff87"), null, "[]", null, null, false, null, false, "ri:user-voice-line", false, new Guid("e4ea0dd3-5aa2-4dc3-8699-fda64f03e17d"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOnlineUser" },
                    { new Guid("b16d8a7e-a255-4337-9289-3bf0bad1076a"), null, "[]", null, null, false, null, false, "ri:history-fill", false, new Guid("9905d7f8-61ca-45e9-8e84-141b52b36f36"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureOperationLog" },
                    { new Guid("d746eae2-d07c-4c51-a2ca-6310caf51570"), null, "[]", null, null, false, null, false, "ep:menu", false, new Guid("ce14485c-3d07-442a-81b3-09208d0ae41a"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureSystemMenu" },
                    { new Guid("db6830b8-0923-4d51-960d-744bc17550d5"), null, "[\"permission:btn:add\",\"permission:btn:edit\",\"permission:btn:delete\"]", null, null, false, null, false, null, false, new Guid("88e8ccfc-1ef3-479f-a2bc-e1cceda52931"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePermissionButton" },
                    { new Guid("e4750e3e-015f-4bd0-ac22-e25906de14e9"), null, "[]", null, null, false, null, false, "ri:admin-line", false, new Guid("94a1449a-b1fc-4d71-8cbe-04fd2e38b1d0"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUser" }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_Menus",
                columns: new[] { "Id", "Component", "Name", "ParentId", "Path", "Redirect" },
                values: new object[,]
                {
                    { new Guid("11f7ba7a-8ac3-472a-9aca-026f8c3f19b9"), null, "FrameColorHunt", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/colorhunt", null },
                    { new Guid("3441e578-b483-4e6e-becc-5d0575cbcf12"), null, "https://pure-admin-utils.netlify.app/", new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), "/pureUtilsLink", null },
                    { new Guid("3abae703-8e55-4757-9b42-8518ac219ce7"), null, "FrameVite", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/vite", null },
                    { new Guid("48ce4194-4a1c-464d-9cba-382fa97eaad4"), null, "FrameTailwindcss", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/tailwindcss", null },
                    { new Guid("585ffc34-c35c-455a-a553-a1119077a02c"), null, "FrameVue", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/vue3", null },
                    { new Guid("a860382a-7a83-4a1d-9cd0-5b6eecd1664e"), null, "FrameUiGradients", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/uigradients", null },
                    { new Guid("c3ef4634-29d1-4365-af42-72c3749d904e"), null, "FrameEp", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/ep", null },
                    { new Guid("c50b8c00-8b95-49fc-8f35-7f85c2e59bf7"), null, "FrameRouter", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/vue-router", null },
                    { new Guid("c64d3b0c-e835-46c2-bb48-3e9b24216dab"), null, "FramePinia", new Guid("fa093dc0-8f68-462d-b5e7-2c309152cf27"), "/iframe/pinia", null },
                    { new Guid("e37926f0-9283-4658-b628-f84386dc08ac"), null, "https://pure-admin.github.io/pure-admin-doc", new Guid("913a28a5-6ccd-4e95-b967-fc2c05abea9e"), "/external", null }
                });

            migrationBuilder.InsertData(
                schema: "SapphireArchitecture",
                table: "AccessControl_MenuMetas",
                columns: new[] { "Id", "ActivePath", "Auths", "DynamicLevel", "ExtraIcon", "FrameLoading", "FrameSrc", "HiddenTag", "Icon", "KeepAlive", "MenuId", "Rank", "Roles", "ShowLink", "ShowParent", "Title" },
                values: new object[,]
                {
                    { new Guid("406f1ad8-8495-45c5-8d7e-055dff6f67cf"), null, "[]", null, null, false, "https://tailwindcss.com/docs/installation", false, null, true, new Guid("48ce4194-4a1c-464d-9cba-382fa97eaad4"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureTailwindcssDoc" },
                    { new Guid("499ebe7c-3584-4214-b9b1-b69582f67e4a"), null, "[]", null, null, false, "https://element-plus.org/zh-CN/", false, null, true, new Guid("c3ef4634-29d1-4365-af42-72c3749d904e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEpDoc" },
                    { new Guid("4ac850c4-6abc-48d9-a63d-d4a1b1bdf1ab"), null, "[]", null, null, false, "https://router.vuejs.org/zh/", false, null, true, new Guid("c50b8c00-8b95-49fc-8f35-7f85c2e59bf7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureRouterDoc" },
                    { new Guid("4bb914c5-66fe-4fb1-b2c8-f710c131a449"), null, "[]", null, null, false, "https://cn.vitejs.dev/", false, null, true, new Guid("3abae703-8e55-4757-9b42-8518ac219ce7"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureViteDoc" },
                    { new Guid("60d8a395-1e88-43b5-99be-916ed0166ab4"), null, "[]", null, null, false, null, false, null, false, new Guid("3441e578-b483-4e6e-becc-5d0575cbcf12"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureExternalDoc" },
                    { new Guid("7a517050-4bf2-4c41-9735-2784df43a26b"), null, "[]", null, null, false, "https://cn.vuejs.org/", false, null, true, new Guid("585ffc34-c35c-455a-a553-a1119077a02c"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureVueDoc" },
                    { new Guid("b4a162df-3195-4b54-9d81-b2d5ba467173"), null, "[]", null, null, false, "https://pinia.vuejs.org/zh/index.html", false, null, true, new Guid("c64d3b0c-e835-46c2-bb48-3e9b24216dab"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.purePiniaDoc" },
                    { new Guid("b63a07be-2351-41b4-a3c7-be98295d2250"), null, "[]", null, null, false, "https://uigradients.com/", false, null, true, new Guid("a860382a-7a83-4a1d-9cd0-5b6eecd1664e"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureUiGradients" },
                    { new Guid("d5ce21ea-8e4a-4c05-9b79-2f6e98e9b7b7"), null, "[]", null, null, false, null, false, null, false, new Guid("e37926f0-9283-4658-b628-f84386dc08ac"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureEmbeddedDoc" },
                    { new Guid("de148382-e89a-47c9-994e-71faa04e84d5"), null, "[]", null, null, false, "https://colorhunt.co/", false, null, true, new Guid("11f7ba7a-8ac3-472a-9aca-026f8c3f19b9"), null, "[\"SuperAdmin\",\"admin\"]", true, false, "menus.pureColorHuntDoc" }
                });
        }
    }
}
