﻿namespace SapphireArchitecture.Repository.SystemLogs.Configurations;

internal static class ModelBuilderExtension
{
    internal static void ApplySystemLogsEntityTypeConfiguration(this ModelBuilder modelBuilder, string? schema)
    {
        modelBuilder.ApplyConfiguration(new SystemLogUserLoginEntityTypeConfiguration(schema));
    }
}
