﻿namespace SapphireArchitecture.Repository.SystemLogs.Configurations;

internal class SystemLogOperationLogEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<SystemLogOperationLog>
{
    public void Configure(EntityTypeBuilder<SystemLogOperationLog> builder)
    {
    }
}
