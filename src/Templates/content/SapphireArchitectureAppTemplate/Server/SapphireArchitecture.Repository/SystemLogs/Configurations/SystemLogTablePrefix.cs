﻿namespace SapphireArchitecture.Repository.SystemLogs.Configurations;

internal class SystemLogTablePrefix
{
    internal const string Prefix = "SystemLog_";
}
