﻿namespace SapphireArchitecture.Repository.SystemLogs.Configurations;

internal class SystemLogUserLoginEntityTypeConfiguration(string? schema) : BaseEntityTypeConfiguration(schema), IEntityTypeConfiguration<SystemLogUserLogin>
{
    public void Configure(EntityTypeBuilder<SystemLogUserLogin> builder)
    {
        builder.ToTable($"{SystemLogTablePrefix.Prefix}UserLogins", Schema);
        builder.HasKey(x => x.Id);
        builder.Ignore(b => b.DomainEvents);
    }
}
