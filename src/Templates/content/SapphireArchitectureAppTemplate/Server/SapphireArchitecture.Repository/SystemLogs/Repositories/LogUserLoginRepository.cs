﻿namespace SapphireArchitecture.Repository.SystemLogs.Repositories;

internal class LogUserLoginRepository : ISystemLogUserLoginRepository, IRepository
{
    private readonly SapphireArchitectureDbContext _context;
    private readonly ILogger<LogUserLoginRepository> _logger;

    public LogUserLoginRepository(SapphireArchitectureDbContext context, ILogger<LogUserLoginRepository> logger)
    {
        _context = context ?? throw new ArgumentNullException(nameof(context));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    public async Task InsertAsync(SystemLogUserLogin logUserLogin)
    {
        _context.SystemLogUserLogins.Add(logUserLogin);
        await _context.SaveChangesAsync();
    }

    private static readonly Func<SapphireArchitectureDbContext, string?, bool?, DateTime?, DateTime?, string, bool, int, int, IAsyncEnumerable<SystemLogUserLogin>> GetLoginLogsAsync = EF.CompileAsyncQuery((SapphireArchitectureDbContext context, string? username, bool? isSuccess, DateTime? startDate, DateTime? endDate, string sortBy, bool isDescending, int skip, int take) =>

         context.SystemLogUserLogins

        .Where(x => (isSuccess == null || x.IsSuccess == isSuccess)

        && (string.IsNullOrWhiteSpace(username) || EF.Functions.Like(x.Username, username))
        && (startDate == null || x.LoginAt >= startDate)
        && (endDate == null || x.LoginAt <= endDate))
        .ApplySorting(sortBy, isDescending)
        .Skip(skip)
        .Take(take));

    public async Task<List<SystemLogUserLogin>> GetAllAsync(string? username, bool? isSuccess, DateTime? startTime, DateTime? endTime, int pageNumber, int pageSize, string sortBy, bool isDescending)
    {
        username = $"%{username?.Trim()}%";
        var skip = (pageNumber - 1) * pageSize;
        var take = pageSize;
        if (string.IsNullOrEmpty(sortBy))
        {
            sortBy = "LoginAt";
        }

        return await GetLoginLogsAsync(_context, username, isSuccess, startTime, endTime, sortBy, isDescending, skip, take).ToListAsync();
    }
}