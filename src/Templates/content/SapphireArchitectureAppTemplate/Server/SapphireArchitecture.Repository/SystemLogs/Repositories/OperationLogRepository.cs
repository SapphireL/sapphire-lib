﻿namespace SapphireArchitecture.Repository.SystemLogs.Repositories
{
    internal class OperationLogRepository(SapphireArchitectureDbContext context) : ISystemLogOperationRepository, IRepository
    {
        public async Task InsertAsync(SystemLogOperationLog logUserLogin)
        {
            context.SystemLogOperationLogs.Add(logUserLogin);
            await Task.CompletedTask;
        }
    }
}
